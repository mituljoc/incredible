﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using incr.DTO;
using System.Collections;

namespace incr.Models
{
    public class JsonModels
    {
        public IList rows { get; set; }
        public object item { get; set; }
        public string status { get; set; }
        public string message { get; set; }
    }
}