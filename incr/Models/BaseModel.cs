﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace incr.Models
{
    public class BaseModel
    {
        public int createdBy { get; set; }
        public DateTime createdDate { get; set; }
        public int updatedBy { get; set; }
        public string updatedDate { get; set; }
        public bool IsDeleted { get; set; }
    }
}