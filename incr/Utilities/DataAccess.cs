﻿using Dapper;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;

namespace incr.Utilities
{
    public class DataAccess
    {

        private string _connectionString;
        public DataAccess()
        {
            _connectionString = ConfigurationManager.ConnectionStrings["connectionString"].ToString();
        }
        public int ExecuteNonQuery(string strSQLQuery)
        {
            try
            {
                using (SqlConnection sqlConnnection = new SqlConnection(_connectionString))
                {
                    sqlConnnection.Open();
                    using (SqlCommand sqlCommand = new SqlCommand(strSQLQuery, sqlConnnection))
                    {
                        sqlCommand.CommandTimeout = 2000;
                        sqlCommand.CommandType = CommandType.Text;
                        return sqlCommand.ExecuteNonQuery();
                    }
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
        }

        public int ExecuteNonQuery(string storeProcedureName, SqlParameter[] parameter)
        {
            try
            {
                using (SqlConnection sqlConnnection = new SqlConnection(_connectionString))
                {
                    sqlConnnection.Open();
                    using (SqlCommand cmd = new SqlCommand(storeProcedureName, sqlConnnection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandTimeout = 2000;
                        for (int i = 0; i < parameter.Length; i++)
                        {
                            cmd.Parameters.Add(parameter[i]);
                        }
                        return cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
        }

        public int ExecuteNonQuery(string storeProcedureName, object parameter)
        {
            try
            {
                using (var connection = new SqlConnection(_connectionString))
                {
                    var result = connection.QuerySingle<int>(storeProcedureName, parameter, commandType: CommandType.StoredProcedure);
                    return result;
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
        }


        public object ExecuteScaler(string strSQLQuery)
        {
            try
            {
                using (SqlConnection sqlConnnection = new SqlConnection(_connectionString))
                {
                    sqlConnnection.Open();
                    using (SqlCommand cmd = new SqlCommand(strSQLQuery, sqlConnnection))
                    {
                        cmd.CommandTimeout = 2000;
                        return cmd.ExecuteScalar();
                    }
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
        }

        public object ExecuteScaler(string strSQLQuery, SqlParameter[] parameter)
        {
            try
            {
                using (SqlConnection sqlConnnection = new SqlConnection(_connectionString))
                {
                    sqlConnnection.Open();
                    using (SqlCommand cmd = new SqlCommand(strSQLQuery, sqlConnnection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandTimeout = 2000;
                        for (int i = 0; i < parameter.Length; i++)
                        {
                            cmd.Parameters.Add(parameter[i]);
                        }
                        return cmd.ExecuteScalar();
                    }
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
        }

        public DataSet GetDataSet(string strSQLQuery)
        {
            try
            {
                using (SqlConnection sqlConnnection = new SqlConnection(_connectionString))
                {
                    sqlConnnection.Open();

                    using (SqlCommand cmd = new SqlCommand(strSQLQuery, sqlConnnection))
                    {
                        cmd.CommandTimeout = 2000;
                        using (SqlDataAdapter adapter = new SqlDataAdapter(cmd))
                        {
                            DataSet ds = new DataSet();
                            adapter.Fill(ds);
                            return ds;
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
        }

        public DataTable GetDataTable(string strSQLQuery)
        {
            try
            {
                using (SqlConnection sqlConnnection = new SqlConnection(_connectionString))
                {
                    sqlConnnection.Open();

                    using (SqlCommand cmd = new SqlCommand(SetTransactionIsolationLevel(strSQLQuery), sqlConnnection))
                    {
                        cmd.CommandTimeout = 2000;
                        using (SqlDataAdapter adapter = new SqlDataAdapter(cmd))
                        {
                            DataSet ds = new DataSet();
                            DataTable dt = new DataTable();
                            adapter.Fill(ds);
                            if (ds != null && ds.Tables.Count > 0)
                            {
                                dt = ds.Tables[0];
                            }
                            return dt;
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
        }

        public DataTable GetDataTable(string strSQLQuery, out int totalRecord)
        {
            try
            {
                totalRecord = 0;
                using (SqlConnection sqlConnnection = new SqlConnection(_connectionString))
                {
                    sqlConnnection.Open();

                    using (SqlCommand cmd = new SqlCommand(SetTransactionIsolationLevel(strSQLQuery), sqlConnnection))
                    {
                        cmd.CommandTimeout = 2000;
                        using (SqlDataAdapter adapter = new SqlDataAdapter(cmd))
                        {
                            DataSet ds = new DataSet();
                            DataTable dt = new DataTable();
                            adapter.Fill(ds);
                            if (ds != null && ds.Tables.Count > 0)
                            {
                                dt = ds.Tables[0];
                                totalRecord = Convert.ToInt32(ds.Tables[1].Rows[0]["totalRecord"].ToString());
                            }
                            return dt;
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
        }

        public DataTable GetDataTable(string storeProcedureName, SqlParameter[] parameter, out int totalRecord)
        {
            try
            {
                totalRecord = 0;
                using (SqlConnection sqlConnnection = new SqlConnection(_connectionString))
                {
                    sqlConnnection.Open();
                    using (SqlCommand cmd = new SqlCommand(storeProcedureName, sqlConnnection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandTimeout = 2000;
                        DataTable dt = new DataTable();
                        for (int i = 0; i < parameter.Length; i++)
                        {
                            cmd.Parameters.Add(parameter[i]);
                        }
                        using (SqlDataAdapter adapter = new SqlDataAdapter(cmd))
                        {
                            DataSet ds = new DataSet();
                            adapter.Fill(ds);

                            if (ds != null && ds.Tables.Count > 0)
                            {
                                dt = ds.Tables[0];
                                totalRecord = Convert.ToInt32(ds.Tables[1].Rows[0]["totalRecord"].ToString());
                            }
                            return dt;
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
        }

        public DataTable GetDataTable(string storeProcedureName, SqlParameter[] parameter)
        {
            try
            {
                using (SqlConnection sqlConnnection = new SqlConnection(_connectionString))
                {
                    sqlConnnection.Open();
                    using (SqlCommand cmd = new SqlCommand(storeProcedureName, sqlConnnection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandTimeout = 2000;

                        for (int i = 0; i < parameter.Length; i++)
                        {
                            cmd.Parameters.Add(parameter[i]);
                        }
                        using (SqlDataAdapter adapter = new SqlDataAdapter(cmd))
                        {
                            DataSet ds = new DataSet();
                            adapter.Fill(ds);
                            return ds.Tables[0];
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
        }

        public DataSet GetDataSet(string storeProcedureName, SqlParameter[] parameter, int? timeout = default(int?))
        {
            try
            {
                using (SqlConnection sqlConnnection = new SqlConnection(_connectionString))
                {
                    sqlConnnection.Open();

                    using (SqlCommand cmd = new SqlCommand(storeProcedureName, sqlConnnection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandTimeout = 2000;
                        for (int i = 0; i < parameter.Length; i++)
                        {
                            cmd.Parameters.Add(parameter[i]);
                        }
                        using (SqlDataAdapter adapter = new SqlDataAdapter(cmd))
                        {
                            DataSet ds = new DataSet();

                            if (timeout.HasValue)
                            {
                                adapter.SelectCommand.CommandTimeout = timeout.Value;
                            }
                            adapter.Fill(ds);
                            return ds;
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
        }
        // for return data as list 
        public List<T> GetListof<T>(string strSQLQuery) where T : class
        {
            try
            {
                DefaultTypeMap.MatchNamesWithUnderscores = true;
                using (IDbConnection db = new SqlConnection(_connectionString))
                {
                    return db.Query<T>(strSQLQuery).ToList();
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
        }
        public List<T> GetListof<T>(string storeProcedureName, object parameters) where T : class
        {
            try
            {
                DefaultTypeMap.MatchNamesWithUnderscores = true;
                using (IDbConnection sqlConnnection = new SqlConnection(_connectionString))
                {
                    return sqlConnnection.Query<T>(storeProcedureName, parameters, commandType: CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public List<T> GetListofWithTotalRecord<T>(string strSQLQuery, out int totalRecord) where T : class
        {
            try
            {
                totalRecord = 0;
                List<T> items = new List<T>();
                DefaultTypeMap.MatchNamesWithUnderscores = true;
                using (IDbConnection db = new SqlConnection(_connectionString))
                {
                    using (var result = db.QueryMultiple(strSQLQuery, null))
                    {
                        items = result.Read<T>().ToList();
                        // TODO for total recourd count 
                        var totalRecords = result.Read<dynamic>().ToList();
                        totalRecord = totalRecords[0].totalRecord;
                    }
                }
                return items;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
        }

        public static string GetGridData(string draw, string startRec , int pageSize , string sortCol , string  sortOrder, string sql, List<SqlParam> whereBlockList = null, SqlParameter[] sqlParameters = null)
        {
          
    
      
        
            List<SqlParam> objParaList = new List<SqlParam>();
          
            if (whereBlockList != null)
            {
                objParaList.AddRange(whereBlockList);
            }

            string where = " 1=1 ";
            foreach (SqlParam objPara in objParaList)
            {
                where = where + " and " + objPara.Name + " like '%" + objPara.Value + "%'";
            }

            SqlParameter[] sqlParams = new SqlParameter[]
             {
                 new SqlParameter("@startRec", startRec),
                 new SqlParameter("@pageSize", pageSize),
                 new SqlParameter("@SortBy", sortCol),
                 new SqlParameter("@SortDir", sortOrder),
                 new SqlParameter("@WhereClause", where),
             };

            if (sqlParameters != null)
            {
                sqlParams = sqlParams.Concat(sqlParameters).ToArray();
            }
            int totalRecord = 0;
            DataTable dataTable = (new DataAccess()).GetDataTable(sql, sqlParams, out totalRecord);

            var sb = new StringBuilder();
            sb.Append("{");
            sb.Append(@"""recordsTotal"": ");
            sb.Append(totalRecord);
            sb.Append(",");
            sb.Append(@"""recordsFiltered"": ");
            sb.Append(totalRecord);
            sb.Append(",");
            sb.Append(@"""draw"": ");
            sb.Append(draw);
            sb.Append(", ");
            sb.Append(@"""data"":");
            sb.Append(JsonConvert.SerializeObject(dataTable));
            sb.Append("}");
            return sb.ToString(); ;
        }
        private static string SetTransactionIsolationLevel(string inputString)
        {
            //set transaction isolation level read uncommitted
            if (inputString.ToLower().StartsWith("select"))
            {
                inputString = "set transaction isolation level read uncommitted ;" + inputString;
            }
            return inputString;
        }

    }
    public class SqlParam
    {
        public string Name { get; set; }

        public string Value { get; set; }
    }
}