﻿using incr.Models;
using incr.Repository;
using incr.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace incr.Controllers
{
    public class ProductController : Controller
    {
        protected IncrEntities dataContext = new IncrEntities();
        IProductRepository _ProductRepository = new ProductRepository();
        IErrorLogRepository _errorLog = new ErrorLogRepository();
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult GetAllProduct()
        {  
            JsonModels jsonModel = new JsonModels();
            try
            {
                var ProductList = _ProductRepository.getProductList();
                jsonModel.rows = ProductList;
                jsonModel.status = "200";
                jsonModel.message = "Success";
                return Json(jsonModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                jsonModel.status = "500";
                jsonModel.message = "Error in ProductListController in GetAllProduct method";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), jsonModel.message, "");
                return Json(jsonModel, JsonRequestBehavior.AllowGet);

            }
        }
        public JsonResult GetProductById(int id)
        {
            JsonModels jsonModel = new JsonModels();
            try
            {
                var Product = _ProductRepository.getProductById(id);

                jsonModel.item = Product;
                jsonModel.status = "200";
                jsonModel.message = "Success";
                return Json(jsonModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                jsonModel.status = "500";
                jsonModel.message = "Error in ProductController in GetProductById method";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), jsonModel.message, "");
                return Json(jsonModel, JsonRequestBehavior.AllowGet);

            }
        }
        [HttpPost]
        public JsonResult AddProduct(incr_price_list product)
        {
            JsonModels jsonModel = new JsonModels();
            try
            {
            
                var productDetail = _ProductRepository.addProduct(product);
                jsonModel.item = productDetail;
                jsonModel.status = "200";
                jsonModel.message = "Success";
                var priceList = HttpContext.Cache.Get("priceList") as List<incr_price_list>;
                if (priceList != null)
                {
                    HttpContext.Cache.Remove("priceList");
                }
                return Json(jsonModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                jsonModel.status = "500";
                jsonModel.message = "Error in ProductController in AddProduct";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), jsonModel.message, "");
                return Json(jsonModel, JsonRequestBehavior.AllowGet);

            }
        }

        [HttpPost]
        public JsonResult DeleteProduct(int id)
        {
            JsonModels jsonModel = new JsonModels();
            try
            {
                var getbyID = dataContext.Incr_price_list.Where(m => m.id.Equals(id)).FirstOrDefault();
                var result = _ProductRepository.deleteProduct(getbyID);
                jsonModel.item = result;
                jsonModel.status = "200";
                jsonModel.message = "Success";
                var priceList = HttpContext.Cache.Get("priceList") as List<incr_price_list>;
                if (priceList != null)
                {
                    HttpContext.Cache.Remove("priceList");
                }
                return Json(jsonModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                jsonModel.status = "500";
                jsonModel.message = "Error in DeleteProduct in DeleteProduct";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), jsonModel.message, "");
                return Json(jsonModel, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult UpdateProduct(incr_price_list Product)
        {
            JsonModels jsonModel = new JsonModels();
            try
            {
                var result = _ProductRepository.updateProduct(Product);

                jsonModel.item = result;
                jsonModel.status = "200";
                jsonModel.message = "Success";
                var priceList = HttpContext.Cache.Get("priceList") as List<incr_price_list>;
                if (priceList != null)
                {
                    HttpContext.Cache.Remove("priceList");
                }
                return Json(jsonModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                jsonModel.status = "500";
                jsonModel.message = "Error in Product in UpdateProduct method";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), jsonModel.message, "");
                return Json(jsonModel, JsonRequestBehavior.AllowGet);

            }
        }
    }
}