﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using incr.Models;
using incr.Repository;
using incr.Repository.Interface;
using incr.DTO;
namespace incr.Controllers
{
    public class WorkMasterController : Controller
    {
        protected IncrEntities dataContext = new IncrEntities();
        IErrorLogRepository _errorLog = new ErrorLogRepository();
        public ActionResult Index()
        {

            return View();
        }

        /// <summary>
        /// Created by Rajesh Gami
        /// </summary>
        /// For Get all workmaster list
        /// <returns></returns>
        public JsonResult GetAllWorkMaster()
        {
            JsonModels jsonModel = new JsonModels();
            try
            {
                IWorkMasterRepository _workMasterRepository = new WorkMasterRepository();

                var workMasterList = _workMasterRepository.getWorkMasterList();
                foreach (var item in workMasterList)
                {
                    item.Department.Split(',');
                    foreach (var dept in item.Department)
                    {
                        var depart = dataContext.Incr_Department.Where(m => m.IsDeleted == false && m.RID == dept).ToList(); /*_crudRepository.GetAll().Where(d => d.IsDeleted.Equals(false)).ToList();*/
                      
                    }
                }
                jsonModel.rows = workMasterList;
                jsonModel.status = "200";
                jsonModel.message = "Success";
                return Json(jsonModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                jsonModel.status = "500";
                jsonModel.message = "Error in WorkMasterController in GetAllWorkMaster method";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), jsonModel.message, "");
                return Json(jsonModel, JsonRequestBehavior.AllowGet);

            }
        }
        /// <summary>
        /// Created by Rajesh Gami
        /// </summary>
        /// For Get all DepartmentList
        /// <returns></returns>
        public JsonResult GetAllDepartment()
        {
            JsonModels jsonModel = new JsonModels();
            try
            {
                IWorkMasterRepository _workMasterRepository = new WorkMasterRepository();

                var departmentList = _workMasterRepository.getAllDepartment();

                jsonModel.rows = departmentList;
                jsonModel.status = "200";
                jsonModel.message = "Success";
                return Json(jsonModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                jsonModel.status = "500";
                jsonModel.message = "Error in WorkMasterController in GetAllDepartment method";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), jsonModel.message, "");
                return Json(jsonModel, JsonRequestBehavior.AllowGet);

            }
        }
        /// <summary>
        /// GetWorkMasterById : Created by Rajesh Gami
        /// </summary>
        /// Get workMaster data by workmasterID
        /// <param name="workMasterId"></param>
        /// <returns></returns>
        public JsonResult GetWorkMasterById(int workMasterId)
        {
            JsonModels jsonModel = new JsonModels();
            try
            {
                IWorkMasterRepository _workMasterRepository = new WorkMasterRepository();

                var departmentList = _workMasterRepository.getWorkMasterById(workMasterId);

                jsonModel.item = departmentList;
                jsonModel.status = "200";
                jsonModel.message = "Success";
                return Json(jsonModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                jsonModel.status = "500";
                jsonModel.message = "Error in WorkMasterController in GetOperatorById method";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), jsonModel.message, "");
                return Json(jsonModel, JsonRequestBehavior.AllowGet);

            }
        }
        /// <summary>
        /// Created by Rajesh Gami
        /// </summary>
        /// For Get all workmaster list
        /// <returns></returns>
        public JsonResult GetAllPrice_list()
        {
            JsonModels jsonModel = new JsonModels();
            try
            {
                IWorkMasterRepository _workMasterRepository = new WorkMasterRepository();

                var AllpriceList = _workMasterRepository.GetAllPrice_list();
                //foreach (var item in workMasterList)
                //{
                //    item.Department.Split(',');
                //    foreach (var dept in item.Department)
                //    {
                //        var depart = dataContext.Incr_Department.Where(m => m.IsDeleted == false && m.RID == dept).ToList(); /*_crudRepository.GetAll().Where(d => d.IsDeleted.Equals(false)).ToList();*/

                //    }
                //}
                jsonModel.rows = AllpriceList;
                jsonModel.status = "200";
                jsonModel.message = "Success";
                return Json(jsonModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                jsonModel.status = "500";
                jsonModel.message = "Error in WorkMasterController in GetAllWorkMaster method";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), jsonModel.message, "");
                return Json(jsonModel, JsonRequestBehavior.AllowGet);

            }
        }
        /// <summary>
        /// AddNewWorkMaster
        /// </summary>
        /// Created by Rajesh Gami
        /// <param name="workMasterObj"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult AddNewWorkMaster(WorkMasterDTO workMasterObj)
        {
            JsonModels jsonModel = new JsonModels();
            try
            {
                IWorkMasterRepository _workMasterRepository = new WorkMasterRepository();
             
                var workMasterDetail = _workMasterRepository.addNewWorkMaster(workMasterObj);

                jsonModel.item = workMasterDetail;
                jsonModel.status = "200";
                jsonModel.message = "Success";
                return Json(jsonModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                jsonModel.status = "500";
                jsonModel.message = "Error in WorkMasterController in AddNewWorkMaster";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), jsonModel.message, "");
                return Json(jsonModel, JsonRequestBehavior.AllowGet);

            }
        }
        /// <summary>
        /// UpdateWorkMaster
        /// </summary>
        /// Created by Rajesh Gami
        /// <param name="WorkMasterobj"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult UpdateWorkMaster(WorkMasterDTO workMasterObj)
        {
            JsonModels jsonModel = new JsonModels();
            try
            {
                IWorkMasterRepository _workMasterRepository = new WorkMasterRepository();

                var result = _workMasterRepository.updateWorkMaster(workMasterObj);

                jsonModel.item = result;
                jsonModel.status = "200";
                jsonModel.message = "Success";
                return Json(jsonModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                jsonModel.status = "500";
                jsonModel.message = "Error in WorkMasterController in GetOperatorById method";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), jsonModel.message, "");
                return Json(jsonModel, JsonRequestBehavior.AllowGet);

            }
        }
        /// <summary>
        /// DeleteOperator
        /// </summary>
        /// Created by Rajesh
        /// <param name="workMasterId"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult DeleteWorkMaster(int workMasterId)
        {
            JsonModels jsonModel = new JsonModels();
            try
            {
                IWorkMasterRepository _workMasterRepository = new WorkMasterRepository();


                var getbyID = dataContext.Incr_WorkMaster.Where(m => m.RID.Equals(workMasterId)).FirstOrDefault();
                var result = _workMasterRepository.deleteWorkMaster(getbyID);
                jsonModel.item = result;
                jsonModel.status = "200";
                jsonModel.message = "Success";
                return Json(jsonModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                jsonModel.status = "500";
                jsonModel.message = "Error in WorkMasterController in DeleteOperator";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), jsonModel.message, "");
                return Json(jsonModel, JsonRequestBehavior.AllowGet);
            }
        }
    }
}