﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using incr.Models;
using incr.Repository;
using incr.Repository.Interface;
using incr.DTO;
using System.Data.Entity;

namespace incr.Controllers
{
    public class OperatorController : Controller
    {

        protected IncrEntities dataContext = new IncrEntities();
        IErrorLogRepository _errorLog = new ErrorLogRepository();
        public ActionResult Index()
        {

            return View();
        }
 
        /// <summary>
        /// Created by Rajesh
        /// </summary>
        /// For Get all operatorList which is isDeleted = false
        /// <returns></returns>
        public JsonResult GetAllOperator()
        {
            JsonModels jsonModel = new JsonModels();
            try
            {
                IOperatorRepository _operatorRepository = new OperatorRepository();

                var operatorList = _operatorRepository.getOperatorList();

                jsonModel.rows = operatorList;
                jsonModel.status = "200";
                jsonModel.message = "Success";
                return Json(jsonModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                jsonModel.status = "500";
                jsonModel.message = "Error in OperatorController in GetAllOperator method";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), jsonModel.message, "");
                return Json(jsonModel, JsonRequestBehavior.AllowGet);

            }
        }
        /// <summary>
        /// Created by Rajesh
        /// </summary>
        /// For Get all DepartmentLIst
        /// <returns></returns>
        public JsonResult GetAllDepartment()
        {
            JsonModels jsonModel = new JsonModels();
            try
            {
                IOperatorRepository _operatorRepository = new OperatorRepository();

                var departmentList = _operatorRepository.getAllDepartment();

                jsonModel.rows = departmentList;
                jsonModel.status = "200";
                jsonModel.message = "Success";
                return Json(jsonModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                jsonModel.status = "500";
                jsonModel.message = "Error in OperatorController in GetAllDepartment method";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), jsonModel.message, "");
                return Json(jsonModel, JsonRequestBehavior.AllowGet);

            }
        }
        /// <summary>
        /// GetOperatorById : Created by Rajesh
        /// </summary>
        /// Get operator data by operatorId
        /// <param name="operatorId"></param>
        /// <returns></returns>
        public JsonResult GetOperatorById(int operatorId)
        {
            JsonModels jsonModel = new JsonModels();
            try
            {
                IOperatorRepository _operatorRepository = new OperatorRepository();

                var departmentList = _operatorRepository.getOperatorById(operatorId);

                jsonModel.item = departmentList;
                jsonModel.status = "200";
                jsonModel.message = "Success";
                return Json(jsonModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                jsonModel.status = "500";
                jsonModel.message = "Error in OperatorController in GetOperatorById method";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), jsonModel.message, "");
                return Json(jsonModel, JsonRequestBehavior.AllowGet);

            }
        }
        /// <summary>
        /// AddNewOperator
        /// </summary>
        /// Created by Rajesh
        /// <param name="operatorobj"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult AddNewOperator(OperatorMasterDTO operatorobj)
        {
            JsonModels jsonModel = new JsonModels();
            try
            {
                IOperatorRepository _operatorRepository = new OperatorRepository();
                var departmentList = _operatorRepository.addNewOperator(operatorobj);
                jsonModel.item = departmentList;
                jsonModel.status = "200";
                jsonModel.message = "Success";
                return Json(jsonModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                jsonModel.status = "500";
                jsonModel.message = "Error in OperatorController in GetOperatorById method";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), jsonModel.message, "");
                return Json(jsonModel, JsonRequestBehavior.AllowGet);

            }
        }
        /// <summary>
        /// UpdateOperator
        /// </summary>
        /// Created by Rajesh
        /// <param name="operatorobj"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult UpdateOperator(OperatorMasterDTO operatorobj)
        {
            JsonModels jsonModel = new JsonModels();
            try
            {
                IOperatorRepository _operatorRepository = new OperatorRepository();
                var result = _operatorRepository.updateOperator(operatorobj);
                jsonModel.item = result;
                jsonModel.status = "200";
                jsonModel.message = "Success";
                return Json(jsonModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                jsonModel.status = "500";
                jsonModel.message = "Error in OperatorController in GetOperatorById method";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), jsonModel.message, "");
                return Json(jsonModel, JsonRequestBehavior.AllowGet);

            }
        }
        /// <summary>
        /// DeleteOperator
        /// </summary>
        /// Created by Rajesh
        /// <param name="operatorId"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult DeleteOperator(int operatorId)
        {
            JsonModels jsonModel = new JsonModels();
            try
            {
                IOperatorRepository _operatorRepository = new OperatorRepository();


                var getbyID = dataContext.Incr_OperatorMaster.Where(m => m.RID.Equals(operatorId)).FirstOrDefault();
                var result = _operatorRepository.deleteOperator(getbyID);
                jsonModel.item = result;
                jsonModel.status = "200";
                jsonModel.message = "Success";
                return Json(jsonModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                jsonModel.status = "500";
                jsonModel.message = "Error in OperatorController in GetOperatorById method";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), jsonModel.message, "");
                return Json(jsonModel, JsonRequestBehavior.AllowGet);

            }
        }

        public JsonResult GetAllOperator_supervisor()
        {
            JsonModels jsonModel = new JsonModels();
            try
            {
                IOperatorRepository _operatorRepository = new OperatorRepository();

                var operatorList = _operatorRepository.getOperatorList_supervisor();

                jsonModel.rows = operatorList;
                jsonModel.status = "200";
                jsonModel.message = "Success";
                return Json(jsonModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                jsonModel.status = "500";
                jsonModel.message = "Error in OperatorController in GetAllOperator method";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), jsonModel.message, "");
                return Json(jsonModel, JsonRequestBehavior.AllowGet);

            }
        }

    }
}
