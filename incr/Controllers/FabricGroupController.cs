﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using incr.Models;
using incr.Repository;
using incr.Repository.Interface;
using incr.DTO;

namespace incr.Controllers
{
    public class FabricGroupController : Controller
    {
        protected IncrEntities dataContext = new IncrEntities();
        IFabricGroupRepository _fabricGroupRepository = new FabricGroupRepository();
        IErrorLogRepository _errorLog = new ErrorLogRepository();
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// Created by Rajesh Gami
        /// </summary>
        /// For Get all GetAllFabricGroup
        /// <returns></returns>
        public JsonResult GetAllFabricGroup()
        {
            JsonModels jsonModel = new JsonModels();
            try
            {
                var fabricGroupList = _fabricGroupRepository.getFabricGroupList();
                jsonModel.rows = fabricGroupList;
                jsonModel.status = "200";
                jsonModel.message = "Success";
                return Json(jsonModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                jsonModel.status = "500";
                jsonModel.message = "Error in FabricGroupController in GetAllFabricGroup method";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), jsonModel.message, "");
                return Json(jsonModel, JsonRequestBehavior.AllowGet);

            }
        }
        /// <summary>
        /// getFabricGroupById : Created by Rajesh Gami
        /// </summary>
        /// getFabricGroupById
        /// <param name="fabricGroupId"></param>
        /// <returns></returns>
        public JsonResult GetFabricGroupById(int fabricGroupId)
        {
            JsonModels jsonModel = new JsonModels();
            try
            {
                var fabricGroupDetail = _fabricGroupRepository.getFabricGroupById(fabricGroupId);

                jsonModel.item = fabricGroupDetail;
                jsonModel.status = "200";
                jsonModel.message = "Success";
                return Json(jsonModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                jsonModel.status = "500";
                jsonModel.message = "Error in FabricGroupController in GetFabricGroupById method";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), jsonModel.message, "");
                return Json(jsonModel, JsonRequestBehavior.AllowGet);

            }
        }
        /// <summary>
        /// AddFabricGroup
        /// </summary>
        /// Created by Rajesh Gami
        /// <param name="fabricGroupObj"></param>
        /// <returns></returns>FabricDTO
        [HttpPost]
        public JsonResult AddFabricGroup(FabricDTO fabricGroupObj)
        {
            JsonModels jsonModel = new JsonModels();
            try
            {
                var fabricGroupDetail = _fabricGroupRepository.addFabricGroup(fabricGroupObj);

                jsonModel.item = fabricGroupDetail;
                jsonModel.status = "200";
                jsonModel.message = "Success";
                return Json(jsonModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                jsonModel.status = "500";
                jsonModel.message = "Error in FabricGroupController in AddFabricGroup";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), jsonModel.message, "");
                return Json(jsonModel, JsonRequestBehavior.AllowGet);

            }
        }
        /// <summary>
        /// updateFabricGroup
        /// </summary>
        /// Created by Rajesh Gami
        /// <param name=""></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult UpdateFabricGroup(FabricDTO fabricGroupObj)
        {
            JsonModels jsonModel = new JsonModels();
            try
            {
                var result = _fabricGroupRepository.updateFabricGroup(fabricGroupObj);

                jsonModel.item = result;
                jsonModel.status = "200";
                jsonModel.message = "Success";
                return Json(jsonModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                jsonModel.status = "500";
                jsonModel.message = "Error in FabricGroupController in UpdateFabricGroup method";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), jsonModel.message, "");
                return Json(jsonModel, JsonRequestBehavior.AllowGet);

            }
        }
        /// <summary>
        /// deleteFabricGroup
        /// </summary>
        /// Created by Rajesh
        /// <param name="fabricGroupId"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult DeleteFabricGroup(int fabricGroupId)
        {
            JsonModels jsonModel = new JsonModels();
            try
            {
                var getbyID = dataContext.Incr_FabricGroup.Where(m => m.RID.Equals(fabricGroupId)).FirstOrDefault();
                var result = _fabricGroupRepository.deleteFabricGroup(getbyID);
                jsonModel.item = result;
                jsonModel.status = "200";
                jsonModel.message = "Success";
                return Json(jsonModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                jsonModel.status = "500";
                jsonModel.message = "Error in FabricGroupController in DeleteFabricGroup";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), jsonModel.message, "");
                return Json(jsonModel, JsonRequestBehavior.AllowGet);

            }
        }
    }
}