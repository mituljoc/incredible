﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using incr.Models;
using incr.Repository;
using incr.Repository.Interface;
using incr.DTO;
using System.Web.Caching;

namespace incr.Controllers
{
    public class SupplierController : Controller
    {
        protected IncrEntities dataContext = new IncrEntities();
        ISupplierRepository _supplierRepository = new SupplierRepository();
        IErrorLogRepository _errorLog = new ErrorLogRepository();
        public ActionResult Index()
        {

            return View();
        }
        /// <summary>
        /// Created by Rajesh Gami
        /// </summary>
        /// For Get all supplier list
        /// <returns></returns>
        public JsonResult GetAllSupplier()
        {
          
            JsonModels jsonModel = new JsonModels();
            try
            {
               IOrderRepository _orderRepository = new OrderRepository();
               var supplierList = _supplierRepository.getSupplierList();

                jsonModel.rows = supplierList;
                jsonModel.status = "200";
                jsonModel.message = "Success";
                return Json(jsonModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                jsonModel.status = "500";
                jsonModel.message = "Error in SupplierController in GetAllSupplier method";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), jsonModel.message, "");
                return Json(jsonModel, JsonRequestBehavior.AllowGet);

            }
        }
        /// <summary>
        /// GetSupplierById : Created by Rajesh Gami
        /// </summary>
        /// Get supplier data by supplierId
        /// <param name="supplierId"></param>
        /// <returns></returns>
        public JsonResult GetSupplierById(int supplierId)
        {
            JsonModels jsonModel = new JsonModels();
            try
            {
                var supplierDetail = _supplierRepository.getSupplierById(supplierId);

                jsonModel.item = supplierDetail;
                jsonModel.status = "200";
                jsonModel.message = "Success";
                return Json(jsonModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                jsonModel.status = "500";
                jsonModel.message = "Error in SupplierController in GetSupplierById method";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), jsonModel.message, "");
                return Json(jsonModel, JsonRequestBehavior.AllowGet);

            }
        }
        /// <summary>
        /// AddNewSupplier
        /// </summary>
        /// Created by Rajesh Gami
        /// <param name="supplierObj"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult AddNewSupplier(SupplierDTO supplierObj)
        {
            JsonModels jsonModel = new JsonModels();
            try
            {
                var supplierDetail = _supplierRepository.addNewSupplier(supplierObj);

                jsonModel.item = supplierDetail;
                jsonModel.status = "200";
                jsonModel.message = "Success";
                return Json(jsonModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                jsonModel.status = "500";
                jsonModel.message = "Error in SupplierController in AddNewSupplier";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), jsonModel.message, "");
                return Json(jsonModel, JsonRequestBehavior.AllowGet);

            }
        }
        /// <summary>
        /// UpdateSupplier
        /// </summary>
        /// Created by Rajesh Gami
        /// <param name="WorkMasterobj"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult UpdateSupplier(SupplierDTO supplierObj)
        {
            JsonModels jsonModel = new JsonModels();
            try
            {
                var result = _supplierRepository.updateSupplier(supplierObj);

                jsonModel.item = result;
                jsonModel.status = "200";
                jsonModel.message = "Success";
                return Json(jsonModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                jsonModel.status = "500";
                jsonModel.message = "Error in WorkMasterController in GetOperatorById method";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), jsonModel.message, "");
                return Json(jsonModel, JsonRequestBehavior.AllowGet);

            }
        }
        /// <summary>
        /// DeleteSupplier
        /// </summary>
        /// Created by Rajesh
        /// <param name="supplierId"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult DeleteSupplier(int supplierId)
        {
            JsonModels jsonModel = new JsonModels();
            try
            {
                var getbyID = dataContext.Incr_Supplier.Where(m => m.RID.Equals(supplierId)).FirstOrDefault();
                var result = _supplierRepository.deleteSupplier(getbyID);
                jsonModel.item = result;
                jsonModel.status = "200";
                jsonModel.message = "Success";
                return Json(jsonModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                jsonModel.status = "500";
                jsonModel.message = "Error in WorkMasterController in DeleteOperator";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), jsonModel.message, "");
                return Json(jsonModel, JsonRequestBehavior.AllowGet);

            }
        }
    }
}