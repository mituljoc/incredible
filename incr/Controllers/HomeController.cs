﻿using System.Web.Mvc;
using incr.Models;
using incr.Repository;
using incr.Repository.Interface;
using incr.DTO;
using System.Configuration;
using System.Net.Mail;
using System;
using System.Net;
using System.Web;

namespace AdminLTE1.Controllers
{
    public class HomeController : Controller
    {
        protected IncrEntities dataContext = new IncrEntities();
        IDashboardRepository _dashboardRepository = new DashboardRespository();
        IErrorLogRepository _errorLog = new ErrorLogRepository();
        /// <summary>
        ///Dashboard : Created by Rajesh
        /// </summary>
        /// <returns></returns>
       
        public ActionResult Index()
        {
            return View();
        }
        [AllowAnonymous]

        public ActionResult Login()
        {
            return View();
        }
        [AllowAnonymous]

        public ActionResult SignIn(string userName,string password)
        {

            if (userName.ToLower() == "admin@123" && password.ToLower() == "admin123")
            {
                Session["UserName"] = userName;
                return RedirectToAction("index", "home");
              
            }
            else {

                ViewBag.error = "error";
                return View("login");
            }
        }

        public ActionResult AnotherLink()
        {
            return View("Index");
        }

        public ActionResult Logout()
        {
            Session.Abandon();
            return RedirectToAction("login", "home");
        }

        /*******************DON'T DELETE BELOW METHOD : I have commented this code - Rajesh Gami ********************/
      
        //[System.Obsolete] 
        //public void SendErrorMail(string ipAddress, string message, string innerException, string errorPath, string otherDetail)
        //{
        //    ViewBag.senderEmail = ConfigurationSettings.AppSettings["FromMail"].ToString();
        //    ViewBag.toMail = ConfigurationSettings.AppSettings["SendErrorTo"].ToString();
        //    ViewBag.password = ConfigurationSettings.AppSettings["FromPassword"].ToString();
         

        //    var sub = "INCR Exception";
        //    var newline = "<br/>";
        //    string body = "<h3> Message : </h3>" + message +"<div></div>"+ newline + "<h3> Error path : </h3> " + errorPath + "<div></div>" + newline + "<h3> Inner Exception : </h3> " + innerException + "<div></div>" + newline + "<h3> IPAddress : </h3> " + ipAddress + "<div></div>" + newline + "<h3> Other Detail : </h3> " + otherDetail;

        //    var smtp = new SmtpClient
        //    {
        //        Host = ConfigurationSettings.AppSettings["Host"].ToString(),
        //    Port = Convert.ToInt32(ConfigurationSettings.AppSettings["Port"]),
        //        EnableSsl = true,
        //        DeliveryMethod = SmtpDeliveryMethod.Network,
        //        UseDefaultCredentials = false,
        //        Credentials = new NetworkCredential(ViewBag.senderEmail, ViewBag.password)
        //    };
        //    using (var mess = new MailMessage(ViewBag.senderEmail, ViewBag.toMail)
        //    {
        //        Subject = sub,
        //        Body = body,
        //        IsBodyHtml = true
        //    })
        //    {
        //        smtp.Send(mess);
        //    }

        //}
    }
}
