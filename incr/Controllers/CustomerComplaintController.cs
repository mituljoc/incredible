﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using incr.Models;
using incr.Repository;
using incr.Repository.Interface;
using incr.DTO;

namespace incr.Controllers
{
    public class CustomerComplaintController : Controller
    {
        protected IncrEntities dataContext = new IncrEntities();
        ICustomerComplaintRepository _repository = new CustomerComplaintRepository();
        IErrorLogRepository _errorLog = new ErrorLogRepository();
        public ActionResult Index()
        {

            return View();
        }

        /// <summary>
        /// Created by Rajesh Gami
        /// </summary>
        /// For Get all Complaint list
        /// <returns></returns>
        public JsonResult GetAllCustComplaint()
        {
            JsonModels jsonModel = new JsonModels();
            try
            {
                var workMasterList = _repository.getCustComplaintList();
                
                jsonModel.rows = workMasterList;
                jsonModel.status = "200";
                jsonModel.message = "Success";
                return Json(jsonModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                jsonModel.status = "500";
                jsonModel.message = "Error in CustomerComplaint in GetAllCustComplaint method";
                _errorLog.errorLogHandle("",ex.Message,ex.InnerException.ToString(), jsonModel.message,"");
                return Json(jsonModel, JsonRequestBehavior.AllowGet);

            }
        }
        /// <summary>
        /// Created by Rajesh Gami
        /// </summary>
        /// For Get all DepartmentList
        /// <returns></returns>
        public JsonResult GetAllDepartment()
        {
            JsonModels jsonModel = new JsonModels();
            try
            {
                IWorkMasterRepository _repository = new WorkMasterRepository();

                var departmentList = _repository.getAllDepartment();

                jsonModel.rows = departmentList;
                jsonModel.status = "200";
                jsonModel.message = "Success";
                return Json(jsonModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                jsonModel.status = "500";
                jsonModel.message = "Error in CustomerComplaint in GetAllDepartment method";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), jsonModel.message, "");
                return Json(jsonModel, JsonRequestBehavior.AllowGet);

            }
        }
        /// <summary>
        /// GetCustComplaintById : Created by Rajesh Gami
        /// </summary>
        /// Get customer complaint data by id
        /// <param name="custComplaintId"></param>
        /// <returns></returns>
        public JsonResult GetCustComplaintById(int custComplaintId)
        {
            JsonModels jsonModel = new JsonModels();
            try
            {
                var departmentList = _repository.getCustComplaintById(custComplaintId);

                jsonModel.item = departmentList;
                jsonModel.status = "200";
                jsonModel.message = "Success";
                return Json(jsonModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                jsonModel.status = "500";
                jsonModel.message = "Error in CustomerComplaint in GetCustComplaintById method";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), jsonModel.message, "");
                return Json(jsonModel, JsonRequestBehavior.AllowGet);

            }
        }
        /// <summary>
        /// AddNewWorkMaster
        /// </summary>
        /// Created by Rajesh Gami
        /// <param name="customerObj"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult AddNewCustComplaint(CustomerComplaintDTO customerObj)
        {
            JsonModels jsonModel = new JsonModels();
            try
            {
                var complaintDetail = _repository.addNewCustComplaint(customerObj);

                jsonModel.item = complaintDetail;
                jsonModel.status = "200";
                jsonModel.message = "Success";
                return Json(jsonModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                jsonModel.status = "500";
                jsonModel.message = "Error in CustomerComplaint in AddNewCustComplaint";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), jsonModel.message, "");
                return Json(jsonModel, JsonRequestBehavior.AllowGet);

            }
        }
        /// <summary>
        /// updateCustComplaint
        /// </summary>
        /// Created by Rajesh Gami
        /// <param name="customerObj"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult UpdateCustComplaint(CustomerComplaintDTO customerObj)
        {
            JsonModels jsonModel = new JsonModels();
            try
            {
                var result = _repository.updateCustComplaint(customerObj);

                jsonModel.item = result;
                jsonModel.status = "200";
                jsonModel.message = "Success";
                return Json(jsonModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                jsonModel.status = "500";
                jsonModel.message = "Error in CustomerComplaint in UpdateCustComplaint method";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), jsonModel.message, "");
                return Json(jsonModel, JsonRequestBehavior.AllowGet);

            }
        }
        /// <summary>
        /// DeleteCustComplaint
        /// </summary>
        /// Created by Rajesh
        /// <param name="custComplaintId"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult DeleteCustComplaint(int custComplaintId)
        {
            JsonModels jsonModel = new JsonModels();
            try
            {
            
                var getbyID = dataContext.Incr_CustomerComplaint.Where(m => m.RID.Equals(custComplaintId)).FirstOrDefault();
                var result = _repository.deleteCustComplaint(getbyID);
                jsonModel.item = result;
                jsonModel.status = "200";
                jsonModel.message = "Success";
                return Json(jsonModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                jsonModel.status = "500";
                jsonModel.message = "Error in CustomerComplaint in DeleteCustComplaint";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), jsonModel.message, "");
                return Json(jsonModel, JsonRequestBehavior.AllowGet);

            }
        }

        /// <summary>
        /// getOrderByCustomerId : Created by Rajesh Gami
        /// </summary>
        /// getOrderByCustomerId
        /// <param name="customerId"></param>
        /// <returns></returns>
        public JsonResult getOrderByCustomerId(int customerId)
        {
            JsonModels jsonModel = new JsonModels();
            try
            {
                var OrderByCustomerId = _repository.getOrderByCustomerId(customerId);

                jsonModel.item = OrderByCustomerId;
                jsonModel.status = "200";
                jsonModel.message = "Success";
                return Json(jsonModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                jsonModel.status = "500";
                jsonModel.message = "Error in CustomerComplaint in getOrderByCustomerId method";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), jsonModel.message, "");
                return Json(jsonModel, JsonRequestBehavior.AllowGet);

            }
        }
    }
}