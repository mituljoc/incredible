﻿using incr.services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using incr.Models;
using incr.Repository;
using incr.Repository.Interface;
using incr.DTO;
using System.Data.Entity;
using System.Web.Caching;

namespace AdminLTE1.Controllers
{

    public class OrderController : Controller
    {
        protected IncrEntities dataContext = new IncrEntities();
        private OrderService _orderService;
        private IOrderRepository _orderRepository = new OrderRepository();
        IErrorLogRepository _errorLog = new ErrorLogRepository();
        public OrderController()
        {
            _orderRepository = new OrderRepository();
            _orderService = new OrderService();
        }
        // 


        public ActionResult Index()
        {
            incr.Utilities.DataAccess _dataAccess = new incr.Utilities.DataAccess();
            incr.Models.OrderDTO obj = new incr.Models.OrderDTO();
            obj = _dataAccess.GetListof<incr.Models.OrderDTO>(string.Format("exec incr_getNextorderNumber")).SingleOrDefault();
            ViewBag.Job = obj.JobNo;
            incr.Models.OrderDTO obj1 = new incr.Models.OrderDTO();
            return View(obj1);
        }

        public ActionResult Invoice()
        {
            return View("Invoice");
        }
        public JsonResult checkOrderNumber(string id)
        {
            JsonModels jsonModel = new JsonModels();
            try
            {
                incr.Utilities.DataAccess _dataAccess = new incr.Utilities.DataAccess();
                incr.Models.OrderDTO obj = new incr.Models.OrderDTO();
                obj = _dataAccess.GetListof<incr.Models.OrderDTO>(string.Format("exec incr_checkorderNumber '{0}'",id)).SingleOrDefault();
                jsonModel.item = obj.JobNo;
                jsonModel.status = "200";
                jsonModel.message = "Success";
                return Json(jsonModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                jsonModel.status = "500";
                jsonModel.message = "Error in orderController in GetOrderById method";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), jsonModel.message, "");
                return Json(jsonModel, JsonRequestBehavior.AllowGet);

            }
        }
        public ActionResult OrderPreView(long id)
        {
           string htmlforPrint= _orderService.getOrderReciptForPrint(id);
            TempData["htmlforPrint"] = htmlforPrint;
            return View("OrderPrintPreview");
        }

        public ActionResult order(long id)
        {
            ViewBag.Mode = "";
            incr.Utilities.DataAccess _dataAccess = new incr.Utilities.DataAccess();
            incr.Models.OrderDTO obj = new incr.Models.OrderDTO();
            if (id != 0)
            {
                obj = _dataAccess.GetListof<incr.Models.OrderDTO>(string.Format("exec incr_getorderDetails {0}", id)).SingleOrDefault();
                return View("index", obj);
            }
            return View("index", obj);
        }
        public ActionResult viewDetails(long id)
        {
            ViewBag.Mode = "view";
            incr.Utilities.DataAccess _dataAccess = new incr.Utilities.DataAccess();
            incr.Models.OrderDTO obj = new incr.Models.OrderDTO();
            if (id != 0)
            {
                obj = _dataAccess.GetListof<incr.Models.OrderDTO>(string.Format("exec incr_getorderDetails {0}", id)).SingleOrDefault();
                return View("index", obj);
            }
            return View("index", obj);
        }
        public ActionResult InvoicePreView( string id)
        {
            var orders = _orderRepository.getOrderByIds(id);
            return View("InvoicePreView", orders);
        }
        public ActionResult InvoiceView(string id)
        {
            var orders = _orderRepository.getInvoiceDetails(id);
            return View("InvoicePreView", orders);
        }
        public JsonResult SaveOrderDetails(FormCollection formCollection)
        {
            try
            {
                string id = formCollection["orderid"] ?? "0";
                string Particular3Rate = formCollection["Particular3-rate"] ?? "";
                OrderDTO incrOrderMaster = new OrderDTO();
                incrOrderMaster.Id = Convert.ToInt16(id);
                incrOrderMaster.CustomerName = formCollection["customerName"] ?? "";
                incrOrderMaster.JobNo = formCollection["JobNo"] ?? "";
                incrOrderMaster.Address = formCollection["Address"] ?? "";
                incrOrderMaster.Mobile = formCollection["mobile"] ?? "";
                incrOrderMaster.JobBy = formCollection["JobBy"] ?? "";
                incrOrderMaster.SalesBy = formCollection["SalesBy"] ?? "";
                incrOrderMaster.JobById = Convert.ToInt64(formCollection["JobById"] ?? "0");
                incrOrderMaster.SalesById = Convert.ToInt64(formCollection["SalesById"] ?? "0");
                incrOrderMaster.CustomerId = Convert.ToInt64(formCollection["customerId"] ?? "0");
                incrOrderMaster.Delivery = formCollection["delivery"] ?? "";
                incrOrderMaster.PlyType = formCollection["ply"] ?? "";
                incrOrderMaster.DeliveryDate = Convert.ToDateTime(formCollection["DeliveryDate"] ?? DateTime.Now.ToString());
                incrOrderMaster.OrderDate = Convert.ToDateTime(formCollection["txtorderdate"] ?? DateTime.Now.ToString());

                incrOrderMaster.Spring = formCollection["Spring"] ?? "";
                incrOrderMaster.Foaming = formCollection["Foaming"] ?? "";
                incrOrderMaster.LinningType = formCollection["LinningType"] ?? "";
                incrOrderMaster.StitchForSofa = formCollection["StitchForSofa"] ?? "";
                incrOrderMaster.CushionStitch = formCollection["CushionSitch"] ?? "";
                incrOrderMaster.Fitting = formCollection["Fitting"] ?? "";
                incrOrderMaster.CushionBack = formCollection["CushionBack"] ?? "";
                incrOrderMaster.Packing = formCollection["Packing"] ?? "";
                incrOrderMaster.TapestryCushion = formCollection["TapestryCushion"] ?? "";
                incrOrderMaster.TapestryBodyMTR = ConverValue(formCollection["TapestryBodyMTR"] ?? "0");

                incrOrderMaster.TapestryBody = formCollection["TapestryBody"] ?? "";
                incrOrderMaster.TapestrySeat = formCollection["TapestrySeat"] ?? "";

                incrOrderMaster.TapestrySeatMTR = ConverValue(formCollection["TapestrySeatMTR"] ?? "0");
                incrOrderMaster.TapestryCushionMTR = ConverValue(formCollection["TapestryCushionMTR"] ?? "0");

                incrOrderMaster.TapestryBodyFabricType = formCollection["TapestryBodyfabric"] ?? "";
                incrOrderMaster.TapestrySeatFabricType = formCollection["TapestrySeatfabric"] ?? "";
                incrOrderMaster.TapestryCushionFabricType = formCollection["TapestryCushionfabric"] ?? "";
                incrOrderMaster.Particular1 = formCollection["Particular1"] ?? "";
                incrOrderMaster.Particular2 = formCollection["Particular2"] ?? "";
                incrOrderMaster.Particular3 = formCollection["Particular3"] ?? "";
                incrOrderMaster.Particular4 = formCollection["Particular3"] ?? "";


                incrOrderMaster.Particular1Qty =(formCollection["Particular1-qty"] ?? "0");
                incrOrderMaster.Particular2Qty = (formCollection["Particular2-qty"] ?? "0");
                incrOrderMaster.Particular3Qty =(formCollection["Particular3-qty"] ?? "0");
                incrOrderMaster .Particular4Qty=(formCollection["Particular4-qty"] ?? "0");

                incrOrderMaster.Particular1Rate = ConverValue(formCollection["Particular1-rate"] ?? "0");
                incrOrderMaster.Particular2Rate = ConverValue(formCollection["Particular2-rate"] ?? "0");
                incrOrderMaster.Particular3Rate = ConverValue(formCollection["Particular3-rate"] ?? "0");
                incrOrderMaster.Particular4Rate = ConverValue(formCollection["Particular4-rate"] ?? "0");

                incrOrderMaster.Particular1Total = ConverValue(formCollection["Particular1Total"] ?? "0");
                incrOrderMaster.Particular2Total = ConverValue(formCollection["Particular2Total"] ?? "0");
                incrOrderMaster.Particular3Total = ConverValue(formCollection["Particular3Total"] ?? "0");
                incrOrderMaster.Particular4Total = ConverValue(formCollection["Particular4Total"] ?? "0");

       
                incrOrderMaster.CarpantaryTopWidth = ConverValue(formCollection["carpantary-top-width"] ?? "0");
                incrOrderMaster.CarpantaryMiddleWidth = ConverValue(formCollection["carpantary-middle-width"] ?? "0");
                incrOrderMaster.CarpantaryBottomWidth = ConverValue(formCollection["carpantary-bottom-width"] ?? "0");
                incrOrderMaster.CarpantaryLeftHight = ConverValue(formCollection["carpantary-left-hight"] ?? "0");
                incrOrderMaster.CarpantaryRightHight = ConverValue(formCollection["carpantary-right-hight"] ?? "0");
                incrOrderMaster.CarpantaryRightHight1 = ConverValue(formCollection["carpantary-right-hight1"] ?? "0");
                incrOrderMaster.CarpantaryRightHight2 = ConverValue(formCollection["carpantary-right-hight2"] ?? "0");
                incrOrderMaster.CarpantaryRightHight3 = ConverValue(formCollection["carpantary-right-hight3"] ?? "0");
                incrOrderMaster.CarpantaryRightWidth = ConverValue(formCollection["carpantary-right-width"] ?? "0");
                 string fabric= (formCollection["chkWithoutfabric"] ?? "false");
                incrOrderMaster.IsWithoutfabric =false;
                if (fabric.ToUpper() == "ON")
                {
                    incrOrderMaster.IsWithoutfabric = true;
                }
            
         
                incrOrderMaster.HandleLength = ConverValue(formCollection["handle-length"] ?? "0");
                incrOrderMaster.HandleHeight = ConverValue(formCollection["handle-height"] ?? "0");
                incrOrderMaster.HandleWidth = ConverValue(formCollection["handle-width"] ?? "0");

                incrOrderMaster.DrawingType = formCollection["Drawing-type"] ?? "";
                incrOrderMaster.LeftNumberOfsits = Convert.ToInt64(formCollection["lcs"] ?? "0");
                incrOrderMaster.RightNumberOfsits = Convert.ToInt64(formCollection["rcs"] ?? "0");

                incrOrderMaster.DrawingRightSize0 = ConverValue(formCollection["rw0"] ?? "0");
                incrOrderMaster.DrawingRightSize1 = ConverValue(formCollection["rw1"] ?? "0");
                incrOrderMaster.DrawingRightSize2 = ConverValue(formCollection["rw2"] ?? "0");
                incrOrderMaster.DrawingRightSize3 = ConverValue(formCollection["rw3"] ?? "0");
                incrOrderMaster.DrawingRightSize4 = ConverValue(formCollection["rw4"] ?? "0");
                incrOrderMaster.DrawingRightSize5 = ConverValue(formCollection["rw5"] ?? "0");
                incrOrderMaster.DrawingRightSize6 = ConverValue(formCollection["rw6"] ?? "0");

                incrOrderMaster.DrawingleftSize1 = ConverValue(formCollection["lw1"] ?? "0");
                incrOrderMaster.DrawingleftSize2 = ConverValue(formCollection["lw2"] ?? "0");
                incrOrderMaster.DrawingleftSize3 = ConverValue(formCollection["lw3"] ?? "0");
                incrOrderMaster.DrawingleftSize4 = ConverValue(formCollection["lw4"] ?? "0");
                incrOrderMaster.DrawingleftSize5 = ConverValue(formCollection["lw5"] ?? "0");
                incrOrderMaster.DrawingleftSize6 = ConverValue(formCollection["lw6"] ?? "0");
                incrOrderMaster.Drawingfullwidth = ConverValue(formCollection["dfullwidth"] ?? "0"); ;
                incrOrderMaster.Drawingfullheight = ConverValue(formCollection["dfullheight"] ?? "0");
                incrOrderMaster.LongerSide = formCollection["LongerSide"] ?? "";
                incrOrderMaster.LinningQty = formCollection["linningqty"] ?? "";
                incrOrderMaster.BodyFabricTypeId= Convert.ToInt64(formCollection["TapestryBodyfabricId"] ?? "0");
                incrOrderMaster.SeatFabricTypeId= Convert.ToInt64(formCollection["TapestrySeatFabricTypeId"] ?? "0");
                incrOrderMaster.CushionFabricTypeId= Convert.ToInt64(formCollection["CushionFabricTypeId"] ?? "0");
                incrOrderMaster.Particula4FabricTypeId = Convert.ToInt64(formCollection["Particula4FabricTypeId"] ?? "0");
                incrOrderMaster.Discount = Convert.ToInt64(formCollection["Discount"] ?? "0");
                incrOrderMaster.Extra = Convert.ToInt64(formCollection["Extra"] ?? "0");
                incrOrderMaster.TransPortPrice = ConverValue(formCollection["TransPort"] ?? "0");
                incrOrderMaster.SGST = ConverValue(formCollection["SGST"] ?? "0");
                incrOrderMaster.CGST = ConverValue(formCollection["CGST"] ?? "0");
                incrOrderMaster.NetOrderPrice = ConverValue(formCollection["netAmmaunt"] ?? "0");
                incrOrderMaster.TotalOrderPrice = ConverValue(formCollection["grossAmmaunt"] ?? "0");
                incrOrderMaster.DrawingHandle1 = ConverValue(formCollection["handle1"] ?? "0");
                incrOrderMaster.DrawingHandle2 = ConverValue(formCollection["handle2"] ?? "0");
                incrOrderMaster.DrawingHandle3 = ConverValue(formCollection["handle3"] ?? "0");
                incrOrderMaster.DrawingHandle4 = ConverValue(formCollection["handle4"] ?? "0");
                incrOrderMaster.Particular5 = formCollection["Particular5"] ?? "";
                incrOrderMaster.Particular5Qty = (formCollection["Particular5qty"] ?? "0");
                incrOrderMaster.Particular5Rate = ConverValue(formCollection["Particular5rate"] ?? "0");
                incrOrderMaster.Particular5FabricTypeId = Convert.ToInt64(formCollection["Particular5FabricTypeId"] ?? "0");
                incrOrderMaster.ParticularNameId = Convert.ToInt64(formCollection["tmpParticularNameId"] ?? "0");
                if (Convert.ToString(formCollection["ParticularNameId"] ?? "")!="")
                {
                 

                    incr_price_list incr_price_list = new incr_price_list();
                    incr_price_list.Name= Convert.ToString(formCollection["ParticularNameId"] ?? "");
                    incr_price_list.Pert1 = incrOrderMaster.Particular1;
                    incr_price_list.Pert1_mtr = incrOrderMaster.Particular1Qty;
                    incr_price_list.Pert1_rate = incrOrderMaster.Particular1Rate;

                    incr_price_list.Pert2 = incrOrderMaster.Particular2;
                    incr_price_list.Pert2_mtr = incrOrderMaster.Particular2Qty;
                    incr_price_list.Pert2_rate = incrOrderMaster.Particular2Rate;

                    incr_price_list.Pert3 = incrOrderMaster.Particular3;
                    incr_price_list.Pert3_mtr = incrOrderMaster.Particular3Qty;
                    incr_price_list.Pert3_rate = incrOrderMaster.Particular3Rate;

                    incr_price_list.Pert4 = incrOrderMaster.Particular4;
                    incr_price_list.Pert4_mtr = incrOrderMaster.Particular4Qty;
                    incr_price_list.Pert4_rate = incrOrderMaster.Particular4Rate;

                    incr_price_list.Pert5 = incrOrderMaster.Particular5;
                    incr_price_list.Pert5_mtr = incrOrderMaster.Particular5Qty;
                    incr_price_list.Pert5_rate = incrOrderMaster.Particular5Rate;


                    incr_price_list.Price_per = formCollection["hdnPrice_per"]??"";
                    incr_price_list.Code = formCollection["hdnCoder"] ?? ""; 
                    incr_price_list.Type = formCollection["hdntype"] ?? "";
                    incr_price_list.Range = formCollection["hdnRange"] ?? "";
                    incr_price_list.Without_fabric = incrOrderMaster.IsWithoutfabric;
                    IProductRepository OrderRepository  = new ProductRepository();
                    incrOrderMaster.ParticularNameId= OrderRepository.addProductFromOrderPage(incr_price_list, incr_price_list.Name);
                }

                incrOrderMaster.Partyname =formCollection["Partyname"] ?? "";
                incrOrderMaster.Polish = formCollection["Polish"] ?? "";
               long jobId= _orderService.SaveOrder(incrOrderMaster);
                JsonModels jsonModel = new JsonModels();
                jsonModel.status = "200";
                jsonModel.message = "Success";
                jsonModel.item = new { jobId=jobId };
                return Json(jsonModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                 throw ex; //TODO: exception/error message print log file for track exception 
            }


        }
       private decimal ConverValue(string strvalue)
        {
            decimal value = 0;
            decimal.TryParse(strvalue, out value);
            return value;
        }
        public string LoadData()
        {
            try
            {

                var draw = Request.Form.GetValues("draw").FirstOrDefault();
                var start = Request.Form.GetValues("start").FirstOrDefault();
                var length = Request.Form.GetValues("length").FirstOrDefault();
                var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
                var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                return incr.Utilities.DataAccess.GetGridData(draw, start, pageSize, sortColumn, sortColumnDir, "incr_orders_search", null, null);
            }
            catch (Exception ex)
            {
                throw ex; //TODO: exception/error message print log file for track exception 
            }

        }
        public ActionResult Orders()
        {

            return View("workcenter");
        }
        public JsonResult GetInvoiceStatus( )
        {
            JsonModels jsonModel = new JsonModels();
            try
            {
                var Status = _orderRepository.GetInvoiceStatus();
                jsonModel.item = Status;
                jsonModel.status = "200";
                jsonModel.message = "Success";
                return Json(jsonModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                jsonModel.status = "500";
                jsonModel.message = "Error in orderController in GetOrderById method";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), jsonModel.message, "");
                return Json(jsonModel, JsonRequestBehavior.AllowGet);
            }
        }

        
        public JsonResult GetOrderById(int orderId)
        {
            JsonModels jsonModel = new JsonModels();
            try
            {
                var order = _orderRepository.getOrderById(orderId);
                jsonModel.item = order;
                jsonModel.status = "200";
                jsonModel.message = "Success";
                return Json(jsonModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                jsonModel.status = "500";
                jsonModel.message = "Error in orderController in GetOrderById method";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), jsonModel.message, "");
                return Json(jsonModel, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetOrderByInvoiceIds(string InvoiceId)
        {
            JsonModels jsonModel = new JsonModels();
            try
            {
                var order = _orderRepository.GetOrderByInvoiceIds(InvoiceId);
                jsonModel.item = order;
                jsonModel.status = "200";
                jsonModel.message = "Success";
                return Json(jsonModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                jsonModel.status = "500";
                jsonModel.message = "Error in orderController in GetOrderById method";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), jsonModel.message, "");
                return Json(jsonModel, JsonRequestBehavior.AllowGet);
            }
        }
        
        public JsonResult GenerateInvoice(string id)
        {
            JsonModels jsonModel = new JsonModels();
            _orderRepository.GenerateInvoice(id);
         
            jsonModel.status = "200";
            jsonModel.message = "Success";
            return Json(jsonModel, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetOrderByIds(string orderIds)
        {
            JsonModels jsonModel = new JsonModels();
            try
            {
                var order = _orderRepository.getOrderByIds(orderIds);
                jsonModel.item = order;
                jsonModel.status = "200";
                jsonModel.message = "Success";
                return Json(jsonModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                jsonModel.status = "500";
                jsonModel.message = "Error in orderController in GetOrderById method";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), jsonModel.message, "");
                return Json(jsonModel, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult StartJob(JsonDynamicWrapper wrapper)
        {
            JsonModels jsonModel = new JsonModels();
            try
            {
                _orderRepository.startJob(wrapper.Id, wrapper.WorkById, wrapper.SupervisedId, wrapper.Remarks, wrapper.VehicleNo);
                jsonModel.status = "200";
                jsonModel.message = "Success";
                return Json(jsonModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                jsonModel.status = "500";
                jsonModel.message = "Error in orderController in GetOrderById method";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), jsonModel.message, "");
                return Json(jsonModel, JsonRequestBehavior.AllowGet);

            }
        }

        public JsonResult CloseInvoice(JsonDynamicWrapper wrapper)
        {
            JsonModels jsonModel = new JsonModels();
            try
            {
                _orderRepository.closeInvoice(wrapper.InvoiceId, wrapper.InvoiceStatusId, wrapper.Amount);
                jsonModel.status = "200";
                jsonModel.message = "Success";
                return Json(jsonModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                jsonModel.status = "500";
                jsonModel.message = "Error in orderController in GetOrderById method";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), jsonModel.message, "");
                return Json(jsonModel, JsonRequestBehavior.AllowGet);

            }
        }

        public JsonResult StartProduction(int orderId)
        {
            JsonModels jsonModel = new JsonModels();
            try
            {
                _orderRepository.StartProduction(orderId);
                jsonModel.status = "200";
                jsonModel.message = "Success";
                return Json(jsonModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                jsonModel.status = "500";
                jsonModel.message = "Error in orderController in StartProduction method";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), jsonModel.message, "");
                return Json(jsonModel, JsonRequestBehavior.AllowGet);

            }
        }

        public JsonResult FinishJob(JsonDynamicWrapper wrapper)
        {
            JsonModels jsonModel = new JsonModels();
            try
            {
                if (wrapper.VehicleNo == null)
                {
                    wrapper.VehicleNo = "";
                }
                _orderRepository.finishJob(wrapper.Id, wrapper.WorkById, wrapper.SupervisedId, wrapper.Remarks,wrapper.VehicleNo);
                jsonModel.status = "200";
                jsonModel.message = "Success";
                return Json(jsonModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                jsonModel.status = "500";
                jsonModel.message = "Error in orderController in GetOrderById method";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), jsonModel.message, "");
                return Json(jsonModel, JsonRequestBehavior.AllowGet);

            }
        }

        public JsonResult GetAllOrders(string type)
        {
            if (type == "CARPENTARY")
            {
                ViewBag.Title = "CARPENTARY";
            }

            JsonModels jsonModel = new JsonModels();
            try
            {
                IOrderRepository _orderRepository = new OrderRepository();
                var orderList = _orderRepository.getAllOrders(type);
                jsonModel.item = orderList;
                jsonModel.status = "200";
                jsonModel.message = "Success";
                return Json(jsonModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                jsonModel.status = "500";
                jsonModel.message = "Error in OrderController in GetAllOrders method" + ex.ToString();
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), jsonModel.message, "");
                return Json(jsonModel, JsonRequestBehavior.AllowGet);

            }
        }
        public JsonResult GetInvoiceBYStatus(string type)
        {
            if (type == "CARPENTARY")
            {
                ViewBag.Title = "CARPENTARY";
            }

            JsonModels jsonModel = new JsonModels();
            try
            {
                IOrderRepository _orderRepository = new OrderRepository();
                var orderList = _orderRepository.GetInvoiceBYStatus(type);
                jsonModel.item = orderList;
                jsonModel.status = "200";
                jsonModel.message = "Success";
                return Json(jsonModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                jsonModel.status = "500";
                jsonModel.message = "Error in OrderController in GetInvoiceBYStatus method" + ex.ToString();
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), jsonModel.message, "");
                return Json(jsonModel, JsonRequestBehavior.AllowGet);

            }
        }
        /// <summary>
        /// Created By Rajesh
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public JsonResult GetClosedInvoiceBYStatus(string type)
        {
            if (type == "CARPENTARY")
            {
                ViewBag.Title = "CARPENTARY";
            }

            JsonModels jsonModel = new JsonModels();
            try
            {
                IOrderRepository _orderRepository = new OrderRepository();
                var orderList = _orderRepository.GetClosedInvoiceBYStatus(type);
                jsonModel.item = orderList;
                jsonModel.status = "200";
                jsonModel.message = "Success";
                return Json(jsonModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                jsonModel.status = "500";
                jsonModel.message = "Error in OrderController in GetClosedInvoiceBYStatus method" + ex.ToString();
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), jsonModel.message, "");
                return Json(jsonModel, JsonRequestBehavior.AllowGet);

            }
        }
        public JsonResult GetPriceList()
        {

            JsonModels jsonModel = new JsonModels();
            try
            {
                var priceList = HttpContext.Cache.Get("priceList") as List<incr_price_list>;
                if (priceList == null)
                {
                    IOrderRepository _orderRepository = new OrderRepository();

                    priceList = _orderRepository.getPriceList();

                    HttpContext.Cache.Insert("priceList", priceList, new CacheDependency(Server.MapPath("~/CacheFile.txt")));
                }
                jsonModel.rows = priceList;
                jsonModel.status = "200";
                jsonModel.message = "Success";
                return Json(jsonModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                jsonModel.status = "500";
                jsonModel.message = "Error in OperatorController in GetAllOperator method";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), jsonModel.message, "");
                return Json(jsonModel, JsonRequestBehavior.AllowGet);

            }
        }
        // GET: Order
        public ActionResult table()
        {
            return View("table");
        }

        /// <summary>
        /// DeleteOrder
        /// </summary>
        /// Created by Rajesh
        /// <param name="orderId"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult DeleteOrder(int orderId)
        {
            JsonModels jsonModel = new JsonModels();
            try
            {
                var getbyID = dataContext.incr_order_master.Where(m => m.id.Equals(orderId)).FirstOrDefault();
                var result = _orderRepository.DeleteOrder(getbyID);
                jsonModel.item = result;
                jsonModel.status = "200";
                jsonModel.message = "Success";
                return Json(jsonModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                jsonModel.status = "500";
                jsonModel.message = "Error in OrderController in DeleteOrder";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), jsonModel.message, "");
                return Json(jsonModel, JsonRequestBehavior.AllowGet);
            }
        }
    }

    public class JsonDynamicWrapper
    {
        public long Id { get; set; }
        public long WorkById { get; set; }
        public long SupervisedId { get; set; }
        public string Remarks { get; set; }
        public decimal Amount { get; set; }
        public long InvoiceId { get; set; }
        public long InvoiceStatusId { get; set; }
        public string VehicleNo { get; set; }
    }
}