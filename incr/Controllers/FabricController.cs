﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using incr.Models;
using incr.Repository;
using incr.Repository.Interface;
using incr.DTO;

namespace incr.Controllers
{
    public class FabricController : Controller
    {
        protected IncrEntities dataContext = new IncrEntities();
        IFabricRepository _fabricRepository = new FabricRepository();
        IErrorLogRepository _errorLog = new ErrorLogRepository();
        public ActionResult Index()
        {

            return View();
        }
        /// <summary>
        /// Created by Rajesh Gami
        /// </summary>
        /// For Get all GetAllFabric
        /// <returns></returns>
        public JsonResult GetAllFabric()
        {
            JsonModels jsonModel = new JsonModels();
            try
            {
                var fabricList = _fabricRepository.getFabricList();
                jsonModel.rows = fabricList;
                jsonModel.status = "200";
                jsonModel.message = "Success";
                return Json(jsonModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                jsonModel.status = "500";
                jsonModel.message = "Error in FabricController in GetAllFabric method";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), jsonModel.message, "");
                return Json(jsonModel, JsonRequestBehavior.AllowGet);

            }
        }
        /// <summary>
        /// getFabricById : Created by Rajesh Gami
        /// </summary>
        /// getFabricById
        /// <param name="fabricId"></param>
        /// <returns></returns>
        public JsonResult GetFabricById(int fabricId)
        {
            JsonModels jsonModel = new JsonModels();
            try
            {
                var fabricDetail = _fabricRepository.getFabricById(fabricId);

                jsonModel.item = fabricDetail;
                jsonModel.status = "200";
                jsonModel.message = "Success";
                return Json(jsonModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                jsonModel.status = "500";
                jsonModel.message = "Error in FabricController in GetFabricGroupById method";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), jsonModel.message, "");
                return Json(jsonModel, JsonRequestBehavior.AllowGet);

            }
        }
        /// <summary>
        /// AddFabric
        /// </summary>
        /// Created by Rajesh Gami
        /// <param name="fabricObj"></param>
        /// <returns></returns>FabricDTO
        [HttpPost]
        public JsonResult AddFabric(FabricDTO fabricObj)
        {
            JsonModels jsonModel = new JsonModels();
            try
            {
                var fabricDetail = _fabricRepository.addFabric(fabricObj);
                jsonModel.item = fabricDetail;
                jsonModel.status = "200";
                jsonModel.message = "Success";
                return Json(jsonModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                jsonModel.status = "500";
                jsonModel.message = "Error in FabricController in AddFabric";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), jsonModel.message, "");
                return Json(jsonModel, JsonRequestBehavior.AllowGet);

            }
        }
        /// <summary>
        /// updateFabricGroup
        /// </summary>
        /// Created by Rajesh Gami
        /// <param name=""></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult UpdateFabric(FabricDTO fabricObj)
        {
            JsonModels jsonModel = new JsonModels();
            try
            {
                var result = _fabricRepository.updateFabric(fabricObj);

                jsonModel.item = result;
                jsonModel.status = "200";
                jsonModel.message = "Success";
                return Json(jsonModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                jsonModel.status = "500";
                jsonModel.message = "Error in FabricController in UpdateFabric method";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), jsonModel.message, "");
                return Json(jsonModel, JsonRequestBehavior.AllowGet);

            }
        }
        /// <summary>
        /// DeleteFabric
        /// </summary>
        /// Created by Rajesh
        /// <param name="fabricId"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult DeleteFabric(int fabricId)
        {
            JsonModels jsonModel = new JsonModels();
            try
            {
                var getbyID = dataContext.Incr_FabricMaster.Where(m => m.RID.Equals(fabricId)).FirstOrDefault();
                var result = _fabricRepository.deleteFabric(getbyID);
                jsonModel.item = result;
                jsonModel.status = "200";
                jsonModel.message = "Success";
                return Json(jsonModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                jsonModel.status = "500";
                jsonModel.message = "Error in FabricController in DeleteFabricGroup";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), jsonModel.message, "");
                return Json(jsonModel, JsonRequestBehavior.AllowGet);
            }
        }
    }
}