﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using incr.Models;
using incr.Repository;
using incr.Repository.Interface;
using incr.DTO;
using System.Data.Entity;
using Newtonsoft.Json.Linq;

namespace incr.Controllers
{
    public class OperatorWorkMasterController : Controller
    {
        protected IncrEntities dataContext = new IncrEntities();
        IErrorLogRepository _errorLog = new ErrorLogRepository();
        // GET: OperatorWorkMaster

        private Utilities.DataAccess _dataAccess;
        public OperatorWorkMasterController()
        {
            _dataAccess = new Utilities.DataAccess();
        }
        public ActionResult Index()
        {
            return View();
        }

        
        /// <summary>
        /// Created by Rajesh Gami
        /// </summary>
        /// For Get all operator work master list which is isDeleted = false
        /// <returns></returns>
        public JsonResult GetAllOpeartorWorkMaster()
        {
            JsonModels jsonModel = new JsonModels();
            try
            {
                IOperatorWorkRepository _operatorWorkRepository = new OperatorWorkRepository();

                var operatorWorkList = _operatorWorkRepository.getAllOpeartorWorkMaster();
        
                jsonModel.rows = operatorWorkList;
                jsonModel.status = "200";
                jsonModel.message = "Success";
                return Json(jsonModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                jsonModel.status = "500";
                jsonModel.message = "Error in OperatorController in GetAllOperator method";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), jsonModel.message, "");
                return Json(jsonModel, JsonRequestBehavior.AllowGet);

            }
        }

        /// <summary>
        /// AddNewOperatorWorkMaster
        /// </summary>
        /// Created by Rajesh Gami
        /// <param name="OperatorworkMasterObj"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult AddNewOperatorWorkMaster(string data)
        {
            dynamic json = JValue.Parse(data);

            JsonModels jsonModel = new JsonModels();
            try
            {
                OperatorWorkMasterDTO workMasterObj = new OperatorWorkMasterDTO();
                workMasterObj.DepartmentId = json.DepartmentId.ToString();
                workMasterObj.WorkId = json.WorkId.ToString();
                workMasterObj.OperatorId = json.OperatorId.ToString();
                IOperatorWorkRepository _workMasterRepository = new OperatorWorkRepository();

                long Id = _workMasterRepository.addNewOperatorWorkMaster(workMasterObj);

                string sql = "";
                foreach (dynamic price in json.OperatorPriceList)
                {
                    decimal oprice = 0;
                    if (price.operatorPrice!= null)
                    {
                        decimal.TryParse(price.operatorPrice.ToString(), out oprice);
                    }

                    sql = sql + string.Format("exec InserUpdateOperatorPriceList {0},{1},{2},'{3}' ;", Id, workMasterObj.OperatorId, price.id.ToString(), oprice);

                }
                if (sql != "")
                {
                    _dataAccess.ExecuteNonQuery(sql);
                }
             
                jsonModel.status = "200";
                jsonModel.message = "Success";
                return Json(jsonModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                jsonModel.status = "500";
                jsonModel.message = "Error in WorkMasterController in AddNewWorkMaster";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), jsonModel.message, "");
                return Json(jsonModel, JsonRequestBehavior.AllowGet);

            }
        }



        /// <summary>
        /// UpdateWorkMaster
        /// </summary>
        /// Created by Rajesh Gami
        /// <param name="WorkMasterobj"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult UpdateWorkMaster(string data)
        {
            dynamic json = JValue.Parse(data);

            JsonModels jsonModel = new JsonModels();
            try
            {
                OperatorWorkMasterDTO workMasterObj = new OperatorWorkMasterDTO();
                workMasterObj.DepartmentId = json.DepartmentId.ToString();
                workMasterObj.WorkId = json.WorkId.ToString();
                workMasterObj.OperatorId = json.OperatorId.ToString();
                workMasterObj.RID = Convert.ToInt16( json.RID.ToString());
                IOperatorWorkRepository _workMasterRepository = new OperatorWorkRepository();

                var result = _workMasterRepository.updateWorkMaster(workMasterObj);
                string sql = "";
                foreach (dynamic price in json.OperatorPriceList)
                {
                    decimal oprice = 0;
                    if (price.operatorPrice != null)
                    {
                        decimal.TryParse(price.operatorPrice.ToString(), out oprice);
                    }
                    sql = sql + string.Format("exec InserUpdateOperatorPriceList {0},{1},{2},'{3}' ;", workMasterObj.RID, workMasterObj.OperatorId, price.id.ToString(), oprice);
                }
                if (sql != "")
                {
                    _dataAccess.ExecuteNonQuery(sql);
                }
                jsonModel.item = result;
                jsonModel.status = "200";
                jsonModel.message = "Success";
                return Json(jsonModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                jsonModel.status = "500";
                jsonModel.message = "Error in OperatorWorkMasterController in UpdateWorkMaster method";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), jsonModel.message, "");
                return Json(jsonModel, JsonRequestBehavior.AllowGet);

            }
        }

        /// <summary>
        /// GetWorkMasterById : Created by Rajesh Gami
        /// </summary>
        /// Get workMaster data by workmasterID
        /// <param name="operatorWorkMasterId"></param>
        /// <returns></returns>
        /// 
        public JsonResult getPriceListForEdit(int opWorkId)
        {
            JsonModels jsonModel = new JsonModels();
            try
            {
                IOperatorWorkRepository _workMasterRepository = new OperatorWorkRepository();

                var PriceList = _workMasterRepository.getPriceListForEdit(opWorkId);

                jsonModel.item = PriceList;
                jsonModel.status = "200";
                jsonModel.message = "Success";
                return Json(jsonModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                jsonModel.status = "500";
                jsonModel.message = "Error in WorkMasterController in GetOperatorById method";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), jsonModel.message, "");
                return Json(jsonModel, JsonRequestBehavior.AllowGet);

            }

            
        }
        public JsonResult GetWorkMasterById(int operatorWorkMasterId)
        {
            JsonModels jsonModel = new JsonModels();
            try
            {
                IOperatorWorkRepository _workMasterRepository = new OperatorWorkRepository();

                var workMaster = _workMasterRepository.getWorkMasterById(operatorWorkMasterId);
                var PriceList = _workMasterRepository.getPriceListForEdit(operatorWorkMasterId);
                jsonModel.item = new { workMaster = workMaster, priceList = PriceList };
                jsonModel.status = "200";
                jsonModel.message = "Success";
                return Json(jsonModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                jsonModel.status = "500";
                jsonModel.message = "Error in WorkMasterController in GetOperatorById method";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), jsonModel.message, "");
                return Json(jsonModel, JsonRequestBehavior.AllowGet);

            }
        }
        /// <summary>
        /// DeleteWorkMaster
        /// </summary>
        /// Created by Rajesh
        /// <param name="workMasterId"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult DeleteWorkMaster(int workMasterId)
        {
            JsonModels jsonModel = new JsonModels();
            try
            {
                IOperatorWorkRepository _workMasterRepository = new OperatorWorkRepository();
                var getbyID = dataContext.Incr_OperatorWorkMaster.Where(m => m.RID.Equals(workMasterId)).FirstOrDefault();
                var result = _workMasterRepository.deleteWorkMaster(getbyID);
                jsonModel.item = result;
                jsonModel.status = "200";
                jsonModel.message = "Success";
                return Json(jsonModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                jsonModel.status = "500";
                jsonModel.message = "Error in WorkMasterController in DeleteOperator";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), jsonModel.message, "");
                return Json(jsonModel, JsonRequestBehavior.AllowGet);

            }
        }
    }
}