﻿using System.Web.Mvc;
using incr.Models;
using incr.Repository;
using incr.Repository.Interface;
using incr.DTO;
using System;

namespace incr.Controllers
{
    public class DashboardController : Controller
    {
        protected IncrEntities dataContext = new IncrEntities();
        IDashboardRepository _dashboardRepository = new DashboardRespository();
        IErrorLogRepository _errorLog = new ErrorLogRepository();
        /// <summary>
        /// Created by Rajesh
        /// </summary>
        ///
        /// <returns></returns>
        public JsonResult GetOrderList()
        {
            JsonModels jsonModel = new JsonModels();
            try
            {
                var totalOrderList = _dashboardRepository.getOrderList();

                jsonModel.rows = totalOrderList;
                jsonModel.status = "200";
                jsonModel.message = "Success";
                return Json(jsonModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                jsonModel.status = "500";
                jsonModel.message = "Error in DashboardController in GetOrderList method";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), jsonModel.message, "");
                return Json(jsonModel, JsonRequestBehavior.AllowGet);

            }
        }

        /// <summary>
        /// Created by Rajesh
        /// </summary>
        ///
        /// <returns></returns>
        public JsonResult GetCarpentaryList()
        {
            JsonModels jsonModel = new JsonModels();
            try
            {
                var getCarpentaryList = _dashboardRepository.getCarpentaryList();

                jsonModel.rows = getCarpentaryList;
                jsonModel.status = "200";
                jsonModel.message = "Success";
                return Json(jsonModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                jsonModel.status = "500";
                jsonModel.message = "Error in DashboardCOntroller in GetCarpentaryList method";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), jsonModel.message, "");
                return Json(jsonModel, JsonRequestBehavior.AllowGet);

            }
        }

        /// <summary>
        /// Created by Rajesh
        /// </summary>
        ///
        /// <returns></returns>
        public JsonResult GetLiningList()
        {
            JsonModels jsonModel = new JsonModels();
            try
            {
                var getLiningList = _dashboardRepository.getLiningList();

                jsonModel.rows = getLiningList;
                jsonModel.status = "200";
                jsonModel.message = "Success";
                return Json(jsonModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                jsonModel.status = "500";
                jsonModel.message = "Error in DashboardCOntroller in GetLiningList method";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), jsonModel.message, "");
                return Json(jsonModel, JsonRequestBehavior.AllowGet);

            }
        }

        /// <summary>
        /// Created by Rajesh
        /// </summary>
        ///
        /// <returns></returns>
        public JsonResult GetFittingList()
        {
            JsonModels jsonModel = new JsonModels();
            try
            {
                var getFittingList = _dashboardRepository.getFittingList();

                jsonModel.rows = getFittingList;
                jsonModel.status = "200";
                jsonModel.message = "Success";
                return Json(jsonModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                jsonModel.status = "500";
                jsonModel.message = "Error in DashboardCOntroller in GetFittingList method";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), jsonModel.message, "");
                return Json(jsonModel, JsonRequestBehavior.AllowGet);

            }
        }

        /// <summary>
        /// Created by Rajesh
        /// </summary>
        ///
        /// <returns></returns>
        public JsonResult GetDeliveryList()
        {
            JsonModels jsonModel = new JsonModels();
            try
            {
                var getDeliveryList = _dashboardRepository.getDeliveryList();

                jsonModel.rows = getDeliveryList;
                jsonModel.status = "200";
                jsonModel.message = "Success";
                return Json(jsonModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                jsonModel.status = "500";
                jsonModel.message = "Error in DashboardController in getDeliveryList method";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), jsonModel.message, "");
                return Json(jsonModel, JsonRequestBehavior.AllowGet);

            }
        }
        /// <summary>
        /// Created by Rajesh
        /// </summary>
        ///
        /// <returns></returns>
        public JsonResult GetInvoiceList()
        {
            JsonModels jsonModel = new JsonModels();
            try
            {
                var getInvoiceList = _dashboardRepository.getInvoiceList();
                jsonModel.rows = getInvoiceList;
                jsonModel.status = "200";
                jsonModel.message = "Success";
                return Json(jsonModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                jsonModel.status = "500";
                jsonModel.message = "Error in DashboardController in getInvoiceList method";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), jsonModel.message, "");
                return Json(jsonModel, JsonRequestBehavior.AllowGet);

            }
        }
        /// <summary>
        /// Created by Rajesh
        /// </summary>
        ///
        /// <returns></returns>
        public JsonResult GetLeaderBoardList()
        {
            JsonModels jsonModel = new JsonModels();
            try
            {
                var getLeaderBoardList = _dashboardRepository.getLeaderBoardList();
                jsonModel.rows = getLeaderBoardList;
                jsonModel.status = "200";
                jsonModel.message = "Success";
                return Json(jsonModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                jsonModel.status = "500";
                jsonModel.message = "Error in DashboardController in GetLeaderBoardList method";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), jsonModel.message, "");
                return Json(jsonModel, JsonRequestBehavior.AllowGet);

            }
        }
        /// <summary>
        /// Created by Rajesh
        /// </summary>
        ///
        /// <returns></returns>
        public JsonResult GetHighestCustomerOrderList()
        {
            JsonModels jsonModel = new JsonModels();
            try
            {
                var getHighestCustomerOrderList = _dashboardRepository.getHighestCustomerOrderList();
                jsonModel.rows = getHighestCustomerOrderList;
                jsonModel.status = "200";
                jsonModel.message = "Success";
                return Json(jsonModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                jsonModel.status = "500";
                jsonModel.message = "Error in DashboardController in GetHighestCustomerOrderList method";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), jsonModel.message, "");
                return Json(jsonModel, JsonRequestBehavior.AllowGet);

            }
        }
        /// <summary>
        /// Created by Rajesh
        /// </summary>
        ///
        /// <returns></returns>
        public JsonResult GetComplaintByOperatorList()
        {
            JsonModels jsonModel = new JsonModels();
            try
            {
                var getComplaintByOperatorList = _dashboardRepository.getComplaintByOperatorList();
                jsonModel.rows = getComplaintByOperatorList;
                jsonModel.status = "200";
                jsonModel.message = "Success";
                return Json(jsonModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                jsonModel.status = "500";
                jsonModel.message = "Error in DashboardController in getComplaintByOperatorList method";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), jsonModel.message, "");
                return Json(jsonModel, JsonRequestBehavior.AllowGet);

            }
        }
    }
}