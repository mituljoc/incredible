﻿var ORDER = {
    event: function () {
        $("#btnSaveOrder").click(function () {
            ORDER.saveOrderDetails();
        });

        $('input[name=rcs]').change(function () {
            if (this.value == '2') {
                $('.rw').hide();
            }
            else if (this.value == '3') {
                $('.rw').hide();
                $('.r3').show();
            }
        });
        $('input[name=Drawing-type]').on('ifChanged', function (event) {
            if (this.checked) {
                if (this.value == 'Corner') {
                    $('.corner').show();
                    $('.Longer').hide();
                    
                }
                else if (this.value == 'Longer') {
                    $('.corner').hide();
                    $('.Longer').show();
                
                }
            }

        });
        $('input[name=lcs]').on('ifChanged', function (event) {
            if (this.checked) {
                if (this.value == '2') {
                    $('.lw').hide();
                }
                else if (this.value == '3') {
                    $('.lw').hide();
                    $('.l3').show();
                }
                else if (this.value == '4') {
                    $('.lw').hide();
                    $('.l3').show();
                    $('.l4').show();
                }
                else if (this.value == '6') {
                    $('.lw').hide();
                    $('.l3').show();
                    $('.l4').show();
                    $('.l6').show();

                }
            }
        });

        $('input[name=rcs]').on('ifChanged', function (event) {
            if (this.checked) {
                if (this.value == '2') {
                    $('.rw').hide();
                }
                else if (this.value == '3') {
                    $('.rw').hide();
                    $('.r3').show();
                }
                else if (this.value == '4') {
                    $('.rw').hide();
                    $('.r3').show();
                    $('.r4').show();
                }
                else if (this.value == '6') {
                    $('.rw').hide();
                    $('.r3').show();
                    $('.r4').show();
                    $('.r6').show();

                }
            }
        });

        $("input[name='Particular1-qty']").change(function () {
            var qty = $("input[name=Particular1-qty]").val();
            var rate = $("input[name=Particular1-rate]").val();
            $("#Particular1Total").text((qty * rate).toFixed(2));
        });

        $("input[name='Particular1-rate']").change(function () {
            var qty = $("input[name=Particular1-qty]").val();
            var rate = $("input[name=Particular1-rate]").val();
            $("#Particular1Total").text((qty * rate).toFixed(2));
        });

        $("input[name='Particular2-qty']").change(function () {
            var qty = $("input[name=Particular2-qty]").val();
            var rate = $("input[name=Particular2-rate]").val();
            $("#Particular2Total").text((qty * rate).toFixed(2));
        });

        $("input[name='Particular2-rate']").change(function () {
            var qty = $("input[name=Particular2-qty]").val();
            var rate = $("input[name=Particular2-rate]").val();
            $("#Particular2Total").text((qty * rate).toFixed(2));
        });

        $("input[name='Particular3-qty']").change(function () {
            var qty = $("input[name=Particular3-qty]").val();
            var rate = $("input[name=Particular3-rate]").val();
            $("#Particular3Total").text((qty * rate).toFixed(2));
        });

        $("input[name='Particular3-rate']").change(function () {
            var qty = $("input[name=Particular3-qty]").val();
            var rate = $("input[name=Particular3-rate]").val();
            $("#Particular3Total").text((qty * rate).toFixed(2));
        });

        $("input[name='TapestryBodyMTR']").change(function () {
            var qty = $("input[name=TapestryBodyMTR]").val();
            var rate = $("input[name=TapestryBodyRate]").val();
            $("#TapestrybodyTotal").text((qty * rate).toFixed(2));
        });

        $("input[name='TapestryBodyRate']").change(function () {
            var qty = $("input[name=TapestryBodyMTR]").val();
            var rate = $("input[name=TapestryBodyRate]").val();
            $("#TapestrybodyTotal").text((qty * rate).toFixed(2));
        });

        $("input[name='TapestrySeatMTR']").change(function () {
            var qty = $("input[name=TapestrySeatMTR]").val();
            var rate = $("input[name=TapestrySeatRate]").val();
            $("#TapestrySeatTotal").text((qty * rate).toFixed(2));
        });

        $("input[name='TapestrySeatRate']").change(function () {
            var qty = $("input[name=TapestrySeatMTR]").val();
            var rate = $("input[name=TapestrySeatRate]").val();
            $("#TapestrySeatTotal").text((qty * rate).toFixed(2));
        });

        $("input[name='TapestryCushionMTR']").change(function () {
            var qty = $("input[name=TapestryCushionMTR]").val();
            var rate = $("input[name=TapestryCushionRate]").val();
            $("#TapestryCushionTotal").text((qty * rate).toFixed(2));
        });
        $("input[name='TapestryCushionRate']").change(function () {
            var qty = $("input[name=TapestryCushionMTR]").val();
            var rate = $("input[name=TapestryCushionRate]").val();
            $("#TapestryCushionTotal").text((qty * rate).toFixed(2));
        });

    },


    saveOrderDetails: function () {
        var formElement = document.getElementById("orderDetail");
        var request = new XMLHttpRequest();
        request.onreadystatechange = function () {
           
                $('#message').show();
            
        };
        request.open("POST", "/order/SaveOrderDetails");
        request.send(new FormData(formElement));
    },
    init: function () {
        ORDER.event();
        var type = $("input[name='Drawing-type']:checked").val();
        if (type == 'Corner') {
            $('.corner').show();
            $('.Longer').hide();
            var value = $("input[name='lcs']:checked").val();
            if (value == '2') {
                $('.lw').hide();
            }
            else if (value == '3') {
                $('.lw').hide();
                $('.l3').show();
            }
            else if (value == '4') {
                $('.lw').hide();
                $('.l3').show();
                $('.l4').show();
            }
            else if (value == '6') {
                $('.lw').hide();
                $('.l3').show();
                $('.l4').show();
                $('.l6').show();

            }

            var value1 = $("input[name='rcs']:checked").val();
            if (value1 == '2') {
                $('.rw').hide();
            }
            else if (value1 == '3') {
                $('.rw').hide();
                $('.r3').show();
            }
            else if (value1 == '4') {
                $('.rw').hide();
                $('.r3').show();
                $('.r4').show();
            }
            else if (value1 == '6') {
                $('.rw').hide();
                $('.r3').show();
                $('.r4').show();
                $('.r6').show();

            }
        }
        else if (type == 'Longer') {
            $('.corner').hide();
            $('.Longer').show();
          }
    }
}
$(document).ready(function () {
    ORDER.init();
});