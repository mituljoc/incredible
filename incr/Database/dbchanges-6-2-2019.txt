﻿alter table incr_order_master add DrawingType varchar(150)

alter table incr_order_master add LeftNumberOfsits bigint
alter table incr_order_master add RightNumberOfsits bigint

alter table incr_order_master add DrawingRightSize0 decimal(18,3)
alter table incr_order_master add DrawingRightSize1 decimal(18,3)
alter table incr_order_master add DrawingRightSize2 decimal(18,3)
alter table incr_order_master add DrawingRightSize3 decimal(18,3)
alter table incr_order_master add DrawingRightSize4 decimal(18,3)
alter table incr_order_master add DrawingRightSize5 decimal(18,3)
alter table incr_order_master add DrawingRightSize6 decimal(18,3)

alter table incr_order_master add DrawingleftSize1 decimal(18,3)
alter table incr_order_master add DrawingleftSize2 decimal(18,3)
alter table incr_order_master add DrawingleftSize3 decimal(18,3)
alter table incr_order_master add DrawingleftSize4 decimal(18,3)
alter table incr_order_master add DrawingleftSize5 decimal(18,3)
alter table incr_order_master add DrawingleftSize6 decimal(18,3)

------------------------------------------------------------------------------------------------------------------
ALTER PROCEDURE [dbo].[incr_inserOrderDetails]
	     --  @id bigint,
           @job_no varchar(50),
           @customer_name varchar(250),
           @mobile varchar(15),
           @address nvarchar(500),
           --@order_date datetime,
           @sales_by varchar(150),
           @job_by varchar(150),
           @ply_type varchar(50),
           @spring varchar(50),
           @foaming varchar(50),
           @linning_type varchar(150),
           @cushion_back varchar(150),
           @stitch_for_sofa varchar(150),
           @cushion_stitch varchar(150),
           @fitting varchar(150),
           @packing varchar(150),
           @delivery varchar(150),
           @delivery_date datetime,
           @TapestryBody varchar(250),
           @TapestrySeat varchar(250),
           @TapestryCushion varchar(250),
           @TapestryBodyMTR decimal(18,3),
           @TapestrySeatMTR decimal(18,3),
           @TapestryCushionMTR decimal(18,3),
           @TapestryBodyRate decimal(18,3),
           @TapestrySeatRate decimal(18,3),
           @TapestryCushionRate decimal(18,3),
           @Particular1 varchar(250),
           @Particular2 varchar(250),
           @Particular3 varchar(250),
           @Particular1Qty bigint,
           @Particular2Qty bigint,
           @Particular3Qty bigint,
           @Particular1Rate decimal(18,3),
           @Particular2Rate decimal(18,3),
           @Particular3Rate decimal(18,3),
           @CarpantaryTopWidth decimal(18,3),
			@CarpantaryMiddleWidth decimal(18,3),
			@CarpantaryBottomWidth decimal(18,3),
			@CarpantaryLeftHight decimal(18,3),
			@CarpantaryRightHight decimal(18,3),
			@HandleLength decimal(18,3),
			@HandleHeight decimal(18,3),
			@HandleWidth decimal(18,3),
			@DrawingType varchar(150),
@LeftNumberOfsits bigint,
@RightNumberOfsits bigint,
@DrawingRightSize0 decimal(18,3),
@DrawingRightSize1 decimal(18,3),
@DrawingRightSize2 decimal(18,3),
@DrawingRightSize3 decimal(18,3),
@DrawingRightSize4 decimal(18,3),
@DrawingRightSize5 decimal(18,3),
@DrawingRightSize6 decimal(18,3),
@DrawingleftSize1 decimal(18,3),
@DrawingleftSize2 decimal(18,3),
@DrawingleftSize3 decimal(18,3),
@DrawingleftSize4 decimal(18,3),
@DrawingleftSize5 decimal(18,3),
@DrawingleftSize6 decimal(18,3)

	
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
declare   @id bigint=0
    -- Insert statements for procedure here
if(@id=0)
begin
INSERT INTO [raj-new].[dbo].[incr_order_master]
           ([job_no]
           ,[customer_name]
           ,[mobile]
           ,[address]
           ,[order_date]
           ,[sales_by]
           ,[job_by]
           ,[ply_type]
           ,[spring]
           ,[foaming]
           ,[linning_type]
           ,[cushion_back]
           ,[stitch_for_sofa]
           ,[cushion_stitch]
           ,[fitting]
           ,[packing]
           ,[delivery]
           ,[delivery_date]
           ,[TapestryBody]
           ,[TapestrySeat]
           ,[TapestryCushion]
           ,[TapestryBodyMTR]
           ,[TapestrySeatMTR]
           ,[TapestryCushionMTR]
           ,[TapestryBodyRate]
           ,[TapestrySeatRate]
           ,[TapestryCushionRate]
           ,[Particular1]
           ,[Particular2]
           ,[Particular3]
           ,[Particular1Qty]
           ,[Particular2Qty]
           ,[Particular3Qty]
           ,[Particular1Rate]
           ,[Particular2Rate]
           ,[Particular3Rate]
        ,[CarpantaryTopWidth]
      ,[CarpantaryMiddleWidth]
      ,[CarpantaryBottomWidth]
      ,[CarpantaryLeftHight]
      ,[CarpantaryRightHight]
      ,[HandleLength]
      ,[HandleHeight]
      ,[HandleWidth],
      DrawingType ,
LeftNumberOfsits ,
RightNumberOfsits ,
DrawingRightSize0 ,
DrawingRightSize1 ,
DrawingRightSize2 ,
DrawingRightSize3 ,
DrawingRightSize4 ,
DrawingRightSize5 ,
DrawingRightSize6 ,
DrawingleftSize1 ,
DrawingleftSize2 ,
DrawingleftSize3 ,
DrawingleftSize4 ,
DrawingleftSize5 ,
DrawingleftSize6
      )
     VALUES
         (@job_no,
          @customer_name,
          @mobile,
          @address,
          GETDATE(),
          @sales_by,
          @job_by,
          @ply_type,
          @spring,
          @foaming,
          @linning_type,
          @cushion_back,
          @stitch_for_sofa,
          @cushion_stitch,
          @fitting,
          @packing,
          @delivery,
          @delivery_date,
          @TapestryBody,
          @TapestrySeat,
          @TapestryCushion,
          @TapestryBodyMTR,
          @TapestrySeatMTR,
          @TapestryCushionMTR,
          @TapestryBodyRate,
          @TapestrySeatRate,
          @TapestryCushionRate,
          @Particular1,
          @Particular2,
          @Particular3,
          @Particular1Qty,
          @Particular2Qty,
          @Particular3Qty,
          @Particular1Rate,
          @Particular2Rate,
          @Particular3Rate,
            @CarpantaryTopWidth
      ,@CarpantaryMiddleWidth
      ,@CarpantaryBottomWidth
      ,@CarpantaryLeftHight
      ,@CarpantaryRightHight
      ,@HandleLength
      ,@HandleHeight
      ,@HandleWidth,
      @DrawingType ,
@LeftNumberOfsits ,
@RightNumberOfsits ,
@DrawingRightSize0 ,
@DrawingRightSize1 ,
@DrawingRightSize2 ,
@DrawingRightSize3 ,
@DrawingRightSize4 ,
@DrawingRightSize5 ,
@DrawingRightSize6 ,
@DrawingleftSize1 ,
@DrawingleftSize2 ,
@DrawingleftSize3 ,
@DrawingleftSize4 ,
@DrawingleftSize5 ,
@DrawingleftSize6 
          )
end
else
begin
update [incr_order_master]set [customer_name]=@customer_name where id=@id
end

  
END

-----------------------------------------------------------------------------------------
ALTER PROCEDURE [dbo].[incr_orders_search]
	@startRec int,
	@PageSize int,
	@SortBy varchar(100),
	@SortDir varchar(4),
	@WhereClause nvarchar(max)  
AS
BEGIN

	SET NOCOUNT ON;

	Declare @Firstrecord int
	Declare @LastRecord int
	set @LastRecord =( @startRec + @PageSize )
	set @Firstrecord= @startRec+1
	--print @Firstrecord
	declare @sql varchar(max)        
	

	 set @sql =  ' select * From 
				( 
				 select ROW_NUMBER() OVER(order by id desc) as RowNumber ,* from 
					(
					select 
					*
	
					From 
						incr_order_master
	 where '+ @WhereClause +'  
	  ) as t  
	  ) as a where  RowNumber BETWEEN '+CONVERT (varchar, @Firstrecord)+' AND '
	  +CONVERT(varchar, @LastRecord) + ' order by '+ @SortBy + ' ' + @SortDir

	
print (@sql)
exec(@sql)	

exec('select count( id) as totalRecord

					From 
						incr_order_master where ' + @WhereClause  )

END


  


   
    

 
     


   

