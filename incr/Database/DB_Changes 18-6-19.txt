

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Incr_OperatorMaster]') AND type in (N'U'))
DROP TABLE [dbo].[Incr_OperatorMaster]
GO

CREATE TABLE [dbo].[Incr_OperatorMaster](
	[RID] [int] IDENTITY(1,1) NOT NULL,
	[OperatorName] [nvarchar](100) NULL,
	[Carpentery] [bit] NULL,
	[Linning] [bit] NULL,
	[Packing] [bit] NULL,
	[Delivery] [bit] NULL,
	[Invoice] [bit] NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[UpdatedDate] [datetime] NULL,
	[IsDeleted] [bit] NULL,
 CONSTRAINT [PK_Incr_OperatorMaster] PRIMARY KEY CLUSTERED 
(
	[RID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO