--------------------------------------------- Insert new Deaprtment ---------------------
INSERT INTO [dbo].[Incr_Department]
           ([departmentName]
           ,[createdBy]
           ,[createdDate]
           ,[updatedBy]
           ,[updatedDate]
           ,[IsDeleted])
     VALUES
           ('Fitting'
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,0)
GO

--------------------- Add new column in OperatorMaster -------------------------
ALTER TABLE [dbo].[Incr_OperatorMaster] ADD  Fitting bit NULL
Go

-------------------- Update Fitting = false for already exist data ----------------
update Incr_OperatorMaster set Fitting = 0 where Fitting is null
GO
