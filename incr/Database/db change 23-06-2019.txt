ALTER TABLE incr_order_master
DROP COLUMN TapestryBodyRate;
ALTER TABLE incr_order_master
DROP COLUMN TapestrySeatRate;
ALTER TABLE incr_order_master
DROP COLUMN TapestryCushionRate;

ALTER TABLE incr_order_master
add  TapestryBodyFabricType varchar(150);

ALTER TABLE incr_order_master
add  TapestryCushionFabricType varchar(150);

ALTER TABLE incr_order_master
add  TapestrySeatFabricType varchar(150);
alter table [incr_order_master] add linningqty varchar(20)

------------------------------------------------------------------------------------
ALTER PROCEDURE [dbo].[incr_inserOrderDetails]
	     @id bigint,
           @job_no varchar(50),
           @customer_name varchar(250),
           @mobile varchar(15),
           @address nvarchar(500),
           --@order_date datetime,
           @sales_by varchar(150),
           @job_by varchar(150),
           @ply_type varchar(50),
           @spring varchar(50),
           @foaming varchar(50),
           @linning_type varchar(150),
           @cushion_back varchar(150),
           @stitch_for_sofa varchar(150),
           @cushion_stitch varchar(150),
           @fitting varchar(150),
           @packing varchar(150),
           @delivery varchar(150),
           @delivery_date datetime,
           @TapestryBody varchar(250),
           @TapestrySeat varchar(250),
           @TapestryCushion varchar(250),
           @TapestryBodyMTR decimal(18,3),
           @TapestrySeatMTR decimal(18,3),
           @TapestryCushionMTR decimal(18,3),
           @TapestryBodyFabricType varchar(150),
           @TapestrySeatFabricType varchar(150),
           @TapestryCushionFabricType varchar(150),
           @Particular1 varchar(250),
           @Particular2 varchar(250),
           @Particular3 varchar(250),
           @Particular1Qty bigint,
           @Particular2Qty bigint,
           @Particular3Qty bigint,
           @Particular1Rate decimal(18,3),
           @Particular2Rate decimal(18,3),
           @Particular3Rate decimal(18,3),
           @CarpantaryTopWidth decimal(18,3),
			@CarpantaryMiddleWidth decimal(18,3),
			@CarpantaryBottomWidth decimal(18,3),
			@CarpantaryLeftHight decimal(18,3),
			@CarpantaryRightHight decimal(18,3),
			@HandleLength decimal(18,3),
			@HandleHeight decimal(18,3),
			@HandleWidth decimal(18,3),
			@DrawingType varchar(150),
@LeftNumberOfsits bigint,
@RightNumberOfsits bigint,
@DrawingRightSize0 decimal(18,3),
@DrawingRightSize1 decimal(18,3),
@DrawingRightSize2 decimal(18,3),
@DrawingRightSize3 decimal(18,3),
@DrawingRightSize4 decimal(18,3),
@DrawingRightSize5 decimal(18,3),
@DrawingRightSize6 decimal(18,3),
@DrawingleftSize1 decimal(18,3),
@DrawingleftSize2 decimal(18,3),
@DrawingleftSize3 decimal(18,3),
@DrawingleftSize4 decimal(18,3),
@DrawingleftSize5 decimal(18,3),
@DrawingleftSize6 decimal(18,3),
@Drawingfullheight decimal(18,3),
@Drawingfullwidth decimal(18,3),
@LongerSide varchar(150),
@linningqty varchar(150)

	
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
if(@id=0)
begin
INSERT INTO [dbo].[incr_order_master]
           ([job_no]
           ,[customer_name]
           ,[mobile]
           ,[address]
           ,[order_date]
           ,[sales_by]
           ,[job_by]
           ,[ply_type]
           ,[spring]
           ,[foaming]
           ,[linning_type]
           ,[cushion_back]
           ,[stitch_for_sofa]
           ,[cushion_stitch]
           ,[fitting]
           ,[packing]
           ,[delivery]
           ,[delivery_date]
           ,[TapestryBody]
           ,[TapestrySeat]
           ,[TapestryCushion]
           ,[TapestryBodyMTR]
           ,[TapestrySeatMTR]
           ,[TapestryCushionMTR]
           ,[TapestryBodyFabricType]
           ,[TapestrySeatFabricType]
           ,[TapestryCushionFabricType]
           ,[Particular1]
           ,[Particular2]
           ,[Particular3]
           ,[Particular1Qty]
           ,[Particular2Qty]
           ,[Particular3Qty]
           ,[Particular1Rate]
           ,[Particular2Rate]
           ,[Particular3Rate]
        ,[CarpantaryTopWidth]
      ,[CarpantaryMiddleWidth]
      ,[CarpantaryBottomWidth]
      ,[CarpantaryLeftHight]
      ,[CarpantaryRightHight]
      ,[HandleLength]
      ,[HandleHeight]
      ,[HandleWidth],
      DrawingType ,
LeftNumberOfsits ,
RightNumberOfsits ,
DrawingRightSize0 ,
DrawingRightSize1 ,
DrawingRightSize2 ,
DrawingRightSize3 ,
DrawingRightSize4 ,
DrawingRightSize5 ,
DrawingRightSize6 ,
DrawingleftSize1 ,
DrawingleftSize2 ,
DrawingleftSize3 ,
DrawingleftSize4 ,
DrawingleftSize5 ,
DrawingleftSize6,
Drawingfullheight,
Drawingfullwidth,
LongerSide,
linningqty
      )
     VALUES
         (@job_no,
          @customer_name,
          @mobile,
          @address,
          GETDATE(),
          @sales_by,
          @job_by,
          @ply_type,
          @spring,
          @foaming,
          @linning_type,
          @cushion_back,
          @stitch_for_sofa,
          @cushion_stitch,
          @fitting,
          @packing,
          @delivery,
          @delivery_date,
          @TapestryBody,
          @TapestrySeat,
          @TapestryCushion,
          @TapestryBodyMTR,
          @TapestrySeatMTR,
          @TapestryCushionMTR,
          @TapestryBodyFabricType,
          @TapestrySeatFabricType,
          @TapestryCushionFabricType,
          @Particular1,
          @Particular2,
          @Particular3,
          @Particular1Qty,
          @Particular2Qty,
          @Particular3Qty,
          @Particular1Rate,
          @Particular2Rate,
          @Particular3Rate,
            @CarpantaryTopWidth
      ,@CarpantaryMiddleWidth
      ,@CarpantaryBottomWidth
      ,@CarpantaryLeftHight
      ,@CarpantaryRightHight
      ,@HandleLength
      ,@HandleHeight
      ,@HandleWidth,
      @DrawingType ,
@LeftNumberOfsits ,
@RightNumberOfsits ,
@DrawingRightSize0 ,
@DrawingRightSize1 ,
@DrawingRightSize2 ,
@DrawingRightSize3 ,
@DrawingRightSize4 ,
@DrawingRightSize5 ,
@DrawingRightSize6 ,
@DrawingleftSize1 ,
@DrawingleftSize2 ,
@DrawingleftSize3 ,
@DrawingleftSize4 ,
@DrawingleftSize5 ,
@DrawingleftSize6 ,
@Drawingfullheight,
@Drawingfullwidth,
@LongerSide,
@linningqty
          )
end
else
begin
update [incr_order_master]set 
        customer_name=  @customer_name,
         mobile= @mobile,
         address =@address,
        
       sales_by=   @sales_by,
       job_by=   @job_by,
         ply_type= @ply_type,
         spring= @spring,
        foaming=  @foaming,
         linning_type= @linning_type,
          cushion_back=@cushion_back,
         stitch_for_sofa= @stitch_for_sofa,
         cushion_stitch= @cushion_stitch,
         fitting= @fitting,
         packing= @packing,
        delivery=  @delivery,
         delivery_date= @delivery_date,
         TapestryBody= @TapestryBody,
         TapestrySeat= @TapestrySeat,
         TapestryCushion= @TapestryCushion,
         TapestryBodyMTR= @TapestryBodyMTR,
         TapestrySeatMTR= @TapestrySeatMTR,
         TapestryCushionMTR= @TapestryCushionMTR,
          TapestryBodyFabricType=@TapestryBodyFabricType,
         TapestrySeatFabricType= @TapestrySeatFabricType,
         TapestryCushionFabricType= @TapestryCushionFabricType,
         Particular1= @Particular1,
         Particular2= @Particular2,
        Particular3=  @Particular3,
       Particular1Qty=   @Particular1Qty,
        Particular2Qty=  @Particular2Qty,
        Particular3Qty=  @Particular3Qty,
         Particular1Rate= @Particular1Rate,
          Particular2Rate=@Particular2Rate,
         Particular3Rate= @Particular3Rate,
           CarpantaryTopWidth= @CarpantaryTopWidth
      ,CarpantaryMiddleWidth=@CarpantaryMiddleWidth
      ,CarpantaryBottomWidth=@CarpantaryBottomWidth
      ,CarpantaryLeftHight=@CarpantaryLeftHight
      ,CarpantaryRightHight=@CarpantaryRightHight
      ,HandleLength=@HandleLength
      ,HandleHeight=@HandleHeight
      ,HandleWidth=@HandleWidth,
      DrawingType=@DrawingType ,
LeftNumberOfsits=@LeftNumberOfsits ,
RightNumberOfsits=@RightNumberOfsits ,
DrawingRightSize0=@DrawingRightSize0 ,
DrawingRightSize1=@DrawingRightSize1 ,
DrawingRightSize2=@DrawingRightSize2 ,
DrawingRightSize3=@DrawingRightSize3 ,
DrawingRightSize4=@DrawingRightSize4 ,
DrawingRightSize5=@DrawingRightSize5 ,
DrawingRightSize6=@DrawingRightSize6 ,
DrawingleftSize1=@DrawingleftSize1 ,
DrawingleftSize2=@DrawingleftSize2 ,
DrawingleftSize3=@DrawingleftSize3 ,
DrawingleftSize4=@DrawingleftSize4 ,
DrawingleftSize5=@DrawingleftSize5 ,
DrawingleftSize6=@DrawingleftSize6 ,
Drawingfullheight=@Drawingfullheight,
Drawingfullwidth=@Drawingfullwidth,
LongerSide=@LongerSide,
linningqty=@linningqty

where id=@id
end

  
END


    
  





   
    

 
     


   

