
GO
/****** Object:  StoredProcedure [dbo].[incr_finishJob]    Script Date: 11/28/2019 09:14:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[incr_finishJob]
@id bigint,
@operatorId bigint,
@supervised_by bigint,
@remarks nvarchar(500)
as
begin

     declare @flow_id bigint 
	select @flow_id=current_flow_code_id from incr_order_master where id=@id
	
	update [Incr_FlowStep] set out_time=getdate(),remarks=@remarks ,[operator_id]=@operatorId,[supervised_by]=@supervised_by
	where flow_code_id=@flow_id and order_id=@id
	
	declare @curentFlowStepId bigint
	
	SELECT TOP 1000 @curentFlowStepId= [id]
  FROM [Incr_FlowStep] where [flow_code_id] = @flow_id and order_id=@id

	declare @nextStepCode bigint 
		SELECT TOP 1 @nextStepCode= [flow_code_id]
  FROM [Incr_FlowStep] where id>  @curentFlowStepId and order_id=@id order by id asc
if (isnull(@nextStepCode,0)<>0)
begin
	update incr_order_master set current_flow_code_id =@nextStepCode where id =@id
	end
	
 end	
