

/******************* incr_getOrderForInvoice minor changes please update SP ************************/

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[incr_getOrderForInvoice]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
DROP PROCEDURE incr_getOrderForInvoice
END
Go

Create procedure [dbo].[incr_getOrderForInvoice]
@id Varchar(max)
as
begin

declare @InvoiceNumber varchar(150)
select @InvoiceNumber= invoicenumber from Incr_Invoice where invoiceid in( select invoiceid from incr_invoiceorder where orderid   in(select data  from dbo.incr_Split(@id,',')))

	if(isnull(@InvoiceNumber,'')='')
	begin
		set @InvoiceNumber='#invoiceNumber'
	end
select @InvoiceNumber as InvoiceNumber, * 
,(select fabricgroupid from incr_fabricmaster where RID=bodyfabrictypeid)as BodyFabricGroupId
,(select fabricgroupid from incr_fabricmaster where RID=SeatFabricTypeId)as SeatFabricGroupId

,(select fabricgroupid from incr_fabricmaster where RID=CushionFabricTypeId)as CushionFabricGroupId
,(select top 1 vehicleNo from Incr_FlowStep where order_id = @id and name= 'Delivery') as vehicleNo
,(select top 1 o.OperatorName from Incr_OperatorMaster o inner join Incr_FlowStep fs on o.RID = fs.operator_id where fs.order_id = @id and fs.name= 'Delivery' ) as driverName

 from [incr_order_master] where id in(select data  from dbo.incr_Split(@id,','))
end
Go