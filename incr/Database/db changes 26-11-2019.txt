ALTER PROCEDURE [dbo].[incr_startJob]
@id bigint,
@operatorId bigint,
@supervised_by bigint,
@remarks nvarchar(500)
as
begin
    declare @flow_id bigint 
	select @flow_id=current_flow_code_id from incr_order_master where id=@id
	update [Incr_FlowStep] set in_time=getdate() ,[operator_id]=@operatorId,[supervised_by]=@supervised_by,remarks=@remarks
	where flow_code_id=@flow_id and order_id=@id
 end	       
	   