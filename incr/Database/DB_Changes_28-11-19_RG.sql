

IF COL_LENGTH('Incr_FlowStep', 'vehicleNo') IS NULL
BEGIN
    ALTER TABLE [dbo].[Incr_FlowStep]
    ADD vehicleNo varchar(50) null
END

/*****************************Update incr_startJob SP*******************************/

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[incr_startJob]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
DROP PROCEDURE incr_startJob
END
Go
Create PROCEDURE [dbo].[incr_startJob]
@id bigint,
@operatorId bigint,
@supervised_by bigint,
@remarks nvarchar(500),
@vehicleNo nvarchar(50)
as
begin
    declare @flow_id bigint 
	select @flow_id=current_flow_code_id from incr_order_master where id=@id
	update [Incr_FlowStep] set in_time=getdate() ,[operator_id]=@operatorId,[supervised_by]=@supervised_by,remarks=@remarks,vehicleNo = @vehicleNo

	where flow_code_id=@flow_id and order_id=@id
 end	       
	   
Go

/************************* Update SP incr_finishJob******************************/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[incr_finishJob]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
DROP PROCEDURE incr_finishJob
END
Go
Create PROCEDURE [dbo].[incr_finishJob]
@id bigint,
@operatorId bigint,
@supervised_by bigint,
@remarks nvarchar(500),
@vehicleNo nvarchar(50)
as
begin

     declare @flow_id bigint 
	select @flow_id=current_flow_code_id from incr_order_master where id=@id
	
	update [Incr_FlowStep] set out_time=getdate(),remarks=@remarks ,[operator_id]=@operatorId,[supervised_by]=@supervised_by,vehicleNo = @vehicleNo
	where flow_code_id=@flow_id and order_id=@id
	
	declare @curentFlowStepId bigint
	
	SELECT TOP 1000 @curentFlowStepId= [id]
  FROM [Incr_FlowStep] where [flow_code_id] = @flow_id and order_id=@id

	declare @nextStepCode bigint 
		SELECT TOP 1 @nextStepCode= [flow_code_id]
  FROM [Incr_FlowStep] where id>  @curentFlowStepId and order_id=@id order by id asc
if (isnull(@nextStepCode,0)<>0)
begin
	update incr_order_master set current_flow_code_id =@nextStepCode where id =@id
	end
	
 end	

GO

/*****************************Update incr_getOrderForInvoice SP*******************************/

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[incr_getOrderForInvoice]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
DROP PROCEDURE incr_getOrderForInvoice
END
Go

CREATE procedure [dbo].[incr_getOrderForInvoice]
@id Varchar(max)
as
begin

declare @InvoiceNumber varchar(150)
select @InvoiceNumber= invoicenumber from Incr_Invoice where invoiceid in( select invoiceid from incr_invoiceorder where orderid   in(select data  from dbo.incr_Split(@id,',')))

	if(isnull(@InvoiceNumber,'')='')
	begin
		set @InvoiceNumber='#invoiceNumber'
	end
select @InvoiceNumber as InvoiceNumber, * 
,(select fabricgroupid from incr_fabricmaster where RID=bodyfabrictypeid)as BodyFabricGroupId
,(select fabricgroupid from incr_fabricmaster where RID=SeatFabricTypeId)as SeatFabricGroupId

,(select fabricgroupid from incr_fabricmaster where RID=CushionFabricTypeId)as CushionFabricGroupId
,(select top 1 vehicleNo from Incr_FlowStep where order_id = @id) as vehicleNo
,(select top 1 o.OperatorName from Incr_OperatorMaster o inner join Incr_FlowStep fs on o.RID = fs.operator_id where fs.order_id = @id ) as driverName

 from [incr_order_master] where id in(select data  from dbo.incr_Split(@id,','))
end
GO


