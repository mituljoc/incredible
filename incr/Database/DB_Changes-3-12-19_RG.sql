
/*************** Add Polish department into DepartMent Table: Just run query****************/

declare @totalCount int
set @totalCount = (select count(RID) from [dbo].[Incr_Department] where departmentName like '%Polish%')
while @totalCount = 0
BEGIN
	INSERT INTO [dbo].[Incr_Department]
		 ([departmentName] ,[createdBy] ,[createdDate] ,[updatedBy] ,[updatedDate] ,[IsDeleted])
     VALUES
           ('Polish',NULL,NULL,NULL,NULL,0)
END