ALTER procedure [dbo].[incr_getOrderForInvoice]
@id Varchar(max)
as
begin

declare @InvoiceNumber varchar(150)
select @InvoiceNumber= invoicenumber from Incr_Invoice where invoiceid in( select invoiceid from incr_invoiceorder where orderid   in(select data  from dbo.incr_Split(@id,',')))

	if(isnull(@InvoiceNumber,'')='')
	begin
		set @InvoiceNumber='#invoiceNumber'
	end
select @InvoiceNumber as InvoiceNumber, [incr_order_master].* 
,(select fabricgroupid from incr_fabricmaster where RID=bodyfabrictypeid)as BodyFabricGroupId
,(select fabricgroupid from incr_fabricmaster where RID=SeatFabricTypeId)as SeatFabricGroupId

,(select fabricgroupid from incr_fabricmaster where RID=CushionFabricTypeId)as CushionFabricGroupId
,(select top 1 vehicleNo from Incr_FlowStep where order_id = [incr_order_master].id and name= 'Delivery') as vehicleNo
,(select top 1 o.OperatorName from Incr_OperatorMaster o inner join Incr_FlowStep fs on o.RID = fs.operator_id where fs.order_id = [incr_order_master].id  and fs.name= 'Delivery' ) as driverName
,incr_price_list.Name ,isnull(incr_price_list.price_per,'') as Price_per

 from [incr_order_master]
 left  join incr_price_list on incr_price_list.id=[incr_order_master].ParticularNameId 
  where [incr_order_master].id in(select data  from dbo.incr_Split(@id,','))
end
