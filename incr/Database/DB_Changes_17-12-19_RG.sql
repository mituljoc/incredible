CREATE TABLE [dbo].[Incr_invoice_payment](
	[RID] [int] IDENTITY(1,1) NOT NULL,
	[InvoiceId] [int] NULL,
	[PaidAmount] [decimal](10, 2) NULL,
	[StatusId] [nvarchar](20) NULL,
	[PaidDate] [datetime] NULL,
	[Outstanding] [decimal](10, 2) NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_Incr_invoice_payment] PRIMARY KEY CLUSTERED 
(
	[RID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO