
--------------------------------- Must be run 'DB_Changes_17-12-19_RG' before this

ALTER TABLE Incr_invoice_payment
ALTER COLUMN InvoiceId bigint;
Go
----------------------------------------------

ALTER PROCEDURE [dbo].[incr_CloseInvoice]
@invoiceId bigint,
@statusId bigint,
@amount decimal(18,2)
as
begin

update Incr_Invoice set amauntPaid=@amount+isnull(amauntPaid,0) ,statusid =@statusId where invoiceid=@invoiceId
	
declare @totalAmount decimal(10,2)

	set @totalAmount = (select TotalPrice from Incr_Invoice where InvoiceId = @invoiceId)

	Insert into [dbo].[Incr_invoice_payment]
	([InvoiceId],
	[PaidAmount],
	[StatusId],
	[PaidDate],
	[Outstanding],
	[CreatedDate])
	values
	(@invoiceId,
	@amount,
	@statusId,
	GETDATE(),
	(@totalAmount - @amount),
	GETDATE())
end	
Go