
  insert into Incr_Codes ([Code]
      ,[Discription]
      ,[ParentId]
      ,[sequence])values('POLISH','polish',1,3)
      
      update Incr_Codes set [sequence]= 4 where code ='FITTING'
      update Incr_Codes set [sequence]= 5 where code ='PACKING'
      update Incr_Codes set [sequence]= 6 where code ='DELIVERY'
      update Incr_Codes set [sequence]= 7 where code ='INVOICE'
-----------------------------------------------------------------
alter table incr_order_master add Partyname varchar(500)
alter table incr_order_master add Polish varchar(150)
--------------------------------------------------------------

alter proc [dbo].[incr_getNextorderNumber]

as
begin
declare @job_no varchar(50)
declare @orderId bigint
select top 1 @orderId=id from [incr_order_master]  order by id desc 
 set @job_no=dbo.incr_fn_getnumber(6,(@orderId+1),'INCR');
select @job_no as job_no ,0 as customer_id
end

---------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[incr_inserOrderDetails]
	     @id bigint,
	       @Sales_By_Id bigint,
	     
	       
	       @Job_By_Id bigint,
	       @Customer_Id bigint,
           @job_no varchar(50),
           @customer_name varchar(250),
           @mobile varchar(15),
           @address nvarchar(500),

           @sales_by varchar(150),
           @job_by varchar(150),
           @ply_type varchar(50),
           @spring varchar(50),
           @foaming varchar(50),
           @linning_type varchar(150),
           @cushion_back varchar(150),
           @stitch_for_sofa varchar(150),
           @cushion_stitch varchar(150),
           @fitting varchar(150),
           @packing varchar(150),
           @delivery varchar(150),
           @delivery_date datetime,
           @TapestryBody varchar(250),
           @TapestrySeat varchar(250),
           @TapestryCushion varchar(250),
           @TapestryBodyMTR decimal(18,3),
           @TapestrySeatMTR decimal(18,3),
           @TapestryCushionMTR decimal(18,3),
           @TapestryBodyFabricType varchar(150),
           @TapestrySeatFabricType varchar(150),
           @TapestryCushionFabricType varchar(150),
           @Particular1 varchar(250),
           @Particular2 varchar(250),
           @Particular3 varchar(250),
           @Particular1Qty varchar(150),
           @Particular2Qty varchar(150),
           @Particular3Qty varchar(150),
           @Particular1Rate decimal(18,3),
           @Particular2Rate decimal(18,3),
           @Particular3Rate decimal(18,3),
           @CarpantaryTopWidth decimal(18,3),
			@CarpantaryMiddleWidth decimal(18,3),
			@CarpantaryBottomWidth decimal(18,3),
			@CarpantaryLeftHight decimal(18,3),
			@CarpantaryRightHight decimal(18,3),
			@HandleLength decimal(18,3),
			@HandleHeight decimal(18,3),
			@HandleWidth decimal(18,3),
			@DrawingType varchar(150),
@LeftNumberOfsits bigint,
@RightNumberOfsits bigint,
@DrawingRightSize0 decimal(18,3),
@DrawingRightSize1 decimal(18,3),
@DrawingRightSize2 decimal(18,3),
@DrawingRightSize3 decimal(18,3),
@DrawingRightSize4 decimal(18,3),
@DrawingRightSize5 decimal(18,3),
@DrawingRightSize6 decimal(18,3),
@DrawingleftSize1 decimal(18,3),
@DrawingleftSize2 decimal(18,3),
@DrawingleftSize3 decimal(18,3),
@DrawingleftSize4 decimal(18,3),
@DrawingleftSize5 decimal(18,3),
@DrawingleftSize6 decimal(18,3),
@Drawingfullheight decimal(18,3),
@Drawingfullwidth decimal(18,3),

@BodyFabricTypeId bigint,
@SeatFabricTypeId bigint,
@CushionFabricTypeId bigint,
@Discount decimal(18,2),
@TransPortPrice decimal(18,2),
@Extra decimal(18,2),
@SGST 	decimal(18,2),
@CGST decimal(18,2),
@LongerSide varchar(150),
@linningqty varchar(150),
@CarpantaryRightWidth decimal(18,2),
@NetOrderPrice decimal(18,2),
@TotalOrderPrice decimal(18,2),
@DrawingHandle1 decimal(18,2),
@DrawingHandle2 decimal(18,2),
@DrawingHandle3 decimal(18,2),
@DrawingHandle4 decimal(18,2),
@Particular4Rate decimal(18,2),
@Particular4Qty varchar(100),
@Particular1Total decimal(18,2),
@Particular2Total decimal(18,2),
@Particular3Total decimal(18,2),
@Particular4Total  decimal(18,2),
@ParticularNameId  bigint,
@Particular4 varchar(250)
,
@Particula4FabricTypeId bigint
,
@IsWithoutfabric bit
,
@CarpantaryRightHight1 decimal(18,2)
,
@CarpantaryRightHight2 decimal(18,2)
,
@CarpantaryRightHight3 decimal(18,2),
@Particular5 varchar(250),
@Particular5Qty varchar(250),
@Particular5Rate decimal(18,2),
@Particular5FabricTypeId bigint
,@Polish varchar(150)
,@Partyname varchar(500)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

declare @BodyFabricPrice decimal(18,2)
declare @SeatFabricPrice decimal(18,2)
declare @CushionFabricPrice decimal(18,2)
declare @netPrice  decimal(18,2)
select @TapestryBody =fabricGroup ,@TapestryBodyFabricType =facricName,@BodyFabricPrice=fabricPrice from Incr_FabricMaster where RID=@BodyFabricTypeId
 select @TapestrySeat =fabricGroup ,@TapestrySeatFabricType =facricName,@SeatFabricPrice=fabricPrice from Incr_FabricMaster where RID=@SeatFabricTypeId
 select @TapestryCushion =fabricGroup ,@TapestryCushionFabricType =facricName,@CushionFabricPrice=fabricPrice from Incr_FabricMaster where RID=@CushionFabricTypeId

 select top 1 @sales_by =Operatorname from Incr_OperatorMaster where Rid=@sales_by_id
  
  select top 1 @job_by =Operatorname from Incr_OperatorMaster where Rid=@job_by_id
  
  select top 1 @customer_name= supplierName from [Incr_Supplier]where rid=@customer_id
  

    -- Insert statements for procedure here
if(@id=0)
begin
INSERT INTO [dbo].[incr_order_master]
           ([job_no]
           ,[customer_name]
           ,[mobile]
           ,[address]
           ,[order_date]
           ,[sales_by]
           ,[job_by]
           ,[ply_type]
           ,[spring]
           ,[foaming]
           ,[linning_type]
           ,[cushion_back]
           ,[stitch_for_sofa]
           ,[cushion_stitch]
           ,[fitting]
           ,[packing]
           ,[delivery]
           ,[delivery_date]
           ,[TapestryBody]
           ,[TapestrySeat]
           ,[TapestryCushion]
           ,[TapestryBodyMTR]
           ,[TapestrySeatMTR]
           ,[TapestryCushionMTR]
           ,[TapestryBodyFabricType]
           ,[TapestrySeatFabricType]
           ,[TapestryCushionFabricType]
           ,[Particular1]
           ,[Particular2]
           ,[Particular3]
           ,[Particular1Qty]
           ,[Particular2Qty]
           ,[Particular3Qty]
           ,[Particular1Rate]
           ,[Particular2Rate]
           ,[Particular3Rate]
        ,[CarpantaryTopWidth]
      ,[CarpantaryMiddleWidth]
      ,[CarpantaryBottomWidth]
      ,[CarpantaryLeftHight]
      ,[CarpantaryRightHight]
      ,[HandleLength]
      ,[HandleHeight]
      ,[HandleWidth],
      DrawingType ,
LeftNumberOfsits ,
RightNumberOfsits ,
DrawingRightSize0 ,
DrawingRightSize1 ,
DrawingRightSize2 ,
DrawingRightSize3 ,
DrawingRightSize4 ,
DrawingRightSize5 ,
DrawingRightSize6 ,
DrawingleftSize1 ,
DrawingleftSize2 ,
DrawingleftSize3 ,
DrawingleftSize4 ,
DrawingleftSize5 ,
DrawingleftSize6,
Drawingfullheight,
Drawingfullwidth,
LongerSide,
linningqty,
customer_id,sales_by_id,job_by_id,BodyFabricTypeId,
SeatFabricTypeId ,
CushionFabricTypeId ,
Discount ,
TransPortPrice ,
Extra,
SGST,
CGST ,
CarpantaryRightWidth,
BodyFabricPrice,SeatFabricPrice,CushionFabricPrice,
NetOrderPrice,TotalOrderPrice,DrawingHandle1,DrawingHandle2,DrawingHandle3,DrawingHandle4,
 Particular4Qty, 
Particular1Total, 
Particular2Total,
Particular3Total ,
Particular4Total  ,
ParticularNameId  ,
Particular4 ,
Particula4FabricTypeId,
IsWithoutfabric ,
CarpantaryRightHight1 ,
CarpantaryRightHight2 ,
CarpantaryRightHight3,
Particular4Rate,
Particular5 ,
Particular5Qty ,
Particular5Rate ,
Particular5FabricTypeId,
Polish ,
Partyname 

      )
     VALUES
         (@job_no,
          @customer_name,
          @mobile,
          @address,
          GETDATE(),
          @sales_by,
          @job_by,
          @ply_type,
          @spring,
          @foaming,
          @linning_type,
          @cushion_back,
          @stitch_for_sofa,
          @cushion_stitch,
          @fitting,
          @packing,
          @delivery,
          @delivery_date,
          @TapestryBody,
          @TapestrySeat,
          @TapestryCushion,
          @TapestryBodyMTR,
          @TapestrySeatMTR,
          @TapestryCushionMTR,
          @TapestryBodyFabricType,
          @TapestrySeatFabricType,
          @TapestryCushionFabricType,
          @Particular1,
          @Particular2,
          @Particular3,
          @Particular1Qty,
          @Particular2Qty,
          @Particular3Qty,
          @Particular1Rate,
          @Particular2Rate,
          @Particular3Rate,
            @CarpantaryTopWidth
      ,@CarpantaryMiddleWidth
      ,@CarpantaryBottomWidth
      ,@CarpantaryLeftHight
      ,@CarpantaryRightHight
      ,@HandleLength
      ,@HandleHeight
      ,@HandleWidth,
      @DrawingType ,
@LeftNumberOfsits ,
@RightNumberOfsits ,
@DrawingRightSize0 ,
@DrawingRightSize1 ,
@DrawingRightSize2 ,
@DrawingRightSize3 ,
@DrawingRightSize4 ,
@DrawingRightSize5 ,
@DrawingRightSize6 ,
@DrawingleftSize1 ,
@DrawingleftSize2 ,
@DrawingleftSize3 ,
@DrawingleftSize4 ,
@DrawingleftSize5 ,
@DrawingleftSize6 ,
@Drawingfullheight,
@Drawingfullwidth,
@LongerSide,
@linningqty,
@Customer_Id,
@sales_by_id,
@Job_By_Id,
@BodyFabricTypeId ,
@SeatFabricTypeId ,
@CushionFabricTypeId ,
@Discount, 
@TransPortPrice ,
@Extra,
@SGST,
@CGST,
@CarpantaryRightWidth,
@BodyFabricPrice,@SeatFabricPrice,@CushionFabricPrice,@NetOrderPrice,@TotalOrderPrice
,@DrawingHandle1,@DrawingHandle2,@DrawingHandle3,@DrawingHandle4,
 @Particular4Qty ,
@Particular1Total ,
@Particular2Total,
@Particular3Total ,
@Particular4Total  ,
@ParticularNameId  ,
@Particular4 ,
@Particula4FabricTypeId,
@IsWithoutfabric ,
@CarpantaryRightHight1 ,
@CarpantaryRightHight2 ,
@CarpantaryRightHight3,
@Particular4Rate,
@Particular5 ,
@Particular5Qty ,
@Particular5Rate ,
@Particular5FabricTypeId,
@Polish ,
@Partyname 
          )
          
          declare @orderId bigint
         set @orderId = SCOPE_IDENTITY()
         
         declare @job varchar(50)
         set @job=dbo.incr_fn_getnumber(6,@orderId,'INCR');
         
          declare @flow_id bigint
      select top 1 @flow_id=id from incr_codes where code='PENDING' 
       update [incr_order_master] set [job_no] = @job,current_flow_code_id=@flow_id where id =@orderId
      
        insert into [Incr_FlowStep]( [name]
      ,[order_id]
      ,[flow_code_id],[created_on]) select Discription,@orderId,id ,getdate()from [Incr_Codes] where [ParentId] in(SELECT [Id]
  FROM [Incr_Codes] where code='ORDERFLOW'
      )order by sequence asc
          
      select @job as JobNo   ,@orderId as id
         
end
else
begin
update [incr_order_master]set 
        customer_name=  @customer_name,
         mobile= @mobile,
         address =@address,
        
       sales_by=   @sales_by,
       job_by=   @job_by,
         ply_type= @ply_type,
         spring= @spring,
        foaming=  @foaming,
         linning_type= @linning_type,
          cushion_back=@cushion_back,
         stitch_for_sofa= @stitch_for_sofa,
         cushion_stitch= @cushion_stitch,
         fitting= @fitting,
         packing= @packing,
        delivery=  @delivery,
         delivery_date= @delivery_date,
         TapestryBody= @TapestryBody,
         TapestrySeat= @TapestrySeat,
         TapestryCushion= @TapestryCushion,
         TapestryBodyMTR= @TapestryBodyMTR,
         TapestrySeatMTR= @TapestrySeatMTR,
         TapestryCushionMTR= @TapestryCushionMTR,
          TapestryBodyFabricType=@TapestryBodyFabricType,
         TapestrySeatFabricType= @TapestrySeatFabricType,
         TapestryCushionFabricType= @TapestryCushionFabricType,
         Particular1= @Particular1,
         Particular2= @Particular2,
        Particular3=  @Particular3,
       Particular1Qty=   @Particular1Qty,
        Particular2Qty=  @Particular2Qty,
        Particular3Qty=  @Particular3Qty,
         Particular1Rate= @Particular1Rate,
          Particular2Rate=@Particular2Rate,
         Particular3Rate= @Particular3Rate,
           CarpantaryTopWidth= @CarpantaryTopWidth
      ,CarpantaryMiddleWidth=@CarpantaryMiddleWidth
      ,CarpantaryBottomWidth=@CarpantaryBottomWidth
      ,CarpantaryLeftHight=@CarpantaryLeftHight
      ,CarpantaryRightHight=@CarpantaryRightHight
      ,HandleLength=@HandleLength
      ,HandleHeight=@HandleHeight
      ,HandleWidth=@HandleWidth,
      DrawingType=@DrawingType ,
LeftNumberOfsits=@LeftNumberOfsits ,
RightNumberOfsits=@RightNumberOfsits ,
DrawingRightSize0=@DrawingRightSize0 ,
DrawingRightSize1=@DrawingRightSize1 ,
DrawingRightSize2=@DrawingRightSize2 ,
DrawingRightSize3=@DrawingRightSize3 ,
DrawingRightSize4=@DrawingRightSize4 ,
DrawingRightSize5=@DrawingRightSize5 ,
DrawingRightSize6=@DrawingRightSize6 ,
DrawingleftSize1=@DrawingleftSize1 ,
DrawingleftSize2=@DrawingleftSize2 ,
DrawingleftSize3=@DrawingleftSize3 ,
DrawingleftSize4=@DrawingleftSize4 ,
DrawingleftSize5=@DrawingleftSize5 ,
DrawingleftSize6=@DrawingleftSize6 ,
Drawingfullheight=@Drawingfullheight,
Drawingfullwidth=@Drawingfullwidth,
LongerSide=@LongerSide,
linningqty=@linningqty,
customer_id=@Customer_Id,
sales_by_id=@sales_by_id,
job_by_id=@Job_By_Id,
BodyFabricTypeId=@BodyFabricTypeId ,
SeatFabricTypeId=@SeatFabricTypeId ,
CushionFabricTypeId=@CushionFabricTypeId ,
Discount=@Discount, 
TransPortPrice=@TransPortPrice ,
Extra=@Extra,
SGST=@SGST ,
CGST=@CGST ,
CarpantaryRightWidth=@CarpantaryRightWidth,
BodyFabricPrice=@BodyFabricPrice,SeatFabricPrice=@SeatFabricPrice,CushionFabricPrice=@CushionFabricPrice,
NetOrderPrice=@NetOrderPrice,TotalOrderPrice=@TotalOrderPrice
,DrawingHandle1=@DrawingHandle1,DrawingHandle2=@DrawingHandle2,DrawingHandle3=@DrawingHandle3,DrawingHandle4=@DrawingHandle4,ParticularNameId =@ParticularNameId ,
Particular4 =@Particular4,
Particula4FabricTypeId=@Particula4FabricTypeId,
IsWithoutfabric =@IsWithoutfabric,
CarpantaryRightHight1= @CarpantaryRightHight1,
CarpantaryRightHight2=@CarpantaryRightHight2 ,
CarpantaryRightHight3=@CarpantaryRightHight3,

Particular4Qty =@Particular4Qty ,
Particular1Total =@Particular1Total ,
Particular2Total =@Particular2Total ,
Particular3Total=@Particular3Total ,
Particular4Total =@Particular4Total ,
Particular4Rate=@Particular4Rate,
Particular5=@Particular5 ,
Particular5Qty=@Particular5Qty ,
Particular5Rate=@Particular5Rate ,
Particular5FabricTypeId=@Particular5FabricTypeId,
Polish=@Polish ,
Partyname=@Partyname
 where id=@id
  
 
 select  id  ,[job_no] as JobNo from [incr_order_master] where id=@id
end

  
END
----------------------------------------------------------------------------------------