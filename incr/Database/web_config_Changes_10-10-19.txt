Add Below line in web.config under <system.web>  
 
==>>   <sessionState timeout="720"></sessionState>


like this :

<system.web>
    <compilation debug="true" targetFramework="4.5" />
    <httpRuntime targetFramework="4.5" />
    <sessionState timeout="720"></sessionState>
  </system.web>