﻿using incr.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using incr.DTO;
using incr.Models;
using System.Data.Entity;

namespace incr.Repository
{
    public class ProductRepository : IProductRepository
    {
        protected IncrEntities dataContext = new IncrEntities();
        IErrorLogRepository _errorLog = new ErrorLogRepository();
        public bool addProduct(incr_price_list Product)
        {
            try
            {
                Product.is_deleted = false;
                Product.created_on = DateTime.Now;
                dataContext.Incr_price_list.Add(Product);
                dataContext.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                var errorPath = "Error in addProduct / addProduct";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), errorPath, "");
                throw ex;
            }
        }
        public long addProductFromOrderPage(incr_price_list Product ,string name)
        {
            try
            {

                var Products = (from p in dataContext.Incr_price_list.Where(s => s.Name == name)
                               select new ProductOTD
                               {
                                   Id = p.id
                               }).FirstOrDefault();
                if (Products == null)
                {
                    Product.is_deleted = false;
                    Product.created_on = DateTime.Now;
                    dataContext.Incr_price_list.Add(Product);
                    dataContext.SaveChanges();
                    return Product.id;
                }

                return Products.Id;
            }
            catch (Exception ex)
            {
                var errorPath = "Error in addProduct / addProduct";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), errorPath, "");
                throw ex;
            }
        }

        public bool deleteProduct(incr_price_list Product)
        {
            try
            {
                Product.is_deleted = true;
                dataContext.Entry(Product).State = EntityState.Modified;
                dataContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                var errorPath = "Error in Product / deleteProduct";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), errorPath, "");
                throw ex;
            }
        }

        public ProductOTD getProductById(int id)
        {
            try
            {
                var Product = (from p in dataContext.Incr_price_list.Where(s => s.id == id)
                                   select new ProductOTD
                                   {
                                       Id = p.id,
                                       Name = p.Name,
                                       Code = p.Code,
                                       Range = p.Range,
                                       Pert1 = p.Pert1,
                                       Pert1_mtr = p.Pert1_mtr,
                                       Pert2 = p.Pert2,
                                       Pert2_mtr = p.Pert2_mtr,
                                       Pert1_rate = p.Pert1_rate,
                                       Pert1_total = p.Pert1_total,
                                       Pert2_rate = p.Pert2_rate,
                                       Pert2_total = p.Pert2_total,
                                       Pert3 = p.Pert3,
                                       Pert3_mtr = p.Pert3_mtr,
                                       Pert3_rate = p.Pert3_rate,
                                       Pert3_total = p.Pert3_total,
                                       Pert4 = p.Pert4,
                                       Pert4_mtr = p.Pert4_mtr,
                                       Pert4_rate = p.Pert4_rate,
                                       Pert4_total = p.Pert4_total,
                                       created_on = p.created_on,
                                       is_deleted = p.is_deleted,
                                       Net_total = p.Net_total,
                                       type = p.Type,
                                       Pert5 = p.Pert5,
                                       Pert5_mtr=p.Pert5_mtr,
                                       Pert5_rate=p.Pert5_rate,
                                       Pert5_total=p.Pert5_total,
                                       Price_per=p.Price_per,
                                       Without_fabric=p.Without_fabric
                                   }).FirstOrDefault();
                return Product;
            }
            catch (Exception ex)
            {
                var errorPath = "Error in FabricGroupRepository / getFabricGroupByID";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), errorPath, "");
                throw ex;
            }
        }

        public List<ProductOTD> getProductList()
        {
            try
            {
                var ProductList = (from p in dataContext.Incr_price_list .Where(s => s.is_deleted == false)
                                       select new ProductOTD
                                       {
                                           Id = p.id,
                                           Name = p.Name,
                                           Code=p.Code,
                                           Range=p.Range,
                                           Pert1=p.Pert1,
                                           Pert1_mtr=p.Pert1_mtr,
                                           Pert2=p.Pert2,
                                           Pert2_mtr=p.Pert2_mtr,
                                           Pert1_rate=p.Pert1_rate,
                                           Pert1_total=p.Pert1_total,
                                           Pert2_rate=p.Pert2_rate,
                                           Pert2_total=p.Pert2_total,
                                           Pert3=p.Pert3,
                                           Pert3_mtr=p.Pert3_mtr,
                                           Pert3_rate=p.Pert3_rate,
                                           Pert3_total=p.Pert3_total,
                                           Pert4=p.Pert4,
                                           Pert4_mtr=p.Pert4_mtr,
                                           Pert4_rate=p.Pert4_rate,
                                           Pert4_total=p.Pert4_total,
                                           created_on=p.created_on,
                                           is_deleted=p.is_deleted,
                                           Net_total=p.Net_total,
                                           type=p.Type
                                       }).OrderByDescending(x => x.Id).ToList();
                return ProductList;
            }
            catch (Exception ex)
            {
                var errorPath = "Error in FabricGroupRepository / getFabricGroupList";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), errorPath, "");
                throw ex;
            }
        }

        public bool updateProduct(incr_price_list fabricObj)
        {
            try
            {
                decimal total1 = 0, total2 = 0, total3 = 0, total4 = 0, total5 = 0;
                decimal.TryParse(fabricObj.Pert1_total.ToString(), out total1);
                decimal.TryParse(fabricObj.Pert2_total.ToString(), out total2);
                decimal.TryParse(fabricObj.Pert3_total.ToString(), out total3);
                decimal.TryParse(fabricObj.Pert4_total.ToString(), out total4);
                decimal.TryParse(fabricObj.Pert5_total.ToString(), out total5);
                fabricObj.Net_total = Math.Round( (total1 + total2 + total3 + total4 + total5),2);
                dataContext.Entry(fabricObj).State = EntityState.Modified;
            dataContext.SaveChanges();
            return true;
            }
            catch (Exception ex)
            {
                var errorPath = "Error in updateProduct / updateProduct";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), errorPath, "");
                throw ex;
            }
        }
    }
}