﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using incr.Models;
using incr.Repository.Interface;
using incr.DTO;
using System.Data.Entity;

namespace incr.Repository
{
    public class OperatorRepository : IOperatorRepository
    {
        protected IncrEntities dataContext = new IncrEntities();
        IErrorLogRepository _errorLog = new ErrorLogRepository();
        public List<Incr_OperatorMaster> getOperatorList()
        {
            try
            {
                var operatorList = dataContext.Incr_OperatorMaster.Where(m=> m.IsDeleted != true ).ToList();
                return operatorList;
            }
            catch (Exception ex)
            {
                var errorPath = "Error in OperatorRepository / getOperatorList";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), errorPath, "");
                throw ex;
            }
        }

        public List<Incr_OperatorMaster> getOperatorList_supervisor()
        {
            try
            {
                var operatorList = dataContext.Incr_OperatorMaster.Where(m => m.IsDeleted != true && m.is_supervisor ==true).ToList(); /*_crudRepository.GetAll().Where(d => d.IsDeleted.Equals(false)).ToList();*/
                return operatorList;
            }
            catch (Exception ex)
            {
                var errorPath = "Error in OperatorRepository / getOperatorList_supervisor";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), errorPath, "");
                throw ex;
            }
        }
        public List<Incr_Department> getAllDepartment()
        {
            try
            {
                var departmentList = dataContext.Incr_Department.Where(m => m.IsDeleted.Equals(false)).ToList(); /*_crudRepository.GetAll().Where(d => d.IsDeleted.Equals(false)).ToList();*/
                return departmentList;
            }
            catch (Exception ex)
            {
                var errorPath = "Error in OperatorRepository / getAllDepartment";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), errorPath, "");
                throw ex;
            }
        }
        public Incr_OperatorMaster getOperatorById(int operatorId)
        {
            try
            {
                var operatorById = dataContext.Incr_OperatorMaster.Where(m => m.RID.Equals(operatorId)).FirstOrDefault(); /*_crudRepository.GetAll().Where(d => d.IsDeleted.Equals(false)).ToList();*/
                return operatorById;
            }
            catch (Exception ex)
            {
                var errorPath = "Error in OperatorRepository / getOperatorById";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), errorPath, "");
                throw ex;
            }
        }
        public bool addNewOperator(OperatorMasterDTO operatorObj)
        {
            try
            {
                Incr_OperatorMaster operatorMaster = new Incr_OperatorMaster();
                operatorMaster.OperatorName = operatorObj.OperatorName;
                operatorMaster.Invoice = operatorObj.Invoice;
                operatorMaster.Linning = operatorObj.Linning;
                operatorMaster.Packing = operatorObj.Packing;
                operatorMaster.Delivery = operatorObj.Delivery;
                operatorMaster.Carpentery = operatorObj.Carpentery;
                operatorMaster.Fitting = operatorObj.Fitting;
                operatorMaster.is_supervisor = operatorObj.is_supervisor;
                operatorMaster.IsDeleted = false;
                operatorMaster.CreatedDate = DateTime.Now;
                operatorMaster.Polish = operatorObj.Polish;
                dataContext.Incr_OperatorMaster.Add(operatorMaster);
                dataContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                var errorPath = "Error in OperatorRepository / addNewOperator";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), errorPath, "");
                throw ex;
            }
        }

        public bool updateOperator(OperatorMasterDTO operatorObj)
        {
            try
            {
                Incr_OperatorMaster operatorMaster = new Incr_OperatorMaster();
                operatorMaster.OperatorName = operatorObj.OperatorName;
                operatorMaster.Invoice = operatorObj.Invoice;
                operatorMaster.Linning = operatorObj.Linning;
                operatorMaster.Packing = operatorObj.Packing;
                operatorMaster.Delivery = operatorObj.Delivery;
                operatorMaster.Carpentery = operatorObj.Carpentery;
                operatorMaster.Fitting = operatorObj.Fitting;
                operatorMaster.IsDeleted = false;
                operatorMaster.CreatedDate = operatorObj.CreatedDate;
                operatorMaster.CreatedBy = operatorObj.CreatedBy;
                operatorMaster.UpdatedBy = operatorObj.UpdatedBy;
                operatorMaster.UpdatedDate = DateTime.Now;
                operatorMaster.RID = operatorObj.RID;
                operatorMaster.is_supervisor = operatorObj.is_supervisor;
                operatorMaster.Polish = operatorObj.Polish;
                dataContext.Entry(operatorMaster).State = EntityState.Modified;
                dataContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                var errorPath = "Error in OperatorRepository / updateOperator";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), errorPath, "");
                throw ex;
            }
        }

        public bool deleteOperator(Incr_OperatorMaster operatorObj)
        {
            try
            {
                //Incr_OperatorMaster operatorMaster = new Incr_OperatorMaster();


                operatorObj.IsDeleted = true;
                dataContext.Entry(operatorObj).State = EntityState.Modified;
                dataContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                var errorPath = "Error in OperatorRepository / deleteOperator";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), errorPath, "");
                throw ex;
            }
        }
    }
}