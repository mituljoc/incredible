﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using incr.Models;
using incr.Repository.Interface;
using incr.DTO;
using System.Data.Entity;
using System.Data;

namespace incr.Repository
{
    public class OperatorWorkRepository : IOperatorWorkRepository
    {
        protected IncrEntities dataContext = new IncrEntities();
        IErrorLogRepository _errorLog = new ErrorLogRepository();
     
        public List<OperatorWorkMasterDTO> getAllOpeartorWorkMaster()
        {
            try
            {
                var operatorWork = (from operaWork in dataContext.Incr_OperatorWorkMaster.Where(ow=> ow.IsDeleted == false)                             
                                  select new OperatorWorkMasterDTO
                                  {
                                      RID = operaWork.RID,
                                      OperatorId = operaWork.OperatorId,
                                      DepartmentId = operaWork.DepartmentId,
                                      WorkId = operaWork.WorkId,
                                      Price = operaWork.Price
                                  }).ToList();

                foreach (var item in operatorWork)
                {
                    if (item.DepartmentId != null && item.DepartmentId != "")
                    {
                        List<string> str = new List<string>();
                        Array ar = item.DepartmentId.Split(',');
                        foreach (var depart in ar)
                        {
                            int id = Convert.ToInt32(depart);
                            var workMaster = (from dept in dataContext.Incr_Department
                                              join work in dataContext.Incr_WorkMaster on dept.RID equals id
                                              select new DepartmentDTO
                                              {
                                                  departmentName = dept.departmentName,
                                              }).FirstOrDefault();
                            if (workMaster != null)
                            {
                                str.Add(workMaster.departmentName);
                            }
                            
                        }
                        item.DepartmentList = str;
                    }           
                    if (item.OperatorId != null && item.OperatorId != "" && (Convert.ToInt32(item.OperatorId) > 0))
                    {
                        var oprID = Convert.ToInt32(item.OperatorId);
                        var operatorName = (from opr in dataContext.Incr_OperatorMaster.Where(o => o.RID == oprID)
                                            select new OperatorMasterDTO
                                            {
                                                OperatorName = opr.OperatorName
                                            }).FirstOrDefault();
                        if (operatorName != null)
                        {
                            item.OperatorId = operatorName.OperatorName;
                        }
                        
                    }
                    if (item.WorkId != null && item.WorkId != "" && (Convert.ToInt32(item.WorkId) > 0))
                    {
                        var workID = Convert.ToInt32(item.WorkId);
                        var workName = (from opr in dataContext.Incr_WorkMaster.Where(o => o.RID == workID)
                                        select new WorkMasterDTO
                                        {
                                            WorkName = opr.WorkName
                                        }).FirstOrDefault();
                        if (workName != null)
                        {
                            item.WorkId = workName.WorkName;
                        }
                        
                    }
                }
                return operatorWork;
            }
            catch (Exception ex)
            {
                var errorPath = "Error in OperatorWorkRepository / getAllOpeartorWorkMaster";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), errorPath, "");
                throw ex;
            }
        }

        public dynamic getPriceListForEdit(long opworkId)
        {

            try
            {
                var priceList = (from pl in dataContext.Incr_OperatorPriceList
                                 join p in dataContext.Incr_price_list on (pl.pricelist_id) equals p.id.ToString()
                                 where pl.work_id == opworkId
                                 select new
                                 {
                                     Code = p.Code,
                                     Name = p.Name,
                                     id = p.id,
                                     Price_per = p.Price_per,
                                     operatorPrice = pl.price_per_fit
                                 }).ToList();

                return priceList;
            }
            catch (Exception ex )
            {
                string xs = ex.ToString();
                return null;
            }
 
        }
        /// <summary>
        /// Created by Rajesh Gami
        /// </summary>
        /// <param name="workMasterObj"></param>
        /// <returns></returns>
        public long addNewOperatorWorkMaster(OperatorWorkMasterDTO workMasterObj)
        {
            try
            {
                Incr_OperatorWorkMaster workMaster = new Incr_OperatorWorkMaster();

                workMaster.OperatorId = workMasterObj.OperatorId;
                workMaster.DepartmentId = workMasterObj.DepartmentId;
                workMaster.WorkId = workMasterObj.WorkId;
                workMaster.IsDeleted = false;
                workMaster.CreatedDate = DateTime.Now;
                dataContext.Incr_OperatorWorkMaster.Add(workMaster);
                dataContext.SaveChanges();
               return workMaster.RID;
            }
            catch (Exception ex)
            {
                var errorPath = "Error in OperatorWorkRepository / addNewOpeartorWorkMaster";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), errorPath, "");
                throw ex;
            }
        }
        /// <summary>
        /// Created by Rajesh
        /// </summary>
        /// <param name="workMasterId"></param>
        /// <returns></returns>
        public OperatorWorkMasterDTO getWorkMasterById(int operatorWorkMasterId)
        {
            try
            {
                var workMasterById = (from opr in dataContext.Incr_OperatorWorkMaster.Where(m => m.RID == operatorWorkMasterId)
                                      select new OperatorWorkMasterDTO
                                      {
                                        RID = opr.RID,
                                       OperatorId = opr.OperatorId,
                                          DepartmentId = opr.DepartmentId,
                                          WorkId = opr.WorkId,
                                          Price = opr.Price,
                                          CreatedBy = opr.CreatedBy,
                                          CreatedDate = opr.CreatedDate,
                                          UpdatedBy = opr.UpdatedBy,
                                          UpdatedDate = opr.UpdatedDate,
                                          IsDeleted = opr.IsDeleted,

                                      }).FirstOrDefault();
                   
                return workMasterById;
            }
            catch (Exception ex)
            {
                var errorPath = "Error in OperatorWorkRepository / getWorkMasterById";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), errorPath, "");
                throw ex;
            }
        }


        public bool updateWorkMaster(OperatorWorkMasterDTO workMasterObj)
        {
            try
            {
                Incr_OperatorWorkMaster workMaster = new Incr_OperatorWorkMaster();
                workMaster.OperatorId = workMasterObj.OperatorId;
                workMaster.WorkId = workMasterObj.WorkId;
                workMaster.DepartmentId = workMasterObj.DepartmentId;
                workMaster.Price = workMasterObj.Price;
                workMaster.IsDeleted = false;
                workMaster.CreatedDate = workMasterObj.CreatedDate;
                workMaster.CreatedBy = workMasterObj.CreatedBy;
                workMaster.UpdatedBy = workMasterObj.UpdatedBy;
                workMaster.UpdatedDate = DateTime.Now;
                workMaster.RID = workMasterObj.RID;
                dataContext.Entry(workMaster).State = EntityState.Modified;
                dataContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                var errorPath = "Error in OperatorWorkRepository / updateWorkMaster";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), errorPath, "");
                throw ex;
            }
        }

        public bool deleteWorkMaster(Incr_OperatorWorkMaster workMasterObj)
        {
            try
            {
                workMasterObj.IsDeleted = true;
                dataContext.Entry(workMasterObj).State = EntityState.Modified;
                dataContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                var errorPath = "Error in OperatorWorkRepository / deleteWorkMaster";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), errorPath, "");
                throw ex;
            }
        }
    }
}