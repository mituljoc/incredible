﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using incr.Models;
using incr.Repository.Interface;
using incr.DTO;
using System.Data.Entity;
namespace incr.Repository
{
    public class FabricGroupRepository : IFabricGroupRepository
    {
        protected IncrEntities dataContext = new IncrEntities();
        IErrorLogRepository _errorLog = new ErrorLogRepository();
        /// <summary>
        /// Created by Rajesh 
        /// </summary>
        /// <returns></returns>
        public List<FabricDTO> getFabricGroupList()
        {
            try
            {
                var fabricGroupList = (from f in dataContext.Incr_FabricGroup.Where(s => s.IsDeleted == false)
                                      select new FabricDTO
                                      {
                                          GroupId = f.RID,
                                          FabricGroup = f.FabricGroup
                                      }).ToList();
                return fabricGroupList;
            }
            catch (Exception ex)
            {
                var errorPath = "Error in FabricGroupRepository / getFabricGroupList";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), errorPath, "");
                throw ex;
            }
        }
        /// <summary>
        /// Created by Rajesh 
        /// </summary>
        /// Get detail of fabric group by ID
        /// <returns></returns>
        public Incr_FabricGroup getFabricGroupById(int fabricGroupId)
        {
            try
            {
                var fabricGroupbyId = dataContext.Incr_FabricGroup.Where(m => m.RID.Equals(fabricGroupId)).FirstOrDefault(); /*_crudRepository.GetAll().Where(d => d.IsDeleted.Equals(false)).ToList();*/
                return fabricGroupbyId;
            }
            catch (Exception ex)
            {
                var errorPath = "Error in FabricGroupRepository / getFabricGroupByID";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), errorPath, "");
                throw ex;
            }
        }
        /// <summary>
        /// Created by Rajesh 
        /// </summary>
        /// <returns></returns>
        public bool addFabricGroup(FabricDTO fabricGroupObj)
        {
            try
            {
                Incr_FabricGroup FGModel = new Incr_FabricGroup();

                FGModel.FabricGroup = fabricGroupObj.FabricGroup;
                FGModel.IsDeleted = false;
                FGModel.CreatedBy = fabricGroupObj.CreatedBy;
                FGModel.CreatedDate = DateTime.Now;
                dataContext.Incr_FabricGroup.Add(FGModel);
                dataContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                var errorPath = "Error in FabricGroupRepository / addFabricGroup";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), errorPath, "");
                throw ex;
            }
        }
        /// <summary>
        /// Created by Rajesh 
        /// </summary>
        /// <returns></returns>

        public bool updateFabricGroup(FabricDTO fabricGroupObj)
        {
            try
            {
                Incr_FabricGroup FGModel = new Incr_FabricGroup();
                FGModel.RID = fabricGroupObj.RID;
                FGModel.FabricGroup = fabricGroupObj.FabricGroup;
                FGModel.IsDeleted = false;
                FGModel.CreatedDate = fabricGroupObj.CreatedDate;
                FGModel.UpdatedBy = fabricGroupObj.UpdatedBy;
                FGModel.UpdatedDate = DateTime.Now;
                dataContext.Entry(FGModel).State = EntityState.Modified;
                dataContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                var errorPath = "Error in FabricGroupRepository / updateFabricGroup";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), errorPath, "");
                throw ex;
            }
        }
        /// <summary>
        /// Created by Rajesh 
        /// </summary>
        /// Virtual delete
        /// <returns></returns>

        public bool deleteFabricGroup(Incr_FabricGroup fabricGroupObj)
        {
            try
            {
                fabricGroupObj.IsDeleted = true;
                dataContext.Entry(fabricGroupObj).State = EntityState.Modified;
                dataContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                var errorPath = "Error in FabricGroupRepository / deleteFabricGroup";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), errorPath, "");
                throw ex;
            }
        }
    }
}