﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using incr.Repository.Interface;
using incr.Models;
using incr.Models.DBContext;
using System.Data.Entity;
using System.Linq.Expressions;

namespace incr.Repository
{

     public partial class CRUDRepository<TEntity, TContext> : ICRUDRepository<TEntity, TContext>
        where TEntity : class
        where TContext : DBEntityContext, new()
    {
        protected TContext dataContext = new TContext();

        //public CRUDRepository()
        //{
        //    this.dbContext = new DBEntityContext<T>();
        //    TEntity = dbContext.Set<T>();
        //}

        public IQueryable<TEntity> GetAll()
        {
            return dataContext.Set<TEntity>();
        }

        public TEntity GetById(Expression<Func<TEntity, bool>> filter)
        {
            if (filter == null)
                throw new ArgumentNullException("filter");

            TEntity entity = dataContext.Set<TEntity>().FirstOrDefault(filter);
            if (entity == null)
                return default(TEntity);

            return entity;
        }

        public TEntity Create (TEntity entity)
        {
            if (entity == null)
                throw new ArgumentNullException("entity");

            dataContext.Entry(entity).State = EntityState.Added;
            dataContext.SaveChanges();
            return entity;
        }

        public TEntity Update(TEntity entity)
        {
            dataContext.Entry(entity).State = EntityState.Modified;
            dataContext.SaveChanges();
            return entity;
        }

        public void Delete(TEntity entity)
        {
            dataContext.Entry(entity).State = EntityState.Deleted;
            dataContext.SaveChanges();
        }

        public void Save()
        {
            dataContext.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose( bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dataContext.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}