﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using incr.Models;
using incr.Repository.Interface;
using incr.DTO;
using System.Data.Entity;
namespace incr.Repository
{
    public class FabricRepository : IFabricRepository
    {
        protected IncrEntities dataContext = new IncrEntities();
        IErrorLogRepository _errorLog = new ErrorLogRepository();
        /// <summary>
        /// Created by Rajesh 
        /// </summary>
        /// <returns></returns>
        public List<FabricDTO> getFabricList()
        {
            try
            {
                var fabricList = (from f in dataContext.Incr_FabricMaster.Where(s => s.IsDeleted == false)
                                       select new FabricDTO
                                       {
                                           RID = f.RID,
                                           FabricGroup = f.FabricGroup,
                                           FabricPrice = f.FabricPrice,
                                           FacricName = f.FacricName,
                                           FabricGroupId= f.FabricGroupId
                                       }).ToList();
                return fabricList;
            }
            catch (Exception ex)
            {
                var errorPath = "Error in FabricRepository / getFabricList";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), errorPath, "");
                throw ex;
            }
        }
        /// <summary>
        /// Created by Rajesh 
        /// </summary>
        /// Get detail of fabric  by ID
        /// <returns></returns>
        public Incr_FabricMaster getFabricById(int fabricId)
        {
            try
            {
                var fabricbyId = dataContext.Incr_FabricMaster.Where(m => m.RID.Equals(fabricId)).FirstOrDefault(); /*_crudRepository.GetAll().Where(d => d.IsDeleted.Equals(false)).ToList();*/
                return fabricbyId;
            }
            catch (Exception ex)
            {
                var errorPath = "Error in FabricRepository / getFabricById";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), errorPath, "");
                throw ex;
            }
        }
        /// <summary>
        /// Created by Rajesh 
        /// </summary>
        /// <returns></returns>
        public bool addFabric(FabricDTO fabricObj)
        {
            try
            {
                Incr_FabricMaster FGModel = new Incr_FabricMaster();
                var fGroupId = Convert.ToInt32(fabricObj.FabricGroupId);
                var fabric = (from f in dataContext.Incr_FabricGroup.Where(s => s.RID == fGroupId)
                                  select new FabricDTO
                                  {
                                      RID = f.RID,
                                      FabricGroup = f.FabricGroup,
                                  }).FirstOrDefault();

                FGModel.FacricName = fabricObj.FacricName;
                FGModel.FabricGroup = fabric.FabricGroup;
                FGModel.FabricGroupId = fabricObj.FabricGroupId;
                FGModel.FabricPrice = fabricObj.FabricPrice;
                FGModel.IsDeleted = false;
                FGModel.CreatedBy = fabricObj.CreatedBy;
                FGModel.CreatedDate = DateTime.Now;
                dataContext.Incr_FabricMaster.Add(FGModel);
                dataContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                var errorPath = "Error in FabricRepository / addFabric";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), errorPath, "");
                throw ex;
            }
        }
        /// <summary>
        /// Created by Rajesh 
        /// </summary>
        /// <returns></returns>
        public bool updateFabric(FabricDTO fabricObj)
        {
            try
            {
                Incr_FabricMaster FGModel = new Incr_FabricMaster();
                var fGroupId = Convert.ToInt32(fabricObj.FabricGroupId);
                var fabric = (from f in dataContext.Incr_FabricGroup.Where(s => s.RID == fGroupId)
                              select new FabricDTO
                              {
                                  RID = f.RID,
                                  FabricGroup = f.FabricGroup,
                              }).FirstOrDefault();
                FGModel.RID = fabricObj.RID;
                FGModel.FacricName = fabricObj.FacricName;
                FGModel.FabricGroup = fabric.FabricGroup;
                FGModel.FabricGroupId = fabricObj.FabricGroupId;
                FGModel.FabricPrice = fabricObj.FabricPrice;
                FGModel.IsDeleted = false;
                FGModel.CreatedDate = fabricObj.CreatedDate;
                FGModel.UpdatedBy = fabricObj.UpdatedBy;
                FGModel.UpdatedDate = DateTime.Now;
                dataContext.Entry(FGModel).State = EntityState.Modified;
                dataContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                var errorPath = "Error in FabricRepository / updateFabric";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), errorPath, "");
                throw ex;
            }
        }
        /// <summary>
        /// Created by Rajesh 
        /// </summary>
        /// Virtual delete
        /// <returns></returns>
        public bool deleteFabric(Incr_FabricMaster fabricObj)
        {
            try
            {
                fabricObj.IsDeleted = true;
                dataContext.Entry(fabricObj).State = EntityState.Modified;
                dataContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                var errorPath = "Error in FabricRepository / deleteFabric";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), errorPath, "");
                throw ex;
            }
        }
    }
}