﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using incr.Models;
using incr.Repository.Interface;
using incr.DTO;
using System.Data.Entity;

namespace incr.Repository
{
    public class DashboardRespository :IDashboardRepository
    {
        protected IncrEntities dataContext = new IncrEntities();
        IErrorLogRepository _errorLog = new ErrorLogRepository();
        /// <summary>
        /// Created by Rajesh 
        /// </summary>
        /// <returns></returns>
        public List<incr_order_master> getOrderList()
        {
            try
            {
                var orderList = dataContext.incr_order_master.ToList();
                return orderList;
            }
            catch (Exception ex)
            {
                var errorPath = "Error in DashboardRepository / getOrderList";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), errorPath, "");
                throw ex;
            }
        }

        public List<OrderDTO> getCarpentaryList()
        {
            try
            {
                var carpentaryList = (from om in dataContext.incr_order_master 
                                 join o in dataContext.Incr_OperatorMaster on om.sales_by_id equals o.RID
                                 where o.Carpentery == true
                                 select new OrderDTO {
                                  Id = om.id,
                                  CustomerName = om.customer_name
                                 }).ToList();
                return carpentaryList;
            }
            catch (Exception ex)
            {
                var errorPath = "Error in DashboardRepository / getCarpentaryList";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), errorPath, "");
                throw ex;
            }
        }
        public List<OrderDTO> getLiningList()
        {
            try
            {
                var liningList = (from om in dataContext.incr_order_master
                                      join o in dataContext.Incr_OperatorMaster on om.sales_by_id equals o.RID
                                      where o.Linning == true
                                      select new OrderDTO
                                      {
                                          Id = om.id,
                                          CustomerName
                                          = om.customer_name


                                      }).ToList();
                return liningList;
            }
            catch (Exception ex)
            {
                var errorPath = "Error in DashboardRepository / getLinningList";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), errorPath, "");
                throw ex;
            }
        }

        public List<OrderDTO> getFittingList()
        {
            try
            {
                var fittingList = (from om in dataContext.incr_order_master
                                  join o in dataContext.Incr_OperatorMaster on om.sales_by_id equals o.RID
                                  where o.Packing == true
                                  select new OrderDTO
                                  {
                                      Id = om.id,
                                      CustomerName = om.customer_name


                                  }).ToList();
                return fittingList;
            }
            catch (Exception ex)
            {
                var errorPath = "Error in DashboardRepository / getFittingList";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), errorPath, "");
                throw ex;
            }
        }

        public List<OrderDTO> getDeliveryList()
        {
            try
            {
                var deliveryList = (from om in dataContext.incr_order_master
                                   join o in dataContext.Incr_OperatorMaster on om.sales_by_id equals o.RID
                                   where o.Delivery == true
                                   select new OrderDTO
                                   {
                                       Id = om.id,
                                       CustomerName = om.customer_name


                                   }).ToList();
                return deliveryList;
            }
            catch (Exception ex)
            {
                var errorPath = "Error in DashboardRepository / getDeliveryList";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), errorPath, "");
                throw ex;
            }
        }

        public List<OrderDTO> getInvoiceList()
        {
            try
            {
                var invoiceList = (from om in dataContext.incr_order_master
                                    join o in dataContext.Incr_OperatorMaster on om.sales_by_id equals o.RID
                                    where o.Invoice == true
                                    select new OrderDTO
                                    {
                                        Id = om.id,
                                        CustomerName = om.customer_name


                                    }).ToList();
                return invoiceList;
            }
            catch (Exception ex)
            {
                var errorPath = "Error in DashboardRepository / getInvoiceList";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), errorPath, "");
                throw ex;
            }
        }
        public List<OrderDTO> getLeaderBoardList()
        {
            try
            {
                var leaderBoardList = (from a in dataContext.getLeaderBoard()
                                   select new OrderDTO
                                   {

                                       SalesBy = a.sales_by,
                                       TotalLeaderOrder = a.TotalOrders
                                   }).ToList();
                return leaderBoardList;
            }
            catch (Exception ex)
            {
                var errorPath = "Error in DashboardRepository / getLeaderboardList";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), errorPath, "");
                throw ex;
            }
        }
        public List<OrderDTO> getHighestCustomerOrderList()
        {
            try
            {
                var customerOrder = (from a in dataContext.getHighestCustomerOrders()
                                   select new OrderDTO
                                   {
                                       CustomerName = a.customer_name,
                                       TotalCustomerOrder = a.TotalOrders
                                   }).ToList();
                return customerOrder;
            }
            catch (Exception ex)
            {
                var errorPath = "Error in DashboardRepository / getHighestCustomerOrder";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), errorPath, "");
                throw ex;
            }
        }
        public List<OrderDTO> getComplaintByOperatorList()
        {
            try
            {
                var complaintByOperator = (from a in dataContext.getComplaintByOperator()
                                   select new OrderDTO
                                   {
                                      SalesBy = a.OperatorName,
                                      TotalComplaint = a.TotalOrders                                      

                                   }).ToList();
                return complaintByOperator;
            }
            catch (Exception ex)
            {
                var errorPath = "Error in DashboardRepository / getComplaintByOperatorList";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), errorPath, "");
                throw ex;
            }
        }
    }
}