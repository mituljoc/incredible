﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using incr.Models;
using incr.Repository.Interface;
using incr.DTO;
using System.Data.Entity;

namespace incr.Repository
{
    public class WorkMasterRepository : IWorkMasterRepository
    {
        protected IncrEntities dataContext = new IncrEntities();
        IErrorLogRepository _errorLog = new ErrorLogRepository();
        public List<WorkMasterDTO> getWorkMasterList()
        {
            try
            {
                var workMasterList = (from workTbl in dataContext.Incr_WorkMaster.Where(o => o.IsDeleted == false)
                                      select new WorkMasterDTO
                                      {
                                          RID = workTbl.RID,
                                          WorkName = workTbl.WorkName,
                                          Department = workTbl.Department,
                                      }).ToList();
                foreach (var item in workMasterList)
                {
                    if (item.Department != null && item.Department != "")
                    {
                        List<string> str = new List<string>();
                        Array ar = item.Department.Split(',');
                        foreach (var depart in ar)
                        {
                            int id = Convert.ToInt32(depart);
                            var workMaster = (from dept in dataContext.Incr_Department
                                              join work in dataContext.Incr_WorkMaster on dept.RID equals id
                                              select new DepartmentDTO
                                              {
                                                  departmentName = dept.departmentName,
                                              }).FirstOrDefault();
                            str.Add(workMaster.departmentName);
                        }
                        item.DepartmentList = str;
                    }
                   
                }
                return workMasterList;
            }
            catch (Exception ex)
            {
                var errorPath = "Error in WorkMasterRepository / getWorkMasterList";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), errorPath, "");
                throw ex;
            }
        }
        public List<Incr_Department> getAllDepartment()
        {
            try
            {
                var departmentList = dataContext.Incr_Department.Where(m => m.IsDeleted == false).ToList(); /*_crudRepository.GetAll().Where(d => d.IsDeleted.Equals(false)).ToList();*/
                return departmentList;
            }
            catch (Exception ex)
            {
                var errorPath = "Error in WorkMasterRepository / GetAllDepartment";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), errorPath, "");
                throw ex;
            }
        }
        public Incr_WorkMaster getWorkMasterById(int workMasterId)
        {
            try
            {
                var workMasterById = dataContext.Incr_WorkMaster.Where(m => m.RID.Equals(workMasterId)).FirstOrDefault(); /*_crudRepository.GetAll().Where(d => d.IsDeleted.Equals(false)).ToList();*/
                return workMasterById;
            }
            catch (Exception ex)
            {
                var errorPath = "Error in WorkMasterRepository / getWorkMasterById";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), errorPath, "");
                throw ex;
            }
        }

        public List<incr_price_list> GetAllPrice_list()
        {
            try
            {
                var AllpriceList = dataContext.Incr_price_list.Where(m => m.is_deleted != true).ToList();
                /*_crudRepository.GetAll().Where(d => d.IsDeleted.Equals(false)).ToList();*/
                return AllpriceList;
            }
            catch (Exception ex)
            {
                var errorPath = "Error in WorkMasterRepository / GetAllDepartment";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), errorPath, "");
                throw ex;
            }
        }

        //public List<incr_price_list> GetAllPrice_list( lon)
        //{
        //    try
        //    {
        //        var AllpriceList = dataContext.Incr_price_list.Where(m => m.is_deleted != true).ToList();

        //        var innerJoin = from price in dataContext.Incr_price_list // outer sequence
        //                        join op in dataContext.Incr_OperatorPriceList //inner sequence 
        //                        on s.StandardID equals st.StandardID // key selector 
        //                        select new
        //                        { // result selector 
        //                            StudentName = s.StudentName,
        //                            StandardName = st.StandardName
        //                        };

        //        return AllpriceList;
        //    }
        //    catch (Exception ex)
        //    {
        //        var errorPath = "Error in WorkMasterRepository / GetAllDepartment";
        //        _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), errorPath, "");
        //        throw ex;
        //    }
        //}
        public bool addNewWorkMaster(WorkMasterDTO workMasterObj)
        {
            try
            {
                Incr_WorkMaster workMaster = new Incr_WorkMaster();

                workMaster.WorkName = workMasterObj.WorkName;
                workMaster.Department = workMasterObj.Department;
                workMaster.IsDeleted = false;
                workMaster.CreatedDate = DateTime.Now;
                dataContext.Incr_WorkMaster.Add(workMaster);
                dataContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                var errorPath = "Error in WorkMasterRepository / addNewWorkMaster";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), errorPath, "");
                throw ex;
            }
        }

        public bool updateWorkMaster(WorkMasterDTO workMasterObj)
        {
            try
            {
                Incr_WorkMaster workMaster = new Incr_WorkMaster();
                workMaster.WorkName = workMasterObj.WorkName;
                workMaster.Department = workMasterObj.Department;
                workMaster.IsDeleted = false;
                workMaster.CreatedDate = workMasterObj.CreatedDate;
                workMaster.CreatedBy = workMasterObj.CreatedBy;
                workMaster.UpdatedBy = workMasterObj.UpdatedBy;
                workMaster.UpdatedDate = DateTime.Now;
                workMaster.RID = workMasterObj.RID;
                dataContext.Entry(workMaster).State = EntityState.Modified;
                dataContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                var errorPath = "Error in WorkMasterRepository / updateWorkMaster";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), errorPath, "");
                throw ex;
            }
        }

        public bool deleteWorkMaster(Incr_WorkMaster workMasterObj)
        {
            try
            {
                workMasterObj.IsDeleted = true;
                dataContext.Entry(workMasterObj).State = EntityState.Modified;
                dataContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                var errorPath = "Error in WorkMasterRepository / deleteWorkMaster";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), errorPath, "");
                throw ex;
            }
        }
    }
}