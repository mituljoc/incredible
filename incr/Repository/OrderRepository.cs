﻿using incr.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using incr.Models;
using System.Data.Entity;

namespace incr.Repository
{
    public class OrderRepository : IOrderRepository
    {
        protected IncrEntities dataContext = new IncrEntities();
        IErrorLogRepository _errorLog = new ErrorLogRepository();
        private Utilities.DataAccess _dataAccess;

        public OrderRepository()
        {
            _dataAccess = new Utilities.DataAccess();
        }

        public object GetInvoiceBYStatus(string type)
        {
            try
            {
                var orderList = (from order in dataContext.incr_order_master
                                 join co in dataContext.Incr_Codes on order.current_flow_code_id equals co.Id
                      
                                 join ino in dataContext.Incr_InvoiceOrder on order.id equals ino.OrderId
                                 join In in dataContext.Incr_Invoice on ino.InvoiceId equals In.InvoiceId
                                 join coIn in dataContext.Incr_Codes on In.StatusId equals coIn.Id
                                 where (co.Code.ToUpper() == type.ToUpper() && coIn.Code.ToUpper() != "CLOSED")
                                 select new
                                 {
                                     Id = order.id,
                                     InvoiceId = In.InvoiceId,
                                     InvoiceStatusId=In.StatusId.ToString(),
                                     coIn.Discription,
                                     In.InvoiceNumber,
                                     JobNo = order.job_no,
                                     CustomerName = order.customer_name,
                                     Mobile = order.mobile,
                                     Address = order.address,
                                     CustomerId = order.customer_id,
                                     OrderDateStr = (order.order_date).ToString(),
                                     DeliveryDateStr = order.delivery_date.ToString(),
                                     InvoiceDate = In.CreatedOn.ToString(),
                                     InvoiceAmount = In.TotalPrice,
                                     InvoiceOrderAmount = ino.TotalOrderPrice
                                 }).OrderByDescending(x => x.Id).ToList();
                return orderList;


            }
            catch (Exception ex)
            {
                var errorPath = "Error in OrderRepository / getAllOrders";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), errorPath, "");
                throw ex;
            }
        }
        /// <summary>
        /// Created by Rajesh : Get all closed Invoiced order
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public object GetClosedInvoiceBYStatus(string type)
        {
            try
            {
                var orderList = (from order in dataContext.incr_order_master
                                 join co in dataContext.Incr_Codes on order.current_flow_code_id equals co.Id
                                 join ino in dataContext.Incr_InvoiceOrder on order.id equals ino.OrderId
                                 join In in dataContext.Incr_Invoice on ino.InvoiceId equals In.InvoiceId
                                 join coIn in dataContext.Incr_Codes on In.StatusId equals coIn.Id
                                 join pay in dataContext.Incr_invoice_payment on In.InvoiceId equals pay.InvoiceId
                                 where (co.Code.ToUpper() == type.ToUpper() && coIn.Code.ToUpper() == "CLOSED" && pay.StatusId == "14")
                                 select new
                                 {
                                     Id = order.id,
                                     InvoiceId = In.InvoiceId,
                                     InvoiceStatusId = In.StatusId.ToString(),
                                     coIn.Discription,
                                     In.InvoiceNumber,
                                     JobNo = order.job_no,
                                     CustomerName = order.customer_name,
                                     Mobile = order.mobile,
                                     Address = order.address,
                                     CustomerId = order.customer_id,
                                     OrderDateStr = (order.order_date).ToString(),
                                     DeliveryDateStr = order.delivery_date.ToString(),
                                     InvoiceDate = In.CreatedOn.ToString(),
                                     InvoiceAmount = In.TotalPrice,
                                     InvoiceOrderAmount = ino.TotalOrderPrice,
                                     PaidDateStr = pay.PaidDate.ToString()
                                 }).OrderByDescending(x => x.Id).ToList();
                return orderList;


            }
            catch (Exception ex)
            {
                var errorPath = "Error in OrderRepository / GetClosedInvoiceBYStatus";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), errorPath, "");
                throw ex;
            }
        }
        public object GetInvoiceStatus()
        {
            try
            {
                var InvoiceStatus = (from Status in dataContext.Incr_Codes
                                 join coParent in dataContext.Incr_Codes on Status.ParentId equals coParent.Id
                                 where (coParent.Code.ToUpper() == "INVOICESTATUS")
                                 select new
                                 {
                                     Id = Status.Id.ToString(),
                                     Code = Status.Code,
                                     Discription = Status.Discription,
                                 }).OrderByDescending(x => x.Id).ToList();
                return InvoiceStatus;


            }
            catch (Exception ex)
            {
                var errorPath = "Error in OrderRepository / GetInvoiceStatus";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), errorPath, "");
                throw ex;
            }

        }
        public object getAllOrders(string type)
        {
            try
            {
                var orderList = (from order in dataContext.incr_order_master
                                 join co in dataContext.Incr_Codes on order.current_flow_code_id equals co.Id
                                 join flow in dataContext.Incr_FlowStep on order.id equals flow.order_id
                                 join price in dataContext.Incr_price_list on order.ParticularNameId equals price.id 
                                 into pr from rt in pr.DefaultIfEmpty()
                                 where ((flow.flow_code_id == order.current_flow_code_id) && (co.Code.ToUpper() == type.ToUpper() || type.ToUpper() == "ALL") && order.IsDeleted != true)
                                 select new
                                 {
                                     Id = order.id,
                                     JobNo = order.job_no,
                                     CustomerName = order.customer_name,
                                     FileName =(rt.Name == null)?"-": rt.Name,
                                     Mobile = order.mobile,
                                     Address = order.address,
                                     CustomerId = order.customer_id,
                                     OrderDateStr = (order.order_date).ToString(),
                                     DeliveryDateStr = order.delivery_date.ToString(),
                                     IsStart = flow.in_time == null ? false : true,
                                     SupervisedId = flow.supervised_by == null ? "0" : flow.supervised_by.ToString(),
                                     WorkById = flow.operator_id == null ? "0" : flow.operator_id.ToString(),
                                     Remarks = flow.remarks,
                                     VehicleNo = flow.vehicleNo,
                                     product=rt.Name
                                 }).OrderByDescending(x => x.Id).ToList();
                return orderList;

            }
            catch (Exception ex)
            {
                var errorPath = "Error in OrderRepository / getAllOrders";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), errorPath, "");
                throw ex;
            }
        }
        public bool DeleteOrder(incr_order_master orderObj)
        {
            try
            {
                orderObj.IsDeleted = true;
                dataContext.Entry(orderObj).State = EntityState.Modified;
                dataContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                var errorPath = "Error in OrderRepository / DeleteOrder";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), errorPath, "");
                throw ex;
            }
        }

        incr_order_master IOrderRepository.getOrderById(int orderId)
        {
            try
            {
                var Order = dataContext.incr_order_master.Where(m => m.id.Equals(orderId)).FirstOrDefault();
                return Order;

            }
            catch (Exception ex)
            {
                var errorPath = "Error in OrderRepository / getOrderById";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), errorPath, "");
                throw ex;
            }
        }
        public object GetOrderByInvoiceIds(string InvoiceId)
        {
            try
            {
                try
                {
                    var orderList = _dataAccess.GetListof<dynamic>(string.Format("exec incr_getOrderFormInvoice '{0}'", InvoiceId.TrimEnd(','))).ToList();

                    var olist = (from ord in orderList
                                 select new
                                 {
                                     customer_name = ord.customer_name,
                                     job_no = ord.job_no,
                                     id = ord.id,
                                     SGST = ord.SGST,
                                     CGST = ord.CGST,
                                     TransportPrice = ord.TransPortPrice,
                                     Extra = ord.Extra,
                                     Discount = ord.Discount,
                                     NetOrderPrice = ord.NetOrderPrice,
                                     TotalOrderPrice = ord.TotalOrderPrice,
                                     Address = ord.address,
                                     AmauntPaid = ord.AmauntPaid
                                 }).ToList();
                    return olist;
                }
                catch (Exception ex)
                {
                    var errorPath = "Error in OrderRepository / getOrderByIds";
                    _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), errorPath, "");
                    throw ex;
                }

            }
            catch (Exception ex)
            {
                var errorPath = "Error in OrderRepository / getOrderById";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), errorPath, "");
                throw ex;
            }
        }
        object IOrderRepository.getOrderByIds(string orderId)
        {
            try
            {
                try
                {
                    var orderList = _dataAccess.GetListof<dynamic>(string.Format("exec incr_getOrderForInvoice '{0}'", orderId.TrimEnd(','))).ToList();

                    var olist = (from ord in orderList
                                 select new
                                 {
                                     InvoiceNumber = ord.InvoiceNumber,
                                     customer_name = ord.customer_name,
                                     job_no = ord.job_no,
                                     id = ord.id,
                                     InvoiceDate = DateTime.Now.ToString("dd/MM/yyyy"),
                                     SGST = ord.SGST,
                                     CGST = ord.CGST,
                                     TransportPrice = ord.TransPortPrice,
                                     Extra = ord.Extra,
                                     Discount = ord.Discount,
                                     NetOrderPrice = ord.NetOrderPrice,
                                     TotalOrderPrice = ord.TotalOrderPrice,
                                     Address = ord.address,
                                     VehicleNo = ord.vehicleNo,
                                     DriverName = ord.driverName,
                                     Name= ord.Name,
                                     Price_per = ord.Price_per,
                                     Partyname=ord.Partyname,
                                     Particular1= getparticular(ord.Particular1, ord.Particular1Qty, ord.Particular1Rate),
                                     Particular2= getparticular(ord.Particular2, ord.Particular2Qty, ord.Particular2Rate),
                                     Particular3 = getparticular(ord.Particular3, ord.Particular3Qty, ord.Particular3Rate),
                                     Particular4 = getparticular(ord.Particular4, ord.Particular4Qty, ord.Particular4Rate),
                                     Particular5 = getparticular(ord.Particular5, ord.Particular5Qty, ord.Particular5Rate)
                                 }).ToList();
                    return olist;
                }
                catch (Exception ex)
                {
                    var errorPath = "Error in OrderRepository / getOrderByIds";
                    _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), errorPath, "");
                    throw ex;
                }

            }
            catch (Exception ex)
            {
                var errorPath = "Error in OrderRepository / getOrderById";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), errorPath, "");
                throw ex;
            }
        }

        object IOrderRepository.getInvoiceDetails(string invoiceId)
        {
            try
            {
                try
                {
                    var orderList = _dataAccess.GetListof<dynamic>(string.Format("exec incr_getOrderForInvoiceId '{0}'", invoiceId.TrimEnd(','))).ToList();

                    var olist = (from ord in orderList
                                 select new
                                 {
                                     InvoiceNumber = ord.InvoiceNumber,
                                     customer_name = ord.customer_name,
                                     job_no = ord.job_no,
                                     InvoiceDate= ord.InvoiceDate,
                                     id = ord.id,
                                     SGST = ord.SGST,
                                     CGST = ord.CGST,
                                     TransportPrice = ord.TransPortPrice,
                                     Extra = ord.Extra,
                                     Discount = ord.Discount,
                                     NetOrderPrice = ord.NetOrderPrice,
                                     TotalOrderPrice = ord.TotalOrderPrice,
                                     Address = ord.address,
                                     VehicleNo = ord.vehicleNo,
                                     DriverName = ord.driverName,
                                     Name = ord.Name,
                                     Price_per = ord.Price_per,
                                     Partyname = ord.Partyname,
                                     Particular1 = getparticular(ord.Particular1, ord.Particular1Qty, ord.Particular1Rate),
                                     Particular2 = getparticular(ord.Particular2, ord.Particular2Qty, ord.Particular2Rate),
                                     Particular3 = getparticular(ord.Particular3, ord.Particular3Qty, ord.Particular3Rate),
                                     Particular4 = getparticular(ord.Particular4, ord.Particular4Qty, ord.Particular4Rate),
                                     Particular5 = getparticular(ord.Particular5, ord.Particular5Qty, ord.Particular5Rate)
                                 }).ToList();
                    return olist;
                }
                catch (Exception ex)
                {
                    var errorPath = "Error in OrderRepository / getOrderByIds";
                    _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), errorPath, "");
                    throw ex;
                }

            }
            catch (Exception ex)
            {
                var errorPath = "Error in OrderRepository / getOrderById";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), errorPath, "");
                throw ex;
            }
        }
        public string getparticular(dynamic particular, dynamic particularQty, dynamic particularRate)
        {
            if (particularQty != null && particularQty!="")
            {
                decimal pqty = 0;
                decimal Rate = 0;
                foreach (string qty in particularQty.ToString().Split('+'))
                {
                    decimal piqty = 0;
                    decimal.TryParse(qty, out piqty);
                    pqty = pqty + piqty;
                }
                decimal.TryParse(particularRate.ToString(), out Rate);
                Rate = pqty * Rate;
                if (Rate > 0)
                {
                    return Rate.ToString("0.00")+" "+ particular +"<br/>";
                }
            }
            return "";
        }

        public int startJob(long orderId, long workById, long supervisedById, string remarks, string vehicleNo)
        {
            return _dataAccess.ExecuteNonQuery(string.Format("exec incr_startJob {0},{1},{2},'{3}','{4}'", orderId, workById, supervisedById, remarks,vehicleNo));
        }

        public int StartProduction(long orderId)
        {

            return _dataAccess.ExecuteNonQuery(string.Format("exec incr_StartProduction {0}", orderId));

        }
        public int GenerateInvoice(string orderIds)
        {
            return _dataAccess.ExecuteNonQuery(string.Format("exec incr_generateInvoice '{0}'", orderIds));
        }
        public int finishJob(long orderId, long workById, long supervisedById, string remarks, string vehicleNo)
        {
            return _dataAccess.ExecuteNonQuery(string.Format("exec incr_finishJob {0},{1},{2},'{3}','{4}'", orderId, workById, supervisedById, remarks, vehicleNo));
        }

        public int closeInvoice(long invoiceId, long statusId, decimal amount)
        {
            return _dataAccess.ExecuteNonQuery(string.Format("exec incr_CloseInvoice {0},{1},{2}", invoiceId, statusId, amount));
        }

        public List<incr_price_list> getPriceList()
        {
            try
            {
                var Incr_price_list = dataContext.Incr_price_list.Where(s => s.is_deleted != true).OrderBy(s => s.Name).ToList(); /*_crudRepository.GetAll().Where(d => d.IsDeleted.Equals(false)).ToList();*/
                return Incr_price_list;
            }
            catch (Exception ex)
            {
                var errorPath = "Error in OrderRepository / Incr_price_list";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), errorPath, "");
                throw ex;
            }
        }
    }
}