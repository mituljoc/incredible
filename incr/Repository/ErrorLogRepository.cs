﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using incr.Models;
using incr.Repository.Interface;
using incr.DTO;
using System.Data.Entity;
using incr.Controllers;
using AdminLTE1.Controllers;

namespace incr.Repository
{
    public class ErrorLogRepository : IErrorLogRepository
    {
        protected IncrEntities dataContext = new IncrEntities();

        public bool errorLogHandle(string ipAddress, string message, string innerException, string errorPath, string otherDetail)
        {
            try
            {
                ErrorLog errorLog = new ErrorLog();
                errorLog.IPAddress = ipAddress;
                errorLog.Message = message;
                errorLog.InnerException = innerException;
                errorLog.ErrorPath = errorPath;
                errorLog.ErrorDate = DateTime.Now;
                errorLog.OtherDetail = otherDetail;
                dataContext.ErrorLogs.Add(errorLog);
                dataContext.SaveChanges();
                //HomeController homeController = new HomeController();
                //homeController.SendErrorMail(ipAddress, message, innerException, errorPath, otherDetail);
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
     
    }
}