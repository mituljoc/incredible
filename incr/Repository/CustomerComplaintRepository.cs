﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using incr.Models;
using incr.Repository.Interface;
using incr.DTO;
using System.Data.Entity;

namespace incr.Repository
{
    public class CustomerComplaintRepository : ICustomerComplaintRepository
    {
        protected IncrEntities dataContext = new IncrEntities();
        IErrorLogRepository _errorLog = new ErrorLogRepository();
        /// <summary>
        /// Created by Rajesh 
        /// </summary>
        /// <returns></returns>
        public List<CustomerComplaintDTO> getCustComplaintList()
        {
            try
            {
                var complaintList = (from c in dataContext.Incr_CustomerComplaint.Where(s => s.IsDeleted == false)
                                      select new CustomerComplaintDTO
                                      {
                                          RID = c.RID,
                                          Title = c.Title,
                                          CustomerName = c.CustomerName,
                                          OrderNumner = c.OrderNumner,
                                          Department = c.Department,
                                          Status = c.Status,
                                          Date = c.Date,
                                          Problem = c.Problem,
                                          Type = c.Type,
                                          AddedBy = c.AddedBy,
                                          WorkedBy = c.WorkedBy,
                                          Priority = c.Priority,
                                          Comment = c.Comment,
                                          IsDeleted = c.IsDeleted,
                                          DateStr = c.CreatedDate.ToString()
                                      }).ToList();
                foreach (var item in complaintList)
                {
                    if (item.WorkedBy != null && item.WorkedBy != "" &&(Convert.ToInt32(item.WorkedBy) > 0) )
                    {
                        int operatorID = Convert.ToInt32(item.WorkedBy);
                        var workBy = (from o in dataContext.Incr_OperatorMaster
                                      where o.RID == operatorID
                                      select new OperatorMasterDTO
                                      {
                                          OperatorName = o.OperatorName,
                                      }).FirstOrDefault();
                        item.WorkedBy = workBy.OperatorName;
                    }

                    if (item.AddedBy != null && item.AddedBy != "" && (Convert.ToInt32(item.AddedBy) > 0))
                    {
                        int oprId = Convert.ToInt32(item.AddedBy);
                        var addedBy = (from o in dataContext.Incr_OperatorMaster
                                       where o.RID == oprId
                                       select new OperatorMasterDTO
                                       {
                                           OperatorName = o.OperatorName,
                                       }).FirstOrDefault();
                        item.AddedBy = addedBy.OperatorName;
                    }
                    if (item.CustomerName != null && item.CustomerName != "" && (Convert.ToInt32(item.CustomerName) > 0))
                    {
                        int custId = Convert.ToInt32(item.CustomerName);
                        var customerName = (from dept in dataContext.Incr_Supplier
                                            join work in dataContext.Incr_CustomerComplaint on dept.RID equals custId
                                            select new SupplierDTO
                                            {
                                                SupplierName = dept.SupplierName,
                                            }).FirstOrDefault();
                        item.CustomerName = customerName.SupplierName;
                    }
                }
                return complaintList;
            }
            catch (Exception ex)
            {
                var errorPath = "Error in CustomerComplaintRepository / getCustComplaintList";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), errorPath, "");
                throw ex;
            }
        }
        public List<Incr_Department> getAllDepartment()
        {
            try
            {
                var departmentList = dataContext.Incr_Department.Where(m => m.IsDeleted == false).ToList(); /*_crudRepository.GetAll().Where(d => d.IsDeleted.Equals(false)).ToList();*/
                return departmentList;
            }
            catch (Exception ex)
            {
                var errorPath = "Error in CustomerComplaintRepository / getAllDepartment";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), errorPath, "");
                throw ex;
            }
        }
        /// <summary>
        /// Created by Rajesh 
        /// </summary>
        /// Get detail of c by ID
        /// <returns></returns>
        public Incr_CustomerComplaint getCustComplaintById(int custComplaintId)
        {
            try
            {
                var custComplaintById = dataContext.Incr_CustomerComplaint.Where(m => m.RID.Equals(custComplaintId)).FirstOrDefault(); /*_crudRepository.GetAll().Where(d => d.IsDeleted.Equals(false)).ToList();*/
                return custComplaintById;
            }
            catch (Exception ex)
            {
                var errorPath = "Error in CustomerComplaintRepository / getCustComplaintByID";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), errorPath, "");
                throw ex;
            }
        }
        /// <summary>
        /// Created by Rajesh 
        /// </summary>
        /// <returns></returns>
        public bool addNewCustComplaint(CustomerComplaintDTO custComplaintObj)
        {
            try
            {
                Incr_CustomerComplaint custComplaint = new Incr_CustomerComplaint();

                custComplaint.Title = custComplaintObj.Title;
                custComplaint.CustomerName = custComplaintObj.CustomerName;
                custComplaint.OrderNumner = custComplaintObj.OrderNumner;
                custComplaint.Department = custComplaintObj.Department;
                custComplaint.Status = custComplaintObj.Status;
                custComplaint.Date = custComplaintObj.Date;
                custComplaint.Problem = custComplaintObj.Problem;
                custComplaint.AddedBy = custComplaintObj.AddedBy;
                custComplaint.WorkedBy = custComplaintObj.WorkedBy;
                custComplaint.Priority = custComplaintObj.Priority;
                custComplaint.Comment = custComplaintObj.Comment;
                custComplaint.Type = custComplaintObj.Type;
                custComplaint.IsDeleted = false;
                custComplaint.CreatedBy = custComplaintObj.CreatedBy;
                custComplaint.CreatedDate = DateTime.Now;
                dataContext.Incr_CustomerComplaint.Add(custComplaint);
                dataContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                var errorPath = "Error in CustomerComplaintRepository / addNewComplaint";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), errorPath, "");
                throw ex;
            }
        }
        /// <summary>
        /// Created by Rajesh 
        /// </summary>
        /// <returns></returns>

        public bool updateCustComplaint(CustomerComplaintDTO custComplaintObj)
        {
            try
            {
                Incr_CustomerComplaint custComplaint = new Incr_CustomerComplaint();
                custComplaint.RID = custComplaintObj.RID;
                custComplaint.Title = custComplaintObj.Title;
                custComplaint.CustomerName = custComplaintObj.CustomerName;
                custComplaint.OrderNumner = custComplaintObj.OrderNumner;
                custComplaint.Department = custComplaintObj.Department;
                custComplaint.Status = custComplaintObj.Status;
                custComplaint.Date = custComplaintObj.Date;
                custComplaint.Problem = custComplaintObj.Problem;
                custComplaint.AddedBy = custComplaintObj.AddedBy;
                custComplaint.WorkedBy = custComplaintObj.WorkedBy;
                custComplaint.Priority = custComplaintObj.Priority;
                custComplaint.Comment = custComplaintObj.Comment;
                custComplaint.Type = custComplaintObj.Type;
                custComplaint.IsDeleted = false;
                custComplaint.CreatedBy = custComplaintObj.CreatedBy;
                custComplaint.CreatedDate = custComplaintObj.CreatedDate;
                custComplaint.UpdatedBy = custComplaintObj.UpdatedBy;
                custComplaint.UpdatedDate = DateTime.Now;
                dataContext.Entry(custComplaint).State = EntityState.Modified;
                dataContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                var errorPath = "Error in CustomerComplaintRepository / updateCustComplaint";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), errorPath, "");
                throw ex;
            }
        }
        /// <summary>
        /// Created by Rajesh 
        /// </summary>
        /// Virtual delete
        /// <returns></returns>

        public bool deleteCustComplaint(Incr_CustomerComplaint custComplaintObj)
        {
            try
            {
                custComplaintObj.IsDeleted = true;
                dataContext.Entry(custComplaintObj).State = EntityState.Modified;
                dataContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                var errorPath = "Error in CustomerComplaintRepository / deleteCustComplaintList";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), errorPath, "");
                throw ex;
            }
        }

        public List<incr_order_master>  getOrderByCustomerId(int customerId)
        {
            try
            {
                var OrderByCustomerId = dataContext.incr_order_master.Where(m => m.customer_id == customerId).ToList(); /*_crudRepository.GetAll().Where(d => d.IsDeleted.Equals(false)).ToList();*/
               
                return OrderByCustomerId;
            }
            catch (Exception ex)
            {
                var errorPath = "Error in CustomerComplaintRepository / getCustComplaintByID";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), errorPath, "");
                throw ex;
            }
        }
    }
}