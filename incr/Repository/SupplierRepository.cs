﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using incr.Models;
using incr.Repository.Interface;
using incr.DTO;
using System.Data.Entity;

namespace incr.Repository
{
    public class SupplierRepository : ISupplierRepository
    {
        protected IncrEntities dataContext = new IncrEntities();
        IErrorLogRepository _errorLog = new ErrorLogRepository();
        /// <summary>
        /// Created by Rajesh 
        /// </summary>
        /// <returns></returns>
        public List<SupplierDTO> getSupplierList()
        {
            try
            {
                var workMasterList = (from supplier in dataContext.Incr_Supplier.Where(s => s.IsDeleted != true)
                                      select new SupplierDTO
                                      {
                                          RID = supplier.RID,
                                          SupplierName = supplier.SupplierName,
                                          Email = supplier.Email,
                                          MobileNo = supplier.MobileNo,
                                          GSTNo = supplier.GSTNo,
                                          Website = supplier.Website,
                                          PaymentTerm = supplier.PaymentTerm,
                                          ContactPerson = supplier.ContactPerson,
                                          Address = supplier.Address,
                                          City = supplier.City,
                                          State = supplier.State,
                                          Country = supplier.Country,
                                          Type = supplier.Type,
                                          IsActive = supplier.IsActive,
                                          IsDeleted = supplier.IsDeleted,
                                          DateStr = supplier.CreatedDate.ToString()
                                      }).ToList();
                return workMasterList;
            }
            catch (Exception ex)
            {
                var errorPath = "Error in SupplierRepository / getSupplierList method";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), errorPath, "");
                throw ex;
            }
        }
        
        /// <summary>
        /// Created by Rajesh 
        /// </summary>
        /// <returns></returns>
        public bool addNewSupplier(SupplierDTO supplierObj)
        {
            try
            {
                Incr_Supplier supplierModel = new Incr_Supplier();

                supplierModel.SupplierName = supplierObj.SupplierName;
                supplierModel.Email = supplierObj.Email;
                supplierModel.MobileNo = supplierObj.MobileNo;
                supplierModel.GSTNo = supplierObj.GSTNo;
                supplierModel.Website = supplierObj.Website;
                supplierModel.PaymentTerm = supplierObj.PaymentTerm;
                supplierModel.ContactPerson = supplierObj.ContactPerson;
                supplierModel.Address = supplierObj.Address;
                supplierModel.City = supplierObj.City;
                supplierModel.State = supplierObj.State;
                supplierModel.Country = supplierObj.Country;
                supplierModel.Type = supplierObj.Type;
                supplierModel.IsActive = supplierObj.IsActive;
                supplierModel.IsDeleted = false;
                supplierModel.CreatedBy = supplierObj.CreatedBy;
                supplierModel.CreatedDate = DateTime.Now;
                dataContext.Incr_Supplier.Add(supplierModel);
                dataContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                var errorPath = "Error in SupplierRepository / addNewSupplier method";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), errorPath, "");
                throw ex;
            }
        }
        /// <summary>
        /// Created by Rajesh 
        /// </summary>
        /// Get detail of supplier by ID
        /// <returns></returns>
        public Incr_Supplier getSupplierById(int supplierId)
        {
            try
            {
                var SupplierById = dataContext.Incr_Supplier.Where(m => m.RID.Equals(supplierId)).FirstOrDefault();
                return SupplierById;
            }
            catch (Exception ex)
            {
                var errorPath = "Error in SupplierRepository / getSupplierById";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), errorPath, "");
                throw ex;
            }
        }
        /// <summary>
        /// Created by Rajesh 
        /// </summary>
        /// <returns></returns>

        public bool updateSupplier(SupplierDTO supplierObj)
        {
            try
            {
                Incr_Supplier supplierModel = new Incr_Supplier();
                supplierModel.RID = supplierObj.RID;
                supplierModel.SupplierName = supplierObj.SupplierName;
                supplierModel.Email = supplierObj.Email;
                supplierModel.MobileNo = supplierObj.MobileNo;
                supplierModel.GSTNo = supplierObj.GSTNo;
                supplierModel.Website = supplierObj.Website;
                supplierModel.PaymentTerm = supplierObj.PaymentTerm;
                supplierModel.ContactPerson = supplierObj.ContactPerson;
                supplierModel.Address = supplierObj.Address;
                supplierModel.City = supplierObj.City;
                supplierModel.State = supplierObj.State;
                supplierModel.Country = supplierObj.Country;
                supplierModel.Type = supplierObj.Type;
                supplierModel.IsActive = supplierObj.IsActive;
                supplierModel.IsDeleted = false;
                supplierModel.CreatedDate = supplierObj.CreatedDate;
                supplierModel.UpdatedBy = supplierObj.UpdatedBy;
                supplierModel.UpdatedDate = DateTime.Now;
                dataContext.Entry(supplierModel).State = EntityState.Modified;
                dataContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                var errorPath = "Error in SupplierRepository / updateSupplier";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), errorPath, "");
                throw ex;
            }
        }
        /// <summary>
        /// Created by Rajesh 
        /// </summary>
        /// Virtual delete
        /// <returns></returns>

        public bool deleteSupplier(Incr_Supplier supplierObj)
        {
            try
            {
                supplierObj.IsDeleted = true;
                dataContext.Entry(supplierObj).State = EntityState.Modified;
                dataContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                var errorPath = "Error in SupplierRepository / deleteSupplier method";
                _errorLog.errorLogHandle("", ex.Message, ex.InnerException.ToString(), errorPath, "");
                throw ex;
            }
        }
    }
}