﻿
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace incr.Repository.Interface
{
    public interface ICRUDRepository<TEntity, TContext>
         where TContext : DbContext, new()
    {
        IQueryable<TEntity> GetAll();
        TEntity GetById(Expression<Func<TEntity, bool>> filter);
        TEntity Create(TEntity model);
        void Delete(TEntity id);
        TEntity Update(TEntity model);
        void Save();
    }
}