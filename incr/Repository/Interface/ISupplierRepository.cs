﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using incr.DTO;
using incr.Models;
namespace incr.Repository.Interface
{
    public interface ISupplierRepository
    {
        List<SupplierDTO> getSupplierList();
        Incr_Supplier getSupplierById(int supplierId);
        bool addNewSupplier(SupplierDTO supplierObj);
        bool updateSupplier(SupplierDTO supplierObj);
        bool deleteSupplier(Incr_Supplier supplierObj);
    }
}