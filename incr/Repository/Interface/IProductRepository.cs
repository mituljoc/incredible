﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using incr.DTO;
using incr.Models;
namespace incr.Repository.Interface
{
    public interface IProductRepository
    {
        List<ProductOTD> getProductList();
        ProductOTD getProductById(int id);
        bool addProduct(incr_price_list product);
        bool updateProduct(incr_price_list product);
        bool deleteProduct(incr_price_list product);
        long addProductFromOrderPage(incr_price_list Product, string name);
    }
}
