﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using incr.DTO;
using incr.Models;
namespace incr.Repository.Interface
{
    public interface IOperatorWorkRepository
    {
        List<OperatorWorkMasterDTO> getAllOpeartorWorkMaster();
        long addNewOperatorWorkMaster(OperatorWorkMasterDTO workMasterObj);
        OperatorWorkMasterDTO getWorkMasterById(int operatorWorkMasterId);
        bool updateWorkMaster(OperatorWorkMasterDTO workMasterObj);
        bool deleteWorkMaster(Incr_OperatorWorkMaster workMasterObj);
        dynamic getPriceListForEdit(long opworkId);
    }
}