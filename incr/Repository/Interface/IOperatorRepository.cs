﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using incr.DTO;
using incr.Models;
namespace incr.Repository.Interface
{
    public interface IOperatorRepository
    {
        List<Incr_OperatorMaster> getOperatorList();
        List<Incr_Department> getAllDepartment();
        List<Incr_OperatorMaster> getOperatorList_supervisor();
        Incr_OperatorMaster getOperatorById(int operatorId);
        bool addNewOperator(OperatorMasterDTO operatorObj);
        bool updateOperator(OperatorMasterDTO operatorObj);
        bool deleteOperator(Incr_OperatorMaster operatorObj);

    }
}