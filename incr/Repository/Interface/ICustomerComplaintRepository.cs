﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using incr.DTO;
using incr.Models;
namespace incr.Repository.Interface
{
    public interface ICustomerComplaintRepository
    {
        List<CustomerComplaintDTO> getCustComplaintList();
        List<Incr_Department> getAllDepartment();
        Incr_CustomerComplaint getCustComplaintById(int custComplaintId);
        bool addNewCustComplaint(CustomerComplaintDTO custComplaintObj);
        bool updateCustComplaint(CustomerComplaintDTO custComplaintObj);
        bool deleteCustComplaint(Incr_CustomerComplaint custComplaintObj);
        List<incr_order_master>  getOrderByCustomerId(int customerId);
    }
}