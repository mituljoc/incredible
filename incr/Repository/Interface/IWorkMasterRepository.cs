﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using incr.DTO;
using incr.Models;
namespace incr.Repository.Interface
{
    public interface IWorkMasterRepository
    {
        List<WorkMasterDTO> getWorkMasterList();
        List<Incr_Department> getAllDepartment();
        List<incr_price_list> GetAllPrice_list();
        Incr_WorkMaster getWorkMasterById(int workMasterId);
        bool addNewWorkMaster(WorkMasterDTO workMasterObj);
        bool updateWorkMaster(WorkMasterDTO workMasterObj);
        bool deleteWorkMaster(Incr_WorkMaster workMasterObj);
    }
}