﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using incr.DTO;
using incr.Models;
namespace incr.Repository.Interface
{
    public interface IOrderRepository
    {
        incr_order_master getOrderById(int orderId);
        List<incr_price_list> getPriceList();
        object getOrderByIds(string orderId);
        object getInvoiceDetails(string invoiceId);
        
        object GetOrderByInvoiceIds(string InvoiceId);
        object getAllOrders(string type);
        object GetInvoiceBYStatus(string type);
        object GetClosedInvoiceBYStatus(string type);
        int startJob(long orderId, long workById, long supervisedById, string remarks, string vehicleNo);
        int closeInvoice(long invoiceId, long statusId, decimal amount);
        int finishJob(long orderId, long workById, long supervisedById, string remarks, string vehicleNo);
        int StartProduction(long orderId);
        int GenerateInvoice(string GenerateInvoice);
        object GetInvoiceStatus();
        bool DeleteOrder(incr_order_master orderObj);
    }
}
