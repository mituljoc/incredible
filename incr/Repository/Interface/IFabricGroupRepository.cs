﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using incr.DTO;
using incr.Models;
namespace incr.Repository.Interface
{
    public interface IFabricGroupRepository
    {
        List<FabricDTO> getFabricGroupList();
        Incr_FabricGroup getFabricGroupById(int fabricGroupId);
        bool addFabricGroup(FabricDTO fabricGroupObj);
        bool updateFabricGroup(FabricDTO fabricGroupObj);
        bool deleteFabricGroup(Incr_FabricGroup fabricGroupObj);
    }
}