﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using incr.DTO;
using incr.Models;

namespace incr.Repository.Interface
{
    public interface IErrorLogRepository
    {
        bool errorLogHandle(string ipAddress,string message, string innerException, string errorPath, string otherDetail);
    }
}