﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using incr.DTO;
using incr.Models;
namespace incr.Repository.Interface
{
    public interface IFabricRepository
    {
        List<FabricDTO> getFabricList();
        Incr_FabricMaster getFabricById(int fabricId);
        bool addFabric(FabricDTO fabricObj);
        bool updateFabric(FabricDTO fabricObj);
        bool deleteFabric(Incr_FabricMaster fabricObj);
    }
}