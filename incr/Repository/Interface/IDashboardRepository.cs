﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using incr.DTO;
using incr.Models;

namespace incr.Repository.Interface
{
    public interface IDashboardRepository
    {
        List<incr_order_master> getOrderList();
        List<OrderDTO> getCarpentaryList();
        List<OrderDTO> getLiningList();
        List<OrderDTO> getFittingList();
        List<OrderDTO> getDeliveryList();
        List<OrderDTO> getInvoiceList();
        List<OrderDTO> getLeaderBoardList();
        List<OrderDTO> getHighestCustomerOrderList();
        List<OrderDTO> getComplaintByOperatorList();

    }
}