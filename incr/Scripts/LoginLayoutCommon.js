﻿
        $('.lazy').Lazy({
            scrollDirection: 'vertical',
            effect: 'fadeIn',
            visibleOnly: true,
        });
localStorage.sessionTimeoutObj = "";
function startDictation() {
    if (window.hasOwnProperty('webkitSpeechRecognition')) {
        var recognition = new webkitSpeechRecognition();
        recognition.continuous = false;
        recognition.interimResults = false;
        recognition.lang = "en-US";
        recognition.start();
        recognition.onresult = function (e) {
            document.getElementById('searchbox1').value = e.results[0][0].transcript;
            recognition.stop();
            document.getElementById('searchFrm').submit();
        };
        recognition.onerror = function (e) {
            recognition.stop();
        }
    }
}
$(document).ready(function () {
    $(" #loginopen").click(function () { window.location.href = '/'; });
    var ua = navigator.userAgent,
    iOS = /iPad|iPhone|iPod/.test(ua),
//iOS11 = /OS 11_0_1|OS 11_0_2|OS 11_0_3|OS 11_1|OS 11_1_1|OS 11_1_2|OS 11_2/.test(ua);
iOS11 = /OS 11_0|OS 11_0_1|OS 11_0_2|OS 11_0_3|OS 11_1|OS 11_1_1|OS 11_1_2|OS 11_2/.test(ua);
    if (iOS && iOS11) { $("body").addClass("iosBugFixCaret"); }
    $("#signupmodalsmall").click(function () {
        if (window.location.href.indexOf('/?P1JlZmVycmFsPQ=') > -1) {
            jQuery(window).load(function () {
                $("body").addClass("modal-open");
                var currentURL = decodeURIComponent(window.location.href);
                
                var code = currentURL.slice('?').split('=')[1];
                $('#Smartbanner').modal('hide');
                setTimeout(function () {
                    //$("#modal1").modal('hide');
                    $("body").addClass("modal-open");
                    $('#referralModal').modal({
                        backdrop: 'static'
                    });
                    code = window.atob(code);
                    $('#txtReferralCodeID').val(code);
                    $("#lbReferralCodeID").html(code);
                    $('#nonReferralTitleDiv').hide();
                    $('#referralTitleDiv').show();
                    $('#NonReferralIdDiv').hide();
                    $('#ReferralIdDiv').show();
                }, 1000)
            });
        }
        else if (window.location.href.indexOf('/?P1JlZmVycmFsPQ') > -1) {
            jQuery(window).load(function () {
                $("body").addClass("modal-open");
                var currentURL = decodeURIComponent(window.location.href);
                var code = (((((currentURL.slice('?').split('&'))[0]).split('?'))[1]).split('PQ'))[1];
                $('#Smartbanner').modal('hide');
                setTimeout(function () {
                    //$("#modal1").modal('hide');
                    $("body").addClass("modal-open");
                    $('#referralModal').modal({
                        backdrop: 'static'
                    });
                    code = window.atob(code);
                    $('#txtReferralCodeID').val(code);
                    $("#lbReferralCodeID").html(code);
                    $('#nonReferralTitleDiv').hide();
                    $('#referralTitleDiv').show();
                    $('#NonReferralIdDiv').hide();
                    $('#ReferralIdDiv').show();
                }, 1000)
            });
        }
        else {
            $('#nonReferralTitleDiv').show();
            $('#referralTitleDiv').hide();
            $('#NonReferralIdDiv').show();
            $('#ReferralIdDiv').hide();
            $("#referralModal").modal('show');
        }
    });
    $("#loginmodalsmall").click(function () {
        $('#IsNotGuestCheckout').show();
        $('#IsGuestCheckout').hide();
        $('#loginModel').modal('show');
        $("#referralModal").modal('hide')
    });
    $("#category").click(function () {
        $("#show").toggle();
        $("#show1").toggle();
        $("#show2").toggle();
    })
    explode();
    setInterval(explode, 50000);
    function explode() {
        var action = '/Account/GetProductionUpdateTime';
        if (getParameterByName("redirect") != null) {
            action += "?redirect=" + getParameterByName("redirect");
        }
        var data = _AjaxWithOutData(action, "Post");
        if (data.Status == "200") {
            if (data.IsUpdateStart == "true") {
                $("#divProdUpdate").html("<div class='undermaintenance'><i class='fa fa-wrench' style='margin-right: 10px;'></i>" + data.UpdateDateTime + "</div>");
            }
            else {
                $("#divProdUpdate").html("");
            }
        }
        else {
            window.location.href = "/Home/Errorpage";
        }
    }
    if ($(window).width() <= 767) {
        $("#SerachDIV").remove();
    }
    else if ($(window).width() > 767) {
        $("#SerachMobileDIV").remove();
    }
    //var topOfOthDiv = $("#mapoffset").offset().top;
    //var topsliderdiv = $("#Aboutclass").offset().top;
    //$(window).scroll(function () {
    //    if ($(window).scrollTop() > topOfOthDiv || $(window).scrollTop() < topsliderdiv) {
    //        $(".booknowfixed").addClass('hideonscroll');
    //    } else {
    //        $(".booknowfixed").removeClass('hideonscroll');
    //    }
    //});
});
$(".select2-selection__rendered").click(function () {
    $(".select2-search__field").focus()
})
$.fn.textWidth = function (_text, _font) {
    var fakeEl = $('<span>').hide().appendTo(document.body).text(_text || this.val() || this.text()).css('font', _font || this.css('font')),
        width = fakeEl.width();
    fakeEl.remove();
    return width;
};
$.fn.autoresize = function (options) {
    options = $.extend({ padding: 10, minWidth: 0, maxWidth: 10000 }, options || {});
    $(this).on('input.select2-search__field', function () {
        $(this).css('width', Math.min(options.maxWidth, Math.max(options.minWidth, $(this).textWidth() + options.padding)));
    }).trigger('input.select2-search__field');
    return this;
}
$("input.select2-search__field").autoresize({ padding: 20, minWidth: 250, maxWidth: 250 });
$("input.select2-search__field_mobile").autoresize({ padding: 20, minWidth: 260, maxWidth: 265 });
$('.select2-search__field').keydown(function (e) {
    if ($('.select2-search__field').val() == null || $('.select2-search__field').val() == '' || $('.select2-search__field').val() == undefined) {
        if (e.keyCode == 8) {
            $(".select2-selection__choice").last().remove();
        }
    }
})
$(document).click(function (e) {
    if ($(".list1").children().length == 1) {
        $("#searchbox1").attr('placeholder', 'I want to find');
        $("#searchbox1").css("width", "250px");
    }
    if ($(".list2").children().length == 1) {
        $("#searchbox2").attr('placeholder', 'I want to find');
        $("#searchbox2").css("width", "250px");
    }
    if ($(".list3").children().length == 1) {
        $("#searchbox3").attr('placeholder', 'I want to find');
        $("#searchbox3").css("width", "250px");
    }
    if ($(".list4").children().length == 1) {

        $("#searchbox4").attr('placeholder', 'I want to find');
        $("#searchbox4").css("width", "250px");
    }
    if (!$(e.target).parents().andSelf().is('.tt-dropdown-menu')) {
        $(".tt-dropdown-menu").hide();
    }
});
$('.modal').on('hidden.bs.modal', function () { setTimeout(function () { $('body').css('padding-right', '0'); }, 100) })
$('.slider').slider({ full_width: true });
$(document).delegate(".opntooltip", "mouseenter", function () {
    $('[data-toggle="popover"]').each(function () {
        $('[data-toggle="popover"]').not(this).popover('hide');
        $(this).popover()
    });
});
var Currentyear = new Date().getFullYear();
var CurrentMonth = new Date().getMonth();
var today = new Date().getDate();
$('#DateOfBirth').datetimepicker({
    format: "DD/MM/YYYY",
    minDate: new Date('1901', '01', '01'),
    maxDate: new Date(Currentyear, CurrentMonth, today),
    widgetPositioning: { horizontal: "auto", vertical: "bottom" }
});
$('#DateOfBirth').val('');
$('.StartDate').datetimepicker({
    format: "DD/MM/YYYY",
    showClear: true,
    minDate: new Date('1901', '01', '01'),
    widgetPositioning: { horizontal: "auto", vertical: "bottom" }
});
$('.EndDate').datetimepicker({
    useCurrent: false, //Important! See issue #1075
    format: "DD/MM/YYYY",
    showClear: true,
    minDate: new Date('1901', '01', '01'),
    widgetPositioning: { horizontal: "auto", vertical: "bottom" }
});
$('#searchbox1').focusin(function () {
    $(this).autoresize({ padding: 20, minWidth: 40, maxWidth: 300 });
    $(this).data('place-holder-text', $(this).attr('placeholder'))
    $(this).attr('placeholder', '');
});
$('#searchbox2').focusin(function () {
    $(this).autoresize({ padding: 20, minWidth: 40, maxWidth: 300 });
    $(this).data('place-holder-text', $(this).attr('placeholder'))
    $(this).attr('placeholder', '');
});
$('#searchbox3').focusin(function () {
    $(this).autoresize({ padding: 20, minWidth: 40, maxWidth: 300 });
    $(this).data('place-holder-text', $(this).attr('placeholder'))
    $(this).attr('placeholder', '');
});
$('#searchbox4').focusin(function () {
    $(this).autoresize({ padding: 20, minWidth: 40, maxWidth: 300 });
    $(this).data('place-holder-text', $(this).attr('placeholder'))
    $(this).attr('placeholder', '');
});

$(function () { $('[data-toggle="popover"]').popover() })
$('#scrollUp').click(function () { $("html,body").animate({ scrollTop: 0 }, "fast"); })
$(document).scroll(function () {
    var y = $(this).scrollTop();
    if (y > 800) {
        $('#scrollUp').fadeIn();
    } else {
        $('#scrollUp').fadeOut();
    }
});
$(document).delegate("#signupmodal", "click", function (e) {
    $('#loginModel').modal('hide');
    if (window.location.href.indexOf('/?P1JlZmVycmFsPQ=') > -1) {
        jQuery(window).load(function () {
            $("body").addClass("modal-open");
            var currentURL = decodeURIComponent(window.location.href);
            
            var code = currentURL.slice('?').split('=')[1];
            $('#Smartbanner').modal('hide');
            setTimeout(function () {
                //$("#modal1").modal('hide');
                $("body").addClass("modal-open");
                $('#referralModal').modal({
                    backdrop: 'static'
                });
                code = window.atob(code);
                $('#txtReferralCodeID').val(code);
                $("#lbReferralCodeID").html(code);
                $('#nonReferralTitleDiv').hide();
                $('#referralTitleDiv').show();
                $('#NonReferralIdDiv').hide();
                $('#ReferralIdDiv').show();
            }, 1000)
        });
    }
    else if (window.location.href.indexOf('/?P1JlZmVycmFsPQ') > -1) {
        jQuery(window).load(function () {
            $("body").addClass("modal-open");
            var currentURL = decodeURIComponent(window.location.href);
            var code = (((((currentURL.slice('?').split('&'))[0]).split('?'))[1]).split('PQ'))[1];
            $('#Smartbanner').modal('hide');
            setTimeout(function () {
                //$("#modal1").modal('hide');
                $("body").addClass("modal-open");
                $('#referralModal').modal({
                    backdrop: 'static'
                });
                code = window.atob(code);
                $('#txtReferralCodeID').val(code);
                $("#lbReferralCodeID").html(code);
                $('#nonReferralTitleDiv').hide();
                $('#referralTitleDiv').show();
                $('#NonReferralIdDiv').hide();
                $('#ReferralIdDiv').show();
            }, 1000)
        });
    }
    else {
        $('#nonReferralTitleDiv').show();
        $('#referralTitleDiv').hide();
        $('#NonReferralIdDiv').show();
        $('#ReferralIdDiv').hide();
        $("#referralModal").modal('show');
    }
    setTimeout(function () { $('body').addClass('modal-open'); }, 500)
})
$(document).delegate("#loginmodal", "click", function (e) {
    $('#IsNotGuestCheckout').show();
    $('#IsGuestCheckout').hide();
    $('#loginModel').modal('show');
    $("#referralModal").modal('hide')
})
$(document).delegate("#backButton", "click", function (e) {
    window.history.back();
});
