﻿
var serviceURL = 'http://times.infixsofttech.com/'

var jsonContentType = "application/json; charset=utf-8";
var jsonDataType = "json";
$.support.cors = true;
function ValidationStringLength(textLength, count) {
    var TotalLength = (textLength.length);
    if (TotalLength > count) {
        //Is valid
        return true;
    }
    else {
        //Is not valid
        return false;
    }
}
// Mukesh 11/08/2018
function toPushOrPopFromArray(element, value, flag) {
    if (flag) {// pop 
        var isIndexFound = element.indexOf(value);
        if (isIndexFound > -1) {
            element.splice(isIndexFound, 1);
            return element.toString();
        }
    }
    else {
        // push
        element.push(value);
        return array.toString();
    }

}
// Mukesh 11/08/2018
function strContact(str1, str2) {
    if (str2) {
        if (str1) {
            return distinctValueFromString(str1 + ',' + str2);
        }
        return distinctValueFromString(str2.toString());
    }
    else {
        if (str1)
            return distinctValueFromString(str1.toString());
    }
    return "";
}
// Mukesh 11/08/2018
function distinctValueFromString(data) {
    arr = $.unique(data.split(','));
    data = arr.join(","); //get unique string back with 
    return data;
}
function setMaskValue(id, maskValue) {
    var mask;
    $("#" + id).mask(maskValue);
}

// Enter only numeric decimal value validation set.
function isNumberKeyDecimal(e) {
    if (window.event) // IE 
    {
        if ((e.keyCode < 48 || e.keyCode > 57) && e.keyCode != 8 && e.which != 46) {
            event.returnValue = false;
            return false;
        }
    }
    else { // Fire Fox
        if ((e.which < 48 || e.which > 57) && e.which != 8 && e.which != 46) {
            e.preventDefault();
            return false;

        }
    }
}
//Enter only positive number
function isOnlyPositiveNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    //if the letter is not digit then display error and don't type anything

    if (charCode != 8 && charCode != 0 && (charCode < 48 || charCode > 57)) {
        //display error message
        //$("#errmsg").html("Digits Only").show().fadeOut("slow");
        return false;
    }
    return true;
}
/*
Check if null  value then retrun a default value
*/
function isNull(value, defaltReturn) {
    if (value == null) {
        return defaltReturn;
    }
    else {
        return value;
    }
}

//$.validator.addMethod("VadlidateSelect", function (value, element) {
//    return ($(element).val() != "0" && $(element).val() != "" && $(element).val() != null)
//}, "Please select atleast one value");


/*
 Get the Querystring value
*/
function getParameterByName(name) {

    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

$.fn.serializeObject = function () {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function () {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

function _ToJson(data) {
    var param_arr = data.split("&");

    var param = new StringBuilder();
    param.append("{");
    for (i = 0; i < param_arr.length; i++) {
        target_arr = param_arr[i].split("=");
        if (jQuery.trim(param.toString()).length > 1) {
            param.append(",");
        }
        param.append("\"" + target_arr[0] + "\"" + ":" + "\"" + target_arr[1] + "\"");
    }
    param.append("}");

    return param.toString();
}


function _AjaxWithOutData(action, Type) {
    var obj;

    jQuery.ajax({
        url: action
        , type: Type
        , dataType: 'json'
        , async: false
        , cache: false
        , contentType: 'application/json; charset=utf-8'
        , beforeSend: function (xhr) { xhr.setRequestHeader('X-Requested-With', 'test-value'); }
        , success: function (data) {
            obj = data;
        }
        , error: function (xhr, textStatus, errorThrown) {
            if (xhr.status == 404) {
                alert('Requested URL not found.');
            } else if (xhr.status == 500) {
                // window.location.href = "http://www.google.com";
                alert('Internel Server Error.');
            } else if (textStatus == 'parsererror') {
                alert('Error.\nParsing JSON Request failed.');
            } else if (textStatus == 'timeout') {
                alert('Request Time out.');
            }
            //else {
            //    alert('Unknow Error.\n' + xhr.responseText);
            //}

        }
    });
    return obj;
}


function _Ajax(action, data, Type) {
    var obj;

    jQuery.ajax({
        url: action
        , type: Type
        , data: data
        , dataType: 'json'
        , async: false
        , cache: false
        , contentType: 'application/json; charset=utf-8'
        , success: function (data) {
            obj = data;
        }
        , error: function (xhr, textStatus, errorThrown) {

            if (xhr.status == 404) {
                alert('Requested URL not found.');
            } else if (xhr.status == 500) {
                //window.location.href = "http://www.google.com";
                alert('Internel Server Error.');
            } else if (textStatus == 'parsererror') {
                alert('Error.\nParsing JSON Request failed.');
            } else if (textStatus == 'timeout') {
                alert('Request Time out.');
            }
            //else {
            //    alert('Unknow Error.\n' + xhr.responseText);
            //}

        }
    });
    return obj;
}

function StringBuilder() {
    var strings = [];

    this.append = function (string) {
        string = verify(string);
        if (string.length > 0) strings[strings.length] = string;
    };

    this.appendLine = function (string) {
        string = verify(string);
        if (this.isEmpty()) {
            if (string.length > 0) strings[strings.length] = string;
            else return;
        }
        else strings[strings.length] = string.length > 0 ? "\r\n" + string : "\r\n";
    };

    this.clear = function () { strings = []; };

    this.isEmpty = function () { return strings.length == 0; };

    this.toString = function () { return strings.join(""); };

    var verify = function (string) {
        if (!defined(string)) return "";
        if (getType(string) != getType(new String())) return String(string);
        return string;
    };

    var defined = function (el) {
        // Changed per Ryan O'Hara's comment:
        return el != null && typeof (el) != "undefined";
    };

    var getType = function (instance) {
        if (!defined(instance.constructor)) throw Error("Unexpected object type");
        var type = String(instance.constructor).match(/function\s+(\w+)/);

        return defined(type) ? type[1] : "undefined";
    };
};

function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode != 46 && charCode > 31 && charCode != 46
    && (charCode < 48 || charCode > 57) && (charCode < 40 || charCode > 41) && (charCode < 35 || charCode > 35) && (charCode < 43 || charCode > 48))
        return false;

    return true;
}

function isDecimalValue(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if ((charCode < 48 || charCode > 57) && charCode != 110 && charCode != 190 && (charCode < 96 || charCode > 105) && (charCode < 37 || charCode > 40) && charCode != 8 && charCode != 46 && charCode != 9)
        return false;

    return true;
}


// Open fancy box
function OpenFacyBox(url, width, height) {
    $.fancybox.open([
          {
              href: url
          }
    ], {
        padding: 0,
        type: 'iframe',
        autoSize: false,
        beforeLoad: function () {
            this.width = width;
            this.height = height;
        }
    });

}

// Open fancy box
function CloseFancyBox() {
    $.fancybox.close();
}

/// This function will close the privous fancybox and open new one.
function OpenAnotherFancybox(url, width, height) {

    CloseFancyBox();

    $.fancybox.open([
         {
             href: url
         }
    ], {
        padding: 0,
        type: 'iframe',
        autoSize: false,
        beforeLoad: function () {
            this.width = width;
            this.height = height;
        }
    });

}

//This function open alert popup box.
function OpenAlertBox(url, width, height) {
    $.fancybox.open([
           {
               href: url
           }
    ], {
        padding: 0,
        type: 'iframe',
        'closeBtn': false,
        helpers: {
            overlay: { closeClick: false }
        },
        autoSize: false,
        beforeLoad: function () {
            this.width = width;
            this.height = height;
        }
    });
}
//this function open image in fancybox

function OpenImage(url) {
    $.fancybox.open([
           {
               href: url
           }
    ], {
        padding: 0,
        'transitionIn': 'elastic',
        'transitionOut': 'elastic',
        'overlayShow': false
    });
}


function OpenAlertAnotherFancybox(url, width, height) {

    CloseFancyBox();

    $.fancybox.open([
         {
             href: url
         }
    ], {
        padding: 0,
        type: 'iframe',
        'closeBtn': false,
        helpers: {
            overlay: { closeClick: false }
        },
        autoSize: false,
        beforeLoad: function () {
            this.width = width;
            this.height = height;
        }
    });

}



function GetQueryStringParams(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
}

function getMyProfilePercentageCal(chid) {

    var perValue = 0;
    if (chid) {
        if (chid.FirstName)
            perValue += 10;
        if (chid.LastName)
            perValue += 10;
        if (chid.Address)
            perValue += 10;
        if (chid.PostalCode)
            perValue += 10;
        if (chid.DateOfBirth)
            perValue += 10;
        if (chid.Gender >= 0)
            perValue += 10;
        if (chid.Email)
            perValue += 10;
        if (chid.ContactNo)
            perValue += 10;
        if (chid.ProfilePic)
            perValue += 10;
        if (chid.CountProfile)
            perValue += 10;
    }
    return perValue;
}
function getMyChildProfilePercentageCal(chid) {
    var perValue = 0;
    if (chid) {
        if (chid.FirstName)
            perValue += 12.5;
        if (chid.LastName)
            perValue += 12.5;
        if (chid.SchoolName)
            perValue += 12.5;
        if (chid.ChildDateOfBirth)
            perValue += 12.5;
        if (chid.Gender >= 0)
            perValue += 12.5;
        if (chid.Photo)
            perValue += 12.5
        if (chid.miResultDate) {
            if (chid.miResultDate.length > 0)
                perValue += 12.5
        }
        if (chid.homResultDate) {
            if (chid.homResultDate.length > 0)
                perValue += 12.5
        }
    }
    return perValue;
}

function getDateRange(dateRangeType) {
    var curr = new Date;
    if (dateRangeType == 'ThisWeek') {
        var first = curr.getDate() - curr.getDay(); // First day is the day of the month - the day of the week
        var last = first + 6; // last day is the first day + 6
        var firstday = new Date(curr.setDate(first + 1)).toUTCString();
        var lastday = new Date(curr.setDate(last)).toUTCString();
        return firstday + '-' + lastday;
    }
    else if (dateRangeType == 'ThisWeekend') {
        var first = curr.getDate() - curr.getDay(); /// First day is the day of the month - the day of the week
        var Saturday = first + 6; // last day is the first day + 6
        var Sunday = first;
        var firstday = new Date(new Date(curr).setDate(Saturday));
        var lastday = new Date(new Date(curr).setDate(Sunday + 7));
        return firstday + '-' + lastday;
    }
    else if (dateRangeType == 'NextWeek') {
        var date = (curr.getDate() + 7);
        var day = (curr.getDay());
        var first = ((date) - (day)) + 1; /// First day is the day of the month - the day of the week
        var last = first + 6;
        var firstday = new Date(new Date(curr).setDate(first));
        var lastday = new Date(new Date(curr).setDate(last));
        return firstday + '-' + lastday;
    }
    else if (dateRangeType == 'NextWeekend') {
        var date = (curr.getDate() + 7);
        var day = (curr.getDay());
        var first = (date) - (day); /// First day is the day of the month - the day of the week
        var Saturday = first + 6; // last day is the first day + 6
        var Sunday = first;// last day is the first day + 7
        var firstday = new Date(new Date(curr).setDate(Saturday));
        var lastday = new Date(new Date(curr).setDate(Sunday + 7));
        return firstday + '-' + lastday;
    }
}


function logout() {
    swal({
        title: "Sorry!",
        text: "Your session has expired. Please log in again to continue.",
        type: "warning",
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Ok',
        closeOnConfirm: false,
    },
 function (isConfirm) {
     if (isConfirm) {
         localStorage.sessionTimeoutObj = null;
         window.location = "/";
     }
 });
}
function EndSession() {
    window.location = "/";
}
function alertToUser() {
    today = new Date();
    $("#sessonModalId").modal({ backdrop: 'static', keyboard: false })
    var sessionTimeout = parseInt($("#hdnSessionBuffer").val()) * 60 * 1000;
    countDownDate = today.setMilliseconds(sessionTimeout);
    clearInterval(timerInterval);
    localStorage.sessionTimeoutObj = window.setTimeout(function () {
        localStorage.sessionTimeoutObj = null;
        window.location = "/";
    }, sessionTimeout);
    setIntervalTime();
}
function ResetSessionAnyUserEvent() {
    var sessionTimeout = parseInt($("#hdnSessionTime").val() - $("#hdnSessionBuffer").val()) * 60 * 1000;
    today = new Date();
    countDownDate = today.setMilliseconds(sessionTimeout);
    clearInterval(timerInterval);
    if (localStorage.sessionTimeoutObj) {
        data = window.clearTimeout(localStorage.sessionTimeoutObj);
        localStorage.sessionTimeoutObj = window.setTimeout(function() {
            window.clearTimeout(localStorage.sessionTimeoutObj);
            alertToUser();
        }, sessionTimeout);
    }
    else {
        localStorage.sessionTimeoutObj = window.setTimeout(function() {
            window.clearTimeout(localStorage.sessionTimeoutObj)
            alertToUser();
        }, sessionTimeout);
    }
    setIntervalTime();
}
// check session on back button call
function CheckExistSession() {
    var url = "/Account/CheckExistSession";
    var result = _AjaxWithOutData(url, "GET");
    if (result != null) {
        if (result.Status == "200" && result.isvalidResult == true)
            window.location.href = "/";
    }
    else
        window.location.href = "/";
}