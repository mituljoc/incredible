﻿(function ($) {
    var methods = {
        init: function (options) {
            var defaults = { indicators: !0, height: 400, transition: 500, interval: 6000 }; options = $.extend(defaults, options); return this.each(function () {
                var $this = $(this); var $slider = $this.find('ul.slides').first(); var $slides = $slider.find('li'); var $active_index = $slider.find('.active').index(); var $active; if ($active_index != -1) { $active = $slides.eq($active_index) }
                function captionTransition(caption, duration) {
                    if (caption.hasClass("center-align")) { caption.velocity({ opacity: 0, translateY: -100 }, { duration: duration, queue: !1 }) }
                    else if (caption.hasClass("right-align")) { caption.velocity({ opacity: 0, translateX: 100 }, { duration: duration, queue: !1 }) }
                    else if (caption.hasClass("left-align")) { caption.velocity({ opacity: 0, translateX: -100 }, { duration: duration, queue: !1 }) }
                }
                function moveToSlide(index) {
                    if (index >= $slides.length) index = 0; else if (index < 0) index = $slides.length - 1; $active_index = $slider.find('.active').index(); if ($active_index != index) {
                        $active = $slides.eq($active_index); $caption = $active.find('.caption'); $active.removeClass('active'); $active.velocity({ opacity: 0 }, { duration: options.transition, queue: !1, easing: 'easeOutQuad', complete: function () { $slides.not('.active').velocity({ opacity: 0, translateX: 0, translateY: 0 }, { duration: 0, queue: !1 }) } }); captionTransition($caption, options.transition); if (options.indicators) { $indicators.eq($active_index).removeClass('active') }
                        $slides.eq(index).velocity({ opacity: 1 }, { duration: options.transition, queue: !1, easing: 'easeOutQuad' }); $slides.eq(index).find('.caption').velocity({ opacity: 1, translateX: 0, translateY: 0 }, { duration: options.transition, delay: options.transition, queue: !1, easing: 'easeOutQuad' }); $slides.eq(index).addClass('active'); if (options.indicators) { $indicators.eq(index).addClass('active') }
                    }
                }
                if (!$this.hasClass('fullscreen')) {
                    if (options.indicators) { $this.height(options.height + 40) }
                    else { $this.height(options.height) }
                    $slider.height(options.height)
                }
                $slides.find('.caption').each(function () { captionTransition($(this), 0) }); $slides.find('img').each(function () { $(this).css('background-image', 'url(' + $(this).attr('src') + ')'); $(this).attr('src', 'data:image/gif;base64,R0lGODlhAQABAIABAP///wAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==') }); if (options.indicators) { var $indicators = $('<ul class="indicators"></ul>'); $slides.each(function (index) { var $indicator = $('<li class="indicator-item"></li>'); $indicator.click(function () { var $parent = $slider.parent(); var curr_index = $parent.find($(this)).index(); moveToSlide(curr_index); clearInterval($interval); $interval = setInterval(function () { $active_index = $slider.find('.active').index(); if ($slides.length == $active_index + 1) $active_index = 0; else $active_index += 1; moveToSlide($active_index) }, options.transition + options.interval) }); $indicators.append($indicator) }); $this.append($indicators); $indicators = $this.find('ul.indicators').find('li.indicator-item') }
                if ($active) { $active.show() }
                else { $slides.first().addClass('active').velocity({ opacity: 1 }, { duration: options.transition, queue: !1, easing: 'easeOutQuad' }); $active_index = 0; $active = $slides.eq($active_index); if (options.indicators) { $indicators.eq($active_index).addClass('active') } }
                $active.find('img').each(function () { $active.find('.caption').velocity({ opacity: 1, translateX: 0, translateY: 0 }, { duration: options.transition, queue: !1, easing: 'easeOutQuad' }) }); $interval = setInterval(function () { $active_index = $slider.find('.active').index(); moveToSlide($active_index + 1) }, options.transition + options.interval); var panning = !1; var swipeLeft = !1; var swipeRight = !1; $this.hammer({ prevent_default: !1 }).bind('pan', function (e) {
                    if (e.gesture.pointerType === "touch") {
                        clearInterval($interval); var direction = e.gesture.direction; var x = e.gesture.deltaX; var velocityX = e.gesture.velocityX; $curr_slide = $slider.find('.active'); $curr_slide.velocity({ translateX: x }, { duration: 50, queue: !1, easing: 'easeOutQuad' }); if (direction === 4 && (x > ($this.innerWidth() / 2) || velocityX < -0.65)) { swipeRight = !0 }
                        else if (direction === 2 && (x < (-1 * $this.innerWidth() / 2) || velocityX > 0.65)) { swipeLeft = !0 }
                        var next_slide; if (swipeLeft) {
                            next_slide = $curr_slide.next(); if (next_slide.length === 0) { next_slide = $slides.first() }
                            next_slide.velocity({ opacity: 1 }, { duration: 300, queue: !1, easing: 'easeOutQuad' })
                        }
                        if (swipeRight) {
                            next_slide = $curr_slide.prev(); if (next_slide.length === 0) { next_slide = $slides.last() }
                            next_slide.velocity({ opacity: 1 }, { duration: 300, queue: !1, easing: 'easeOutQuad' })
                        }
                    }
                }).bind('panend', function (e) {
                    if (e.gesture.pointerType === "touch") {
                        $curr_slide = $slider.find('.active'); panning = !1; curr_index = $slider.find('.active').index(); if (!swipeRight && !swipeLeft) { $curr_slide.velocity({ translateX: 0 }, { duration: 300, queue: !1, easing: 'easeOutQuad' }) }
                        else if (swipeLeft) { moveToSlide(curr_index + 1); $curr_slide.velocity({ translateX: -1 * $this.innerWidth() }, { duration: 300, queue: !1, easing: 'easeOutQuad', complete: function () { $curr_slide.velocity({ opacity: 0, translateX: 0 }, { duration: 0, queue: !1 }) } }) }
                        else if (swipeRight) { moveToSlide(curr_index - 1); $curr_slide.velocity({ translateX: $this.innerWidth() }, { duration: 300, queue: !1, easing: 'easeOutQuad', complete: function () { $curr_slide.velocity({ opacity: 0, translateX: 0 }, { duration: 0, queue: !1 }) } }) }
                        swipeLeft = !1; swipeRight = !1; clearInterval($interval); $interval = setInterval(function () { $active_index = $slider.find('.active').index(); if ($slides.length == $active_index + 1) $active_index = 0; else $active_index += 1; moveToSlide($active_index) }, options.transition + options.interval)
                    }
                }); $this.on('sliderPause', function () { clearInterval($interval) }); $this.on('sliderStart', function () { clearInterval($interval); $interval = setInterval(function () { $active_index = $slider.find('.active').index(); if ($slides.length == $active_index + 1) $active_index = 0; else $active_index += 1; moveToSlide($active_index) }, options.transition + options.interval) })
            })
        }, pause: function () { $(this).trigger('sliderPause') }, start: function () { $(this).trigger('sliderStart') }
    }; $.fn.slider = function (methodOrOptions) { if (methods[methodOrOptions]) { return methods[methodOrOptions].apply(this, Array.prototype.slice.call(arguments, 1)) } else if (typeof methodOrOptions === 'object' || !methodOrOptions) { return methods.init.apply(this, arguments) } else { $.error('Method ' + methodOrOptions + ' does not exist on jQuery.tooltip') } }
}); (function ($) {
    'use strict'; var Waves = Waves || {}; var $$ = document.querySelectorAll.bind(document); function isWindow(obj) { return obj !== null && obj === obj.window }
    function getWindow(elem) { return isWindow(elem) ? elem : elem.nodeType === 9 && elem.defaultView }
    function offset(elem) {
        var docElem, win, box = { top: 0, left: 0 }, doc = elem && elem.ownerDocument; docElem = doc.documentElement; if (typeof elem.getBoundingClientRect !== typeof undefined) { box = elem.getBoundingClientRect() }
        win = getWindow(doc); return { top: box.top + win.pageYOffset - docElem.clientTop, left: box.left + win.pageXOffset - docElem.clientLeft }
    }
    function convertStyle(obj) {
        var style = ''; for (var a in obj) { if (obj.hasOwnProperty(a)) { style += (a + ':' + obj[a] + ';') } }
        return style
    }
    var Effect = {
        duration: 750, show: function (e, element) {
            if (e.button === 2) { return !1 }
            var el = element || this; var ripple = document.createElement('div'); ripple.className = 'waves-ripple'; el.appendChild(ripple); var pos = offset(el); var relativeY = (e.pageY - pos.top); var relativeX = (e.pageX - pos.left); var scale = 'scale(' + ((el.clientWidth / 100) * 10) + ')'; if ('touches' in e) { relativeY = (e.touches[0].pageY - pos.top); relativeX = (e.touches[0].pageX - pos.left) }
            ripple.setAttribute('data-hold', Date.now()); ripple.setAttribute('data-scale', scale); ripple.setAttribute('data-x', relativeX); ripple.setAttribute('data-y', relativeY); var rippleStyle = { 'top': relativeY + 'px', 'left': relativeX + 'px' }; ripple.className = ripple.className + ' waves-notransition'; ripple.setAttribute('style', convertStyle(rippleStyle)); ripple.className = ripple.className.replace('waves-notransition', ''); rippleStyle['-webkit-transform'] = scale; rippleStyle['-moz-transform'] = scale; rippleStyle['-ms-transform'] = scale; rippleStyle['-o-transform'] = scale; rippleStyle.transform = scale; rippleStyle.opacity = '1'; rippleStyle['-webkit-transition-duration'] = Effect.duration + 'ms'; rippleStyle['-moz-transition-duration'] = Effect.duration + 'ms'; rippleStyle['-o-transition-duration'] = Effect.duration + 'ms'; rippleStyle['transition-duration'] = Effect.duration + 'ms'; rippleStyle['-webkit-transition-timing-function'] = 'cubic-bezier(0.250, 0.460, 0.450, 0.940)'; rippleStyle['-moz-transition-timing-function'] = 'cubic-bezier(0.250, 0.460, 0.450, 0.940)'; rippleStyle['-o-transition-timing-function'] = 'cubic-bezier(0.250, 0.460, 0.450, 0.940)'; rippleStyle['transition-timing-function'] = 'cubic-bezier(0.250, 0.460, 0.450, 0.940)'; ripple.setAttribute('style', convertStyle(rippleStyle))
        }, hide: function (e) {
            TouchHandler.touchup(e); var el = this; var width = el.clientWidth * 1.4; var ripple = null; var ripples = el.getElementsByClassName('waves-ripple'); if (ripples.length > 0) { ripple = ripples[ripples.length - 1] } else { return !1 }
            var relativeX = ripple.getAttribute('data-x'); var relativeY = ripple.getAttribute('data-y'); var scale = ripple.getAttribute('data-scale'); var diff = Date.now() - Number(ripple.getAttribute('data-hold')); var delay = 350 - diff; if (delay < 0) { delay = 0 }
            setTimeout(function () { var style = { 'top': relativeY + 'px', 'left': relativeX + 'px', 'opacity': '0', '-webkit-transition-duration': Effect.duration + 'ms', '-moz-transition-duration': Effect.duration + 'ms', '-o-transition-duration': Effect.duration + 'ms', 'transition-duration': Effect.duration + 'ms', '-webkit-transform': scale, '-moz-transform': scale, '-ms-transform': scale, '-o-transform': scale, 'transform': scale, }; ripple.setAttribute('style', convertStyle(style)); setTimeout(function () { try { el.removeChild(ripple) } catch (e) { return !1 } }, Effect.duration) }, delay)
        }, wrapInput: function (elements) {
            for (var a = 0; a < elements.length; a++) {
                var el = elements[a]; if (el.tagName.toLowerCase() === 'input') {
                    var parent = el.parentNode; if (parent.tagName.toLowerCase() === 'i' && parent.className.indexOf('waves-effect') !== -1) { continue }
                    var wrapper = document.createElement('i'); wrapper.className = el.className + ' waves-input-wrapper'; var elementStyle = el.getAttribute('style'); if (!elementStyle) { elementStyle = '' }
                    wrapper.setAttribute('style', elementStyle); el.className = 'waves-button-input'; el.removeAttribute('style'); parent.replaceChild(wrapper, el); wrapper.appendChild(el)
                }
            }
        }
    }; var TouchHandler = {
        touches: 0, allowEvent: function (e) {
            var allow = !0; if (e.type === 'touchstart') { TouchHandler.touches += 1 } else if (e.type === 'touchend' || e.type === 'touchcancel') { setTimeout(function () { if (TouchHandler.touches > 0) { TouchHandler.touches -= 1 } }, 500) } else if (e.type === 'mousedown' && TouchHandler.touches > 0) { allow = !1 }
            return allow
        }, touchup: function (e) { TouchHandler.allowEvent(e) }
    }; function getWavesEffectElement(e) {
        if (TouchHandler.allowEvent(e) === !1) { return null }
        var element = null; var target = e.target || e.srcElement; while (target.parentElement !== null) {
            if (!(target instanceof SVGElement) && target.className.indexOf('waves-effect') !== -1) { element = target; break } else if (target.classList.contains('waves-effect')) { element = target; break }
            target = target.parentElement
        }
        return element
    }
    function showEffect(e) {
        var element = getWavesEffectElement(e); if (element !== null) {
            Effect.show(e, element); if ('ontouchstart' in window) { element.addEventListener('touchend', Effect.hide, !1); element.addEventListener('touchcancel', Effect.hide, !1) }
            element.addEventListener('mouseup', Effect.hide, !1); element.addEventListener('mouseleave', Effect.hide, !1)
        }
    }
    Waves.displayEffect = function (options) {
        options = options || {}; if ('duration' in options) { Effect.duration = options.duration }
        Effect.wrapInput($$('.waves-effect')); if ('ontouchstart' in window) { document.body.addEventListener('touchstart', showEffect, !1) }
        document.body.addEventListener('mousedown', showEffect, !1)
    }; Waves.attach = function (element) {
        if (element.tagName.toLowerCase() === 'input') { Effect.wrapInput([element]); element = element.parentElement }
        if ('ontouchstart' in window) { element.addEventListener('touchstart', showEffect, !1) }
        element.addEventListener('mousedown', showEffect, !1)
    }; window.Waves = Waves; document.addEventListener('DOMContentLoaded', function () { Waves.displayEffect() }, !1)
})