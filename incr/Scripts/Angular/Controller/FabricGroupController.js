﻿var app;
(function () {
    app = angular.module("FCTool", ['ngRoute', 'angularUtils.directives.dirPagination']);
})();
app.service('myService', function ($http) {
    this.GetAllFabricGroup = function () {
        var url = 'FabricGroup/GetAllFabricGroup';
        return $http({ method: 'GET', url: url });
    }
    this.GetFabricGroupById = function (fabricGroupId) {
        return $http.get("FabricGroup/GetFabricGroupById?fabricGroupId=" + fabricGroupId);
    }
    this.AddFabricGroup = function (fabricGroupObj) {
        var url = '/FabricGroup/AddFabricGroup/';
        return $http({ method: 'POST', url: url, data: fabricGroupObj });
    }
    this.UpdateFabricGroup = function (fabricGroupObj) {
        var url = '/FabricGroup/UpdateFabricGroup/';
        return $http({ method: 'POST', url: url, data: fabricGroupObj });
    }
    this.DeleteFabricGroup = function (fabricGroupId) {
        var url = '/FabricGroup/DeleteFabricGroup?fabricGroupId=' + fabricGroupId;
        return $http({ method: 'POST', url: url });
    }
});
/*************** Created by Rajesh Gami *******************/
app.controller("FabricGroupController", function ($scope, $timeout, $rootScope, $window, $http, myService, $filter) {
    $scope.getFabricGroupByIdObj = {};
    $scope.fabricGroupDetail = {};
    $scope.fabricGroupId = 0;
    //$scope.fabricGroupDetail.Type = false;
    $scope.init = function () {
        $scope.GetAllFabricGroup();
    }
    /********************Get All FabricGroup *********************/
    $scope.GetAllFabricGroup = function () {
        $scope.isLoading = true;
        myService.GetAllFabricGroup().then(function (response) {
            if (response.data.status == "200") {
                $scope.GetAllFabricGroupList = response.data.rows;
                $scope.GetAllFabricGroupList.CreatedDate = moment($scope.GetAllFabricGroupList.CreatedDate).format('DD/MM/YYYY');
            }
            else if (response.data.status == "500") {
                window.location.href = "/Home/Error";
            }
        });
        $scope.isLoading = false;
    }
    /********************Add FabricGroup*********************/
    $scope.AddFabricGroup = function (isModalOpen) {
        if (isModalOpen) {

            $('#addFabricGroupModal').modal('show')
        } else {
            if ($scope.fabricGroupDetail.FabricGroup != null && $scope.fabricGroupDetail.FabricGroup != undefined && $scope.fabricGroupDetail.FabricGroup != '') {
                myService.AddFabricGroup($scope.fabricGroupDetail).then(function (response) {
                    if (response.data.status == "200") {
                        $('#addFabricGroupModal').modal('hide');
                        swal({ title: "Added!", text: "Your record has been added.", type: "success" },
                            function () { window.location = '/FabricGroup'; });
                    } else if (response.data.status == "500") {
                        window.location.href = "/Home/Error";
                    }
                });
            } else {
                swal("Please enter Fabric group name");
            }
       
        }
    }
    /*************Open edit modal for update******************/
    $scope.fabricGroupEditModal = function (fabricGroupId) {
        myService.GetFabricGroupById(fabricGroupId).then(function (result) {
            if (result.data.status == "200") {
                $scope.getFabricGroupByIdObj = result.data.item;
                $scope.fabricGroupDetail = $scope.getFabricGroupByIdObj;
                $scope.fabricGroupDetail.CreatedDate = moment($scope.getFabricGroupByIdObj.CreatedDate).format('YYYY-MM-DD HH:mm');
                $('#editFabricGroupModal').modal('show');
            } else if (result.data.status == "500") {
                window.location.href = "/Home/Error";
            }
        });
    }
    /******************** Update FabricGroup *********************/
    $scope.UpdateFabricGroup = function (fabricGroupDetail) {
        if ($scope.fabricGroupDetail.FabricGroup != null && $scope.fabricGroupDetail.FabricGroup != undefined && $scope.fabricGroupDetail.FabricGroup != '') {
            myService.UpdateFabricGroup(fabricGroupDetail).then(function (result) {
                if (result.data.status == "200") {
                    $('#editFabricGroupModal').modal('hide');
                    swal("Data updated successfully");
                    window.location = '/FabricGroup';
                } else if (result.data.status == "500") { window.location.href = "/Home/Error"; }
            });
        } else {
            swal("Please enter Fabric group name");
        }
    }
    /****************Virtual Delete FabricGroup******************/
    $scope.DeleteFabricGroup = function (fabricGroupId) {
        myService.DeleteFabricGroup(fabricGroupId).then(function (result) {
            if (result.data.status == "200") {
                setTimeout(function () {
                    swal({
                        title: "Are you sure you want to delete ?",
                        text: "You will not be able to recover this record!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, delete it!",
                        closeOnConfirm: false
                    },
                    function () {
                        swal({ title: "Deleted!", text: "Your record has been deleted.", type: "success" },
                            function () { window.location = '/FabricGroup'; });

                    });
                }, 1000);

            } else if (result.data.status == "500") { window.location.href = "/Home/Error"; }
        });
    }
    /********************Sort and filter*********************/
    $scope.sort = function (keyname) {
        $scope.sortKey = keyname;   //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    }
});