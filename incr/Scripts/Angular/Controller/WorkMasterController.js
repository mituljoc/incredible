﻿var app;
(function () {
    app = angular.module("FCTool", ['ngRoute', 'angularUtils.directives.dirPagination']);
})();

app.service('myService', function ($http) {
    this.getAllWorkMaster = function () {
        var url = 'WorkMaster/GetAllWorkMaster';
        return $http({ method: 'GET', url: url });
    }
    this.getAllDepartment = function () {
        return $http.get("WorkMaster/GetAllDepartment");
    }
    this.getWorkMasterById = function (workMasterId) {
        return $http.get("WorkMaster/GetWorkMasterById?workMasterId=" + workMasterId);
    }
    this.addNewWorkMaster = function (workMasterObj) {
        var url = '/WorkMaster/AddNewWorkMaster/';
        return $http({ method: 'POST', url: url, data: workMasterObj });
    }
    this.updateWorkMaster = function (workMasterObj) {
        var url = '/WorkMaster/UpdateWorkMaster/';
        return $http({ method: 'POST', url: url, data: workMasterObj });
    }
    this.deleteWorkMaster = function (workMasterId) {
        var url = '/WorkMaster/DeleteWorkMaster?workMasterId=' + workMasterId;
        return $http({ method: 'POST', url: url });
    }
    //this.get = function (ESTQ_Id) {
    //    return $http.get("/api/Quiz/" + ESTQ_Id);
    //}
});

app.controller("WorkMasterController", function ($scope, $timeout, $rootScope, $window, $http, myService, $filter) {

    $scope.getWorkMasterByIdObj = {};
    $scope.workMasterDetail = {};
    $scope.workMasterDetail.DepartmentIds = [];
    $scope.workMasterDetail.DepartmentIdList = [];
    $scope.departmentCount = 0;
    $scope.GetAllDepartmentList = [];
    $scope.workMasterId = 0;
    $scope.myDetail = {};
    $scope.init = function () {
        $scope.getWorkMasterList();
    }
    $scope.getWorkMasterList = function () {
        $scope.isWorkMasterLoading = true;
        myService.getAllWorkMaster().then(function (response) {
            if (response.data.status == "200") {
                $scope.GetAllWorkMasterList = response.data.rows;
            }
            else if (response.data.status == "500") {
                window.location.href = "/Home/Error";
            }
        });
        $scope.isWorkMasterLoading = false;
    }
    $scope.addNewWorkMaster = function (isModalOpen) {
        if (isModalOpen) {
            //$('#addOperatorModal').modal('show')
            myService.getAllDepartment().then(function (response) {
                if (response.data.status == "200") {
                    $scope.GetAllDepartmentList = response.data.rows;
                    $('#addWorkMasterModal').modal('show');
                } else if (response.data.status == "500") {
                    window.location.href = "/Home/Error";
                }
            });
        } else {

            angular.forEach($scope.workMasterDetail.DepartmentIds, function (value, key) {
                var ids = key;
                $scope.workMasterDetail.DepartmentIdList.push(ids);
            });
            $scope.workMasterDetail.Department = $scope.workMasterDetail.DepartmentIdList.toString();
            myService.addNewWorkMaster($scope.workMasterDetail).then(function (response) {
                if (response.data.status == "200") {
                    $('#addWorkMasterModal').modal('hide');
                    swal({ title: "Added!", text: "Your record has been added.", type: "success" },
                        function () { window.location = '/WorkMaster'; });
                } else if (response.data.status == "500") {
                    window.location.href = "/Home/Error";
                }
            });
            //}
        }
    }
    $scope.DeptArray = [];
    $scope.workMasterEditModal = function (workMasterId) {
        myService.getWorkMasterById(workMasterId).then(function (result) {
            if (result.data.status == "200") {
                $scope.getWorkMasterByIdObj = result.data.item;
                $scope.workMasterDetail = $scope.getWorkMasterByIdObj;
                $scope.DeptArray = $scope.workMasterDetail.Department.split(",")
                $scope.workMasterDetail.CreatedDate = moment($scope.getWorkMasterByIdObj.CreatedDate).format('YYYY-MM-DD HH:mm');;
                myService.getAllDepartment().then(function (response) {
                    if (response.data.status == "200") {
                        $scope.GetAllDepartmentList = response.data.rows;
                        $scope.departmentCount = $scope.GetAllDepartmentList.length;
                        $scope.workMasterDetail.DepartmentIds = [];
                        for (var i = 0; i <= ($scope.departmentCount) ; i++) {
                            var val = false;
                            angular.forEach($scope.DeptArray, function (value, key) {
                                if (i == parseInt(value)) {
                                    //var ids = key;
                                    val = true;
                                }
                            });
                            $scope.workMasterDetail.DepartmentIds.push(val);
                        }
                        $('#editWorkMasterModal').modal('show');
                    } else if (response.data.status == "500") {
                        window.location.href = "/Home/Error";
                    }
                });


            } else if (result.data.status == "500") {
                window.location.href = "/Home/Error";
            }
        });
    }

    $scope.updateWorkMaster = function (workMasterDetail) {
        $scope.workMasterDetail.DepartmentIdList = [];
        angular.forEach($scope.workMasterDetail.DepartmentIds, function (value, key) {
            var ids = key;
            if (value == true && key != 0) {
                $scope.workMasterDetail.DepartmentIdList.push(ids);
            }
            
        });
        $scope.workMasterDetail.Department = $scope.workMasterDetail.DepartmentIdList.toString();
        myService.updateWorkMaster(workMasterDetail).then(function (result) {
            if (result.data.status == "200") {
                $('#editWorkMasterModal').modal('hide');
                swal("Data updated successfully");
                window.location = '/WorkMaster';

            } else if (result.data.status == "500") {
                window.location.href = "/Home/Error";
            }
        });
    }

    $scope.removeWorkMaster = function (workMasterId) {
        myService.deleteWorkMaster(workMasterId).then(function (result) {
            if (result.data.status == "200") {
                //$('#deleteOperatorModal').modal('hide');
                setTimeout(function () {
                    swal({
                        title: "Are you sure you want to delete ?",
                        text: "You will not be able to recover this record!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, delete it!",
                        closeOnConfirm: false
                    },
                    function () {
                        swal({ title: "Deleted!", text: "Your record has been deleted.", type: "success" },
                            function () { window.location = '/WorkMaster'; });

                    });
                    // swal("Data deleted successfully");
                }, 1000);

            } else if (result.data.status == "500") {
                window.location.href = "/Home/Error";
            }
        });
    }

    $scope.sort = function (keyname) {
        $scope.sortKey = keyname;   //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    }
});