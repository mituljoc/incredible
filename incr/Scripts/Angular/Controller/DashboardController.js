﻿var app;
(function () {
    app = angular.module("FCTool", ['ngRoute', 'angularUtils.directives.dirPagination']);
})();

app.service('myService', function ($http) {
    this.GetOrderList = function () {
        var url = 'Dashboard/GetOrderList';
        return $http({ method: 'GET', url: url });
    }
    this.GetCarpentaryList = function () {
        var url = 'Dashboard/GetCarpentaryList';
        return $http({ method: 'GET', url: url });
    }
    this.GetLiningList = function () {
        var url = 'Dashboard/GetLiningList';
        return $http({ method: 'GET', url: url });
    }
    this.GetFittingList = function () {
        var url = 'Dashboard/GetFittingList';
        return $http({ method: 'GET', url: url });
    }
    this.GetDeliveryList = function () {
        var url = 'Dashboard/GetDeliveryList';
        return $http({ method: 'GET', url: url });
    }
    this.GetInvoiceList = function () {
        var url = 'Dashboard/GetInvoiceList';
        return $http({ method: 'GET', url: url });
    }
    this.GetLeaderBoardList = function () {
        var url = 'Dashboard/GetLeaderBoardList';
        return $http({ method: 'GET', url: url });
    }
    this.GetHighestCustomerOrderList = function () {
        var url = 'Dashboard/GetHighestCustomerOrderList';
        return $http({ method: 'GET', url: url });
    }
    this.GetComplaintByOperatorList = function () {
        var url = 'Dashboard/GetComplaintByOperatorList';
        return $http({ method: 'GET', url: url });
    }
   
});

app.controller("DashboardController", function ($scope, $timeout, $rootScope, $window, $http, myService, $filter) {

    $scope.init = function () {
        $scope.GetOrderList();
        $scope.GetCarpentaryList();
        $scope.GetLiningList();
        $scope.GetFittingList();
        $scope.GetDeliveryList();
        $scope.GetInvoiceList();
        $scope.GetLeaderBoardList();
        $scope.GetHighestCustomerOrderList();
        $scope.GetComplaintByOperatorList();
    }
    /******************** Get All Order *********************/
    $scope.GetOrderList = function () {
        $scope.isLoadingOrd = true;
        myService.GetOrderList().then(function (response) {
            if (response.data.status == "200") {
                $scope.totalOrderList = response.data.rows;
                $scope.orderCount = $scope.totalOrderList.length;
            }
            else if (response.data.status == "500") {
                window.location.href = "/Home/Error";
            }
        });
        $scope.isLoadingOrd = false;
    }
    /******************** Get All Carpentry order list *********************/
    $scope.GetCarpentaryList = function () {
        $scope.isLoadingCar = true;
        myService.GetCarpentaryList().then(function (response) {
            if (response.data.status == "200") {
                $scope.totalCarpentaryList = response.data.rows;
                $scope.carpenteryCount = $scope.totalCarpentaryList.length;
            }
            else if (response.data.status == "500") {
                window.location.href = "/Home/Error";
            }
        });
        $scope.isLoadingCar = false;
    }
    /******************** Get All Lining order list *********************/
    $scope.GetLiningList = function () {
        $scope.isLoadingLin = true;
        myService.GetLiningList().then(function (response) {
            if (response.data.status == "200") {
                $scope.totalLiningList = response.data.rows;
                $scope.liningCount = $scope.totalLiningList.length;
            }
            else if (response.data.status == "500") {
                window.location.href = "/Home/Error";
            }
        });
        $scope.isLoadingLin = false;
    }
    /******************** Get All Fitting order list *********************/
    $scope.GetFittingList = function () {
        $scope.isLoadingFit = true;
        myService.GetFittingList().then(function (response) {
            if (response.data.status == "200") {
                $scope.totalFittingList = response.data.rows;
                $scope.fittingCount = $scope.totalFittingList.length;
            }
            else if (response.data.status == "500") {
                window.location.href = "/Home/Error";
            }
        });
        $scope.isLoadingFit = false;
    }
    /******************** Get All Delivery order list *********************/
    $scope.GetDeliveryList = function () {
        $scope.isLoadingDel = true;
        myService.GetDeliveryList().then(function (response) {
            if (response.data.status == "200") {
                $scope.totalDeliveryList = response.data.rows;
                $scope.deliveryCount = $scope.totalDeliveryList.length;
            }
            else if (response.data.status == "500") {
                window.location.href = "/Home/Error";
            }
        });
        $scope.isLoadingDel = false;
    }
    /******************** Get All Invoice order list *********************/
    $scope.GetInvoiceList = function () {
        $scope.isLoadingInv = true;
        myService.GetInvoiceList().then(function (response) {
            if (response.data.status == "200") {
                $scope.totalInvoiceList = response.data.rows;
                $scope.invoiceCount = $scope.totalInvoiceList.length;
            }
            else if (response.data.status == "500") {
                window.location.href = "/Home/Error";
            }
        });
        $scope.isLoadingInv = false;
    }

    /******************** Get LeaderBOard *********************/
    $scope.GetLeaderBoardList = function () {
        $scope.isLoadingLea = true;
        myService.GetLeaderBoardList().then(function (response) {
            if (response.data.status == "200") {
                $scope.leaderBoardList = response.data.rows;
                $scope.leaderBoardCount = $scope.leaderBoardList.length;
            }
            else if (response.data.status == "500") {
                window.location.href = "/Home/Error";
            }
        });
        $scope.isLoadingLea = false;
    }

    /******************** Get Highest Customer Order*********************/
    $scope.GetHighestCustomerOrderList = function () {
        $scope.isLoadingHig = true;
        myService.GetHighestCustomerOrderList().then(function (response) {
            if (response.data.status == "200") {
                $scope.highestCustomerOrder = response.data.rows;
                $scope.highestCustomerOrderCount = $scope.highestCustomerOrder.length;
            }
            else if (response.data.status == "500") {
                window.location.href = "/Home/Error";
            }
        });
        $scope.isLoadingHig = false;
    }

    /******************** Get Complaint by operator*********************/
    $scope.GetComplaintByOperatorList = function () {
        $scope.isLoadingCom = true;
        myService.GetComplaintByOperatorList().then(function (response) {
            if (response.data.status == "200") {
                $scope.complaintByOperatorList = response.data.rows;
                $scope.complaintByOperatorListCount = $scope.complaintByOperatorList.length;
            }
            else if (response.data.status == "500") {
                window.location.href = "/Home/Error";
            }
        });
        $scope.isLoadingCom = false;
    }

});