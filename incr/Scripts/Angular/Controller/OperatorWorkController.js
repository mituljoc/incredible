﻿/// <reference path="OperatorWorkController.js" />
var app;
(function () {
    app = angular.module("FCTool", ['ngRoute', 'angularUtils.directives.dirPagination']);
})();

app.service('myService', function ($http) {
    this.getAllOperatorWork = function () {
        var url = 'OperatorWorkMaster/GetAllOpeartorWorkMaster';
        return $http({ method: 'GET', url: url });
    }
    this.getAllDepartment = function () {
        return $http.get("WorkMaster/GetAllDepartment");
    }
    this.getAllOperator = function () {
        var url = 'Operator/GetAllOperator';
        return $http({ method: 'GET', url: url });
    }
    this.getAllWorkMaster = function () {
        var url = 'WorkMaster/GetAllWorkMaster';
        return $http({ method: 'GET', url: url });
    }
    this.getAllPrice_list = function () {
        var url = 'WorkMaster/GetAllPrice_list';
        return $http({ method: 'GET', url: url });
    }
    this.getWorkMasterById = function (operatorWorkMasterId) {
        return $http.get("OperatorWorkMaster/GetWorkMasterById?operatorWorkMasterId=" + operatorWorkMasterId);
    }

  
    this.addNewOperatorWorkMaster = function (workMasterObj) {
        debugger;
        var url = "/OperatorWorkMaster/AddNewOperatorWorkMaster/";

     
        return $http({
            url: url, method: 'POST', data: { data: JSON.stringify(workMasterObj) }});
    }
    this.updateWorkMaster = function (workMasterObj) {
        var url = '/OperatorWorkMaster/UpdateWorkMaster/';
        return $http({ method: 'POST', url: url, data: { data: JSON.stringify(workMasterObj) } });
    }
    this.deleteWorkMaster = function (workMasterId) {
        var url = '/OperatorWorkMaster/DeleteWorkMaster?workMasterId=' + workMasterId;
        return $http({ method: 'POST', url: url });
    }
});

app.controller("OperatorWorkController", function ($scope, $timeout, $rootScope, $window, $http, myService, $filter) {
    $scope.operatorWorkMasterDetail = {};
    $scope.operatorWorkMasterDetail.DepartmentIds = [];
    $scope.operatorWorkMasterDetail.DepartmentIdList = [];
    $scope.operatorWorkMasterEditDetail = {};
    $scope.priceListDetail = {};
    $scope.operatorWorkMasterEditDetail.DepartmentIds = [];
    $scope.operatorWorkMasterEditDetail.DepartmentIdList = [];
    $scope.GetAllOperationList = [];
    $scope.departmentCount = 0;
    $scope.init = function () {
        $scope.getAllOperatorWorkList();
    }
    /* Get all operator workMaster list */
    $scope.getAllOperatorWorkList = function () {
        $scope.isOperatorLoading = true;
        myService.getAllOperatorWork().then(function (response) {
            if (response.data.status == "200") {
                $scope.OperatorWorkList = response.data.rows;
            }
            else if (response.data.status == "500") {
                window.location.href = "/Home/Error";
            }
        });
        $scope.isOperatorLoading = false;
    }

    $scope.fillDepartmentCheck = function (operatorId, isAdd) {
        $scope.operatorWorkMasterDetail.FillDepartmentIds = [];
        angular.forEach($scope.GetAllOperationList, function (value, key) {
            var ids = key;
            var Carpentery = false;
            var Linning = false;
            var Packing = false;
            var Delivery = false;
            var Invoice = false;
            var Fitting = false;

            if (value.RID == operatorId) {
                Carpentery = value.Carpentery;
                Linning = value.Linning;
                Packing = value.Packing;
                Delivery = value.Delivery;
                Invoice = value.Invoice;
                Fitting = value.Fitting;

                angular.forEach($scope.GetAllDepartmentList, function (deptVal, key) {
                    if (Carpentery == true && ('Carpentery' == deptVal.departmentName)) {
                        var deptCar = key + 1;
                        $scope.operatorWorkMasterDetail.FillDepartmentIds.push(deptCar);
                    } if (Linning == true && ('Linning' == deptVal.departmentName)) {
                        var deptLin = key + 1;
                        $scope.operatorWorkMasterDetail.FillDepartmentIds.push(deptLin);
                    } if (Packing == true && ('Packing' == deptVal.departmentName)) {
                        var deptPack = key + 1;
                        $scope.operatorWorkMasterDetail.FillDepartmentIds.push(deptPack);
                    } if (Delivery == true && ('Delivery' == deptVal.departmentName)) {
                        var deptDeli = key + 1;
                        $scope.operatorWorkMasterDetail.FillDepartmentIds.push(deptDeli);
                    } if (Invoice == true && ('Invoice' == deptVal.departmentName)) {
                        var deptInv = key + 1;
                        $scope.operatorWorkMasterDetail.FillDepartmentIds.push(deptInv);
                    }
                    if (Fitting == true && ('Fitting' == deptVal.departmentName)) {
                        var deptInv = key + 1;
                        $scope.operatorWorkMasterDetail.FillDepartmentIds.push(deptInv);
                    }
                });
            }
        });
        $scope.operatorWorkMasterDetail.DepartmentIds = [];
        $scope.operatorWorkMasterEditDetail.DepartmentIds = [];
        for (var i = 0; i <= ($scope.departmentCount) ; i++) {
            var val = false;
            angular.forEach($scope.operatorWorkMasterDetail.FillDepartmentIds, function (value, key) {
                if (i == parseInt(value)) {
                    val = true;
                }
            });
            if (isAdd)
                $scope.operatorWorkMasterDetail.DepartmentIds.push(val);
            else
                $scope.operatorWorkMasterEditDetail.DepartmentIds.push(val);

        }
        //$scope.operatorWorkMasterEditDetail.DepartmentIds.push(val);
        //$scope.operatorWorkMasterDetail.DepartmentIds[operatorId] = true;
    }
    $scope.addNewOPeratorWorkMaster = function (isModalOpen) {
        if (isModalOpen) {
            myService.getAllOperator().then(function (response) {
                if (response.data.status == "200") {
                    myService.getAllPrice_list().then(function(responsePrice){
                        if (responsePrice.data.status == "200") {
                            $scope.operatorWorkMasterDetail.OperatorPriceList = responsePrice.data.rows;
                 
                        } else if (responsePrice.data.status == "500") {
                            window.location.href = "/Home/Error";
                        }
                    });
                    $scope.GetAllOperationList = response.data.rows;  //--------- Get All OperatorList
                    $('#addOperatorWorkModal').modal('show');
                    myService.getAllWorkMaster().then(function (response) {
                        if (response.data.status == "200") {
                            $scope.GetAllWorkMasterList = response.data.rows; //--------- Get All WorkMasterList
                            myService.getAllDepartment().then(function (response) {
                                if (response.data.status == "200") {
                                    $scope.GetAllDepartmentList = response.data.rows;  //------- GetAll Department
                                    $scope.departmentCount = $scope.GetAllDepartmentList.length;
                                } else if (response.data.status == "500") {
                                    window.location.href = "/Home/Error";
                                }
                            });
                        }
                        else if (response.data.status == "500") {
                            window.location.href = "/Home/Error";
                        }
                    });
                }
                else if (response.data.status == "500") {
                    window.location.href = "/Home/Error";
                }
            });

        } else {
            if ($scope.operatorWorkMasterDetail.OperatorId == 0 || $scope.operatorWorkMasterDetail.OperatorId == undefined || $scope.operatorWorkMasterDetail.OperatorId == null || $scope.operatorWorkMasterDetail.OperatorId == '') {
                swal("Oops!", "Please select operator", "warning");
            } else {
                if ($scope.operatorWorkMasterDetail.WorkId == 0 || $scope.operatorWorkMasterDetail.WorkId == undefined || $scope.operatorWorkMasterDetail.WorkId == null || $scope.operatorWorkMasterDetail.WorkId == '') {
                    swal("Oops!", "Please select work", "warning");
                }
                //else if ($scope.operatorWorkMasterDetail.Price < 1 || $scope.operatorWorkMasterDetail.Price == undefined || $scope.operatorWorkMasterDetail.Price == null || $scope.operatorWorkMasterDetail.Price == '') {
                //    swal("Oops!", "Please enter price", "warning");
                //}
                else {
                    angular.forEach($scope.operatorWorkMasterDetail.DepartmentIds, function (value, key) {
                        if (value == true) {
                            var ids = key + 1;
                            $scope.operatorWorkMasterDetail.DepartmentIdList.push(ids);
                        }
                    });

                    debugger;

                    //angular.forEach($scope.operatorWorkMasterDetail.PriceId, function (value, key) {
                    //    $scope.operatorWorkMasterDetail.PriceList.push(value)
                    //});

                    //var priceList = $.grep($scope.operatorWorkMasterDetail.PriceId, function (value, key) {
                    //    $scope.operatorWorkMasterDetail.PriceList.push(value)

                    //});
                    $scope.operatorWorkMasterDetail.DepartmentId = $scope.operatorWorkMasterDetail.DepartmentIdList.toString();
                    myService.addNewOperatorWorkMaster($scope.operatorWorkMasterDetail).then(function (response) {
                        if (response.data.status == "200") {
                            $('#addOperatorWorkModal').modal('hide');
                            swal({ title: "Added!", text: "Your record has been added.", type: "success" },
                                function () { window.location = '/OperatorWorkMaster'; });
                        } else if (response.data.status == "500") {
                            window.location.href = "/Home/Error";
                        }
                    });
                }

            }


        }
    }

    $scope.DeptArray = [];
    $scope.workMasterEditModal = function (workMasterId) {
        myService.getWorkMasterById(workMasterId).then(function (result) {
            if (result.data.status == "200") {
                $scope.getWorkMasterByIdObj = result.data.item.workMaster;
                $scope.operatorWorkMasterEditDetail = $scope.getWorkMasterByIdObj;
                $scope.operatorWorkMasterEditDetail.OperatorPriceList = result.data.item.priceList;
                $scope.DeptArray = $scope.operatorWorkMasterEditDetail.DepartmentId.split(",")
                $scope.operatorWorkMasterEditDetail.CreatedDate = moment($scope.getWorkMasterByIdObj.CreatedDate).format('DD/MM/YYYY');
                myService.getAllOperator().then(function (response) {
                    if (response.data.status == "200") {
                        $scope.GetAllOperationList = response.data.rows;  //--------- Get All OperatorList
                        $('#editOperatorWorkModal').modal('show');
                        myService.getAllWorkMaster().then(function (response) {
                            if (response.data.status == "200") {
                                $scope.GetAllWorkMasterList = response.data.rows; //--------- Get All WorkMasterList
                                myService.getAllDepartment().then(function (response) {
                                    if (response.data.status == "200") {
                                        $scope.GetAllDepartmentList = response.data.rows;
                                        $scope.departmentCount = $scope.GetAllDepartmentList.length;
                                        $scope.operatorWorkMasterEditDetail.DepartmentIds = [];
                                        for (var i = 0; i <= ($scope.departmentCount) ; i++) {
                                            var val = false;
                                            angular.forEach($scope.DeptArray, function (value, key) {
                                                if (i == parseInt(value)) {
                                                    val = true;
                                                }
                                            });
                                            $scope.operatorWorkMasterEditDetail.DepartmentIds.push(val);
                                        }
                                    } else if (response.data.status == "500") {
                                        window.location.href = "/Home/Error";
                                    }
                                });
                            }
                            else if (response.data.status == "500") {
                                window.location.href = "/Home/Error";
                            }
                        });
                    }
                    else if (response.data.status == "500") {
                        window.location.href = "/Home/Error";
                    }
                });

            } else if (result.data.status == "500") {
                window.location.href = "/Home/Error";
            }
        });
    }

    $scope.updateOperatorWorkMaster = function (operatorWorkMasterEditDetail) {
        if ($scope.operatorWorkMasterEditDetail.OperatorId == 0 || $scope.operatorWorkMasterEditDetail.OperatorId == undefined || $scope.operatorWorkMasterEditDetail.OperatorId == null || $scope.operatorWorkMasterEditDetail.OperatorId == '') {
            swal("Oops!", "Please select operator", "warning");
        } else if ($scope.operatorWorkMasterEditDetail.WorkId == 0 || $scope.operatorWorkMasterEditDetail.WorkId == undefined || $scope.operatorWorkMasterEditDetail.WorkId == null || $scope.operatorWorkMasterEditDetail.WorkId == '') {
            swal("Oops!", "Please select work", "warning");
        }  else {
            $scope.operatorWorkMasterEditDetail.DepartmentIdList = [];
            angular.forEach($scope.operatorWorkMasterEditDetail.DepartmentIds, function (value, key) {
                var ids = key;
                if (value == true && key != 0) {
                    $scope.operatorWorkMasterEditDetail.DepartmentIdList.push(ids);
                }
            });
            $scope.operatorWorkMasterEditDetail.DepartmentId = $scope.operatorWorkMasterEditDetail.DepartmentIdList.toString();
            myService.updateWorkMaster(operatorWorkMasterEditDetail).then(function (result) {
                if (result.data.status == "200") {
                    $('#editOperatorWorkModal').modal('hide');
                    swal("Data updated successfully");
                    window.location = '/OperatorWorkMaster';

                } else if (result.data.status == "500") {
                    window.location.href = "/Home/Error";
                }
            });
        }

    }

    $scope.removeWorkMaster = function (workMasterId) {
        myService.deleteWorkMaster(workMasterId).then(function (result) {
            if (result.data.status == "200") {
                setTimeout(function () {
                    swal({
                        title: "Are you sure you want to delete ?",
                        text: "You will not be able to recover this record!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, delete it!",
                        closeOnConfirm: false
                    },
                    function () {
                        swal({ title: "Deleted!", text: "Your record has been deleted.", type: "success" },
                            function () { window.location = '/OperatorWorkMaster'; });

                    });
                }, 1000);

            } else if (result.data.status == "500") {
                window.location.href = "/Home/Error";
            }
        });
    }

    $scope.sort = function (keyname) {
        $scope.sortKey = keyname;   //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    }
});