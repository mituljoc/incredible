﻿var app;
(function () {
    app = angular.module("FCTool", ['ngRoute', 'angularUtils.directives.dirPagination']);
})();
app.service('myService', function ($http) {
    this.GetAllFabric = function () {
        var url = 'Fabric/GetAllFabric';
        return $http({ method: 'GET', url: url });
    }
    this.GetFabricById = function (fabricId) {
        return $http.get("Fabric/GetFabricById?fabricId=" + fabricId);
    }
    this.AddFabric = function (fabricObj) {
        var url = '/Fabric/AddFabric/';
        return $http({ method: 'POST', url: url, data: fabricObj });
    }
    this.UpdateFabric = function (fabricObj) {
        var url = '/Fabric/UpdateFabric/';
        return $http({ method: 'POST', url: url, data: fabricObj });
    }
    this.DeleteFabric = function (fabricId) {
        var url = '/Fabric/DeleteFabric?fabricId=' + fabricId;
        return $http({ method: 'POST', url: url });
    }
    this.GetAllFabricGroup = function () {
        var url = 'FabricGroup/GetAllFabricGroup';
        return $http({ method: 'GET', url: url });
    }
});
/*************** Created by Rajesh Gami *******************/
app.controller("FabricController", function ($scope, $timeout, $rootScope, $window, $http, myService, $filter) {
    $scope.getFabricByIdObj = {};
    $scope.fabricDetail = {};
    $scope.fabricId = 0;
    //$scope.fabricDetail.Type = false;
    $scope.init = function () {
        $scope.GetAllFabric();
    }
    /********************Get All Fabric *********************/
    $scope.GetAllFabric = function () {
        $scope.isLoading = true;
        myService.GetAllFabric().then(function (response) {
            if (response.data.status == "200") {
                $scope.GetAllFabricList = response.data.rows;
                $scope.GetAllFabricList.CreatedDate = moment($scope.GetAllFabricList.CreatedDate).format('DD/MM/YYYY');
            }
            else if (response.data.status == "500") {
                window.location.href = "/Home/Error";
            }
        });
        $scope.isLoading = false;
    }

    /********************Add Fabric*********************/
    $scope.AddFabric = function (isModalOpen) {
        if (isModalOpen) {
            myService.GetAllFabricGroup().then(function (response) {
                if (response.data.status == "200") {
                    $scope.GetAllFabricGroupList = response.data.rows;
                }
                else if (response.data.status == "500") {
                    window.location.href = "/Home/Error";
                }
            });
            $('#addFabricModal').modal('show')
          
        } else {
            if ($scope.fabricDetail.FacricName != null && $scope.fabricDetail.FacricName != undefined && $scope.fabricDetail.FacricName != '') {
                if ($scope.fabricDetail.FabricGroupId != null && $scope.fabricDetail.FabricGroupId != undefined && $scope.fabricDetail.FabricGroupId != '') {
                    if ($scope.fabricDetail.FabricPrice != null && $scope.fabricDetail.FabricPrice != undefined && $scope.fabricDetail.FabricPrice != '') {
                        myService.AddFabric($scope.fabricDetail).then(function (response) {
                            if (response.data.status == "200") {
                                $('#addFabricModal').modal('hide');
                                swal({ title: "Added!", text: "Your record has been added.", type: "success" },
                                    function () { window.location = '/Fabric'; });
                            } else if (response.data.status == "500") {
                                window.location.href = "/Home/Error";
                            }
                        });
                    } else {
                        swal("Please enter Fabric price");
                    }
                }
                else {
                    swal("Please select Fabric group");
                }
            }
            else {
                swal("Please enter Fabric name");
            }
          
        }
    }
    /*************Open edit modal for update******************/
    $scope.fabricEditModal = function (fabricId) {
        myService.GetFabricById(fabricId).then(function (result) {
            if (result.data.status == "200") {
                $scope.getFabricByIdObj = result.data.item;
                $scope.fabricDetail = $scope.getFabricByIdObj;
                $scope.fabricDetail.CreatedDate = moment($scope.getFabricByIdObj.CreatedDate).format('YYYY-MM-DD HH:mm');
                myService.GetAllFabricGroup().then(function (response) {
                    if (response.data.status == "200") {
                        $scope.GetAllFabricGroupList = response.data.rows;
                    }
                    else if (response.data.status == "500") {
                        window.location.href = "/Home/Error";
                    }
                });
                $('#editFabricModal').modal('show');
            } else if (result.data.status == "500") {
                window.location.href = "/Home/Error";
            }
        });
    }
    /******************** Update Fabric *********************/
    $scope.UpdateFabric = function (fabricDetail) {
        if ($scope.fabricDetail.FacricName != null && $scope.fabricDetail.FacricName != undefined && $scope.fabricDetail.FacricName != '') {
            if ($scope.fabricDetail.FabricGroupId != null && $scope.fabricDetail.FabricGroupId != undefined && $scope.fabricDetail.FabricGroupId != '') {
                if ($scope.fabricDetail.FabricPrice != null && $scope.fabricDetail.FabricPrice != undefined && $scope.fabricDetail.FabricPrice != '') {
                    myService.UpdateFabric(fabricDetail).then(function (result) {
                        if (result.data.status == "200") {
                            $('#editFabricModal').modal('hide');
                            swal("Data updated successfully");
                            window.location = '/Fabric';
                        } else if (result.data.status == "500") { window.location.href = "/Home/Error"; }
                    });
                } else {
                    swal("Please enter Fabric price");
                }
            }
            else {
                swal("Please select Fabric group");
            }
        }
        else {
            swal("Please enter Fabric name");
        }
    }
    /****************Virtual Delete Fabric******************/
    $scope.DeleteFabric = function (fabricId) {
        myService.DeleteFabric(fabricId).then(function (result) {
            if (result.data.status == "200") {
                setTimeout(function () {
                    swal({
                        title: "Are you sure you want to delete ?",
                        text: "You will not be able to recover this record!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, delete it!",
                        closeOnConfirm: false
                    },
                    function () {
                        swal({ title: "Deleted!", text: "Your record has been deleted.", type: "success" },
                            function () { window.location = '/Fabric'; });

                    });
                }, 1000);

            } else if (result.data.status == "500") { window.location.href = "/Home/Error"; }
        });
    }
    /********************Sort and filter*********************/
    $scope.sort = function (keyname) {
        $scope.sortKey = keyname;   //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    }
});