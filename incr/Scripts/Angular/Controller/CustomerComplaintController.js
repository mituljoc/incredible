﻿var app;
(function () {
    app = angular.module("FCTool", ['ngRoute', 'angularUtils.directives.dirPagination']);
})();
//app.directive('datepicker', function () {
//    return {
//        restrict: 'A',
//        require: 'ngModel',
//        compile: function () {
//            return {
//                pre: function (scope, element, attrs, ngModelCtrl) {
//                    var format, dateObj;
//                    format = (!attrs.dpFormat) ? 'd/m/yyyy' : attrs.dpFormat;
//                    if (!attrs.initDate && !attrs.dpFormat) {
//                        // If there is no initDate attribute than we will get todays date as the default
//                        dateObj = new Date();
//                        scope[attrs.ngModel] = dateObj.getDate() + '/' + (dateObj.getMonth() + 1) + '/' + dateObj.getFullYear();
//                    } else if (!attrs.initDate) {
//                        // Otherwise set as the init date
//                        scope[attrs.ngModel] = attrs.initDate;
//                    } else {
//                        // I could put some complex logic that changes the order of the date string I
//                        // create from the dateObj based on the format, but I'll leave that for now
//                        // Or I could switch case and limit the types of formats...
//                    }
//                    // Initialize the date-picker
//                    $(element).datepicker({
//                        format: format,
//                    }).on('changeDate', function (ev) {
//                        // To me this looks cleaner than adding $apply(); after everything.
//                        scope.$apply(function () {
//                            ngModelCtrl.$setViewValue(ev.format(format));
//                        });
//                    });
//                }
//            }
//        }
//    }
//});
app.service('myService', function ($http) {
    this.getAllCustComplaint = function () {
        var url = 'CustomerComplaint/GetAllCustComplaint';
        return $http({ method: 'GET', url: url });
    }
    this.getAllDepartment = function () {
        return $http.get("CustomerComplaint/GetAllDepartment");
    }
    this.getCustComplaintById = function (custComplaintId) {
        return $http.get("CustomerComplaint/GetCustComplaintById?custComplaintId=" + custComplaintId);
    }
    this.addNewCustComplaint = function (customerObj) {
        var url = '/CustomerComplaint/AddNewCustComplaint/';
        return $http({ method: 'POST', url: url, data: customerObj });
    }
    this.updateCustComplaint = function (customerObj) {
        var url = '/CustomerComplaint/UpdateCustComplaint/';
        return $http({ method: 'POST', url: url, data: customerObj });
    }
    this.deleteCustComplaint = function (custComplaintId) {
        var url = '/CustomerComplaint/DeleteCustComplaint?custComplaintId=' + custComplaintId;
        return $http({ method: 'POST', url: url });
    }
    this.getAllSupplier = function () {
        var url = 'Supplier/GetAllSupplier';
        return $http({ method: 'GET', url: url });
    }
    this.getAllOperator = function () {
        var url = 'Operator/GetAllOperator';
        return $http({ method: 'GET', url: url });
    }
    this.getOrderByCustomerId = function (customerId) {
        return $http.get("CustomerComplaint/getOrderByCustomerId?customerId=" + customerId);
      
    }
    
});

app.controller("CustomerComplaintController", function ($scope, $timeout, $rootScope, $window, $http, myService, $filter) {

    $scope.getCustComplaintByIdObj = {};
    $scope.complaintDetail = {};
    $scope.complaintDetail.DepartmentIds = [];
    $scope.complaintDetail.DepartmentIdList = [];
    $scope.departmentCount = 0;
    $scope.GetAllDepartmentList = [];
    $scope.custComplaintId = 0;
    $scope.orderCountByCustomer = 0;
    $scope.init = function () {
        $scope.getCustComplaintList();
        $('.datepicker').datetimepicker({
            autoclose: true,
            format: 'mm-dd-yyyy',
            setDate: new Date(),
        });
    }
    $scope.getCustComplaintList = function () {
        $scope.isLoading = true;
        myService.getAllCustComplaint().then(function (response) {
            if (response.data.status == "200") {
                $scope.CustComplaintList = response.data.rows;
            }
            else if (response.data.status == "500") {
                window.location.href = "/Home/Error";
            }
        });
        $scope.isLoading = false;
    }
    $scope.addNewCustComplaint = function (isModalOpen) {
        if (isModalOpen) {
            //$('#addOperatorModal').modal('show')
            $scope.complaintDetail = {};
            $scope.complaintDetail.DepartmentIds = [];
            $scope.complaintDetail.DepartmentIdList = [];
            $scope.complaintDetail.Type = "Other";
            $scope.complaintDetail.Status = "Incoming";
            $scope.complaintDetail.Priority = "Urgent";
            myService.getAllDepartment().then(function (response) {
                if (response.data.status == "200") {
                    $scope.GetAllDepartmentList = response.data.rows;
                    myService.getAllSupplier().then(function (response) {
                        if (response.data.status == "200") {
                            $scope.GetAllSupplierList = response.data.rows;
                        }
                        else if (response.data.status == "500") {
                            window.location.href = "/Home/Error";
                        }
                    });
                    myService.getAllOperator().then(function (response) {
                        if (response.data.status == "200") {
                            $scope.GetAllOperationList = response.data.rows;
                        }
                        else if (response.data.status == "500") {
                            window.location.href = "/Home/Error";
                        }
                    });
                    $scope.ComplaintStatus = ["Incoming", "In process", "Completed"];
                    $scope.ComplaintPriority = ["Urgent","Medium","Low"]
                    $('#addCustComplaintModal').modal('show');
                } else if (response.data.status == "500") {
                    window.location.href = "/Home/Error";
                }
            });
        } else {
            if ($scope.complaintDetail.DepartmentIds != null && $scope.complaintDetail.DepartmentIds != '') {
                angular.forEach($scope.complaintDetail.DepartmentIds, function (value, key) {
                    var ids = key;
                    if (value == true && key != 0) {
                        $scope.complaintDetail.DepartmentIdList.push(ids);
                    }
                });
                $scope.complaintDetail.Department = $scope.complaintDetail.DepartmentIdList.toString();
                if ($scope.complaintDetail.AddedBy != null && $scope.complaintDetail.AddedBy != '') {
                    if ($scope.complaintDetail.WorkedBy != null && $scope.complaintDetail.WorkedBy != '') {
                        if ($scope.complaintDetail.CustomerName != null && $scope.complaintDetail.CustomerName != '') {
                            if ($scope.complaintDetail.Title != null && $scope.complaintDetail.Title != '') {
                                if ($scope.complaintDetail.Type != null && $scope.complaintDetail.Type != '') {
                                    if ($scope.complaintDetail.Problem != null && $scope.complaintDetail.Problem != '') {
                                        myService.addNewCustComplaint($scope.complaintDetail).then(function (response) {
                                            if (response.data.status == "200") {
                                                $('#addCustComplaintModal').modal('hide');
                                                swal({ title: "Added!", text: "Your record has been added.", type: "success" },
                                                    function () { window.location = '/CustomerComplaint'; });
                                            } else if (response.data.status == "500") {
                                                window.location.href = "/Home/Error";
                                            }
                                        });
                                    } else swal("Please enter problem");
                                } else swal("Please select type");
                            } else swal("Please enter title");
                        }else
                            swal("Please select customer")
                    }else
                        swal("Please select worked by operator")
                } else 
                    swal("Please select added by operator")
            } else 
                swal("Please select department")
        }
    }
    $scope.DeptArray = [];
    $scope.custComplaintEditModal = function (custComplaintId) {
        myService.getCustComplaintById(custComplaintId).then(function (result) {
            if (result.data.status == "200") {
                $scope.getCustComplaintByIdObj = result.data.item;
                $scope.complaintDetail = $scope.getCustComplaintByIdObj;
                $scope.DeptArray = $scope.complaintDetail.Department.split(",")
                $scope.complaintDetail.CreatedDate = moment($scope.getCustComplaintByIdObj.CreatedDate).format('YYYY-MM-DD HH:mm');

                myService.getAllDepartment().then(function (response) {
                    if (response.data.status == "200") {
                        $scope.GetAllDepartmentList = response.data.rows;
                        $scope.departmentCount = $scope.GetAllDepartmentList.length;
                        $scope.complaintDetail.DepartmentIds = [];
                        for (var i = 0; i <= ($scope.departmentCount) ; i++) {
                            var val = false;
                            angular.forEach($scope.DeptArray, function (value, key) {
                                if (i == parseInt(value)) {
                                    val = true;
                                }
                            });
                            $scope.complaintDetail.DepartmentIds.push(val);
                        }
                        $('#editCustComplaintModal').modal('show');
                        myService.getAllSupplier().then(function (response) {
                            if (response.data.status == "200") {
                                $scope.GetAllSupplierList = response.data.rows;
                            }
                            else if (response.data.status == "500") {
                                window.location.href = "/Home/Error";
                            }
                        });
                        myService.getAllOperator().then(function (response) {
                            if (response.data.status == "200") {
                                $scope.GetAllOperationList = response.data.rows;
                            }
                            else if (response.data.status == "500") {
                                window.location.href = "/Home/Error";
                            }
                        });
                        $scope.ComplaintStatus = ["Incoming", "In process", "Completed"];
                        $scope.ComplaintPriority = ["Urgent", "Medium", "Low"]
                    } else if (response.data.status == "500") {
                        window.location.href = "/Home/Error";
                    }
                });


            } else if (result.data.status == "500") {
                window.location.href = "/Home/Error";
            }
        });
    }

    $scope.updateCustComplaint = function (complaintDetail) {
        $scope.complaintDetail.DepartmentIdList = [];
        if (complaintDetail.DepartmentIds != null && complaintDetail.DepartmentIds != '') {
            angular.forEach(complaintDetail.DepartmentIds, function (value, key) {
                var ids = key;
                if (value == true && key != 0) {
                    $scope.complaintDetail.DepartmentIdList.push(ids);
                }
            });
            if ($scope.complaintDetail.DepartmentIdList.length != 0) {
                if (complaintDetail.AddedBy != null && complaintDetail.AddedBy != '') {
                    if (complaintDetail.WorkedBy != null && complaintDetail.WorkedBy != '') {
                        if (complaintDetail.CustomerName != null && complaintDetail.CustomerName != '') {
                            if (complaintDetail.Title != null && complaintDetail.Title != '') {
                                if (complaintDetail.Type != null && complaintDetail.Type != '') {
                                    if (complaintDetail.Problem != null && complaintDetail.Problem != '') {
                                        $scope.complaintDetail.Department = $scope.complaintDetail.DepartmentIdList.toString();
                                        myService.updateCustComplaint(complaintDetail).then(function (result) {
                                            if (result.data.status == "200") {
                                                $('#editCustComplaintModal').modal('hide');
                                                swal("Data updated successfully");
                                                window.location = '/CustomerComplaint';

                                            } else if (result.data.status == "500") {
                                                window.location.href = "/Home/Error";
                                            }
                                        });
                                    } else swal("Please enter problem");
                                } else swal("Please select type");
                            } else swal("Please enter title");
                        } else
                            swal("Please select customer")
                    } else
                        swal("Please select worked by operator")
                } else
                    swal("Please select added by operator")
            } else swal("Please select department")
            
        } else
            swal("Please select department")
    }

    $scope.removeCustComplaint = function (custComplaintId) {
        myService.deleteCustComplaint(custComplaintId).then(function (result) {
            if (result.data.status == "200") {
                setTimeout(function () {
                    swal({
                        title: "Are you sure you want to delete ?",
                        text: "You will not be able to recover this record!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, delete it!",
                        closeOnConfirm: false
                    },
                    function () {
                        swal({ title: "Deleted!", text: "Your record has been deleted.", type: "success" },
                            function () { window.location = '/CustomerComplaint'; });

                    });
                    // swal("Data deleted successfully");
                }, 1000);

            } else if (result.data.status == "500") {
                window.location.href = "/Home/Error";
            }
        });
    }
    $scope.getOrderByCustomerId = function (customerId) {
        myService.getOrderByCustomerId(customerId).then(function (response) {
            if (response.data.status == "200") {
                $scope.orderListByCustomer = response.data.item;
              $scope.orderCountByCustomer =  $scope.orderListByCustomer.length
            }
            else if (response.data.status == "500") {
                window.location.href = "/Home/Error";
            }
        });
    }
    $scope.sort = function (keyname) {
        $scope.sortKey = keyname;   //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    }
});