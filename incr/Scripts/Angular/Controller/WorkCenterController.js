﻿var app;
(function () {
    app = angular.module("FCTool", ['ngRoute', 'angularUtils.directives.dirPagination']);
})();
app.service('workCenter', function ($http) {
    this.getAllOrders = function (type) {
        var url = 'GetAllOrders?type=' + type;
        return $http({ method: 'GET', url: url });
    }
    this.getOrderByIds = function (orderId) {
        var url = "getOrderByIds?orderIds=" + orderId;
        return $http({ method: 'GET', url: url });
    }
    this.generateInvoice = function (orderId) {
        var url = "GenerateInvoice/"+ orderId;
        return $http({ method: 'GET', url: url });
    }

    this.getAllOperator = function () {
        var url = '/Operator/GetAllOperator';
        return $http({ method: 'GET', url: url });
    }
    this.startJOB = function (data) {
        var wrapper = { d: data };
        var url = 'StartJob';
        return $http({ method: 'POST', url: url, data: data });
    }
    this.finishJOB = function (data) {
        var wrapper = { d: data };
        var url = 'finishjob';
        return $http({ method: 'POST', url: url, data: data });
    }
    this.startProduction = function (orderId) {

        var url = "startProduction?orderId=" + orderId;;
        return $http({ method: 'GET', url: url });
    }
    this.DeleteOrder = function (orderId) {
        var url = '/Order/DeleteOrder?orderId=' + orderId;
        return $http({ method: 'POST', url: url });
    }

});

app.controller("WorkCenterController", function ($scope, $timeout, $rootScope, $window, $http, workCenter, $filter) {

    $scope.WorkCenterDetail = { Id: 0, WorkById: 0, SupervisedId: 0, Remarks: "" }
    $scope.IsWorkCenter = true;
    $scope.IsPendingOrder = false;
    $scope.IsInvoice = false;
    $scope.IsPendingInvoice = false;
    $scope.WorkCenter = '';
    $scope.type = '';
    $scope.Next = false;
    $scope.Previous = false;
    $scope.init = function () {
        $scope.type = $scope.getParameterByName('type');
        var type = $scope.getParameterByName('type');
        if (type == "CARPENTARY") {
            $scope.Next = true;
            $scope.Previous = true;
            $('#headerTital').text('Carpentary')
        }
        if (type == "ALL") {
            $scope.IsWorkCenter = false;
            $('#headerTital').text('Running orders')
        }
        if (type == "LINNING") {
            $scope.Next = true;
            $scope.Previous = true;
            $('#headerTital').text('Linning')
        }
        if (type == "FITTING") {
            $scope.Next = true;
            $scope.Previous = true;
            $('#headerTital').text('Fitting')
        }
        if (type == "POLISH") {
            $scope.Next = true;
            $scope.Previous = true;
            $('#headerTital').text('Polish')
        }
        if (type == "PACKING") {
            $scope.Next = true;
            $scope.Previous = true;
            $('#headerTital').text('Packing')
        }
        if (type == "DELIVERY") {
            $scope.Next = true;
            $scope.Previous = true;
            $('#headerTital').text('Delivery')
        }
        if (type == "INVOICE") {
            $scope.Next = false;
            $scope.Previous = true;
            $scope.IsInvoice = true;
            $scope.IsWorkCenter = false;
            $('#headerTital').text('Invoice')
        }
     
        if (type == "PENDING") {
            $scope.Next = true;
            $scope.Previous = false;
            $scope.IsWorkCenter = false;
            $scope.IsPendingOrder = true;
            $('#headerTital').text('Pending')
        }
        $scope.WorkCenter = type;
        // alert(step);
        $scope.getOrderList(type);
        $scope.getOpearatorList();
    }
    $scope.gotoNext = function (type) {
        $scope.GetAllOrderList = null;
        var orderType = "INVOICE";
        if (type == "PENDING") {
            orderType = "CARPENTARY";
            $('#headerTital').text('Carpentary')
        }
        if (type == "CARPENTARY") {
            orderType = "LINNING";
            $('#headerTital').text('Linning')
        }
        if (type == "LINNING") {
            orderType = "POLISH";
            $('#headerTital').text('Polish')
        }
        if (type == "POLISH") {
            orderType = "FITTING";
            $('#headerTital').text('Fitting')

        }
        if (type == "FITTING") {
            orderType = "PACKING";
            $('#headerTital').text('Packing')
        }
        if (type == "PACKING") {
            orderType = "DELIVERY";
            $('#headerTital').text('Delivery')
        }
        if (type == "DELIVERY") {
            orderType = "INVOICE";
            $scope.IsWorkCenter = false;
            $('#headerTital').text('Invoice')
        }
        if (type == "INVOICE") {
            orderType = "INVOICE";
            $scope.IsWorkCenter = false;
            $('#headerTital').text('Invoice')
        }
        $scope.WorkCenter = orderType;
        $scope.type = orderType;
        $scope.getParameterByNameNextPrev('type');
        $scope.getOrderList(orderType);
        $scope.getOpearatorList();
        window.location = '/order/orders?type=' + orderType;
    }
    $scope.gotoPrevious = function (type) {
        $scope.GetAllOrderList = null;
        var orderType = "PENDING";
        if (type == "PENDING") {
            $scope.IsWorkCenter = false;
            $scope.IsPendingOrder = true;
            $('#headerTital').text('Pending')
            orderType = "PENDING";
        }
        if (type == "CARPENTARY") {
            $scope.IsWorkCenter = false;
            $scope.IsPendingOrder = true;
            $('#headerTital').text('Pending')
            orderType = "PENDING";
        }
        if (type == "LINNING") {
            orderType = "CARPENTARY";
            $('#headerTital').text('Carpentary')
        }
        if (type == "POLISH") {
            orderType = "LINNING";
            $('#headerTital').text('Linning')
        }
        if (type == "FITTING") {
            orderType = "POLISH";
            $('#headerTital').text('Polish')
        }
        if (type == "PACKING") {
            orderType = "FITTING";
            $('#headerTital').text('Fitting')
        }
        if (type == "DELIVERY") {
            orderType = "PACKING";
            $('#headerTital').text('Packing')
        }
        if (type == "INVOICE") {
            orderType = "DELIVERY";
            $('#headerTital').text('Delivery')
            $scope.IsInvoice = true;
            $scope.IsWorkCenter = false;

        }
        $scope.WorkCenter = orderType;
        $scope.type = orderType;
        $scope.getParameterByNameNextPrev('type');
        $scope.getOrderList(orderType);
        $scope.getOpearatorList();
        window.location = '/order/orders?type=' + orderType;
    }
    $scope.getParameterByNameNextPrev = function (name) {
        var regexS = "[\\?&]" + name + "=([^&#]*)",
            regex = new RegExp(regexS),
            results = regex.exec($scope.type);
        if (results == null) {
            return "";
        } else {
            return decodeURIComponent(results[1].replace(/\+/g, " "));
        }
    },
    $scope.getParameterByName = function (name) {
        var regexS = "[\\?&]" + name + "=([^&#]*)",
            regex = new RegExp(regexS),
            results = regex.exec(window.location.search);
        if (results == null) {
            return "";
        } else {
            return decodeURIComponent(results[1].replace(/\+/g, " "));
        }
    },
        $scope.getOpearatorList = function () {
            $scope.isOperatorLoading = true;
            workCenter.getAllOperator().then(function (response) {
                if (response.data.status == "200") {
                    debugger;
                    $scope.GetAllOperationList = [];
                    $.each(response.data.rows, function (i, item) {
                        if ($scope.WorkCenter == 'CARPENTARY' && item.Carpentery == true) {
                            $scope.GetAllOperationList.push(item);
                        }
                        else if ($scope.WorkCenter == 'FITTING' && item.Packing == true) {
                            $scope.GetAllOperationList.push(item);
                        }
                        else if ($scope.WorkCenter == 'PACKING' && item.Packing == true) {
                            $scope.GetAllOperationList.push(item);
                        }
                        else if ($scope.WorkCenter == 'DELIVERY' && item.Delivery == true) {
                            $scope.GetAllOperationList.push(item);
                        }
                        else if ($scope.WorkCenter == 'INVOICE' && item.Invoice == true) {
                            $scope.GetAllOperationList.push(item);
                        }
                        else if ($scope.WorkCenter == 'LINNING' && item.Linning == true) {
                            $scope.GetAllOperationList.push(item);
                        }

                        else if ($scope.WorkCenter == 'POLISH' && item.Polish == true) {
                            $scope.GetAllOperationList.push(item);
                        }



                    });
                    //$scope.GetAllOperationList = response.data.rows;
                }
                else if (response.data.status == "500") {
                    window.location.href = "/Home/Error";
                }
            });
            $scope.isOperatorLoading = false;
        },
      
    $scope.getOrderList = function (type) {
        $scope.isOrderLoading = true;
        workCenter.getAllOrders(type).then(function (response) {
            if (response.data.status == "200") {
                $scope.GetAllOrderList = response.data.item;
            }
            else if (response.data.status == "500") {
                window.location.href = "/Home/Error";
            }
        });
        $scope.isOrderLoading = false;
    }
    $scope.startWorkCenterModel = function () {

        var order = $scope.getSelected();
        debugger;
        if (order == null) {
            swal("Oops!", "Please select JOB.", "warning");
        }
        else if (order.IsStart) {
            swal("Oops!", "JOB already started.", "warning");
        }
        else {
            var orderId = order.Id;
            $scope.forstartJOB = true;
            $scope.WorkCenterDetail = { Id: order.Id, WorkById: order.WorkById, SupervisedId: order.SupervisedId, Remarks: order.Remarks, VehicleNo: order.VehicleNo }
            $scope.orderDetail = order;
            $('#startworkCenter').modal('show');
        }

    },
    $scope.generateInvoiceModel = function () {
        var order = $scope.getSelectedForInvoice();
        if (order == null) {
            swal("Oops!", "Please select JOB.", "warning");
        }
        else {

            var customerId = 0;
            var custCount = 0;
            var orderIds = '';
            for (var i = 0; i < order.length; i++) {
                orderIds = orderIds + order[i].Id + ',';
                if (order[i].CustomerId != customerId) {
                    customerId = order[i].CustomerId;
                    custCount++;
                }
            }
            if (custCount > 1)
            {
                swal("Oops!", "Please select jobs from the same customer for generate invoice for multiple jobs.", "warning");
            }
            else {
                $scope.orderIdForInvoice = orderIds;
                $scope.forstartJOB = true;
                $scope.WorkCenterDetail = { OrderIds:orderIds, Id: order[0].Id, WorkById: order[0].WorkById, SupervisedId: order[0].SupervisedId, Remarks: order[0].Remarks }
                $scope.orderDetail = order[0];

                workCenter.getOrderByIds(orderIds).then(function (result) {
                    if (result.data.status == "200") {
                        $scope.getOrderByIdObj = result.data.item;
                        $scope.InvoiceOrderDetail = $scope.getOrderByIdObj;
                        debugger;
                

                        $('#mdlGenerateInvoice').modal('show');

                    } else if (result.data.status == "500") {
                        window.location.href = "/Home/Error";
                    }
                });
              
            }

        }

    },
    $scope.GenerateInvoice = function (orderIds) {
        workCenter.generateInvoice(orderIds).then(function (result) {
            if (result.data.status == "200") {
                $('#mdlGenerateInvoice').modal('hide');
                $('#InvoicePreView').modal('hide');
                $scope.orderIdForInvoice = '';
                $("#print").load("/order/InvoicePreView/" + orderIds);
                swal({ title: "Invoice!", text: "Invoice successfully Generated.", type: "success" },
                   function () {

                       var divToPrint = document.getElementById('print');

                       var newWin = window.open('', 'Print-Window');

                       newWin.document.open();

                       newWin.document.write('<html><body onload="window.print()">' + divToPrint.innerHTML + '</body></html>');

                       newWin.document.close();

                       setTimeout(function () { newWin.close(); location.reload(); }, 3);
                   });

            } else if (result.data.status == "500") {
                window.location.href = "/Home/Error";
            }
        });
 
    },
     $scope.getSelected = function () {
         var order = null;
         for (var i = 0; i < $scope.GetAllOrderList.length; i++) {
             if ($scope.GetAllOrderList[i].Selected == true) {
                 order = $scope.GetAllOrderList[i];
                 break;
             }
         }
         return order;
     },
       $scope.getSelectedForInvoice = function () {
           var order = [];
           for (var i = 0; i < $scope.GetAllOrderList.length; i++) {
               if ($scope.GetAllOrderList[i].Selected == true) {
                   order.push($scope.GetAllOrderList[i]);
               }
           }
           return order;
       },
  $scope.CheckBox_Checked = function (selectedJobId) {
      for (var i = 0; i < $scope.GetAllOrderList.length; i++) {
          if ($scope.GetAllOrderList[i].Id != selectedJobId && !$scope.IsInvoice) {
              $scope.GetAllOrderList[i].Selected = false;
          }
      }
  }
    $scope.finishWorkCenterModel = function () {
        var order = $scope.getSelected();
        if (order == null) {
            swal("Oops!", "Please select JOB.", "warning");
        }
        else if (!order.IsStart) {
            swal("Oops!", "Please start JOB.", "warning");
        }
        else {
            var orderId = order.Id;
            $scope.forstartJOB = false;
            $scope.WorkCenterDetail = { Id: order.Id, WorkById: order.WorkById, SupervisedId: order.SupervisedId, Remarks: order.Remarks, VehicleNo: order.VehicleNo }
            $scope.orderDetail = order;
            $('#startworkCenter').modal('show');

        }
    },
       $scope.viewOrderDetails = function (orderId) {
           $('#ifrm').attr('src', "/order/viewDetails/" + orderId)
           $('#orderDetails').modal('show');
       },
        $scope.InvoicePreView = function (orderIds) {
            $('#ifrmInvoicePreView').attr('src', "/order/InvoicePreView/" + orderIds)
            $('#InvoicePreView').modal('show');
        },

    
    $scope.startJOB = function (orderId) {
        $scope.isOperatorLoading = true;
        if ($scope.WorkCenterDetail.WorkById == "0") {
            swal("Oops!", "Please select Work by.", "warning");
        }
        else if ($scope.WorkCenterDetail.SupervisedId == "0") {
            swal("Oops!", "Please select Supervised by.", "warning");
        }
        else {
            workCenter.startJOB($scope.WorkCenterDetail).then(function (response) {
                if (response.data.status == "200") {
                    $('#startworkCenter').modal('hide');
                    swal({ title: "Started!", text: "Job successfully started.", type: "success" },
                        function () { location.reload(); });
                } else if (response.data.status == "500") {
                    window.location.href = "/Home/Error";
                }
            });
        }
        $scope.isOperatorLoading = false;
    },
        $scope.startProduction = function () {
            var order = $scope.getSelected();
            if (order == null) {
                swal("Oops!", "Please select JOB.", "warning");
            }

            else {
                var orderId = order.Id;
                swal({
                    title: "Are you sure?",
                    text: "Production will be started for selected JOB!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Yes, Start it!",
                    cancelButtonText: "No, Cancel it!",
                    closeOnConfirm: false,
                    closeOnCancel: true
                },
    function (isConfirm) {
        if (isConfirm) {
            workCenter.startProduction(orderId).then(function (result) {
                if (result.data.status == "200") {
                    swal({ title: "Started!", text: "Job successfully started.", type: "success" },
                          function () { location.reload(); });

                } else if (result.data.status == "500") {
                    window.location.href = "/Home/Error";
                }
            });
        } else {

        }
    });
            }
        },

    $scope.finishJOB = function (orderId) {
        $scope.isOperatorLoading = true;
        if ($scope.WorkCenterDetail.WorkById == "0") {
            swal("Oops!", "Please select Work by.", "warning");
        }
        else if ($scope.WorkCenterDetail.SupervisedId == "0") {
            swal("Oops!", "Please select Supervised by.", "warning");
        }
        else {
            workCenter.finishJOB($scope.WorkCenterDetail).then(function (response) {
                if (response.data.status == "200") {
                    $('#startworkCenter').modal('hide');
                    swal({ title: "Finished!", text: "Job successfully finished.", type: "success" },
                        function () { location.reload(); });
                } else if (response.data.status == "500") {
                    window.location.href = "/Home/Error";
                }
            });
        }
        $scope.isOperatorLoading = false;
        },
       
    $scope.sort = function (keyname) {
        $scope.sortKey = keyname;
        $scope.reverse = !$scope.reverse;
        }

    /****************Virtual Delete Order******************/
    $scope.DeleteOrder = function (orderId) {
        workCenter.DeleteOrder(orderId).then(function (result) {
            if (result.data.status == "200") {
                setTimeout(function () {
                    swal({
                        title: "Are you sure you want to delete ?",
                        text: "You will not be able to recover this record!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, delete it!",
                        closeOnConfirm: false
                    },
                        function () {
                            swal({ title: "Deleted!", text: "Your record has been deleted.", type: "success" },
                                function () { window.location = '/order/orders?type=' + $scope.type; });
                            //order/orders?type=ALL  $scope.type
                        });
                }, 1000);

            } else if (result.data.status == "500") { window.location.href = "/Home/Error"; }
        });
    }
});
