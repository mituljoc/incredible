﻿var app;
(function () {
    app = angular.module("FCTool", ['ngRoute', 'angularUtils.directives.dirPagination']);
})();
app.service('myService', function ($http) {
    this.GetAllProduct = function () {
        var url = 'Product/GetAllProduct';
        return $http({ method: 'GET', url: url });
    }
    this.GetProductById = function (productId) {
        return $http.get("Product/GetProductById?id=" + productId);
    }
    this.AddProduct = function (Prouct) {
        var url = '/Product/AddProduct/';
        return $http({ method: 'POST', url: url, data: Prouct });
    }
    this.UpdateProduct = function (Prouct) {
        var url = '/Product/UpdateProduct/';
        return $http({ method: 'POST', url: url, data: Prouct });
    }
    this.DeleteProduct = function (productId) {
        var url = '/Product/DeleteProduct?Id=' + productId;
        return $http({ method: 'POST', url: url });
    }
 
});

app.controller("ProductController", function ($scope, $timeout, $rootScope, $window, $http, myService, $filter) {
    $scope.getFabricByIdObj = {};
    $scope.productDetail = {};
    $scope.IsUpdate = false;

    $scope.init = function () {
        $scope.GetAllProduct();
    }
    /********************Get All Product *********************/
    $scope.GetAllProduct = function () {
        $scope.isLoading = true;
        myService.GetAllProduct().then(function (response) {
            if (response.data.status == "200") {
                $scope.GetAllProductList = response.data.rows;
                $scope.GetAllProductList.CreatedDate = moment($scope.GetAllProductList.CreatedDate).format('DD/MM/YYYY');
            }
            else if (response.data.status == "500") {
                window.location.href = "/Home/Error";
            }
        });
        $scope.isLoading = false;
    }

    /********************Add Prouct*********************/
    $scope.AddProduct = function (isModalOpen) {
        $scope.IsUpdate = false;
           if (isModalOpen) {
            $scope.productDetail = {};
            $scope.productDetail.type = "3+2";
            $scope.productDetail.Range = "ECONOMIC";
            $scope.productDetail.Price_per = "FEET";
            $('#addProuctModal').modal('show')

        } else {
            if ($scope.productDetail.Name=="") {
                swal("Please enter Product name");
            }
            else if ($scope.productDetail.Code == "") {
                swal("Please enter Product Code");
            }
            else {
                $scope.productDetail.Net_total = (parseFloat($scope.productDetail.Pert1_total) + parseFloat($scope.productDetail.Pert2_total) + parseFloat($scope.productDetail.Pert3_total) + parseFloat($scope.productDetail.Pert4_total) + parseFloat($scope.productDetail.Pert5_total));
            myService.AddProduct($scope.productDetail).then(function (response) {
                if (response.data.status == "200") {
                    $('#addProuctModal').modal('hide');
                    swal({ title: "Added!", text: "Your record has been added.", type: "success" },
                        function () { window.location = '/Product'; });
                } else if (response.data.status == "500") {
                    window.location.href = "/Home/Error";
                }
            });
        }
        }
    }

    /*************Open edit modal for update******************/
    $scope.getProductById = function (productId) {
        $scope.IsUpdate = true;
        myService.GetProductById(productId).then(function (result) {
            if (result.data.status == "200") {
                $scope.productDetail = result.data.item;
                $('#addProuctModal').modal('show');
            } else if (result.data.status == "500") {
                window.location.href = "/Home/Error";
            }
        });
    }
    /******************** Update UpdateProduct *********************/
    $scope.UpdateProduct = function () {
        if ($scope.productDetail.Name == "") {
            swal("Please enter Product name");
        }
        else if ($scope.productDetail.Code == "") {
            swal("Please enter Product Code");
        }
        else {
            $scope.productDetail.Net_total = (parseFloat($scope.productDetail.Pert1_total) + parseFloat($scope.productDetail.Pert2_total) + parseFloat($scope.productDetail.Pert3_total) + parseFloat($scope.productDetail.Pert4_total) + parseFloat($scope.productDetail.Pert5_total));
            myService.UpdateProduct($scope.productDetail).then(function (response) {
                if (response.data.status == "200") {
                    $('#addProuctModal').modal('hide');
                    swal({ title: "Added!", text: "Your record has been added.", type: "success" },
                        function () { window.location = '/Product'; });
                } else if (response.data.status == "500") {
                    window.location.href = "/Home/Error";
                }
            });
        }
        
    }
    /****************Virtual Delete Fabric******************/
    $scope.DeleteProduct = function (Id) {

        swal({
            title: "Are you sure you want to delete ?",
            text: "You will not be able to recover this record!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        },
                   function () {
                       myService.DeleteProduct(Id).then(function (result) {
                           if (result.data.status == "200") {
                               swal({ title: "Deleted!", text: "Your record has been deleted.", type: "success" },
                              function () { window.location = '/Product'; });
                           } else if (result.data.status == "500") { window.location.href = "/Home/Error"; }
                       });
                 

                   });
    
    }
    /********************Sort and filter*********************/
    $scope.sort = function (keyname) {
        $scope.sortKey = keyname;   //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    }
});