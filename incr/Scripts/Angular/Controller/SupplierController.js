﻿var app;
(function () {
    app = angular.module("FCTool", ['ngRoute', 'angularUtils.directives.dirPagination']);
})();
app.service('myService', function ($http) {
    this.getAllSupplier = function () {
        var url = 'Supplier/GetAllSupplier';
        return $http({ method: 'GET', url: url });
    }
    this.getSupplierById = function (supplierId) {
        return $http.get("Supplier/GetSupplierById?supplierId=" + supplierId);
    }
    this.addNewSupplier = function (supplierObj) {
        var url = '/Supplier/AddNewSupplier/';
        return $http({ method: 'POST', url: url, data: supplierObj });
    }
    this.updateSupplier = function (supplierObj) {
        var url = '/Supplier/UpdateSupplier/';
        return $http({ method: 'POST', url: url, data: supplierObj });
    }
    this.deleteSupplier = function (supplierId) {
        var url = '/Supplier/DeleteSupplier?supplierId=' + supplierId;
        return $http({ method: 'POST', url: url });
    }
});
/*************** Created by Rajesh Gami *******************/
app.controller("SupplierController", function ($scope, $timeout, $rootScope, $window, $http, myService, $filter) {
    $scope.getSupplierByIdObj = {};
    $scope.supplierDetail = {};
    $scope.supplierId = 0;
    $scope.initFun = function () {
        $scope.getSupplierList();
    }
    /********************Get All supplier*********************/
    $scope.getSupplierList = function () {
        $scope.isLoading = true;
        myService.getAllSupplier().then(function (response) {
            if (response.data.status == "200") {
                $scope.GetAllSupplierList = response.data.rows;
                $scope.GetAllSupplierList.CreatedDate = moment($scope.GetAllSupplierList.CreatedDate).format('DD/MM/YYYY');
            }
            else if (response.data.status == "500") {
                window.location.href = "/Home/Error";
            }
            $scope.isLoading = false;
        });
        
    }
    /********************Add new supplier*********************/
    $scope.supplierDetail.Type = "Customer";
    $scope.supplierDetail.IsActive = true;
    $scope.addNewSupplier = function (isModalOpen) {
        if (isModalOpen) {
            $('#addSupplierModal').modal('show')
        } else {
            myService.addNewSupplier($scope.supplierDetail).then(function (response) {
                if (response.data.status == "200") {
                    $('#addSupplierModal').modal('hide');
                    swal({ title: "Added!", text: "Your record has been added.", type: "success" },
                        function () { window.location = '/Supplier'; });
                } else if (response.data.status == "500") {
                    window.location.href = "/Home/Error";
                }
            });
        }
    }
    /*************Open edit modal for update******************/
    $scope.supplierEditModal = function (supplierId) {
        myService.getSupplierById(supplierId).then(function (result) {
            if (result.data.status == "200") {
                $scope.getSupplierByIdObj = result.data.item;
                $scope.supplierDetail = $scope.getSupplierByIdObj;
                $scope.supplierDetail.CreatedDate = moment($scope.getSupplierByIdObj.CreatedDate).format('YYYY-MM-DD HH:mm');
                $('#editSupplierModal').modal('show');
            } else if (result.data.status == "500") {
                window.location.href = "/Home/Error";
            }
        });
    }
    /********************Update supplier*********************/
    $scope.updateSupplier = function (supplierDetail) {
        myService.updateSupplier(supplierDetail).then(function (result) {
            if (result.data.status == "200") {
                $('#editSupplierModal').modal('hide');
                swal("Data updated successfully");
                window.location = '/Supplier';
            } else if (result.data.status == "500") { window.location.href = "/Home/Error";}
        });
    }
    /****************Virtual delete supplier******************/
    $scope.removeSupplier = function (supplierId) {
        myService.deleteSupplier(supplierId).then(function (result) {
            if (result.data.status == "200") {
                setTimeout(function () {
                    swal({
                        title: "Are you sure you want to delete ?",
                        text: "You will not be able to recover this record!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, delete it!",
                        closeOnConfirm: false
                    },
                    function () {
                        swal({ title: "Deleted!", text: "Your record has been deleted.", type: "success" },
                            function () { window.location = '/Supplier'; });

                    });
                }, 1000);

            } else if (result.data.status == "500") { window.location.href = "/Home/Error";  }
        });
    }
    /********************Sort and filter*********************/
    $scope.sort = function (keyname) {
        $scope.sortKey = keyname;   //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    }
});