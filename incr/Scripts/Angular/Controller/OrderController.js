﻿var app;
(function () {
    app = angular.module("FCTool", ['ngRoute']);
})();
app.service('OrderService', function ($http) {
    this.getPriceList = function () {
        var url = '/order/getPriceList';
        return $http({ method: 'GET', url: url });
    }
    this.getAllOperator = function () {
        var url = '/Operator/GetAllOperator';
        return $http({ method: 'GET', url: url });
    }
    this.getAllCustomer = function () {
        var url = '/Supplier/GetAllSupplier';
        return $http({ method: 'GET', url: url });
    }
    this.getAllFabricGroup = function () {
        var url = '/FabricGroup/GetAllFabricGroup';
        return  $http({ method: 'GET', url: url });
    }
    this.getAllFabric=function(){
        var url = '/Fabric/GetAllFabric';
        return $http({ method: 'GET', url: url });
    }
});
app.controller("OrderController", function ($scope, $timeout, $rootScope, $window, $http, OrderService, $filter) {
    $scope.orders = {}
    $scope.orderDetail = { customerId: 0, JobById: 0, salesbyid: 0 }
    $scope.isload = false;
    $scope.init = function () {
        var type = $("input[name='Drawing-type']:checked").val();
        if (type == 'Corner') {
            $('.Longer').hide();
            $('.setOnly').hide();
            $('.corner').show();
            $('.LongerRight').removeClass('LongerRight-CLS');
            $('.LongerLeft').removeClass('LongerLeft-CLS');
        }
        else if (type == 'Longer') {
            debugger;
            $("input[name='rcs']").removeAttr('checked');
            $('.corner').hide();
            $('.setOnly').hide();
            $('.Longer').show();
            $('.LongerRight').addClass('LongerRight-CLS');
            var ltype = $("input[name='LongerSide']:checked").val();

            if (ltype == "left") {
                $('.longerLeftchk').show();
                $('.LongerRightchk').hide();
                $('.LongerRight').addClass('LongerRight-CLS');
                $('.LongerLeft').removeClass('LongerLeft-CLS');
                $("input[name='rcs']").removeAttr('checked');
                $('.lw').hide();
                $('.rw').hide();
                $('.l2').show();
                $('.l3').show();
            }
            if (ltype == "right") {
                $('.LongerRight').removeClass('LongerRight-CLS');
                $('.LongerLeft').addClass('LongerLeft-CLS');
                $('.LongerRightchk').show();
                $('.longerLeftchk').hide();
                $("input[name='lcs']").removeAttr('checked');

                $('.lw').hide();
                $('.rw').hide();
                $('.r2').show();
                $('.r3').show();
            }
        }
        else if (type == 'Set') {
            $('.corner').hide();
            $('.Longer').hide();
            $('.LongerRight').removeClass('LongerRight-CLS');
            $('.LongerLeft').removeClass('LongerLeft-CLS');
            $('.set').show();
            $('.setOnly').show();
        }
        var value = $("input[name='lcs']:checked").val();
        if (value == '1') {
            $('.lw').hide();
        }
        if (value == '2') {
            $('.lw').hide();
            $('.l2').show();
        }
        else if (value == '3') {
            $('.lw').hide();
            $('.l2').show();
            $('.l3').show();
        }
        else if (value == '4') {
            $('.lw').hide();
            $('.l2').show();
            $('.l3').show();
            $('.l4').show();
        }
        else if (value == '6') {
            $('.lw').hide();
            $('.l2').show();
            $('.l3').show();
            $('.l4').show();
            $('.l6').show();
        }
        var value1 = $("input[name='rcs']:checked").val();
        if (value1 == '1') {
            $('.rw').hide();
        }
        if (value1 == '2') {
            $('.rw').hide();
            $('.r2').show();
        }
        else if (value1 == '3') {
            $('.rw').hide();
            $('.r2').show();
            $('.r3').show();
        }
        else if (value1 == '4') {
            $('.rw').hide();
            $('.r2').show();
            $('.r3').show();
            $('.r4').show();
        }
        else if (value1 == '6') {
            $('.rw').hide();
            $('.r2').show();
            $('.r3').show();
            $('.r4').show();
            $('.r6').show();
        }
        $scope.event();
        $scope.getPriceList();
        $scope.getOpearatorList();

        $scope.getCustomerList();
        $scope.getAllFabricGroup();
        $scope.getAllFabric();

    }

    $scope.fillAddress = function (customerId) {
        angular.forEach($scope.getAllCustomerList, function (value, key) {
            if (value.RID == customerId) {
                debugger;
                $scope.orderDetail.Mobile = value.MobileNo;
                $scope.orderDetail.Address = value.Address + ', ' + value.City + ' ,' + value.State + ' ,' + value.Country;
                // alert(1);

            }
        });
    },
    $scope.saveOrderDetails = function () {
        $('#btnSaveOrder').hide();
        $('#btnSaveOrderPrint').hide();
        var formElement = document.getElementById("orderDetail");
        var request = new XMLHttpRequest();
        request.onreadystatechange = function () {
            if (request.readyState === 4 && request.status === 200) {

                swal({ title: "Success!", text: "Order place successfully.", type: "success" },
                            function () { window.location = '/order/orders?type=PENDING'; });
            }
        };
        request.open("POST", "/order/SaveOrderDetails");
        request.send(new FormData(formElement));
    },
    $scope.prinRecipt = function () {

        var divToPrint = document.getElementById('print');

        var newWin = window.open('', 'Print-Window');

        newWin.document.open();

        newWin.document.write('<html><body onload="window.print()">' + divToPrint.innerHTML + '</body></html>');

        newWin.document.close();

        setTimeout(function () { newWin.close(); window.location = '/order/orders?type=PENDING'; }, 100);

    },
    $scope.closeRecipt = function () {
        window.location = '/order/orders?type=PENDING';
    },
        $scope.saveOrderandPrintDetails = function () {
            $('#btnSaveOrder').hide();
            $('#btnSaveOrderPrint').hide();
            var formElement = document.getElementById("orderDetail");
            var request = new XMLHttpRequest();
            request.onreadystatechange = function () {
                if (request.readyState === 4 && request.status === 200) {
                    debugger;
                  
                    var data = JSON.parse(request.responseText);
                    
                
          
                    swal({ title: "Success!", text: "Order place successfully.", type: "success" },
                                function () {
                                    //var divToPrint = document.getElementById('print');

                                    //var newWin = window.open('', 'Print-Window');

                                    //newWin.document.open();

                                    //newWin.document.write('<html><body onload="window.print()">' + divToPrint.innerHTML + '</body></html>');

                                    //newWin.document.close();

                                    //setTimeout(function () { newWin.close(); window.location = '/order/orders?type=PENDING'; }, 100);

                                    $("#print").load("/order/OrderPreView/" + data.item.jobId);
                                    $('#orderDetails').modal('show');
                                });
                }
            };
            request.open("POST", "/order/SaveOrderDetails");
            request.send(new FormData(formElement));
        },
    $scope.getOpearatorList = function () {
        $scope.isOperatorLoading = true;
        OrderService.getAllOperator().then(function (response) {
            if (response.data.status == "200") {
                $scope.GetAllOperationList = response.data.rows;
                $scope.orderDetail.salesbyid = $('#tmpsalesby').val();
                //$scope.orderDetail.JobById = $('#tmpjobby').val();
            }
            else if (response.data.status == "500") {
                window.location.href = "/Home/Error";
            }
        });
        $scope.isOperatorLoading = false;
    },

        $scope.getPriceList = function () {
            $scope.isOperatorLoading = true;
            OrderService.getPriceList().then(function (response) {
                if (response.data.status == "200") {
                    $scope.GetPriceList = response.data.rows;
                    $scope.orderDetail.ParticularNameId = $('#tmpParticularNameId').val();
       
                    setTimeout(function () {
                       // $('#ParticularNameId').editableSelect();

       $('#ParticularNameId').editableSelect()
    .on('select.editable-select', function (e, li) {
        $('#tmpParticularNameId').val(li.val());
        $scope.orderDetail.ParticularNameId = li.val();
        $scope.getPriceFromList();
   
        $('#Particular1-qty').val($scope.orderDetail.Particular1Qty);
             
    })

                    }, 1000);
                    
                }
                else if (response.data.status == "500") {
                    window.location.href = "/Home/Error";
                }
            });
            $scope.isOperatorLoading = false;
        },

   $scope.getAllFabricGroup = function () {
       $scope.isOperatorLoading = true;
       OrderService.getAllFabricGroup().then(function (response) {
           if (response.data.status == "200") {
               debugger;
               $scope.AllFabricGroupLIST = response.data.rows;
               $scope.orderDetail.BodyFabricGroupId = $('#tmpBodyFabricGroupId').val();
               $scope.orderDetail.CushionFabricGroupId = $('#tmpCushionFabricGroupId').val();
               $scope.orderDetail.SeatFabricGroupId = $('#tmpSeatFabricGroupId').val();
               $scope.orderDetail.Particula4GroupId = $('#tmpParticula4GroupId').val();
               $scope.orderDetail.Particular5GroupId = $('#tmpParticular5GroupId').val();
                     }
           else if (response.data.status == "500") {
               window.location.href = "/Home/Error";
           }
       });
       $scope.isOperatorLoading = false;
   }
    $scope.getAllFabric = function () {
        $scope.isOperatorLoading = true;
        OrderService.getAllFabric().then(function (response) {
            if (response.data.status == "200") {
                $scope.AllFabricLIST = response.data.rows;
           
                $scope.orderDetail.TapestryBodyfabricId = $('#tmpBodyFabricTypeId').val();
                $scope.orderDetail.CushionFabricTypeId = $('#tmpCushionFabricTypeId').val();
                $scope.orderDetail.TapestrySeatFabricTypeId = $('#tmpSeatFabricTypeId').val();
                $scope.orderDetail.Particula4FabricTypeId = $('#tmpParticula4FabricTypeId').val();
                $scope.orderDetail.Particular5FabricTypeId = $('#tmpParticular5FabricTypeId').val();
                $scope.updatePrice();
            }
            else if (response.data.status == "500") {
                window.location.href = "/Home/Error";
            }
        });
        $scope.isOperatorLoading = false;
    }
    $scope.getCustomerList = function () {
        OrderService.getAllCustomer().then(function (response) {
            if (response.data.status == "200") {
                $scope.getAllCustomerList = response.data.rows;
                $scope.orderDetail.customerId = $('#tmpcustomerId').val();
                $scope.fillAddress($scope.orderDetail.customerId);
            }
            else if (response.data.status == "500") {
                window.location.href = "/Home/Error";
            }
        });
    },
    $scope.getFebricPrice = function (id) {
        var price=0;
        angular.forEach($scope.AllFabricLIST, function (value, key) {
            if (value.RID == id) {
                debugger;
                price = value.FabricPrice;
           }
        });
        return price;
    }
    $scope.getPriceFromList = function () {
        var price = 0;
        angular.forEach($scope.GetPriceList, function (value, key) {
         
            if (value.id == $scope.orderDetail.ParticularNameId)
            {
                 //if ($('#tmpParticularNameId').val() != $scope.orderDetail.ParticularNameId)
                    {
                        $scope.orderDetail.Particular1Qty = value.Pert1_mtr;
                        $scope.orderDetail.Particular1Rate = value.Pert1_rate;
                        $scope.orderDetail.Particular1Total = value.Pert1_total;
                        $('#Particular1').val(value.Pert1);
            
                    if (value.Without_fabric)
                    {
                        $scope.orderDetail.IsWithoutfabric = true;
                        $(".Fabric").prop("disabled", true);
                        $scope.orderDetail.Particular2Qty ='';
                        $scope.orderDetail.Particular3Qty ='';
                        $scope.orderDetail.Particular4Qty ='';
                        $scope.orderDetail.Particular5Qty ='';

                        $scope.orderDetail.Particular2Rate ='';
                        $scope.orderDetail.Particular3Rate ='';
                        $scope.orderDetail.Particular4Rate = '';
                        $scope.orderDetail.Particular5Rate = '';

                        $scope.orderDetail.Particular2Total = '';
                        $scope.orderDetail.Particular3Total = '';
                        $scope.orderDetail.Particular4Total ='';
                        $scope.orderDetail.Particular5Total = '';
                        $('#Particular2').val('');
                        $('#Particular3').val('');
                        $('#Particular4').val('');
                        $('#Particular5').val('');
                        $("#chkWithoutfabric").prop("checked", true);
                    }
                    else {
                        $scope.orderDetail.IsWithoutfabric = false;
                        $("#chkWithoutfabric").prop("checked", false);
                        $(".Fabric").prop("disabled", false);
                        $scope.orderDetail.Particular2Qty = value.Pert2_mtr;
                        $scope.orderDetail.Particular3Qty = value.Pert3_mtr;
                        $scope.orderDetail.Particular4Qty = value.Pert4_mtr;
                        $scope.orderDetail.Particular5Qty = value.Pert5_mtr;

                        $scope.orderDetail.Particular2Rate = value.Pert2_rate;
                        $scope.orderDetail.Particular3Rate = value.Pert3_rate;
                        $scope.orderDetail.Particular4Rate = value.Pert4_rate;
                        $scope.orderDetail.Particular5Rate = value.Pert5_rate;

                        $scope.orderDetail.Particular2Total = value.Pert2_total;
                        $scope.orderDetail.Particular3Total = value.Pert3_total;
                        $scope.orderDetail.Particular4Total = value.Pert4_total;
                        $scope.orderDetail.Particular5Total = value.Pert5_total;
                        $('#Particular2').val(value.Pert2);
                        $('#Particular3').val(value.Pert3);
                        $('#Particular4').val(value.Pert4);
                        $('#Particular5').val(value.Pert5);
                    }
                    var type = value.type;
                    if (type == "LAUNGER")
                    {
                    
                        $("input[name=Drawing-type][value=Longer]").prop('checked', true).trigger('change');;
                    }
                    else if (type == "CORNER") {
                        $("input[name=Drawing-type][value=Corner]").prop('checked', true).trigger('change');;
                    }
                    else {
                        $("input[name=Drawing-type][value=Set]").prop('checked', true).trigger('change');;
                    }
                    $scope.updatePrice();
                    }
                    $scope.orderDetail.Code = value.Code;
                    $scope.orderDetail.Range = value.Range;
                    $('#hdnCoder').val(value.Code);
                    $('#hdnPrice_per').val(value.Price_per);
                    $('#hdnRange').val(value.Range);
                    $('#hdnhdntype').val(value.type);
                    
                    
            }
        });
        $scope.updatePrice();
        $scope.$apply();
        return price;
 
    }
    $scope.changeWithoutfabric = function () {
        debugger;
        var price = 0;
       
        angular.forEach($scope.GetPriceList, function (value, key) {

            if (value.id == $scope.orderDetail.ParticularNameId) {
                //if ($('#tmpParticularNameId').val() != $scope.orderDetail.ParticularNameId)
                {
                   

                    if ($("#chkWithoutfabric").prop("checked") == true) {
                        $(".Fabric").prop("disabled", true);
                        $scope.orderDetail.Particular2Qty = '';
                        $scope.orderDetail.Particular3Qty = '';
                        $scope.orderDetail.Particular4Qty = '';
                        $scope.orderDetail.Particular5Qty = '';

                        $scope.orderDetail.Particular2Rate = '';
                        $scope.orderDetail.Particular3Rate = '';
                        $scope.orderDetail.Particular4Rate = '';
                        $scope.orderDetail.Particular5Rate = '';

                        $scope.orderDetail.Particular2Total = '';
                        $scope.orderDetail.Particular3Total = '';
                        $scope.orderDetail.Particular4Total = '';
                        $scope.orderDetail.Particular5Total = '';
                        $('#Particular2').val('');
                        $('#Particular3').val('');
                        $('#Particular4').val('');
                        $('#Particular5').val('');


                    }
                    else {
                        if ($scope.isload) {
                            $(".Fabric").prop("disabled", false);
                            $scope.orderDetail.Particular2Qty = value.Pert2_mtr;
                            $scope.orderDetail.Particular3Qty = value.Pert3_mtr;
                            $scope.orderDetail.Particular4Qty = value.Pert4_mtr;
                            $scope.orderDetail.Particular5Qty = value.Pert5_mtr;

                            $scope.orderDetail.Particular2Rate = value.Pert2_rate;
                            $scope.orderDetail.Particular3Rate = value.Pert3_rate;
                            $scope.orderDetail.Particular4Rate = value.Pert4_rate;
                            $scope.orderDetail.Particular5Rate = value.Pert5_rate;

                            $scope.orderDetail.Particular2Total = value.Pert2_total;
                            $scope.orderDetail.Particular3Total = value.Pert3_total;
                            $scope.orderDetail.Particular4Total = value.Pert4_total;
                            $scope.orderDetail.Particular5Total = value.Pert5_total;
                            $('#Particular2').val(value.Pert2);
                            $('#Particular3').val(value.Pert3);
                            $('#Particular4').val(value.Pert4);
                            $('#Particular5').val(value.Pert5);
                        }
                    }
                    if ($scope.isload) {
                        $scope.updatePrice();
                    }
                    $scope.orderDetail.Code = value.Code;
                    $scope.orderDetail.Range = value.Range;
                }
            }
        });
        $scope.isload = true;
        return price;
    },
    $scope.updatePrice = function () {
        debugger;
        $scope.orderDetail.Particular1Total = $scope.GetNumericValue($scope.orderDetail.Particular1Qty) * $scope.orderDetail.Particular1Rate;
        $scope.orderDetail.Particular2Total =  $scope.GetNumericValue($scope.orderDetail.Particular2Qty) * $scope.orderDetail.Particular2Rate;
        $scope.orderDetail.Particular3Total = $scope.GetNumericValue( $scope.orderDetail.Particular3Qty) * $scope.orderDetail.Particular3Rate;
        $scope.orderDetail.Particular4Total = $scope.GetNumericValue($scope.orderDetail.Particular4Qty) * $scope.orderDetail.Particular4Rate;
        $scope.orderDetail.Particular5Total = $scope.GetNumericValue($scope.orderDetail.Particular5Qty) * $scope.orderDetail.Particular5Rate;
        $scope.orderDetail.ParticularNetTotal = $scope.orderDetail.Particular1Total + $scope.orderDetail.Particular2Total + $scope.orderDetail.Particular3Total + $scope.orderDetail.Particular4Total + $scope.orderDetail.Particular5Total;
        //$scope.orderDetail.BodyfabricPrice = $scope.orderDetail.TapestryBodyMTR * $scope.getFebricPrice($scope.orderDetail.TapestryBodyfabricId);
        //$scope.orderDetail.CushionFabricPrice = $scope.orderDetail.TapestryCushionMTR * $scope.getFebricPrice($scope.orderDetail.CushionFabricTypeId);
        //$scope.orderDetail.SeatFabricPrice = $scope.orderDetail.TapestrySeatMTR * $scope.getFebricPrice($scope.orderDetail.TapestrySeatFabricTypeId);

        $scope.orderDetail.NetTotal = $scope.orderDetail.ParticularNetTotal;
           // $scope.orderDetail.Particular1Total + $scope.orderDetail.Particular2Total + $scope.orderDetail.Particular3Total + $scope.orderDetail.Particular4Total;// + $scope.orderDetail.BodyfabricPrice + $scope.orderDetail.CushionFabricPrice + $scope.orderDetail.SeatFabricPrice;
        $scope.orderDetail.SGSTValue = ($scope.orderDetail.NetTotal * $scope.orderDetail.SGST) / 100;
        $scope.orderDetail.CGSTValue=($scope.orderDetail.NetTotal * $scope.orderDetail.CGST) / 100;
        $scope.orderDetail.GrossAmaunt = $scope.orderDetail.NetTotal + $scope.orderDetail.SGSTValue + $scope.orderDetail.CGSTValue + $scope.orderDetail.TransPort - $scope.orderDetail.Discount + $scope.orderDetail.Extra
        $('#netAmmaunt').val($scope.orderDetail.NetTotal);
        $('#grossAmmaunt').val($scope.orderDetail.GrossAmaunt);
    },
    $scope.GetNumericValue=function(value)
    {
        var res = String(value).split("+");
        var total = res.reduce($scope.getSum, 0);
        if (isNaN(total))
        {
            total = 0;
        }
        return total;
    }
    $scope.getSum =function(total, num) {
        return total + parseFloat(num)

    }
    $scope.carpantaryBottom = function () {
        debugger;
        var mw= $("input[name=carpantary-middle-width]").val();
        var rw= $("input[name=carpantary-right-width]").val();
     
        var total = 0;
        if (isNaN(rw)) {
            rw = 0;
        }
        if (isNaN(mw)) {
            mw = 0;
        }
        total = parseFloat(mw) + parseFloat(rw);
        $("input[name=carpantary-bottom-width]").val(total);
    }
    $scope.carpantaryHeight = function () {
        debugger;
        var h1 = $("input[name=carpantary-right-hight1]").val();
        var h2 = $("input[name=carpantary-right-hight2]").val();
        var h3 = $("input[name=carpantary-right-hight3]").val();
        if (isNaN(h1)) {
            h1 = 0;
        }
        if (isNaN(h2)) {
            h2 = 0;
        }
        if (isNaN(h3)) {
            h3 = 0;
        }
        var total = 0;
      
        total = parseFloat(h1) + parseFloat(h2) + parseFloat(h3);
        $("input[name=carpantary-right-hight]").val(total);
    }
    $scope.event = function () {
        $("#btnSaveOrder").click(function () {
            $scope.saveOrderDetails();
        });

        $('input[type="checkbox"]').on('change', function (event) {
          //  $scope.changeWithoutfabric();
        });
        $("#btnSaveOrderPrint").click(function () {
            $scope.saveOrderandPrintDetails();
        });
        $('input[name=carpantary-middle-width]').change(function () {
            $scope.carpantaryBottom();
        }),
          $('input[name=carpantary-right-width]').change(function () {
              $scope.carpantaryBottom();
          });
        $('input[name=carpantary-right-hight3]').change(function () {
            $scope.carpantaryHeight();
        });
        $('input[name=carpantary-right-hight2]').change(function () {
            $scope.carpantaryHeight();
        });
        $('input[name=carpantary-right-hight1]').change(function () {
            $scope.carpantaryHeight();
        });

        ''

        $('input[name=Drawing-type]').change(function () {
            if (this.checked) {
                if (this.value == 'Corner') {
                    $('.Longer').hide();
                    $('.setOnly').hide();
                    $('.corner').show();
                    $('.LongerRightchk').show();
                    $('.longerLeftchk').show();
                    $('.LongerRight').removeClass('LongerRight-CLS');
                    $('.LongerLeft').removeClass('LongerLeft-CLS');
                    var value = $("input[name='lcs']:checked").val();
                    $("input[name='lcs'][value=2]").prop('checked', true);
                    $("input[name='rcs'][value=2]").prop('checked', true);
                    $('.lw').hide();
                    $('.l2').show();
                    $('.rw').hide();
                    $('.r2').show();
                }
                else if (this.value == 'Longer') {
                    $('.corner').hide();
                    $('.setOnly').hide();
                    $('.Longer').show();
                    $('.LongerRightchk').hide();
                    $('.longerLeftchk').show();
                    $('.LongerRight').addClass('LongerRight-CLS');

                    $("input[name='rcs']").removeAttr('checked');
                    var value = $("input[name='lcs']:checked").val();
                    $('input[name=LongerSide][value=left]').prop('checked', true);
                    $("input[name='lcs'][value=3]").prop('checked', true);
                    $('.lw').hide();
                    $('.l2').show();
                    $('.l3').show();

                }
                else if (this.value == 'Set') {
                    $('.LongerRightchk').show();
                    $('.longerLeftchk').show();
                    $('.corner').hide();
                    $('.Longer').hide();
                    $('.LongerRight').removeClass('LongerRight-CLS');
                    $('.LongerLeft').removeClass('LongerLeft-CLS');
                    $('.set').show();
                    $('.setOnly').show();
                    $("input[name='lcs'][value=1]").prop('checked', true);
                    $("input[name='rcs'][value=1]").prop('checked', true);
                    $('.lw').hide();
                    $('.rw').hide();
                }
            }

        });
        $('input[name=lcs]').change(function () {
            if (this.checked) {
                if (this.value == '1') {
                    $('.lw').hide();
                }
                if (this.value == '2') {
                    $('.lw').hide();
                    $('.l2').show();
                }
                else if (this.value == '3') {
                    $('.lw').hide();
                    $('.l2').show();
                    $('.l3').show();
                }
                else if (this.value == '4') {
                    $('.lw').hide();
                    $('.l2').show();
                    $('.l3').show();
                    $('.l4').show();
                }
                else if (this.value == '5') {
                    $('.lw').hide();
                    $('.l2').show();
                    $('.l3').show();
                    $('.l4').show();
                    $('.l5').show();

                }
                else if (this.value == '6') {
                    $('.lw').hide();
                    $('.l2').show();
                    $('.l3').show();
                    $('.l4').show();
                    $('.l6').show();

                }
            }
        });
        $('input[name=rcs]').change(function () {
            if (this.checked) {
                if (this.value == '1') {
                    $('.rw').hide();
                }
                if (this.value == '2') {
                    $('.rw').hide();
                    $('.r2').show();
                }
                else if (this.value == '3') {
                    $('.rw').hide();
                    $('.r2').show();
                    $('.r3').show();

                }
                else if (this.value == '4') {
                    $('.rw').hide();
                    $('.r2').show();
                    $('.r3').show();
                    $('.r4').show();
                }
                else if (this.value == '5') {
                    $('.rw').hide();
                    $('.r2').show();
                    $('.r3').show();
                    $('.r4').show();
                    $('.r5').show();

                }

                else if (this.value == '6') {
                    $('.rw').hide();
                    $('.r2').show();
                    $('.r3').show();
                    $('.r4').show();
                    $('.r6').show();

                }
            }
        });
        $('input[name=LongerSide]').change(function () {

            if (this.value == "left") {
                $('.longerLeftchk').show();
                $('.LongerRightchk').hide();
                $('.LongerRight').addClass('LongerRight-CLS');
                $('.LongerLeft').removeClass('LongerLeft-CLS');

                $("input[name='rcs']").removeAttr('checked');
                $('.lw').hide();
                $('.rw').hide();
                $('.l2').show();
                $('.l3').show();
                $("input[name='lcs'][value=3]").prop('checked', true);
            }
            if (this.value == "right") {
                $('.LongerRight').removeClass('LongerRight-CLS');
                $('.LongerLeft').addClass('LongerLeft-CLS');
                $('.LongerRightchk').show();
                $('.longerLeftchk').hide();
                $("input[name='rcs']").removeAttr('checked');
                $("input[name='rcs'][value=3]").prop('checked', true);
                $('.lw').hide();
                $('.rw').hide();
                $('.r2').show();
                $('.r3').show();
            }

        });

    

    }
});