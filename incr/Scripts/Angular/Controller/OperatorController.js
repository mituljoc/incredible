﻿var app;
(function () {
    app = angular.module("FCTool", ['ngRoute', 'angularUtils.directives.dirPagination']);
})();

app.service('myService', function ($http) {
    this.getAllOperator = function () {
        var url = 'Operator/GetAllOperator';
        return $http({ method: 'GET', url: url });
    }
    this.getAllDepartment = function () {
        return $http.get("Operator/GetAllDepartment");
    }
    this.getOperatorById = function (operatorId) {
        return $http.get("Operator/GetOperatorById?operatorId=" + operatorId);
    }
    this.addNewOperator = function (operatorObj) {
        var url = '/Operator/AddNewOperator/';
        return $http({ method: 'POST', url: url, data: operatorObj });
    }
    this.updateOperator = function (operatorObj) {
        var url = '/Operator/UpdateOperator/';
        return $http({ method: 'POST', url: url, data: operatorObj });
    }
    this.deleteOperator = function (operatorId) {
        var url = '/Operator/DeleteOperator?operatorId=' + operatorId;
        return $http({ method: 'POST', url: url });
    }
});

                    /*  --------------------- Created by Rajesh Gami ---------------------  */
app.controller("OperatorController", function ($scope, $timeout, $rootScope, $window, $http, myService, $filter) {

    $scope.getOperatorByIdObj = {};
    $scope.operatorDetail = {};
    $scope.operatorDetail.Invoice = false;
    $scope.operatorDetail.Delivery = false;
    $scope.operatorDetail.Packing = false;
    $scope.operatorDetail.Linning = false;
    $scope.operatorDetail.Carpentery = false;
    $scope.operatorDetail.Fitting = false;
    $scope.operatorDetail.chkPacking = false;
    $scope.operatorDetail.chkInvoice = false;
    $scope.operatorDetail.chkLinning = false;
    $scope.operatorDetail.chkCarpentery = false;
    $scope.operatorDetail.chkDelivery = false;
    $scope.operatorDetail.chkFitting = false;
    $scope.operatorDetail.chkPolish = false;
    $scope.operatorDetail.selectedDepartment = [];
    $scope.GetAllDepartmentList = [];
    $scope.OperatorId = 0;
    $scope.myDetail = {};
    $scope.init = function () {
        $scope.getOpearatorList();
    }
    $scope.getOpearatorList = function () {
        $scope.isOperatorLoading = true;
        myService.getAllOperator().then(function (response) {
            if (response.data.status == "200") {
                $scope.GetAllOperationList = response.data.rows;
            }
            else if (response.data.status == "500") {
                window.location.href = "/Home/Error";
            }
        });
        $scope.isOperatorLoading = false;
    }
    $scope.addNewOperator = function (isModalOpen) {
        if (isModalOpen) {
            $('#addOperatorModal').modal('show')
        } else {
            if ($scope.operatorDetail.OperatorName == '' || $scope.operatorDetail.OperatorName == null || $scope.operatorDetail.OperatorName == undefined) {
                swal("Oops!", "Please enter operator name", "warning")
            }
            else if ($scope.operatorDetail.Delivery == false && $scope.operatorDetail.Carpentery == false && $scope.operatorDetail.Linning == false && $scope.operatorDetail.Invoice == false && $scope.operatorDetail.Packing == false && $scope.operatorDetail.Fitting == false && $scope.operatorDetail.Polish == false) {
                swal("Oops!", "Please select department", "warning")
            }    
            else {
                myService.addNewOperator($scope.operatorDetail).then(function (response) {
                    if (response.data.status == "200") {
                        $('#addOperatorModal').modal('hide');
                        swal({ title: "Added!", text: "Your record has been added.", type: "success" },
                            function () { window.location = '/Operator'; });
                    } else if (response.data.status == "500") {
                        window.location.href = "/Home/Error";
                    }
                });
            }
        }
    }
    $scope.operatorEditModal = function (operatorId) {
        myService.getOperatorById(operatorId).then(function (result) {
            if (result.data.status == "200") {
                $scope.getOperatorByIdObj = result.data.item;
                $scope.operatorDetail = $scope.getOperatorByIdObj;
                $scope.operatorDetail.CreatedDate = moment($scope.getOperatorByIdObj.CreatedDate).format('DD/MM/YYYY');
                $('#editOperatorModal').modal('show');

            } else if (result.data.status == "500") {
                window.location.href = "/Home/Error";
            }
        });
    }

    $scope.updateOperator = function (operatorDetail) {
        if ($scope.operatorDetail.OperatorName == '' || $scope.operatorDetail.OperatorName == null || $scope.operatorDetail.OperatorName == undefined) {
            swal("Oops!", "Please enter operator name", "warning")
        }
        else if ($scope.operatorDetail.Delivery == false && $scope.operatorDetail.Carpentery == false && $scope.operatorDetail.Linning == false && $scope.operatorDetail.Invoice == false && $scope.operatorDetail.Packing == false && $scope.operatorDetail.Fitting == false && $scope.operatorDetail.Polish == false) {
            swal("Oops!", "Please select department", "warning")
        } else {
            myService.updateOperator(operatorDetail).then(function (result) {
                if (result.data.status == "200") {
                    $('#editOperatorModal').modal('hide');
                    swal("Data updated successfulyy");
                    window.location = '/Operator';

                } else if (result.data.status == "500") {
                    window.location.href = "/Home/Error";
                }
            });
        }
    }

    $scope.removeOperator = function (operatorId) {
        myService.deleteOperator(operatorId).then(function (result) {
            if (result.data.status == "200") {
                //$('#deleteOperatorModal').modal('hide');
                setTimeout(function () {
                swal({
                    title: "Are you sure you want to delete ?",
                    text: "You will not be able to recover this record!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false
                },
                function () {
                    swal({ title: "Deleted!", text: "Your record has been deleted.", type: "success" },
                        function () { window.location = '/Operator'; });
                  
                });
               // swal("Data deleted successfully");
                }, 1000);

            } else if (result.data.status == "500") {
                window.location.href = "/Home/Error";
            }
        });
    }

    $scope.sort = function (keyname) {
        $scope.sortKey = keyname;   //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    }
});