﻿var app;
(function () {
    app = angular.module("FCTool", ['ngRoute', 'angularUtils.directives.dirPagination']);
})();

app.service('Invoice', function ($http) {
    this.getAllInvoices = function (type) {
        var url = 'GetInvoiceBYStatus?type=' + type;
        return $http({ method: 'GET', url: url });
    }
    this.getAllClosedInvoices = function (type) {
        var url = 'GetClosedInvoiceBYStatus?type=' + type;
        return $http({ method: 'GET', url: url });
    }
    this.getOrderByInvoiceIds = function (orderId) {
        var url = "GetOrderByInvoiceIds?InvoiceId=" + orderId;
        return $http({ method: 'GET', url: url });
    }
    this.generateInvoice = function (orderId) {
        var url = "GenerateInvoice/" + orderId;
        return $http({ method: 'GET', url: url });
    }
    this.closeInvoice = function (data) {
        var wrapper = { d: data };
        var url = 'CloseInvoice';
        return $http({ method: 'POST', url: url, data: data });
    }
    this.getInvoiceStatus = function (orderId) {
        var url = "GetInvoiceStatus";
        return $http({ method: 'GET', url: url });
    }
    

});

app.controller("InvoiceController", function ($scope, $timeout, $rootScope, $window, $http, Invoice, $filter) {

    $scope.WorkCenterDetail = { InvoiceStatusId: 0, Amount: 0,InvoiceId:0 }
    $scope.IsWorkCenter = true;
    $scope.IsPendingOrder = false;
    $scope.IsInvoice = false;
    $scope.IsPendingInvoice = false;
    $scope.WorkCenter = '';
    $scope.invoiceType = '';
    $scope.InvoiceOrder = {};
    $scope.init = function () {
        var type = 'INVOICED';
        $scope.invoiceType = type;
        $scope.InvoiceOrder = 'Pending';
        if (type == "INVOICED") {
            $scope.IsInvoice = false;
            $scope.IsWorkCenter = false;
            $scope.IsPendingInvoice = true;
            $('#headerTital').text('Pending Invoice')
        }
        $scope.WorkCenter = type;
        // alert(step);
        $scope.getOrderList(type);
        $scope.getInvoiceStatusList();

        $(document).ready(function () {
            $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-green'
            });
        });



        $('input').on('ifChecked', function (event) {
            if (this.value == "Closed") {
                $scope.getOrderListClosed('INVOICED');
            }
            else {
                $scope.getOrderList('INVOICED');
            }
        });
    }
    $scope.getParameterByName = function (name) {
        var regexS = "[\\?&]" + name + "=([^&#]*)",
      regex = new RegExp(regexS),
      results = regex.exec(window.location.search);
        if (results == null) {
            return "";
        } else {
            return decodeURIComponent(results[1].replace(/\+/g, " "));
        }
    },
        
    $scope.closeInvoice = function () {
        $scope.isOperatorLoading = true;
        var amt = $("#txtCustAmount").val();
        $scope.WorkCenterDetail.Amount = amt;
        Invoice.closeInvoice($scope.WorkCenterDetail).then(function (response) {
                if (response.data.status == "200") {
                    $('#startworkCenter').modal('hide');
                    swal({ title: "Closed!", text: "Invoice successfully closed.", type: "success" },
                        function () { location.reload(); });
                } else if (response.data.status == "500") {
                    window.location.href = "/Home/Error";
                }
            });
        
        $scope.isOperatorLoading = false;
        }
    $scope.GetAllIvoiceList = [];
    $scope.getOrderList = function (type) {
        $scope.isOrderLoading = true;
        $('#headerTital').text('Pending Invoice')
        $scope.InvoiceOrder = 'Pending';
            Invoice.getAllInvoices(type).then(function (response) {
                if (response.data.status == "200") {
                    $scope.invoiceType = type;
                    $scope.GetAllIvoiceList = response.data.item;
                }
                else if (response.data.status == "500") {
                    window.location.href = "/Home/Error";
                }
            });
            $scope.isOrderLoading = false;
        }
/******** Created by Rajesh *********/
    $scope.GetAllClosedIvoiceList = [];
    $scope.getOrderListClosed = function (type) {
        $scope.isOrderLoading = true;       
        $scope.InvoiceOrder = 'Closed';
        $('#headerTital').text('Closed Invoice')
        Invoice.getAllClosedInvoices(type).then(function (response) {
            if (response.data.status == "200") {
                $scope.invoiceType = type;
                $scope.GetAllClosedIvoiceList = response.data.item;
            }
            else if (response.data.status == "500") {
                window.location.href = "/Home/Error";
            }
        });
        $scope.isOrderLoading = false;
    }
    $scope.getInvoiceStatusList = function () {
        $scope.isOrderLoading = true;
        
        Invoice.getInvoiceStatus().then(function (response) {
            if (response.data.status == "200") {
                $scope.GetAllIvoiceStatusList = response.data.item;
            }
            else if (response.data.status == "500") {
                window.location.href = "/Home/Error";
            }
        });
        $scope.isOrderLoading = false;
    }
    
    $scope.generateInvoiceModel = function () {
        var order = $scope.getSelected();
        debugger;
        if (order == null) {
            swal("Oops!", "Please select JOB.", "warning");
        }
        else {
            var customerId = 0;
            var custCount = 0;
            var InvoiceId = order.InvoiceId;
            debugger;
        
            $scope.orderIdForInvoice = InvoiceId;
            $scope.forstartJOB = true;
            $scope.WorkCenterDetail = { InvoiceId: InvoiceId, InvoiceStatusId: order.InvoiceStatusId, Amount:0 }
            $scope.orderDetail = order;

            Invoice.getOrderByInvoiceIds(InvoiceId).then(function (result) {
                if (result.data.status == "200") {
                    $scope.InvoiceOrderDetail = result.data.item;
                    var amount = 0;
                    var AmauntPaid = 0;
                    for (var i = 0; i < $scope.InvoiceOrderDetail.length; i++) {
                        amount = amount + $scope.InvoiceOrderDetail[i].TotalOrderPrice;
                        AmauntPaid = AmauntPaid + $scope.InvoiceOrderDetail[i].AmauntPaid;
                    }
                    $scope.WorkCenterDetail.Amount = amount - AmauntPaid;

                    for (var i = 0; i < $scope.GetAllIvoiceStatusList.length; i++) {
                        if ($scope.GetAllIvoiceStatusList[i].Code == "CLOSED")
                        {
                            $scope.WorkCenterDetail.InvoiceStatusId = $scope.GetAllIvoiceStatusList[i].Id;
                    }
                    }
         
                    $('#mdlGenerateInvoice').modal('show');

                } else if (result.data.status == "500") {
                    window.location.href = "/Home/Error";
                }
            });

        }
    },
$scope.PrintInvoice = function (orderIds) {
        var divToPrint = document.getElementById('print');

                       var newWin = window.open('', 'Print-Window');

                       newWin.document.open();

                       newWin.document.write('<html><body onload="window.print()">' + divToPrint.innerHTML + '</body></html>');

                       newWin.document.close();

                       setTimeout(function () { newWin.close(); location.reload(); }, 3);
},
 $scope.getSelected = function () {
     var order = null;
     for (var i = 0; i < $scope.GetAllIvoiceList.length; i++) {
         if ($scope.GetAllIvoiceList[i].Selected == true) {
             order = $scope.GetAllIvoiceList[i];
             break;
         }
     }
     return order;
 },
   $scope.getSelectedForInvoice = function () {
       var order = [];
       for (var i = 0; i < $scope.GetAllIvoiceList.length; i++) {
           if ($scope.GetAllIvoiceList[i].Selected == true) {
               order.push($scope.GetAllIvoiceList[i]);
           }
       }
       return order;
   },
$scope.CheckBox_Checked = function (selectedJobId) {
    for (var i = 0; i < $scope.GetAllIvoiceList.length; i++) {
        if ($scope.GetAllIvoiceList[i].Id != selectedJobId && !$scope.IsInvoice) {
            $scope.GetAllIvoiceList[i].Selected = false;
        }
    }
}

    $scope.viewOrderDetails = function (orderId) {
        $('#ifrm').attr('src', "/order/viewDetails/" + orderId)
        $('#orderDetails').modal('show');
    },
     $scope.InvoicePreView = function (orderIds) {
         $('#ifrmInvoicePreView').attr('src', "/order/InvoicePreView/" + orderIds)
         $('#InvoicePreView').modal('show');
     },
      $scope.InvoiceView = function (orderIds) {


          $("#print").load("/order/InvoiceView/" + orderIds, function () {
              $('#InvoicePreView').modal('show');
          });
        
      },
     $scope.sort = function (keyname) {
         $scope.sortKey = keyname;
         $scope.reverse = !$scope.reverse;
     }
});