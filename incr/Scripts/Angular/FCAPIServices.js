﻿angular.module('FCTool', []).
    factory('FCAPIServices', ['$http', function ($http) {

        function FCAPI() {
            // Define the Field which can used in controller
        }

        FCAPI.prototype.getOperator = function () {
            var url = '/Operator/GetAllOperator';
            return $http({ method: 'Get', url: url });
        };
       
        FCAPI.prototype.addOperator = function (ClassTypeName) {
            var url = '/Operator/Create?ClassTypeName=' + ClassTypeName;
            return $http({ method: 'GET', url: url });
        };
        FCAPI.prototype.updateOperator = function (id, operatorModel) {
            var url = '/Operator/Edit?id=' + id + '&operatorModel=' + operatorModel;
            return $http({ method: 'GET', url: url });
        };
        FCAPI.prototype.deleteOperator = function (id) {

            var url = '/Operator/Delete/?id=' + id;
            return $http({ method: 'GET', url: url });
        };
    }]);