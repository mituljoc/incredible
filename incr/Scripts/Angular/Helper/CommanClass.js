﻿function SearchFilterOption(option) {
    var self = this;
    self.isMultitagSearch = option.isMultitagSearch,
    self.isAdvanceSearch = option.isAdvanceSearch,
    self.subCategoryList = option.subCategoryList,
    self.categoryList = option.categoryList,
    self.activityList = option.activityList,
    self.locationList = option.locationList,
    self.locationAreaList = option.locationAreaList,
    self.yearList = option.yearList,
    self.classList = option.classList,
    self.dayList = option.dayList,
    self.timeList = option.timeList,
    self.schoolList = option.schoolList,
    self.isAfterLogin = option.isAfterLogin,
    self.SearchString = option.SearchString,
    self.isALBSearch = option.isALBSearch,
    self.isUOB = option.isUOB
    self.ClassId = 0;
    self.selectedItems = option.selectedItems;
    self.classTypeList = option.classTypeList;
    self.minAge = option.minAge
    self.maxAge = option.maxAge
    self.minDate = option.minDate
    self.maxDate = option.maxDate
    self.childId = option.childId
}
function ClearSearchFilterbyOption(option, searchtype) {
    if (searchtype = "Location") {
        option.locationList = "";
    }
    if (searchtype = "Age") {
        option.minAge = "";
        option.maxAge == "";
    }
    if (searchtype = "Date") {
        option.minDate = "";
        option.maxDate == "";
    }
    var self = this;
    self.isMultitagSearch = option.isMultitagSearch,
    self.isAdvanceSearch = option.isAdvanceSearch,
    self.subCategoryList = option.subCategoryList,
    self.categoryList = option.categoryList,
    self.activityList = option.activityList,   
    self.locationList = option.locationList,
    self.locationAreaList = option.locationAreaList,
    self.yearList = option.yearList,
    self.classList = option.classList,
    self.dayList = option.dayList,
    self.timeList = option.timeList,
    self.schoolList = option.schoolList,
    self.isAfterLogin = option.isAfterLogin,
    self.SearchString = option.SearchString,
    self.isALBSearch = option.isALBSearch,
    self.isUOB = option.isUOB
    self.ClassId = 0;
    self.selectedItems = option.selectedItems;
    self.classTypeList = option.classTypeList;
    self.minAge = option.minAge
    self.maxAge = option.maxAge
    self.minDate = option.minDate
    self.maxDate = option.maxDate
}
function ClearAllFilterOption(option) {

    var self = this;
    option.isMultitagSearch = false;
    option.isAdvanceSearch = false;
    option.categoryList = "";
    option.subCategoryList = "";
    option.locationAreaList = "";
    option.activityList = "";
    option.locationList = "";
    option.yearList = "";
    option.classList = "";
    option.dayList = "";
    option.timeList = "";
    option.schoolList = "";
    option.isAfterLogin = false;
    option.isALBSearch = 0;
    option.ClassId = 0;
    option.classTypeList = "";
    option.lat = "";
    option.long = "";
    option.minAge = "";
    option.maxAge = "";
    option.maxDate = "";
    option.minDate = "";
    option.long = "";
    option.childId = "";
    //option.isUOB = 0;    
    localStorage.setItem("SearchParameters", JSON.stringify(option));
}
function RedirectParamterOption(option) {
    var self = this;
    self.redirectValue = option.redirectValue;
    self.schoolName = option.schoolName;
    self.redirectMode = option.redirectMode;
    self.returnRedirectMode = option.returnRedirectMode;
    self.categoryID = option.categoryID;
    self.className = option.className;
    self.LastPageIndex = option.LastPageIndex;
    self.SchoolID = option.SchoolID;
    self.classType = option.classType;

}
/* guid function: For generate one unique random guid */
function guid() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
          .toString(16)
          .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}
function BookingDetail(option) {
    var self = this
    self.parentid = option.parentid;   
    self.schoolId = option.schoolId;
    self.classId = option.classId;
    self.timeSlotId = option.timeSlotId;
    self.schoolName = option.schoolName;
    self.className = option.className;
    self.classTypeId = option.classTypeId;
    self.isPointComplete = option.isPointComplete;
    self.multiplyEffect = option.multiplyEffect;
    self.isReschedule = option.isReschedule;
    self.reason = option.reason;
    self.rescheduleHistoryId = option.rescheduleHistoryId;
    self.isAppBooking = option.isAppBooking;
    self.paymentguid = option.paymentguid;
    self.transactionid = option.transactionid;
    self.status = option.status;
    self.discountPrice = option.discountPrice;
    self.voucherCode = option.voucherCode;
    self.redeemBalance = option.redeemBalance;
    self.paymentVia = option.paymentVia;
    self.gstAmount = option.gstAmount;  
    self.totalDiscountPrice = option.totalDiscountPrice;
    self.totalAttendeeAddOnPrice = option.totalAttendeeAddOnPrice;
    self.totalAttendee = option.totalAttendee;
    self.totalPackges = option.totalPackges;
    self.isGuestUser = option.isGuestUser;
    self.specialCode = option.specialCode;
    self.staffCodeID = option.staffCodeID;
    self.firstName = option.firstName;
    self.lastName = option.lastName;
    self.email = option.email;
    self.mobileNo = option.mobileNo;
    self.bookingPackageList = option.bookingPackageList; /*List*/
    self.bookingQuestionAnswerList = [];
    self.isBookingProcess = option.isBookingProcess;
    self.lessonInfoForMail = option.lessonInfoForMail;
    self.emailNotification = option.emailNotification;
    self.agentName = option.agentName;
    self.agentEmail = option.agentEmail;
    self.googleMapPath = option.googleMapPath;
    self.schoolTransportationDetails = option.schoolTransportationDetails;
    self.startTime = option.startTime;
    self.endTime = option.endTime;
    self.classdate = option.classdate;
    self.classday = option.classday;
    self.regulerClassTimeSlotID = option.regulerClassTimeSlotID;
    self.uniqueId = guid();

    self.packageSubTotal = function () {
        var total = 0;
        $.each(self.bookingPackageList, function (index, pkg) {
            total = total + parseFloat(pkg.selectedPackageTotalPrice);
        });

        return total;
    }

    self.totalPrice = function () {
        var total = 0;
        total = self.packageSubTotal() - self.totalDiscountPrice;
        return total;
    }
}