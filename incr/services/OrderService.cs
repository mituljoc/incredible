﻿using HiQPdf;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;

namespace incr.services
{
    public delegate void delegatSendOrderReceiptMail(Models.OrderDTO order, string templatePath, string fileName, bool isforoprerator);
    public delegate void delegatGeneratePDFAndSendMail(string htmlString, string fileName, string job);

    public class OrderService
    {
        private Utilities.DataAccess _dataAccess;

        public OrderService()
        {
            _dataAccess = new Utilities.DataAccess();
        }

        public string getOrderReciptForPrint(long jobId)
        {
            Models.OrderDTO objOrder = new Models.OrderDTO();
            objOrder = _dataAccess.GetListof<Models.OrderDTO>("select o.* ,pl.name as ParticularName,pl.code as ParticularCode,pl.range as ParticularRange,fb.FacricName as FabricType4, fb.fabricGroup as FabricGroup4 ,fb5.FacricName as FabricType5,fb5.fabricGroup as FabricGroup5 from[incr_order_master] o left join incr_price_list pl on o.particularnameId = pl.id left join Incr_FabricMaster fb on fb.Rid = o.particula4fabricTypeid  left join Incr_FabricMaster fb5 on fb5.Rid = o.particular5fabricTypeid  where o.id=" + jobId).SingleOrDefault();
            string templatePath = HttpContext.Current.Server.MapPath(Path.Combine("~/content/template/orderreceipt.html"));
            return sendOrderReceiptMail(objOrder, templatePath, "",false,true);
        }

        public long SaveOrder(Models.OrderDTO order)
        {
        
            try
            {
                SqlParameter[] sqlParams = new SqlParameter[] {


                     new SqlParameter("@id", order.Id),

                        new SqlParameter("@Sales_By_Id", order.SalesById),
                           new SqlParameter("@Job_By_Id", order.JobById),
                              new SqlParameter("@Customer_Id", order.CustomerId),
           new SqlParameter("@job_no", order.JobNo),
           new SqlParameter("@customer_name", order.CustomerName),
           new SqlParameter("@mobile", order.Mobile),
           new SqlParameter("@address", order.Address),
           new SqlParameter("@order_date", order.OrderDate),
           new SqlParameter("@sales_by", order.SalesBy),
           new SqlParameter("@job_by", order.JobBy),
           new SqlParameter("@ply_type", order.PlyType),
           new SqlParameter("@spring", order.Spring),
           new SqlParameter("@foaming", order.Foaming),
           new SqlParameter("@linning_type", order.LinningType),
           new SqlParameter("@cushion_back", order.CushionBack),
           new SqlParameter("@stitch_for_sofa", order.StitchForSofa),
           new SqlParameter("@cushion_stitch", order.CushionStitch),
           new SqlParameter("@fitting", order.Fitting),
           new SqlParameter("@packing", order.Packing),
           new SqlParameter("@delivery", order.Delivery),
           new SqlParameter("@delivery_date", order.DeliveryDate),
           new SqlParameter("@TapestryBody", order.TapestryBody),
           new SqlParameter("@TapestrySeat", order.TapestrySeat),
           new SqlParameter("@TapestryCushion", order.TapestryCushion),
           new SqlParameter("@TapestryBodyMTR", order.TapestryBodyMTR),
           new SqlParameter("@TapestrySeatMTR", order.TapestrySeatMTR),
           new SqlParameter("@TapestryCushionMTR", order.TapestryCushionMTR),
           new SqlParameter("@TapestryBodyFabricType", order.TapestryBodyFabricType),
           new SqlParameter("@TapestrySeatFabricType", order.TapestrySeatFabricType),
           new SqlParameter("@TapestryCushionFabricType", order.TapestryCushionFabricType),
           new SqlParameter("@Particular1", order.Particular1),
           new SqlParameter("@Particular2", order.Particular2),
           new SqlParameter("@Particular3", order.Particular3),
                new SqlParameter("@Particular4", order.Particular4),
           new SqlParameter("@Particular1Qty", order.Particular1Qty),
           new SqlParameter("@Particular2Qty", order.Particular2Qty),
           new SqlParameter("@Particular3Qty", order.Particular3Qty),

           new SqlParameter("@Particular1Rate", order.Particular1Rate),
           new SqlParameter("@Particular2Rate", order.Particular2Rate),
           new SqlParameter("@Particular3Rate", order.Particular3Rate),
           new SqlParameter("@Particular4Rate", order.Particular4Rate),
           new SqlParameter("@Particular4Qty", order.Particular4Qty),
           new SqlParameter("@Particular1Total", order.Particular1Total),
           new SqlParameter("@Particular2Total", order.Particular2Total),
           new SqlParameter("@Particular3Total", order.Particular3Total),
           new SqlParameter("@Particular4Total", order.Particular4Total),
           new SqlParameter("@ParticularNameId", order.ParticularNameId),
           new SqlParameter("@CarpantaryBottomWidth", order.CarpantaryBottomWidth),
           new SqlParameter("@CarpantaryLeftHight", order.CarpantaryLeftHight),
           new SqlParameter("@CarpantaryTopWidth", order.CarpantaryTopWidth),
           new SqlParameter("@CarpantaryMiddleWidth", order.CarpantaryMiddleWidth),
           new SqlParameter("@CarpantaryRightHight", order.CarpantaryRightHight),
           new SqlParameter("@HandleHeight", order.HandleHeight),
           new SqlParameter("@HandleWidth", order.HandleWidth),
           new SqlParameter("@HandleLength", order.HandleLength),
           new SqlParameter("@DrawingType", order.DrawingType),
            new SqlParameter("@LeftNumberOfsits", order.LeftNumberOfsits),
            new SqlParameter("@RightNumberOfsits", order.RightNumberOfsits),
            new SqlParameter("@DrawingRightSize0", order.DrawingRightSize0),
            new SqlParameter("@DrawingRightSize1", order.DrawingRightSize1),
            new SqlParameter("@DrawingRightSize2", order.DrawingRightSize2),
            new SqlParameter("@DrawingRightSize3", order.DrawingRightSize3),
            new SqlParameter("@DrawingRightSize4", order.DrawingRightSize4),
            new SqlParameter("@DrawingRightSize5", order.DrawingRightSize5),
            new SqlParameter("@DrawingRightSize6", order.DrawingRightSize6),
            new SqlParameter("@DrawingleftSize1", order.DrawingleftSize1),
            new SqlParameter("@DrawingleftSize2", order.DrawingleftSize2),
            new SqlParameter("@DrawingleftSize3", order.DrawingleftSize3),
            new SqlParameter("@DrawingleftSize4", order.DrawingleftSize4),
            new SqlParameter("@DrawingleftSize5", order.DrawingleftSize5),
            new SqlParameter("@DrawingleftSize6", order.DrawingleftSize6) ,
            new SqlParameter("@Drawingfullheight", order.Drawingfullheight),
            new SqlParameter("@Drawingfullwidth", order.Drawingfullwidth),
            new SqlParameter("@LongerSide", order.LongerSide),
            new SqlParameter("@linningqty", order.LinningQty),
            new SqlParameter("@BodyFabricTypeId", order.BodyFabricTypeId)  ,
            new SqlParameter("@SeatFabricTypeId", order.SeatFabricTypeId),
            new SqlParameter("@CushionFabricTypeId", order.CushionFabricTypeId),
            new SqlParameter("@Discount", order.Discount),
            new SqlParameter("@TransPortPrice", order.TransPortPrice),
            new SqlParameter("@Extra", order.Extra),
            new SqlParameter("@SGST", order.SGST),
            new SqlParameter("@CGST", order.CGST),
            new SqlParameter("@CarpantaryRightWidth", order.CarpantaryRightWidth),
             new SqlParameter("@NetOrderPrice", order.NetOrderPrice),
             new SqlParameter("@TotalOrderPrice", order.TotalOrderPrice),
             new SqlParameter("@DrawingHandle1", order.DrawingHandle1),
             new SqlParameter("@DrawingHandle2", order.DrawingHandle2),
             new SqlParameter("@DrawingHandle3", order.DrawingHandle3),
             new SqlParameter("@DrawingHandle4", order.DrawingHandle4)
             ,
             new SqlParameter("@Particula4FabricTypeId", order.Particula4FabricTypeId)
             ,
             new SqlParameter("@IsWithoutfabric", order.IsWithoutfabric)
             ,
             new SqlParameter("@CarpantaryRightHight1", order.CarpantaryRightHight1)
             ,
             new SqlParameter("@CarpantaryRightHight2", order.CarpantaryRightHight2)
             ,
             new SqlParameter("@CarpantaryRightHight3", order.CarpantaryRightHight3)
        ,
             new SqlParameter("@Particular5", order.Particular5)
              ,
             new SqlParameter("@Particular5Qty", order.Particular5Qty)
              ,
             new SqlParameter("@Particular5Rate", order.Particular5Rate)
              ,
             new SqlParameter("@Particular5FabricTypeId", order.Particular5FabricTypeId),
             new SqlParameter("@Polish", order.Polish),
             new SqlParameter("@Partyname", order.Partyname)
                };

                DataTable dt = _dataAccess.GetDataTable("incr_inserOrderDetails", sqlParams);
                long jobId = 0;
                if (dt != null && dt.Rows.Count > 0)
                {
                    string jobNumber = dt.Rows[0]["JobNo"].ToString();
                     jobId = Convert.ToInt32(dt.Rows[0]["Id"].ToString());
                    Models.OrderDTO objOrder = new Models.OrderDTO();
                    objOrder = _dataAccess.GetListof<Models.OrderDTO>("select o.* ,pl.name as ParticularName,pl.code as ParticularCode,pl.range as ParticularRange,fb.FacricName as FabricType4, fb.fabricGroup as FabricGroup4 ,fb5.FacricName as FabricType5,fb5.fabricGroup as FabricGroup5 from[incr_order_master] o left join incr_price_list pl on o.particularnameId = pl.id left join Incr_FabricMaster fb on fb.Rid = o.particula4fabricTypeid  left join Incr_FabricMaster fb5 on fb5.Rid = o.particular5fabricTypeid  where o.id=" + jobId.ToString()).SingleOrDefault();

                    order.JobNo = jobNumber;
                    string templatePath = HttpContext.Current.Server.MapPath(Path.Combine("~/content/template/orderreceipt.html"));
                    string fileName = HttpContext.Current.Server.MapPath("~/content/files/orderreceipt/job" + order.JobNo.Trim().ToString() + ".pdf");

                    sendOrderReceiptMail(objOrder, templatePath, fileName, false);
                }
                //sendOrderReceiptMail(order, templatePath, fileName);
                return jobId;
            }
            catch (Exception ex)
            {

                throw ex;
            }


        }
        public string  sendOrderReceiptMail(Models.OrderDTO order, string templatePath, string fileName, bool isforoprerator ,bool isPrint=false)
        {
            try
            {
                HtmlDocument docString = new HtmlDocument();

                docString.Load(templatePath);
                string htmlString = docString.DocumentNode.OuterHtml.ToString();
                htmlString = htmlString.Replace("#JOBNUMBER#", order.JobNo);
                htmlString = htmlString.Replace("#customername#", order.CustomerName);
                htmlString = htmlString.Replace("#address#", order.Address);
                htmlString = htmlString.Replace("#mobile#", order.Mobile);
                htmlString = htmlString.Replace("#orderdate#", order.OrderDate.ToString("MM/dd/yyyy"));
                htmlString = htmlString.Replace("#deldate#", order.DeliveryDate.ToString("MM/dd/yyyy"));
                htmlString = htmlString.Replace("#jobby#", order.JobBy);
                htmlString = htmlString.Replace("#salesby#", order.SalesBy);
                htmlString = htmlString.Replace("#Particular1#", order.Particular1);
         
                    htmlString = htmlString.Replace("#Particularqty1#", order.Particular1Qty.ToString());
                    htmlString = htmlString.Replace("#Particularrate1#", order.Particular1Rate.ToString("0.00"));
                    htmlString = htmlString.Replace("#Particulartotal1#", (order.Particular1Total).ToString("0.00"));
                


                htmlString = htmlString.Replace("#Particular2#", order.Particular2);

                htmlString = htmlString.Replace("#Particularqty2#", order.Particular2Qty.ToString());
                htmlString = htmlString.Replace("#Particularrate2#", order.Particular2Rate.ToString("0.00"));
                htmlString = htmlString.Replace("#Particulartotal2#", (order.Particular2Total).ToString("0.00"));



                htmlString = htmlString.Replace("#Particular3#", order.Particular3);

                htmlString = htmlString.Replace("#POLISH#", order.Polish);
                htmlString = htmlString.Replace("#Partyname#", order.Partyname);
                htmlString = htmlString.Replace("#Particularqty3#", order.Particular3Qty.ToString());
                htmlString = htmlString.Replace("#Particularrate3#", order.Particular3Rate.ToString("0.00"));
                htmlString = htmlString.Replace("#Particulartotal3#", (order.Particular3Total).ToString("0.00"));
                htmlString = htmlString.Replace("#ParticularSubtotal#", (order.Particular1Total + order.Particular2Total + order.Particular3Total + order.Particular4Total).ToString("0.00"));
                htmlString = htmlString.Replace("#Particular4#", order.Particular4);
                htmlString = htmlString.Replace("#Particularqty4#", order.Particular4Qty.ToString());
                htmlString = htmlString.Replace("#Particularrate4#", order.Particular4Rate.ToString("0.00"));

                htmlString = htmlString.Replace("#Particulartotal4#", (order.Particular4Total).ToString("0.00"));

                htmlString = htmlString.Replace("#Particular5#", order.Particular5);
                htmlString = htmlString.Replace("#Type5#", order.FabricType5??"");
                htmlString = htmlString.Replace("#Particula5GroupId#", order.FabricGroup5??"");
                
                htmlString = htmlString.Replace("#Particularqty5#", order.Particular5Qty.ToString());
                htmlString = htmlString.Replace("#Particularrate5#", order.Particular5Rate.ToString("0.00"));
                htmlString = htmlString.Replace("#Particulartotal5#", (order.Particular5Total).ToString("0.00"));

                htmlString = htmlString.Replace("#ParticularName#", order.ParticularName);
                htmlString = htmlString.Replace("#CODE#", order.ParticularCode);
                htmlString = htmlString.Replace("#RANGE#", (order.ParticularRange));

                htmlString = htmlString.Replace("#SGSTPER#", (order.SGST.ToString("0.00")));

              
                htmlString = htmlString.Replace("#SGSTValue#", ((order.Particular1Total + order.Particular2Total + order.Particular3Total + order.Particular4Total)*(order.SGST)/100).ToString("0.00"));
                htmlString = htmlString.Replace("#CGSTPER#", (order.CGST.ToString("0.00")));
                htmlString = htmlString.Replace("#CGSTValue#", ((order.Particular1Total + order.Particular2Total + order.Particular3Total + order.Particular4Total) * (order.CGST) / 100).ToString("0.00"));


                htmlString = htmlString.Replace("#Transport#", (order.TransPortPrice.ToString("0.00")));
                htmlString = htmlString.Replace("#Extra#", (order.Extra.ToString("0.00")));
                htmlString = htmlString.Replace("#Discount#", (order.Discount.ToString("0.00")));
                htmlString = htmlString.Replace("#GROSS#", (order.TotalOrderPrice.ToString("0.00")));

                htmlString = htmlString.Replace("#Particularrate2#", "");
                htmlString = htmlString.Replace("#Particulartotal2#", "");
                htmlString = htmlString.Replace("#Particularrate1#", "");
                htmlString = htmlString.Replace("#Particulartotal1#", "");
                htmlString = htmlString.Replace("#Particularqty1#", "");
                htmlString = htmlString.Replace("#Particularqty2#", "");
                htmlString = htmlString.Replace("#Particularqty3#", "");
                htmlString = htmlString.Replace("#Particularrate3#", "");
                htmlString = htmlString.Replace("#Particulartotal3#", "");
                htmlString = htmlString.Replace("#ply#", order.PlyType);
                htmlString = htmlString.Replace("#Spring#", order.Spring);
                htmlString = htmlString.Replace("#Type#", order.LinningType);
                htmlString = htmlString.Replace("#Cushionback#", order.CushionBack);
                htmlString = htmlString.Replace("#Foaming#", order.Foaming);
                htmlString = htmlString.Replace("#Stitch#", order.StitchForSofa);
                htmlString = htmlString.Replace("#Cushionstitch#", order.CushionStitch);
                htmlString = htmlString.Replace("#Tapestrybody#", order.TapestryBody);
                htmlString = htmlString.Replace("#Tapestryseat#", order.TapestrySeat);

                htmlString = htmlString.Replace("#Tapestrycushion#", order.TapestryCushion);
                htmlString = htmlString.Replace("#CarpantaryRightWidth#", order.CarpantaryRightWidth.ToString("0.00"));

                htmlString = htmlString.Replace("#Type1#", order.TapestryBodyFabricType);
                htmlString = htmlString.Replace("#Type2#", order.TapestrySeatFabricType);
                htmlString = htmlString.Replace("#Type3#", order.TapestryCushionFabricType);
                htmlString = htmlString.Replace("#Type4#", order.FabricType4);
                htmlString = htmlString.Replace("#Particula4GroupId#", order.FabricGroup4);
                
                htmlString = htmlString.Replace("#meters1#", order.TapestryBodyMTR.ToString("0.00"));
                htmlString = htmlString.Replace("#meters2#", order.TapestrySeatMTR.ToString("0.00"));
                htmlString = htmlString.Replace("#meters3#", order.TapestryCushionMTR.ToString("0.00"));
                htmlString = htmlString.Replace("#Fitting#", order.Fitting);
                htmlString = htmlString.Replace("#Packing#", order.Packing);
                htmlString = htmlString.Replace("#Deliveryby#", order.Delivery);
                htmlString = htmlString.Replace("#carpantarytop#", order.CarpantaryTopWidth.ToString("0.00"));
                htmlString = htmlString.Replace("#carpantarymiddle#", order.CarpantaryMiddleWidth.ToString("0.00"));
                htmlString = htmlString.Replace("#carpantaryleft#", order.CarpantaryLeftHight.ToString("0.00"));
                htmlString = htmlString.Replace("#carpantaryright#", order.CarpantaryRightHight.ToString("0.00"));
                htmlString = htmlString.Replace("#carpantarybottom#", order.CarpantaryBottomWidth.ToString("0.00"));
                htmlString = htmlString.Replace("#HandleLength#", order.HandleLength.ToString("0.00"));
                htmlString = htmlString.Replace("#HandleHeight#", order.HandleHeight.ToString("0.00"));
                htmlString = htmlString.Replace("#HandleWidth#", order.HandleWidth.ToString("0.00"));
                htmlString = htmlString.Replace("#DrawingRightSize0#", order.DrawingRightSize0.ToString("0.00"));
                htmlString = htmlString.Replace("#DrawingleftSize1#", order.DrawingleftSize1.ToString("0.00"));
                htmlString = htmlString.Replace("#DrawingleftSize2#", order.DrawingleftSize2.ToString("0.00"));
                htmlString = htmlString.Replace("#DrawingleftSize3#", order.DrawingleftSize3.ToString("0.00"));
                htmlString = htmlString.Replace("#DrawingleftSize4#", order.DrawingleftSize4.ToString("0.00"));
                htmlString = htmlString.Replace("#DrawingleftSize5#", order.DrawingleftSize5.ToString("0.00"));
                htmlString = htmlString.Replace("#DrawingleftSize6#", order.DrawingleftSize6.ToString("0.00"));
                htmlString = htmlString.Replace("#DrawingRightSize1#", order.DrawingRightSize1.ToString("0.00"));
                htmlString = htmlString.Replace("#DrawingRightSize2#", order.DrawingRightSize2.ToString("0.00"));
                htmlString = htmlString.Replace("#DrawingRightSize3#", order.DrawingRightSize3.ToString("0.00"));
                htmlString = htmlString.Replace("#DrawingRightSize4#", order.DrawingRightSize4.ToString("0.00"));
                htmlString = htmlString.Replace("#DrawingRightSize5#", order.DrawingRightSize5.ToString("0.00"));
                htmlString = htmlString.Replace("#DrawingRightSize6#", order.DrawingRightSize6.ToString("0.00"));
                htmlString = htmlString.Replace("#CarpantaryRightHight3#", order.CarpantaryRightHight3.ToString("0.00"));
                htmlString = htmlString.Replace("#CarpantaryRightHight2#", order.CarpantaryRightHight2.ToString("0.00"));
                htmlString = htmlString.Replace("#CarpantaryRightHight1#", order.CarpantaryRightHight1.ToString("0.00"));

                htmlString = htmlString.Replace("#DrawingType#", order.DrawingType);
                htmlString = htmlString.Replace("#Drawingfullwidth#", order.Drawingfullwidth.ToString("0.00"));
                htmlString = htmlString.Replace("#Drawingfullheight#", order.Drawingfullheight.ToString("0.00"));
                if (order.DrawingType.ToUpper() == "Longer".ToUpper())
                {
                    htmlString = htmlString.Replace("#Longer#", "");
                    if (order.LongerSide.ToLower() == "left".ToLower())
                    {
                        htmlString = htmlString.Replace("#right2#", "display:none;");
                        htmlString = htmlString.Replace("#right3#", "display:none;");
                        htmlString = htmlString.Replace("#right4#", "display:none;");
                        htmlString = htmlString.Replace("#right5#", "display:none;");
                        htmlString = htmlString.Replace("#right6#", "display:none;");
                        htmlString = htmlString.Replace("#LongerRightchk#", "display:none;");
                        htmlString = htmlString.Replace("#RightNumberOfsits#", "0");
                        htmlString = htmlString.Replace("#LongerRight#", "LongerRight-CLS");

                    }
                    else
                    {
                        htmlString = htmlString.Replace("#LongerLeft#", "LongerLeft-CLS");
                        htmlString = htmlString.Replace("#left2#", "display:none;");
                        htmlString = htmlString.Replace("#left3#", "display:none;");
                        htmlString = htmlString.Replace("#left4#", "display:none;");
                        htmlString = htmlString.Replace("#left5#", "display:none;");
                        htmlString = htmlString.Replace("#left6#", "display:none;");
                        htmlString = htmlString.Replace("#longerLeftchk#", "display:none;");
                        htmlString = htmlString.Replace("#LeftNumberOfsits#", "0");
                    }
                }
                else if (order.DrawingType.ToUpper() == "Set".ToUpper())
                {

                    htmlString = htmlString.Replace("#setOnly#", "");
                }
                else
                {

                    htmlString = htmlString.Replace("#corner#", "");
                }

                if (order.RightNumberOfsits >= 1)
                {
                    htmlString = htmlString.Replace("#right1#", "");
                }
                if (order.RightNumberOfsits >= 2)
                {
                    htmlString = htmlString.Replace("#right2#", "");
                }
                if (order.RightNumberOfsits >= 3)
                {
                    htmlString = htmlString.Replace("#right3#", "");
                }
                if (order.RightNumberOfsits >= 4)
                {
                    htmlString = htmlString.Replace("#right4#", "");
                }
                if (order.RightNumberOfsits >= 5)
                {
                    htmlString = htmlString.Replace("#right5#", "");
                }
                if (order.RightNumberOfsits >= 6)
                {
                    htmlString = htmlString.Replace("#right6#", "");
                }

                if (order.LeftNumberOfsits >= 1)
                {
                    htmlString = htmlString.Replace("#left1#", "");
                }
                if (order.LeftNumberOfsits >= 2)
                {
                    htmlString = htmlString.Replace("#left2#", "");
                }
                if (order.LeftNumberOfsits >= 3)
                {
                    htmlString = htmlString.Replace("#left3#", "");
                }
                if (order.LeftNumberOfsits >= 4)
                {
                    htmlString = htmlString.Replace("#left4#", "");
                }
                if (order.LeftNumberOfsits >= 5)
                {
                    htmlString = htmlString.Replace("#left5#", "");
                }
                if (order.LeftNumberOfsits >= 6)
                {
                    htmlString = htmlString.Replace("#left6#", "");
                }
                htmlString = htmlString.Replace("#LeftNumberOfsits#", order.LeftNumberOfsits.ToString());
                htmlString = htmlString.Replace("#RightNumberOfsits#", order.RightNumberOfsits.ToString());
                htmlString = htmlString.Replace("#corner#", "display:none;");
                htmlString = htmlString.Replace("#setOnly#", "display:none;");
                htmlString = htmlString.Replace("#Longer#", "display:none;");
                htmlString = htmlString.Replace("#LongerRight#", "display:none;");
                htmlString = htmlString.Replace("#left1#", "display:none;");
                htmlString = htmlString.Replace("#left2#", "display:none;");
                htmlString = htmlString.Replace("#left3#", "display:none;");
                htmlString = htmlString.Replace("#left4#", "display:none;");
                htmlString = htmlString.Replace("#left5#", "display:none;");
                htmlString = htmlString.Replace("#left6#", "display:none;");
                htmlString = htmlString.Replace("#right2#", "display:none;");
                htmlString = htmlString.Replace("#right3#", "display:none;");
                htmlString = htmlString.Replace("#right4#", "display:none;");
                htmlString = htmlString.Replace("#right5#", "display:none;");
                htmlString = htmlString.Replace("#right6#", "display:none;");

                htmlString = htmlString.Replace("#LongerLeft#", "");
                htmlString = htmlString.Replace("#DrawingHandle1#", order.DrawingHandle1.ToString("0.00"));
                htmlString = htmlString.Replace("#DrawingHandle2#", order.DrawingHandle2.ToString("0.00"));
                htmlString = htmlString.Replace("#DrawingHandle3#", order.DrawingHandle3.ToString("0.00"));
                htmlString = htmlString.Replace("#DrawingHandle4#", order.DrawingHandle4.ToString("0.00"));
                if (!isPrint)
                {
                    delegatGeneratePDFAndSendMail objdelegatSendOrderReceiptMail = new delegatGeneratePDFAndSendMail(GeneratePDFAndSendMail);

                    objdelegatSendOrderReceiptMail.BeginInvoke(htmlString, fileName, order.JobNo, null, null);
                }
                return htmlString;
            }
            catch
            {
                return "";
            }
        }

        public void GeneratePDFAndSendMail(string htmlString ,string fileName,string job) {
            HtmlToPdf htmlToPdfConverter = new HtmlToPdf();
            htmlToPdfConverter.BrowserWidth = 1000;
            htmlToPdfConverter.Document.PageSize = PdfPageSize.A4;// GetSelectedPageSize();         
            htmlToPdfConverter.Document.PageOrientation = PdfPageOrientation.Portrait;

            htmlToPdfConverter.ConvertHtmlToFile(htmlString, "", fileName);
            string emailBody = "Dear user, <br/><br/> Thank you for placing an order. <br/> Your order number <b>" + job + " </b> will be processed soon.<br/> Please see attached order PDF details. <br/><br/><br/> Thanks.<br/>Incredible.";
            sedMail("Incredible:Order placed :" + job, emailBody, fileName, "dev3.rajsampler@gmail.com");
        }

        public void sedMail(string subject, string body, string attachmentPath, string mailTo)
        {

            string username = ConfigurationSettings.AppSettings["SMTPFrom"];
            string password = ConfigurationSettings.AppSettings["SMTPFromPassword"];
            mailTo = ConfigurationSettings.AppSettings["mailTo"];
            string BCC = ConfigurationSettings.AppSettings["Bcc"];

            if (username != "" || password != "" || subject != "" || body != "")
            {
                Boolean a = false;
                try
                {
                    MailMessage mail = new MailMessage();
                    mail.From = new MailAddress(username);
                    mail.To.Add(mailTo);
                    mail.Bcc.Add(BCC);
                    mail.IsBodyHtml = true;
                    mail.Subject = subject;
                    mail.Body = body;
                    mail.Priority = MailPriority.High;
                    mail.Attachments.Add(new Attachment(attachmentPath));
                    SmtpClient smtp = new SmtpClient("smtp.gmail.com", 587); //SmtpClient smtp = new SmtpClient(smtpstr, smtpport);//SmtpClient smtp = new SmtpClient("smtp.gmail.com", 587);
                    smtp.Credentials = new System.Net.NetworkCredential(username, password);
                    smtp.EnableSsl = true; //smtp.EnableSsl = enableSsl;//smtp.EnableSsl = true;
                    smtp.Send(mail);
                }
                catch (Exception ex)
                {

                }

            }
        }

    }
}