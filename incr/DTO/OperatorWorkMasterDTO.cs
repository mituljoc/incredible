﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace incr.DTO
{
    public class OperatorWorkMasterDTO
    {
        public int RID { get; set; }
        public string OperatorId { get; set; }
        public string DepartmentId { get; set; }
        public string WorkId { get; set; }
        public Nullable<decimal> Price { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public IList DepartmentList { get; set; }
        public IList OperatorPriceList { get; set; }
    
    }
}