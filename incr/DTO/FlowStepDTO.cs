﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace incr.DTO
{
    public class FlowStepDTO
    {
        public long ID { get; set; }
        public string Name { get; set; }
        public Nullable<long> OrderId { get; set; }
        public Nullable<long> FlowCodeId { get; set; }
        public Nullable<long> OperatorId { get; set; }
        public Nullable<long> SupervisedBy { get; set; }
        public Nullable<System.DateTime> INtime { get; set; }
        public Nullable<System.DateTime> OutTime { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string Remarks { get; set; }
    }
}