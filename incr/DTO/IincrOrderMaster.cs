﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace incr.Models
{
    public class IncrOrderMaster
    {
        private DateTime? _orderDate;
        public long Id { get; set; }
        public string JobNo { get; set; }
        public string CustomerName { get; set; }
        public long CustomerId { get; set; }
        public long SalesById { get; set; }
        public long JobById { get; set; }
        public string Mobile { get; set; }
        public string Address { get; set; }
        public DateTime OrderDate
        {
            get
            {
                if (_orderDate == null)
                {
                    return DateTime.Now;
                }
                else
                {
                    return Convert.ToDateTime(_orderDate);
                }
            }

            set

            {

                _orderDate = value;

            }
        }
        public DateTime DeliveryDate { get; set; }
        public string SalesBy { get; set; }
        public string PlyType { get; set; }
        public string Spring { get; set; }
        public string Foaming { get; set; }
        public string LinningType { get; set; }
        public string CushionBack { get; set; }
        public string StitchForSofa { get; set; }
        public string CushionStitch { get; set; }
        public string Fitting { get; set; }
        public string Packing { get; set; }
        public string Vehicle { get; set; }
        public string JobBy { get; set; }
        public string Delivery { get; set; }
        public string TapestryBody { get; set; }
        public string TapestrySeat { get; set; }
        public string TapestryCushion { get; set; }
        public decimal TapestryBodyMTR { get; set; }
        public decimal TapestrySeatMTR { get; set; }
        public decimal TapestryCushionMTR { get; set; }
        public string TapestryBodyFabricType { get; set; }
        public string TapestrySeatFabricType { get; set; }
        public string TapestryCushionFabricType { get; set; }
        public string Particular1 { get; set; }
        public string Particular2 { get; set; }
        public string Particular3 { get; set; }
        public long Particular1Qty { get; set; }
        public long Particular2Qty { get; set; }
        public long Particular3Qty { get; set; }
        public decimal Particular1Rate { get; set; }
        public decimal Particular2Rate { get; set; }
        public decimal Particular3Rate { get; set; }
        public decimal CarpantaryTopWidth { get; set; }
        public decimal CarpantaryMiddleWidth { get; set; }
        public decimal CarpantaryBottomWidth { get; set; }
        public decimal CarpantaryLeftHight { get; set; }
        public decimal CarpantaryRightHight { get; set; }
        public decimal HandleLength { get; set; }
        public decimal HandleHeight { get; set; }
        public decimal HandleWidth { get; set; }
        public string DrawingType { get; set; }
        public long LeftNumberOfsits { get; set; }
        public long RightNumberOfsits { get; set; }

        public decimal DrawingRightSize0 { get; set; }
        public decimal DrawingRightSize1 { get; set; }
        public decimal DrawingRightSize2 { get; set; }
        public decimal DrawingRightSize3 { get; set; }

        public decimal DrawingRightSize4 { get; set; }
        public decimal DrawingRightSize5 { get; set; }
        public decimal DrawingRightSize6 { get; set; }

        public decimal DrawingleftSize1 { get; set; }
        public decimal DrawingleftSize2 { get; set; }
        public decimal DrawingleftSize3 { get; set; }
        public decimal DrawingleftSize4 { get; set; }
        public decimal DrawingleftSize5 { get; set; }
        public decimal DrawingleftSize6 { get; set; }


        public decimal Drawingfullheight { get; set; }
        public decimal Drawingfullwidth { get; set; }
        public string LongerSide { get; set; }
        public string LinningQty { get; set; }
    }
}