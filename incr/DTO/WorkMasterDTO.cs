﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace incr.DTO
{
    public class WorkMasterDTO
    {
        public int RID { get; set; }
        public string WorkName { get; set; }
        public string Department { get; set; }
        //public IList DepartmentIdList { get; set; }
        //public object DepartmentIds { get; set; }
        //public object DepartmentIdsObj { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public IList DepartmentList { get; set; }

    }
}