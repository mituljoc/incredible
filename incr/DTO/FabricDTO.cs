﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace incr.DTO
{
    public class FabricDTO
    {
        public int RID { get; set; }
        public int GroupId { get; set; }
        public string FacricName { get; set; }
        public string FabricGroupId { get; set; }
        public string FabricGroup { get; set; }
        public Nullable<decimal> FabricPrice { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
    }
}