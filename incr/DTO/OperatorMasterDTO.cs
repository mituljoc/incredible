﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace incr.DTO
{
    public class OperatorMasterDTO
    {
        public int RID { get; set; }
        public string OperatorName { get; set; }
        public Nullable<bool> Carpentery { get; set; }
        public Nullable<bool> Linning { get; set; }
        public Nullable<bool> Packing { get; set; }
        public Nullable<bool> Delivery { get; set; }
        public Nullable<bool> Invoice { get; set; }
        public Nullable<bool> Fitting { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<bool> is_supervisor { get; set; }
        public Nullable<bool> Polish { get; set; }

    }
}