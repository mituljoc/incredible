﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace incr.DTO
{
    public class SupplierDTO
    {
        public int RID { get; set; }
        public string SupplierName { get; set; }
        public string Email { get; set; }
        public string MobileNo { get; set; }
        public string GSTNo { get; set; }
        public string Website { get; set; }
        public string PaymentTerm { get; set; }
        public string ContactPerson { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string Type { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string DateStr { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<bool> IsSupplier { get; set; }
        public Nullable<bool> IsCustomer { get; set; }
    }
}