﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace incr.DTO
{
    public class CustomerComplaintDTO
    {
        public long RID { get; set; }
        public string Title { get; set; }
        public string CustomerName { get; set; }
        public string OrderNumner { get; set; }
        public string Department { get; set; }
        public string Status { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public string Problem { get; set; }
        public string Type { get; set; }
        public string AddedBy { get; set; }
        public string WorkedBy { get; set; }
        public string Priority { get; set; }
        public string Comment { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public string DateStr { get; set; }
        public IList DepartmentList { get; set; }
    }
}