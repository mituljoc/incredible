﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace incr.Models
{
    public class OrderDTO
    {
        private DateTime? _orderDate;
        public long Id { get; set; }
        public string JobNo { get; set; }
        public string CustomerName { get; set; }
        public long CustomerId { get; set; }
        public long SalesById { get; set; }
        public long FlowId { get; set; }
        public long JobById { get; set; }
        public string Mobile { get; set; }
        public string Address { get; set; }
        public DateTime OrderDate
        {
            get
            {
                if (_orderDate == null)
                {
                    return DateTime.Now;
                }
                else
                {
                    return Convert.ToDateTime(_orderDate);
                }
            }

            set

            {

                _orderDate = value;

            }
        }
        public DateTime DeliveryDate { get; set; }
        public string SalesBy { get; set; }
        public string PlyType { get; set; }
        public string Spring { get; set; }
        public string Foaming { get; set; }
        public string LinningType { get; set; }
        public string CushionBack { get; set; }
        public string StitchForSofa { get; set; }
        public string CushionStitch { get; set; }
        public string Fitting { get; set; }
        public string Packing { get; set; }
        public string Vehicle { get; set; }
        public string JobBy { get; set; }
        public string Delivery { get; set; }
        public string TapestryBody { get; set; }
        public string TapestrySeat { get; set; }
        public string TapestryCushion { get; set; }
        public decimal TapestryBodyMTR { get; set; }
        public decimal TapestrySeatMTR { get; set; }
        public decimal TapestryCushionMTR { get; set; }
        public string TapestryBodyFabricType { get; set; }
        public string TapestrySeatFabricType { get; set; }
        public string TapestryCushionFabricType { get; set; }
        public string FabricGroup4 { get; set; }
        public string FabricType4 { get; set; }
        public string FabricGroup5 { get; set; }
        public string FabricType5 { get; set; }
        public string Particular1 { get; set; }
        public string Particular2 { get; set; }
        public string Particular3 { get; set; }
        public string Particular4 { get; set; }
        public string Particular5 { get; set; }
        public string  Particular1Qty { get; set; }
        public string Particular2Qty { get; set; }
        public string Particular3Qty { get; set; }
        public string Particular4Qty { get; set; }
        public string Particular5Qty { get; set; }
        public decimal Particular1Rate { get; set; }
        public decimal Particular2Rate { get; set; }
        public decimal Particular3Rate { get; set; }
        public decimal Particular4Rate { get; set; }
        public decimal Particular5Rate { get; set; }
        public decimal Particular1Total { get; set; }
        public decimal Particular2Total { get; set; }
        public decimal Particular3Total { get; set; }
        public decimal Particular4Total { get; set; }
        public decimal Particular5Total { get; set; }
        public long ParticularNameId { get; set; }

        public decimal CarpantaryTopWidth { get; set; }
        public decimal CarpantaryMiddleWidth { get; set; }
        public decimal CarpantaryBottomWidth { get; set; }
        public decimal CarpantaryLeftHight { get; set; }
        public decimal CarpantaryRightHight { get; set; }
        public decimal CarpantaryRightHight1 { get; set; }
        public decimal CarpantaryRightHight2 { get; set; }
        public decimal CarpantaryRightHight3 { get; set; }
        public decimal CarpantaryRightWidth { get; set; }
        public decimal HandleLength { get; set; }
        public decimal HandleHeight { get; set; }
        public decimal HandleWidth { get; set; }
        public string DrawingType { get; set; }
        public long LeftNumberOfsits { get; set; }
        public long RightNumberOfsits { get; set; }

        public decimal DrawingRightSize0 { get; set; }
        public decimal DrawingRightSize1 { get; set; }
        public decimal DrawingRightSize2 { get; set; }
        public decimal DrawingRightSize3 { get; set; }

        public decimal DrawingRightSize4 { get; set; }
        public decimal DrawingRightSize5 { get; set; }
        public decimal DrawingRightSize6 { get; set; }

        public decimal DrawingleftSize1 { get; set; }
        public decimal DrawingleftSize2 { get; set; }
        public decimal DrawingleftSize3 { get; set; }
        public decimal DrawingleftSize4 { get; set; }
        public decimal DrawingleftSize5 { get; set; }
        public decimal DrawingleftSize6 { get; set; }


        public decimal Drawingfullheight { get; set; }
        public decimal Drawingfullwidth { get; set; }
        public string LongerSide { get; set; }

    
        public int? TotalLeaderOrder { get; set; }
        public int? TotalComplaint { get; set; }
        public int? TotalCustomerOrder { get; set; }
        public string LinningQty { get; set; }
        public string OrderDateStr { get; set; }
        public string DeliveryDateStr { get; set; }
        public bool IsStart{ get; set; }
        public long current_flow_code_id { get; set; }
        public long BodyFabricTypeId { get; set; }
        public long SeatFabricTypeId { get; set; }
        public long CushionFabricTypeId { get; set; }
        public long BodyFabricGroupId { get; set; }
        public long SeatFabricGroupId { get; set; }
        public long CushionFabricGroupId { get; set; }
        public long Particula4FabricTypeId { get; set; }
        public long Particula4FabricGroupId { get; set; }
        public long Particular5FabricTypeId { get; set; }
        public long Particular5FabricGroupId { get; set; }
        public bool IsWithoutfabric { get; set; }
        public decimal BodyFabricPrice { get; set; }
        public decimal SeatFabricPrice { get; set; }
        public decimal CushionFabricPrice { get; set; }
        public decimal SGST { get; set; }
        public decimal CGST { get; set; }
        public decimal TransPortPrice { get; set; }
        public decimal Extra { get; set; }
        public decimal Discount { get; set; }
        public decimal NetOrderPrice { get; set; }
        public decimal TotalOrderPrice { get; set; }
        public decimal DrawingHandle1 { get; set; }
        public decimal DrawingHandle2 { get; set; }
        public decimal DrawingHandle3 { get; set; }
        public decimal DrawingHandle4 { get; set; }
        public string ParticularName { get; set; }
        public string ParticularCode { get; set; }
        public string ParticularRange { get; set; }
        public string Partyname { get; set; }
        public string Polish { get; set; }

        
    }
}