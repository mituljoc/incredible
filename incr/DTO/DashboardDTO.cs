﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace incr.DTO
{
    public class DashboardDTO
    {
        public int OrderId { get; set; }
        public string CustomerName { get; set; }
        public string OperatorrName { get; set; }
        public int Amount { get; set; }
        public int OrderRecieved { get; set; }
        public int RunningOrder { get; set; }
        public int TotalEarning { get; set; }
        public int TotalOrders { get; set; }
        public int TotalCarpentary { get; set; }
        public int TotalLining { get; set; }
        public int TotalFitting { get; set; }
        public int TotalDelivery { get; set; }
        public int TotalInvoice { get; set; }
        public int TotalOrdersOpr { get; set; }
        public int TotalOrdersCust { get; set; }
    }
}