﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace incr.DTO
{
    public class ErrorLogDTO
    {
        public long RID { get; set; }
        public string ErrorPath { get; set; }
        public string Message { get; set; }
        public string InnerException { get; set; }
        public Nullable<System.DateTime> ErrorDate { get; set; }
        public string IPAddress { get; set; }
        public string OtherDetail { get; set; }
    }
}